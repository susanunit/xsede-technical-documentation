#software
	:markdown
		#[Software](#software)

		Delta software is provisioned, when possible, using spack to produce modules for use via the lmod based module system. Select NVIDIA NGC containers are made available (see the container section below) and are periodically updated from the NVIDIA NGC site. An automated list of available software can be found on the XSEDE website.

	#software:modules
		:markdown
			## [Managing Your Environment (Modules)](#software:modules)

			Delta provides two sets of modules and a variety of compilers in each set. The default environment is __modtree/gpu__ which loads a recent version of gnu compilers , the openmpi implementation of MPI, and cuda. The environment with gpu support will build binaries that run on both the gpu nodes (with cuda) and cpu nodes (potentially with warning messages because those nodes lack cuda drivers). For situations where the same version of software is to be deployed on both gpu and cpu nodes but with separate builds, the __modtree/cpu__ environment provides the same default compiler and MPI but without cuda. Use module spider package_name to search for software in lmod and see the steps to load it for your environment.

		#software:modules-cmds
			:markdown
				### [Useful Modules commands](#software:modules-cmds)

				1. `module list` command: (display the currently loaded modules)

				<pre>$ module list
				Currently Loaded Modules:
				  1) gcc/11.2.0   3) openmpi/4.1.2   5) modtree/gpu
				  2) ucx/1.11.2   4) cuda/11.6.1</pre>

				2. `module load` *package_name* command: (loads a package or metamodule such as `modtree/gpu` or `netcdf-c`)

				<pre>$ module load modtree/cpu
				
				Due to MODULEPATH changes, the following have been reloaded:
				  1) gcc/11.2.0     2) openmpi/4.1.2     3) ucx/1.11.2
				
				The following have been reloaded with a version change:
				  1) modtree/gpu => modtree/cpu</pre>

				3. `module spider` *package_name* command: (finds modules and displays the ways to load them)

				<pre>$ module spider openblas
				----------------------------------------------------------------------------
				openblas: openblas/0.3.20
				---------------------------------------------------------------------------
				
				    You will need to load all module(s) on any one of the lines below before the
				 "openblas/0.3.20" module is available to load.
				
				      aocc/3.2.0
				      gcc/11.2.0
				 
				    Help:
				      OpenBLAS: An optimized BLAS library</pre>

				4. `module -r spider` *regular expression* command:

				<pre>$ module -r spider "^r$"
				
				----------------------------------------------------------------------------
				  r:
				----------------------------------------------------------------------------
				     Versions:
				        r/4.1.3
				...</pre>

		:markdown
			See also: <a target="_blank" rel="noopener noreferrer" href="https://lmod.readthedocs.io/en/latest/010_user.html">User Guide for Lmod</a>

			Please open a service request ticket by sending email to help@ncsa.illinois.edu for help with software not currently installed on the Delta system. For single user or single project use cases the preference is for the user to use the spack software package manager to install software locally against the system spack installation as documented (docs pending). Delta support staff are available to provide limited assistance. For general installation requests the Delta project office will review requests for broad use and installation effort.

	#software:python
		:markdown
			## [Python](#software:python)

			On Delta, you may install your own python software stacks as needed. There are a couple choices when customizing your python setup. You may use any of these methods with any of the python versions or instances described below (or you may install your own python versions):

			* <a target="_blank" rel="noopener noreferrer" href="https://docs.python.org/3/installing/index.html">pip3:</a> pip3 install --user &lt;python_package&gt;
				* useful when you need just 1 python environment per python version or instance
			* <a target="_blank" rel="noopener noreferrer" href="https://docs.python.org/3/library/venv.html">venv (python virtual environment)</a>
				* can name environments (metadata) and have multiple environments per python version or instance
			* <a target="_blank" rel="noopener noreferrer" href="https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html">conda environments</a>
				* similar to venv but with more flexibility; See this <a target="_blank" rel="noopener noreferrer" href="https://docs.conda.io/projects/conda/en/latest/user-guide/concepts/environments.html">comparison</a> (halfway down the page)

			<p class="portlet-msg-info"><strong>NGC containers for gpu nodes</strong><br />The [Nvidia NGC containers](#containers) on Delta provide optimized python frameworks built for Delta's A100 and A40 gpus. Delta staff recommend using an NGC container when possible with the gpu nodes (or use the anaconda3_gpu module described later).</p>

			The default gcc (latest version) programming environment for either `modtree/cpu` or `modtree/gpu` contains the following:

	#software:python-anaconda
		:markdown
			### [Anaconda](#software:python-anaconda)

			__anaconda3_cpu__

			Use python from the anaconda3_cpu module if you need some of the modules provided by Anaconda in your python workflow. See the "managing environments" section of the Conda getting started guide to learn how to customize Conda for your workflow and add extra python modules to your environment. We recommend starting with anaconda3_cpu for modtree/cpu and the cpu nodes, do not use this module with gpus, use anaconda3_gpu instead.

			<p class="portlet-msg-alert"><span style="font-weight: bold">anaconda and containers</span><br />If you use anaconda with NGC containers, take care to use the python from the container and not the python from anaconda or one of its environments. The container's python should be 1st in $PATH. You may --bind the anaconda directory or other paths into the container so that you can start your conda environments, but with the container's python (/usr/bin/python).</p>

			<pre>$ module load modtree/cpu
			$ module load gcc anaconda3_cpu
			$ which conda
			/sw/external/python/anaconda3_cpu/conda
			$ module list Currently Loaded Modules:
			  1) cue-login-env/1.0   6) libfabric/1.14.0     11) ucx/1.11.2
			  2) default             7) lustre/2.14.0_ddn23  12) openmpi/4.1.2
			  3) gcc/11.2.0          8) openssh/8.0p1        13) modtree/cpu
			  4) knem/1.1.4          9) pmix/3.2.3           14) anaconda3_cpu/4.13.0
			  5) libevent/2.1.8     10) rdma-core/32.0</pre>

			__List of modules in anaconda3_cpu__

			The current list of modules available in anaconda3_cpu (including tensorflow, pytorch, etc), is shown via:

				$ conda list

			__anaconda3_gpu__

			Similar to the setup for anaconda, we have a gpu version of anaconda3 (module load anaconda3_gpu) and have installed pytorch and tensorflow cuda-aware python modules into this version. You may use this module when working with the gpu nodes. See `conda list` after loading the module to review what is already installed. As with anaconda3_cpu, let Delta staff know if there are generally useful modules you would like us to try to install for the broader community.

			A sample tensorflow test script:

			<pre>#!/bin/bash
			#SBATCH --mem=64g
			#SBATCH --nodes=1
			#SBATCH --ntasks-per-node=1
			#SBATCH --cpus-per-task=2     # <- match to OMP_NUM_THREADS
			#SBATCH --partition=gpuA100x4-interactive
			#SBATCH --time=00:10:00
			#SBATCH --account=YOUR_ACCOUNT-delta-gpu
			#SBATCH --job-name=tf_anaconda
			### GPU options ###
			#SBATCH --gpus-per-node=1
			#SBATCH --gpus-per-task=1
			#SBATCH --gpu-bind=verbose,per_task:1
			###SBATCH --gpu-bind=none     # <- or closest
			  
			module purge # drop modules and explicitly load the ones needed
			             # (good job metadata and reproducibility)
			 
			module load anaconda3_gpu
			module list  # job documentation and metadata
			 
			echo "job is starting on `hostname`"
			 
			which python3
			conda list tensorflow
			srun python3 \
			  tf_gpu.py
			exit</pre>

			__Jupyter notebooks__

			The Delta Open OnDemand portal provides an easier way to start a Jupyter notebook. Please see [OpenOnDemand](#ondemand) to access the portal.

			The Jupyter notebook executables are in your $PATH after loading the anaconda3 module. __Don't run Jupyter on the shared login nodes.__ Instead, follow these steps to attach a Jupyter notebook running on a compute node to your local web browser:

			#### Step 1. Start a Jupyter job via srun and note the hostname

			*(you pick the port number for --port)*.

			<pre>$ srun --account=bbka-delta-cpu --partition=cpu-interactive \
			  --time=00:30:00 --mem=32g \
			  jupyter-notebook --no-browser \
			  --port=8991 --ip=0.0.0.0<
			...</pre>

			Or copy and paste one of these URLs:

				http://cn093.delta.internal.ncsa.edu:8891/?token=e5b500e5aef67b1471ed1842b2676e0c0ae4b5652656feea

			or

				http://127.0.0.1:8991/?token=e5b500e5aef67b1471ed1842b2676e0c0ae4b5652656feea

			Use the 2nd URL in __Step 3__. Note the internal hostname in the cluster for __Step 2__.

			When using a container with a gpu node, run the container's jupyter-notebook:

			__NGC container for gpus, jupyter-notebook, bind a directory__

			<pre># container notebook example showing how to access a directory outside
			# of $HOME ( /projects/bbka in the example )
			$ srun --account=bbka-delta-gpu --partition=gpuA100x4-interactive \
			  --time=00:30:00 --mem=64g --gpus-per-node=1 \
			  singularity run --nv --bind /projects/bbka \
			  /sw/external/NGC/pytorch:22.02-py3 jupyter-notebook \
			  --notebook-dir /projects/bbka
			  --no-browser --port=8991 --ip=0.0.0.0
			...
			http://hostname:8888/?token=73d96b99f2cfc4c3932a3433d1b8003c052081c5411795d5</pre>

			In __Step 3__ to start the notebook in your browser, replace `http://hostname:8888/` with `http://127.0.0.1:8991/`

			You may not see the job hostname when running with a container, find it with `squeue`:

			__squeue -u $USER__

			<pre>$ squeue -u $USER
			             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
			            156071 gpuA100x4 singular  arnoldg  R       1:00      1 gpua045</pre>

			Then specify the host your job is using in the next step (`gpua045` for example ).

			#### Step 2. From your local desktop or laptop create an ssh tunnel to the compute node via a login node of delta.

			__ssh tunnel for jupyter__

			<pre>$ ssh -l my_delta_username \
			  -L 127.0.0.1:8991:cn093.delta.internal.ncsa.edu:8991 \
			  dt-login.delta.ncsa.illinois.edu</pre>

			Authenticate with your login and 2-factor as usual.

			#### Step 3. Paste the 2nd URL (containing 127.0.0.1:*port_number* and the token string) from __Step 1__ into your browser

			You will be connected to the Jupyter instance running on your compute node of Delta.

			<p><img alt="" src="/documents/10308/3028413/jupyter-600.jpg/6c754821-367b-45b7-85be-caab3ba699d2?t=1658258517957" style="width: 600px; height: 377px;" /></p>

	#software:python-other
		:markdown
			### [Other Pythons (recent or latest versions)](#software:python-other)

			If you do not need all of the extra modules provided by Anaconda, use the basic python installation under the gcc module. You can add modules via `pip3 install --user &lt;modulename&gt;`, setup virtual environments, and customize as needed for your workflow but starting from a smaller installed base of python than Anaconda.

			<pre>$ module load gcc python
			$ which python
			/sw/spack/delta-2022-03/apps/python/3.10.4-gcc-11.2.0-3cjjp6w/bin/python
			$ module list
			 
			Currently Loaded Modules:
			  1) modtree/gpu   3) gcc/11.2.0    5) ucx/1.11.2      7) python/3.10.4
			  2) default       4) cuda/11.6.1   6) openmpi/4.1.2</pre>

			This is the list of modules available in the python from `pip3 list`:

			<pre>Package            Version
			------------------ ---------
			certifi            2021.10.8
			cffi               1.15.0
			charset-normalizer 2.0.12
			click              8.1.2
			cryptography       36.0.2
			globus-cli         3.4.0
			globus-sdk         3.5.0
			idna               3.3
			jmespath           0.10.0
			pip                22.0.4
			pycparser          2.21
			PyJWT              2.3.0
			requests           2.27.1
			setuptools         58.1.0
			urllib3            1.26.9</pre>

