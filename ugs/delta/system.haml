#system
	:markdown
		#[System Architecture](#system)

		Delta is designed to help applications transition from CPU-only to GPU or hybrid CPU-GPU codes. Delta has some important architectural features to facilitate new discovery and insight:

		* A single processor architecture (AMD) across all node types: CPU and GPU
		* Support for NVIDIA A100 MIG GPU partitioning allowing for fractional use of the A100s if your workload isn't able to exploit an entire A100 efficiently
		* Ray-tracing hardware support from the NVIDIA A40 GPUs
		* Nine large memory (2 TB) nodes 
		* A low-latency and high-bandwidth HPE/Cray Slingshot interconnect between compute nodes
		* Lustre for home, projects and scratch file systems
		* Support for relaxed and non-posix I/O (docs pending)
		* Shared-node jobs and the single core and single MIG GPU slice
		* Resources for persistent services in support of Gateways, Open OnDemand and Data Transport nodes
		* Unique AMD MI-100 resource

	#system:nodes
		:markdown
			##[Model Compute Nodes](#system:nodes)

			The Delta compute ecosystem is composed of 5 node types:

			1. Dual-socket CPU-only compute nodes
			1. Single-socket 4-way NVIDIA A100 GPU compute nodes
			1. Single-socket 4-way NVIDIA A40 GPU compute nodes
			1. Dual-socket 8-way NVIDIA A100 GPU compute nodes
			1. Single-socket 8-way AMD MI100 GPU compute nodes

			The CPU-only and 4-way GPU nodes have 256 GB of RAM per node while the 8-way GPU nodes have 2 TB of RAM. The CPU-only node has 0.74 TB of local storage while all GPU nodes have 1.5 TB of local storage.

	#system:nodes-specs
		:markdown
			###[CPU and GPU Node Specifications](#system:nodes-specs)

	#system:nodes-specs-cpu
		:markdown
			[Table 1. CPU Compute Node Specifications](#system:nodes-specs-cpu)

	%table(border="1" cellpadding="3")
		%tbody
			%tr
				%th Number of nodes
				%td 124
			%tr
				%th CPU
				%td AMD Milan (PCIe Gen4)
			%tr
				%th Sockets per node
				%td 2
			%tr
				%th Cores per socket
				%td 64
			%tr
				%th Cores per node
				%td 128
			%tr
				%th Hardware threads per core
				%td 1
			%tr
				%th Hardware threads per node
				%td 128
			%tr
				%th Clock rate (GHz)
				%td ~2.45
			%tr
				%th RAM (GB)
				%td 256
			%tr
				%th Cache (KB)
				%td 64/512/32768
			%tr
				%th Local storage (TB)
				%td 0.74 TB

	:markdown
		The AMD CPUs are set for 4 NUMA domains per socket (NPS=4).

	#system:nodes-specs-gpu
		:markdown
			[Table 2. GPU Node Specifications](#system:nodes-specs-gpu)

	%table(border="1" cellpadding="3")
		%thead
			%tr
				%th &nbsp;
				%th 4-way NVIDIA A40
				%th 4-way NVIDIA A100
				%th 8-way NVIDIA A100 Large Memory
				%th 8-way AMD MI100 Large Memory
		%tbody
			%tr
				%th Number of nodes
				%td 100
				%td 100
				%td 5
				%td 1
			%tr
				%th GPU
				%td NVIDIA A40 (<a target="_blank" href="https://www.nvidia.com/en-us/data-center/a40/#specs">Vendor page</a>)
				%td NVIDIA A100 (<a target="_blank" href="https://www.nvidia.com/en-us/data-center/a100/#specifications">Vendor page</a>)
				%td NVIDIA A100 (<a target="_blank" href="https://www.nvidia.com/en-us/data-center/a100/#specifications">Vendor page</a>)
				%td AMD MI100 (<a target="_blank" href="https://www.amd.com/en/products/server-accelerators/instinct-mi100">Vendor page</a>)
			%tr
				%th GPUs per node
				%td 4
				%td 4
				%td 8
				%td 8
			%tr
				%th GPU Memory (GB)
				%td 48 DDR6 with ECC
				%td 40
				%td 40
				%td 32
			%tr
				%th CPU
				%td AMD Milan
				%td AMD Milan
				%td AMD Milan
				%td AMD Milan
			%tr
				%th CPU sockets per node
				%td 1
				%td 1
				%td 2
				%td 2
			%tr
				%th Cores per socket
				%td 64
				%td 64
				%td 64
				%td 64
			%tr
				%th Cores per node
				%td 64
				%td 64
				%td 128
				%td 128
			%tr
				%th Hardware threads per core
				%td 1
				%td 1
				%td 1
				%td 1
			%tr
				%th Hardware threads per node
				%td 64
				%td 64
				%td 128
				%td 128
			%tr
				%th Clock rate (GHz)
				%td ~ 2.45
				%td ~ 2.45
				%td ~ 2.45
				%td ~ 2.45
			%tr
				%th RAM (GB)
				%td 256
				%td 256
				%td 2048
				%td 2048
			%tr
				%th Cache (KB) L1/L2/L3
				%td 64/512/32768
				%td 64/512/32768
				%td 64/512/32768
				%td 64/512/32768
			%tr
				%th Local storage (TB)
				%td 1.5
				%td 1.5
				%td 1.5
				%td 1.5

	:markdown
		The AMD CPUs are set for 4 NUMA domains per socket (NPS=4).

		The A40 GPUs are connected via PCIe Gen4 and have the following affinitization to NUMA nodes on the CPU. Note that the relationship between GPU index and NUMA domain are inverse.

	#system:nodes-specs-gpu-a40map
		:markdown
			[Table 2-1. 4-way NVIDIA A40 Mapping and GPU-CPU Affinitization](#system:nodes-specs-gpu-a40map)

	%table(border="1" cellpadding="3")
		%thead
			%tr
				%th &nbsp;
				%th GPU0
				%th GPU1
				%th GPU2
				%th GPU3
				%th HSN
				%th CPU Affinity
				%th NUMA Affinity
		%tbody
			%tr
				%td GPU0
				%td X
				%td SYS
				%td SYS
				%td SYS
				%td SYS
				%td 48-63
				%td 3
			%tr
				%td GPU1
				%td SYS
				%td X
				%td SYS
				%td SYS
				%td SYS
				%td 32-47
				%td 2
			%tr
				%td GPU2
				%td SYS
				%td SYS
				%td X
				%td SYS
				%td SYS
				%td 16-31
				%td 1
			%tr
				%td GPU3
				%td SYS
				%td SYS
				%td SYS
				%td X
				%td PHB
				%td 0-15
				%td 0
			%tr
				%td HSN
				%td SYS
				%td SYS
				%td SYS
				%td PHB
				%td X
				%td &nbsp;
				%td &nbsp;

	:markdown
		__Table 2-1 Legend__

		X    = Self<br />
		SYS  = Connection traversing PCIe as well as the SMP interconnect between NUMA nodes (e.g., QPI/UPI)<br />
		NODE = Connection traversing PCIe as well as the interconnect between PCIe Host Bridges within a NUMA node<br />
		PHB  = Connection traversing PCIe as well as a PCIe Host Bridge (typically the CPU)<br />
		NV#  = Connection traversing a bonded set of # NVLinks

	#system:nodes-specs-gpu-a100map
		:markdown
			[Table 2-2. 4-way NVIDIA A100 Mapping and GPU-CPU Affinitization](#system:nodes-specs-gpu-a100map)

	%table(border="1" cellpadding="3")
		%thead
			%tr
				%th &nbsp;
				%th GPU0
				%th GPU1
				%th GPU2
				%th GPU3
				%th HSN
				%th CPU Affinity
				%th NUMA Affinity
		%tbody
			%tr
				%td GPU0
				%td X
				%td NV4
				%td NV4
				%td NV4
				%td SYS
				%td 48-63
				%td 3
			%tr
				%td GPU1
				%td NV4
				%td X
				%td NV4
				%td NV4
				%td SYS
				%td 32-47
				%td 2
			%tr
				%td GPU2
				%td NV4
				%td NV4
				%td X
				%td NV4
				%td SYS
				%td 16-31
				%td 1
			%tr
				%td GPU3
				%td NV4
				%td NV4
				%td NV4
				%td X
				%td PHB
				%td 0-15
				%td 0
			%tr
				%td HSN
				%td SYS
				%td SYS
				%td SYS
				%td PHB
				%td X
				%td &nbsp;
				%td &nbsp;

	:markdown
		__Table 2-2 Legend__

		X    = Self<br />
		SYS  = Connection traversing PCIe as well as the SMP interconnect between NUMA nodes (e.g., QPI/UPI)<br />
		NODE = Connection traversing PCIe as well as the interconnect between PCIe Host Bridges within a NUMA node<br />
		PHB  = Connection traversing PCIe as well as a PCIe Host Bridge (typically the CPU)<br />
		NV#  = Connection traversing a bonded set of # NVLinks

	#system:nodes-specs-gpu-8a100map
		:markdown
			[Table 2-3. 8-way NVIDIA A100 Mapping and GPU-CPU Affinitization](#system:nodes-specs-gpu-8a100map)

	%table(border="1" cellpadding="3")
		%thead
			%tr
				%th &nbsp;
				%th GPU0
				%th GPU1
				%th GPU2
				%th GPU3
				%th GPU4
				%th GPU5
				%th GPU6
				%th GPU7
				%th HSN
				%th CPU Affinity
				%th NUMA Affinity
		%tbody
			%tr
				%td GPU0
				%td X
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td SYS
				%td 48-63
				%td 3
			%tr
				%td GPU1
				%td NV12
				%td X
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td SYS
				%td 48-63
				%td 3
			%tr
				%td GPU2
				%td NV12
				%td NV12
				%td X
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td SYS
				%td 16-31
				%td 1
			%tr
				%td GPU3
				%td NV12
				%td NV12
				%td NV12
				%td X
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td SYS
				%td 16-31
				%td 1
			%tr
				%td GPU0
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td X
				%td NV12
				%td NV12
				%td NV12
				%td SYS
				%td 112-127
				%td 7
			%tr
				%td GPU1
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td X
				%td NV12
				%td NV12
				%td SYS
				%td 112-127
				%td 7
			%tr
				%td GPU2
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td X
				%td NV12
				%td SYS
				%td 80-95
				%td 5
			%tr
				%td GPU3
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td NV12
				%td X
				%td SYS
				%td 80-95
				%td 5
			%tr
				%td HSN
				%td SYS
				%td SYS
				%td SYS
				%td SYS
				%td SYS
				%td SYS
				%td SYS
				%td SYS
				%td X
				%td &nbsp;
				%td &nbsp;

	:markdown
		__Table 2-3 Legend__

		X    = Self<br />
		SYS  = Connection traversing PCIe as well as the SMP interconnect between NUMA nodes (e.g., QPI/UPI)<br />
		NODE = Connection traversing PCIe as well as the interconnect between PCIe Host Bridges within a NUMA node<br />
		PHB  = Connection traversing PCIe as well as a PCIe Host Bridge (typically the CPU)<br />
		NV#  = Connection traversing a bonded set of # NVLinks

	#system:nodes-login
		:markdown
			##[Login Nodes](#system:nodes-login)

			[Login nodes](#) provide interactive support for code compilation.

	#system:nodes-special
		:markdown
			##[Specialized Nodes](#system:nodes-special)

			Delta will support data transfer nodes (serving the "NCSA Delta" Globus Online collection) and nodes in support of other services. (docs pending)


	#system:network
		:markdown
			##[Network](#system:network)

			Delta is connected to the NPCF core router & exit infrastructure via two 100Gbps connections, NCSA's 400Gbps+ of WAN connectivity carry traffic to/from users on an optimal peering.

			Delta resources will be inter-connected with HPE/Cray's 100Gbps/200Gbps SlingShot interconnect.
			
	#system:filesystems
		:markdown
			##[File Systems](#system:filesystems)

			Note: Users of Delta have access to three file systems at the time of system launch, a fourth relaxed-POSIX file system will be made available at a later date.(docs pending)

	#system:filesystems-delta
		:markdown
			###[Delta](#systems:filesystems-delta)

			The Delta storage infrastructure provides users with their `$HOME` and `$SCRATCH` areas. These file systems are mounted across all Delta nodes and are accessible on the Delta DTN Endpoints. The aggregate performance of this subsystem is 70GB/s and it has 6PB of usable space. These file systems run Lustre via DDN's ExaScaler 6 stack (Lustre 2.14 based).

			#### Hardware

			DDN SFA7990XE (Quantity: 3), each unit contains

			* One additional SS9012 enclosure
			* 168 x 16TB SAS Drives
			* 7 x 1.92TB SAS SSDs
			
			The $HOME file system has 4 OSTs and is set with a default stripe size of 1.

			The `$SCRATCH` file system has 8 OSTs and has Lustre Progressive File Layout (PFL) enabled which automatically restripes a file as the file grows. The thresholds for PFL striping for $SCRATCH are:

		%table(border="1" cellpadding="3")
			%thead
				%tr
					%th File size
					%th Stripe count
			%tbody
				%tr
					%td 0-32M
					%td 1 OST
				%tr
					%td 32M-512M
					%td 4 OST
				%tr
					%td 512M+
					%td 8 OST

		:markdown
			#### Best Practices

			* To reduce the load on the file system metadata services, the ls option for context dependent font coloring, --color, is disabled by default.

			#### Future Hardware

			An additional pool of NVME flash from DDN has been installed in early summer 2022. This flash is *initially* deployed as a tier for "hot" data in scratch. This subsystem will have an aggregate performance of 500GB/s and will have 3PB of raw capacity. As noted above this subsystem will transition to an independent relaxed POSIX namespace file system, communications on that timeline will be announced as updates are available.

	#system:filesystems-taiga
		:markdown
			###[Taiga](#systems:filesystems-taiga)

			Taiga is NCSA's global file system which provides users with their `$WORK` area. This file system is mounted across all Delta systems at `/taiga` (also `/taiga/nsf/delta` is bind mounted at `/projects`) and is accessible on both the Delta and Taiga DTN endpoints. For NCSA & Illinois researchers, Taiga is also mounted on NCSA's HAL and Radiant systems. This storage subsystem has an aggregate performance of 140GB/s and 1PB of its capacity allocated to users of the Delta system. `/taiga` is a Lustre file system running DDN Exascaler software.
			
			#### Hardware

			DDN SFA400NVXE (Quantity: 2), each unit contains:

			* 4 x SS9012 enclosures
			* NVME for metadata and small files

			DDN SFA18XE (Quantity: 1) *coming soon*, unit contains:

			* 10 x SS9012 enclosures
			* NVME for metadata and small files

			<p class="portlet-msg-info">`$WORK` and `$SCRATCH`<br />A "module reset" in a job script will populate `$WORK` and `$SCRATCH` environment variables automatically, or you may set them as `WORK=/projects/<account>/$USER`, `SCRATCH=/scratch/<account>/$USER`.</p>

	#system:filesystems-features
		:markdown
			[Table 3. Filesystems Feature Comparison](#system:filesystems-features)

	%table(border="1" cellpadding="3")
		%thead
			%tr
				%th File System
				%th Quota
				%th Snapshots
				%th Purged
				%th Key Features
		%tbody
			%tr
				%td <code>HOME</code>
				%td <strong>25GB</strong>. 400,000 files per user
				%td No/TBA
				%td No
				%td Area for software, scripts, job files, etc. <strong>NOT</strong> intended as a source/destination for I/O during jobs
			%tr
				%td <code>WORK</code>
				%td <strong>500 GB</strong> Up to 1-25 TB by allocation request
				%td No/TBA
				%td No
				%td Area for shared data for a project, common data sets, software, results, etc.
			%tr
				%td <code>SCRATCH</code>
				%td <strong>1000 GB</strong> Up to 100 TB by allocation request
				%td No
				%td Yes; files older than 30 days (access time)
				%td Area for computation, largest allocations, where I/O from jobs should occur
			%tr
				%td <code>/tmp</code>
				%td <strong>0.74-1.50 TB</strong> shared or dedicated depending on node usage by job(s), no quotas in place
				%td No
				%td After each job
				%td Locally attached disk for fast small file I/O

	#system:filesystems-quota
		:markdown
			###[Quota usage](#system:filesystems-quota)

			The `quota` command allows you to view your use of the file systems and use by your projects. Below is a sample output for a person "user" who is in two projects: aaaa, and bbbb. The home directory quota does not depend on which project group the file is written with. 

			####quota command

			<pre><user>@dt-login01 ~]$ quota
			Quota usage for user <user>:
			-------------------------------------------------------------------------------------------
			| Directory Path | User | User | User  | User | User   | User |
			|                | Block| Soft | Hard  | File | Soft   | Hard |
			|                | Used | Quota| Limit | Used | Quota  | Limit|
			--------------------------------------------------------------------------------------
			| /u/<user>      | 20k  | 25G  | 27.5G | 5    | 300000 | 330000 |
			--------------------------------------------------------------------------------------
			Quota usage for groups user <user> is a member of:
			-------------------------------------------------------------------------------------
			| Directory Path | Group | Group | Group | Group | Group  | Group |
			|                | Block | Soft  | Hard  | File  | Soft   | Hard  |
			|                | Used  | Quota | Limit | Used  | Quota  | Limit |
			-------------------------------------------------------------------------------------------
			| /projects/aaaa | 8k    | 500G  | 550G  | 2     | 300000 | 330000 |
			| /projects/bbbb | 24k   | 500G  | 550G  | 6     | 300000 | 330000 |
			| /scratch/aaaa  | 8k    | 552G  | 607.2G| 2     | 500000 | 550000 |
			| /scratch/bbbb  | 24k   | 9.766T| 10.74T| 6     | 500000 | 550000 |
			------------------------------------------------------------------------------------------</pre>
