#jobs
	:markdown
		#[Running Jobs](#jobs)

	#jobs:accounting
		:markdown
			##[Job Accounting](#jobs:accounting)

			The charge unit for *Delta* is the Service Unit (SU). This corresponds to the equivalent use of one compute core utilizing less than or equal to 2G of memory for one hour, or 1 GPU or fractional GPU using less than the corresponding amount of memory or cores for 1 hour (see table below). *Keep in mind that your charges are based on the resources that are reserved for your job and don't necessarily reflect how the resources are used.* Charges are based on either the number of cores or the fraction of the memory requested, whichever is larger. The minimum charge for any job is 1 SU.

		%table(border="1" cellpadding="3")
			%thead
				%tr
					%th(colspan="2" rowspan="2") Node Type
					%th(colspan="3") Service Unit Equivalence
				%tr
					%th Cores
					%th GPU Fraction
					%th Host Memory
			%tbody
				%tr
					%th(colspan="2") CPU Node
					%td 1
					%td N/A
					%td 2 GB
				%tr
					%th(rowspan="4") GPU node
					%td Quad A100
					%td 2
					%td 1/7 A100
					%td 8 GB
				%tr
					%td Quad A40
					%td 16
					%td 1 A40
					%td 64 GB
				%tr
					%td 8-way A100
					%td 2
					%td 1/7 A100
					%td 32 GB
				%tr
					%td 8-way MI100
					%td 16
					%td 1 MI100
					%td 256 GB

		:markdown
			Please note that a weighting factor will discount the charge for the reduced-precision A40 nodes, as well as the novel AMD MI100 based node - this will be documented through the XSEDE SU converter.

	#jobs:accounting-local
		:markdown
			###[Local Account Charging](#jobs:accounting-local)

			Use the `accounts` command to list the accounts available for charging. CPU and GPU resources will have individual charge names. For example in the following, `abcd-delta-cpu` and `abcd-delta-gpu` are available for user `gbauer` to use for the CPU and GPU resources. 

			<pre>$ accounts
			available Slurm accounts for user gbauer:
			abcd-delta-cpu my_prefix my project
			abcd-delta-gpu my_prefix my project</pre>


	#jobs:accounting-considerations
		:markdown
			###[Job Accounting Considerations](#jobs:accounting-considerations)

			* A node-exclusive job that runs on a compute node for one hour will be charged 128 SUs (128 cores x 1 hour)
			* A node-exclusive job that runs on a 4-way GPU node for one hour will be charge 4 SUs (4 GPU x 1 hour)
			* A node-exclusive job that runs on a 8-way GPU node for one hour will be charge 8 SUs (8 GPU x 1 hour)
			* A shared job that runs on an A100 node will be charged for the fractional usage of the A100 (eg, using 1/7 of an A100 for one hour will be 1/7 GPU x 1 hour, or 1/7 SU per hour, except the first hour will be 1 SU (minimum job charge).

	#jobs:nodeaccess
		:markdown
			##[Accessing the Compute Nodes](#jobs:nodeaccess)

			Delta implements the Slurm batch environment to manage access to the compute nodes. Use the Slurm commands to run batch jobs or for interactive access to compute nodes. See <a target="_blank" rel="noopener noreferrer" href="https://slurm.schedmd.com/quickstart.html">an introduction to Slurm</a>. There are two ways to access compute nodes on Delta.

			Batch jobs can be used to access compute nodes. Slurm provides a convenient direct way to submit batch jobs. See <a target="_blank" rel="noopener noreferrer" href="https://slurm.schedmd.com/heterogeneous_jobs.html#submitting">Heterogeneous Job Support</a> in the Slurm guide.

			Sample Slurm batch job scripts are provided in the [Job Scripts](#jobs:scripts) section below.

			Direct ssh access to a compute node in a running batch job from a dt-loginNN node is enabled, once the job has started.

			<pre>$ squeue --job jobid
			             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
			             12345       cpu     bash   gbauer  R       0:17      1 cn001</pre>

			Then in a terminal session:

			<pre>$ ssh cn001
			cn001.delta.internal.ncsa.edu (172.28.22.64)
			  OS: RedHat 8.4   HW: HPE   CPU: 128x    RAM: 252 GB
			  Site: mgmt  Role: compute
			$</pre>

	#jobs:scheduler
		:markdown
			##[Scheduler](#jobs:scheduler)

			For information, consult: <a target="_blank" rel="noopener noreferrer" href="https://slurm.schedmd.com/quickstart.html">Slurm Quick Start User Guide</a> and the <a target="_blank" rel="noopener noreferrer" href="https://wiki.ncsa.illinois.edu/download/attachments/193200247/slurm_summary.pdf?version=1&modificationDate=1656365559000&api=v2">slurm quick reference guide</a> [PDF].

	#jobs:partitions
		:markdown
			##[Partitions (Queues)](#jobs:partitions)

		%table(border="1" cellpadding="3")
			%thead
				%tr
					%th Partition/Queue
					%th Node Type
					%th Max Nodes per Job
					%th Max Duration
					%th Max Running in Queue/user*
					%th Charge Factor
				%tr
					%td cpu
					%td CPU
					%td TBD
					%td 24 hr <em>/ 48 hr</em>
					%td 8448 cores
					%td 1.0
				%tr
					%td cpu-interactive
					%td CPU
					%td TBD
					%td 30 min
					%td in total
					%td 2.0
				%tr
					%td gpuA100x4*<br />(asterisk indicates this is the default queue, but submit jobs to gpuA100x4)
					%td quad-A100
					%td TBD
					%td 24 hr <em>/ 48 hr</em>
					%td 3200 cores and 200 gpus
					%td 1.0
				%tr
					%td gpuA100x4-interactive
					%td quad-A100
					%td TBD
					%td 30 min
					%td in total
					%td 2.0
				%tr
					%td gpuA100x8
					%td octa-A100
					%td TBD
					%td 24 hr <em>/ 48 hr</em>
					%td TBD
					%td 2.0
				%tr
					%td gpuA100x8-interactive
					%td octa-A100
					%td TBD
					%td 30 min
					%td TBD
					%td 4.0
				%tr
					%td gpuA40x4
					%td quad-A40
					%td TBD
					%td 24 hr <em>/ 48 hr</em>
					%td 3200 cores and 200 gpus
					%td 0.6
				%tr
					%td gpuA40x4-interactive
					%td quad-A40
					%td TBD
					%td 30 min
					%td in total
					%td 1.2
				%tr
					%td gpuMI100x8
					%td octa-MI100
					%td TBD
					%td 24 hr <em>/ 48 hr</em>
					%td TBD
					%td 1.5
				%tr
					%td gpuMI100x8-interactive
					%td octa-MI100
					%td TBD
					%td 30 min
					%td TBD
					%td 3.0

		:markdown
			Table 9. Delta Early Access Period *Production* Partitions/Queues 

			#### sview view of slurm partitions

			<img alt="" src="/documents/10308/3028413/sview_sinfo.jpg/b7b57bbc-55be-49a5-a05b-6ec45bfd34d7?t=1658876644948" style="width: 600px; height: 360px;" />

	#jobs:partitions-policies
		:markdown
			###[Node Policies](#jobs:partitions-policies)

			Node-sharing is the default for jobs. Node-exclusive mode can be obtained by specifying all the consumable resources for that node type or adding the following Slurm options:

				--exclusive --mem=0

			<em>GPU NVIDIA MIG (GPU slicing) for the A100 will be supported at a future date.</em>

			<em>Pre-emptive jobs will be supported at a future date.</em>

	#jobs:interactive
		:markdown
			##[Interactive Sessions](#jobs:interactive)

			Interactive sessions can be implemented in several ways depending on what is needed.

			To start up a bash shell terminal on a cpu or gpu node

			* single core with 1GB of memory, with one task on a cpu node

			<pre>srun --account=account_name --partition=cpu-interactive \
			  --nodes=1 --tasks=1 --tasks-per-node=1 \
			  --cpus-per-task=1 --mem=16g \
			  --pty bash</pre>

			* single core with 20GB of memory, with one task on a A40 gpu node

			<pre>srun --account=account_name --partition=gpuA40x4-interactive \
			  --nodes=1 --gpus-per-node=1 --tasks=1 \
			  --tasks-per-node=1 --cpus-per-task=1 --mem=20g \
			  --pty bash</pre>

			<p class="portlet-msg-info">interactive jobs: a case for mpirun<br />Since interactive jobs are already a child process of srun, one cannot srun applications from within them. Use `mpirun` to launch mpi jobs from within an interactive job. Within standard batch jobs submitted via sbatch, use `srun` to launch MPI codes.</p>

	#jobs:interactive-x11
		:markdown
			###[Interactive X11 Support](#jobs:interactive-x11)

			To run an X11 based application on a compute node in an interactive session, the use of the `--x11` switch with srun is needed. For example, to run a single core job that uses 1g of memory with X11 (in this case an xterm) do the following:

			<pre>srun -A abcd-delta-cpu  --partition=cpu-interactive \
			  --nodes=1 --tasks=1 --tasks-per-node=1 \
			  --cpus-per-task=1 --mem=16g \
			  --x11  xterm</pre>

