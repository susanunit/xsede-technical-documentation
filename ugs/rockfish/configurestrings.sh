#!/bin/sh

inputfile=$1
outputfile=$2

echo "building JHU Rockfish User Guide"

stylegreen="<span\ style=\"color:green;font-style:italic\">"
stylered="<span\ style=\"color:red\">"
styleyellow="<span\ style=\"background-color:yellow\">"
stylenowrap="<span\ style=\"white-space:\ nowrap;\">"
endspan="<\/span>"

xsedeuserportal="http:\/\/portal.xsede.org\/"
xsedeusernews="http:\/\/portal.xsede.org\/news\/"
xsedehelpdesk="https:\/\/portal.xsede.org\/group\/xup\/help-desk"

sed	-e "s/GREEN/$stylegreen/g" \
	-e "s/STYLERED/$stylered/g" \
	-e "s/YELLOW/$styleyellow/g" \
	-e "s/NOWRAP/$stylenowrap/g" \
	-e "s/ESPAN/$endspan/g" \
	\
	-e "s/XSEDEUSERPORTAL/$xsedeuserportal/g" \
	-e "s/XSEDEUSERNEWS/$xsedeusernews/g" \
	-e "s/XSEDEHELPDESK/$xsedehelpdesk/g" \
	\
	< $inputfile > $outputfile

 
