<span style="font-size:225%; font-weight:bold;">JHU Rockfish User Guide</span><br>
<i>Last update: June 15, 2021</i></p>

#notices
	:markdown
<!--		# [Status Updates and Notices](#notices) <font color="red">TBA - Upcoming allocation period</font> -->

#intro
	:markdown
		# [Introduction](#intro)

		Johns Hopkins University's Rockfish <!--(<http://rockfish.jhu.edu>)--> is a community-shared cluster at Johns Hopkins University. It follows the "condominium model" with three main integrated **units**. The **first unit** is based on a National Science Foundation (NSF) Major Research Infrastructure Grant (#1920103) and other main grants like DURIP/DoD, a **second unit** contains medium-size condos ((Schools' condos), and the **last unit** is the collection of condos purchased by individual research groups. All three units share a base infrastructure, and resources are shared by all users. Rockfish provides resources and tools to integrate traditional High Performance Computing (HPC) with Data Intensive Computing and Machine Learning (ML). As a multi-purpose resource for all fields of science, it will provide High Performance and Data Intensive Computing services to Johns Hopkins University, Morgan State University and XSEDE researchers as a level 2 Service Provider.

		Rockfish's compute nodes consist of two 24-core Intel Xeon Cascade Lake 6248R processors, 3.0GHz base frequency and 1 TB NMVe local drive. The regular and GPU nodes have 192GB of DDR4 memory, whereas the large memory nodes have 1.5TB of DDr4 memory.  The GPU nodes also have 4 Nvidia A100 GPUs.

		<p><img alt="" src="/documents/10308/0/JHU-Rockfish.jpg/c43f2fc8-113b-433a-9ca5-e97afd90cd69?t=1614649558703" style="width: 500px; height: 667px; border-width: 1px; border-style: solid;" />  
		Figure 1. Rockfish System  
		XSEDE hostname: <b><code>login.rockfish.jhu.edu</code></b></p>

#admin
	:markdown
		# [Account Administration](#admin)

		A proposal through the XSEDE Resource Allocation request System (XRAS) is required for a research or startup allocation.  See [XSEDE Allocations](/allocations) for more information about about different types of allocations. 

	#admin-configuring
		:markdown
			## [Configuring Your Account](#admin-configuring)

			Rockfish uses the **`bash`** shell by default. [Submit an XSEDE support ticket](XSEDEHELPDESK) to request a different shell.
 
			Reset Password: Users may reset a password in two ways:
	
			1. Email <help@rockfish.jhu.edu> to request a password reset.
			1. Users may also connect to the [Rockfish portal](https://coldfront.rockfish.jhu.edu/user/accounts/password_reset/) and type the email associated with the account.  A link to reset the password will be sent to this email.
 
	#admin-modules
		:markdown
			## [Modules](#admin-modules)

			The Rockfish cluster uses [Modules](https://www.tacc.utexas.edu/research-development/tacc-projects/lmod) (lua modules version 8.3, developed at TACC) to dynamically manage users' shell environments. "`module`" commands will set, modify, or delete environment variables in support of scientific applications, allowing users to select a particular version of an application or a combination of packages. 

			The "`ml available`" command will display (i) the applications that have been compiled using GNU compilers, (ii) external applications like matlab, abaqus, which are independent of the compiler used and (iii) a set of core modules.  Likewise, if the Intel compilers are loaded "`ml avail`" will display applications that are compiled using the Intel compilers. 

			A set of modules are loaded by default at login time. These include Slurm, `gcc/9.3` and `openmpi/3.1`.  We strongly recommend that users utilize this combination of modules whenever possible for best performance. In addition, several scientific applications are built with dependencies on other modules. Users will get a message on the screen if this is the case.  For more information type: 

			<pre class="cmd-line">login1$ <b>ml spider <i>application/version</i></b></pre>

			For example, if you have the `gcc/9.3.0` module loaded and try to load `intel-mpi` you will get::

			<pre>Lmod has detected the following error: These module(s) or extension(s) exist but cannot be loaded as requested: "intel-mpi"
			Try: "module spider intel-mpi" to see how to load the module(s).</pre>

			The "`ml available`" command will also display a letter after the module indicating where it is: 

				L(oaded), D(efault), g(gpu), c(ontainer)
 
	#table1
		:markdown
			Table 1. [Useful Modules Commands ](#table1)

%table(border="1" cellpadding="3" cellspacing="5")
	%tr
		%th Command	
		%th Alias / Shortcut
		%th Description
	%tr
		%td <code>module list</code>
		%td <code>ml</code>
		%td List modules currently loaded
	%tr
		%td <code>module avail</code>
		%td <code>ml av</code>
		%td List all scientific applications with different versions
	%tr
		%td <code>module show <i>modulename</i></code>	
		%td <code>ml show</code>
		%td Show the environment variables and settings in the module file
	%tr
		%td <code>module load <i>modulename</i></code>
		%td <code>ml</code>
		%td Load modules
	%tr
		%td <code>module unload <i>modulename</i></code>
		%td <code>mu <i>modulename</i></code>
		%td Unload the application or module
	%tr
		%td <code>module spider <i>modulename</i>	</code>
		%td <code>ml spider <i>modulename</i>	</code>
		%td Shows available versions for <i>modulename</i>
	%tr
		%td <code>module save <i>modulename</i></code>
		%td <code>ml save <i>modulename</i></code>
		%td Save current modules into a session (default) or named session
	%tr
		%td <code>module swap <i>modulename</i></code>
		%td <code>ml <i>modulename</i></code>
		%td Automatically swaps versions of modules
	%tr
		%td <code>module help</code>
		%td <code>ml help</code>
		%td Shows additional information about the scientific application
 
#architecture
	:markdown
		# [System Architecture](#architecture)

		Rockfish has three types of compute nodes. "regular memory or standard" compute nodes (192GB), large memory nodes (1524GB) and GPU nodes with 4 Nvidia A100 GPUs. All compute nodes have access to three GPFS file sets. Rockfish, nodes and storage, have Mellanox HDR100 connectivity, with topology 1.5:1. Rockfish is managed using the Bright Computing cluster management software and the Slurm workload manager for job scheduling.

	#architecture-compute
		:markdown
			## [Compute Nodes](#architecture-compute)

	#table2
		:markdown
			Table 2. Compute Node Specifications](#table2)

	%table(border="1" cellpadding="3" cellspacing="5")
		%tr
			%th(colspan=2) Regular (memory) compute nodes
		%tr
			%th(align="right") Model 
			%td Lenovo SD530<br>Intel Xeon Gold Cascade Lake 6248R 
		%tr
			%th(align="right") Total cores per node 	
			%td 48 cores per node
		%tr
			%th(align="right") Number of Nodes	
			%td 368
		%tr
			%th(align="right") Clock rate 	
			%td 3.0 GHz
		%tr
			%th(align="right") RAM 	
			%td 192GB 
		%tr
			%th(align="right") Total number of cores	
			%td 17,664
		%tr
			%th(align="right") Local storage 	
			%td 1 TB NVMe
		%tr
			%th(colspan="2") Large Memory Nodes
		%tr
			%th(align="right") Model 
			%td Lenovo SR630<br>Intel Xeon Gold Cascade Lake 6248R 
		%tr
			%th(align="right") Total cores per node 	
			%td 48 cores per node
		%tr
			%th(align="right") Number of Nodes	
			%td 10
		%tr
			%th(align="right") Clock rate 	
			%td 3.0 GHz
		%tr
			%th(align="right") RAM 	
			%td 1524GB
		%tr
			%th(align="right") Total number of cores	
			%td 480
		%tr
			%th(align="right") Local storage 	
			%td 1 TB NVMe
		%tr
			%th(colspan=2) GPU Nodes
		%tr
			%th(align="right") Model 
			%td Lenovo SR670<br>Intel Xeon Gold Cascade Lake 6248R 
		%tr
			%th(align="right") Total cores per node 	
			%td 48 cores per node
		%tr
			%th(align="right") Number of Nodes	
			%td 10
		%tr
			%th(align="right") Clock rate 	
			%td 3.0 GHz
		%tr
			%th(align="right") RAM 	
			%td 192GB
		%tr
			%th(align="right") Total number of cores	
			%td 480
		%tr
			%th(align="right") GPUs	
			%td 4 Nvidia A110 GPUs (40Gb)  PCIe
		%tr
			%th(align="right") Total number of GPUs	
			%td 40
		%tr
			%th(align="right") Local storage 	
			%td 1 TB NVMe
 
	#architecture-login
		:markdown
			## [Login Nodes](#architecture-login)

			Rockfish's three login nodes (`login01-03`) are physical nodes with architecture and features similar to the regular memory compute nodes. Please use the gateway to connect to Rockfish.


	#architecture-datatransfer
		:markdown
			## [Data Transfer Nodes (DTNs)](#architecture-datatransfer)

			These nodes can be used to transfer data to the Rockfish cluster using secure copy, Globus or any other utility like Filezilla.  The endpoint for Globus is "Rockfish User Data". The DTNs are "rfdtn1.rockfish.jhu.edu" and "rfdtn2.rockfish.jhu.edu".  Thee nodes are mounted and available on all file systems.


	#architecture-software
		:markdown
			## [Systems Software Environment](#architecture-software)

	#table3
		:markdown
			Table 3. Systems Software Environment](#table3)

		%table(border="1" cellpadding="3" cellspacing="5")
			%tr
				%th Software function	
				%th Description
			%tr
				%th(align="right") Cluster management	
				%td Bright Cluster Management
			%tr
				%th(align="right") File System management	
				%td Xcat/Confluent
			%tr
				%th(align="right") Operating System	
				%td CentOS 8.2
			%tr
				%th(align="right") File Systems	
				%td GPFS, ZFS
			%tr
				%th(align="right") Scheduler and resource management	
				%td Slurm
			%tr
				%th(align="right") User Environment	
				%td Lua modules
			%tr
				%th(align="right") Compilers	
				%td Intel, GNU, PGI
			%tr
				%th(align="right") Message passing	
				%td Intel MPI, OpenMPI, MVAPICH
 
	#architecture-files
		:markdown 
			## [File Systems](#architecture-files)

	#table4
		:markdown
			Table 4. Rockfish File Systems](#table4)

%table(border="1" cellpadding="3" cellspacing="5")
	%tr
		%th File System	
		%th Quota	
		%th File retention	
		%th Backup	
		%th Features
	%tr
		%td <code>$HOME	</code>
		%td 50GB	 
		%td No file deletion policy	
		%td Backed up to an off-site location	
		%td NVMe File system
	%tr
		%td <code>$SCRATCH4 </code>
		%td 10TB (combined with scratch16)	
		%td 30 day retention. Files that have not been accessed for 30 days will be moved to the <code>/data</code> file system.	
		%td NO	
		%td Optimized for small files. Block sized 4MB
	%tr
		%td <code>$SCRATCH16</code>
		%td Same as above	
		%td Same as above
		%td NO
		%td Block size 16MB<br>Optimized for large files
	%tr
		%td <code>"data"</code>
		%td 10TB	
		%td No deletion policy, but quota driven
		%td optional	
		%td GPFS file set, lower performance

<hr>

#access
	:markdown
		# [Accessing the System](#access)

		Rockfish is accessible only to those users and research groups that have been awarded a Rockfish-specific allocation. XSEDE users can connect to Rockfish in different ways:

	#access-ssh
		:markdown
			## [Secure Shell](#access-ssh)

			<pre class="cmd-line">localhost$ <b>ssh [-XY] login.rockfish.jhu.edu -l userid</b></pre>

			"`login`" is a gateway server that will authenticate credentials and then connect the user to one of three physical login nodes (identical to regular compute nodes). Hostname: `login.rockfish.jhu.edu` (gateway)
 
	#access-sso
		:markdown
			## [XSEDE Single Sign-On (SSO) Hub](#access-sso)

			XSEDE users can also access Rockfish via the XSEDE Single Sign-On Hub. When reporting a problem to the [XSEDE Help Desk](/overview1), please execute the "`gsissh -vvv`" command and include the verbose output in your problem description.
 

#citizenship
	:markdown
		# [Citizenship](#citizenship)

		You share Rockfish with thousands of other users, and what you do on the system affects others. Exercise good citizenship to ensure that your activity does not adversely impact the system and the research community with whom you share it. Here are some rules of thumb:

		* **Don't run jobs on the login nodes**. Login nodes are used by hundreds of users to monitor their jobs, submit jobs, edit and manipulate files and in some cases to compile codes. We strongly request that users abstain from running jobs on login nodes. Sometimes users may want to run quick jobs to check that input files are correct or scientific applications are working properly. If this is the case, make sure this activity does not take more than a few minutes or even better request an interactive session (interact) to fully test your codes.

		* **Don't stress the file systems**. Do not perform activities that may impact the file systems (and the login nodes), for example `rsync` or copying large or many files from one file system to another. Please use globus of the data transfer node (rfdtn1) to copy large amounts of data

		* When [submitting a help-desk ticket](XSEDEHELPDESK), be as informative as possible.

	#citizenship-loginnodes
		:markdown
			## [Login Node Activities](#citizenship-loginnodes)
 
			* Request an interactive session "interact -usage"
 
				<pre class="cmd-line">
				login1$ <b>interact -X -p analysis -n 1 -c 1 -t 120 </b>
				c001> <b>ml matlab ; matlab</b></pre>
	 
			* Compile codes, for example run "`make`". Be careful if you are running commands with multiple processes. "`make -j 4`" may be fine but "`make -j 20`" may impact other users.
			* Check jobs, use this command "`sqme`"
			* Edit files, scripts, manipulate files
			* Submit jobs
			* Check output files
		 
			What is NOT allowed:

			* Run executables e.g. "`./a.out`"
			* multiple `rsync` sessions or copy large number of files 

#files
	:markdown
		# [Managing Files](#files)

	#files-transferring
		:markdown
			## [Transferring your Files](#files-transferring)
 
			1. **`scp`**: Secure copy commands can be used when transferring small amounts of data. We strongly encourage to use the data transfer nodes instead of the gateway.
 
				scp [-r] file-name userid@rfdtn1.rockfish.jhu.edu:/path/to/file/dir
 
			2. **`rsync`**: An alternative to `scp` would be "`rsync`". This command is useful when copying files between file systems or in/out of Rockfish. `rsync` can also be used to sync file systems as new files are created or as files are modified.
 
				<pre class="cmd-line">login1$ <b>rsync -azvh dir1 /new/path/</b> </pre>

				or 

				<pre class="cmd-line">login1$ <b>rsync -azvh DIR2 userid@server.ip.address:/path/to/new/dir</b></pre>
 
			3. **Globus**: We strongly recommend the use of our managed end points via Globus. Rockfish's Globus end point is "`Rockfish User Data`"

	#files-sharing
		:markdown
			## [Sharing Files with Collaborators](#files-sharing)
	
			Users are strongly encouraged to use Globus features to share files with internal or external collaborators.

 
#software
	:markdown
		# [Software](#software)
 
		Rockfish provides a broad application base managed by Lua modules. Most commonly used packages in bioinformatics, molecular dynamics, quantum chemistry, structural mechanics, and genomics are available ("`ml avail`"). Rockfish also supports Singularity containers. 
 
	#software-installed
		:markdown
			## [Installed Software](#software-installed)

			Rockfish uses the Lua modules. Type "`ml avail`" to list all the scientific applications that are installed and available via modules.

			* "`module`" (or "`ml`") : displays a list of installed applications and corresponding versions.
			* "`ml spider APP1`"     : displays all information on package APP1 (if it is installed)
			* "`ml help APP1`"       : displays any additional information on this scientific application.

	#software-building
		:markdown
			## [Building Software](#software-building)

			Users may want to install scientific applications that are used only by the user of by the group in their HOME directories. Then users can create a private module.

			1. Create a directory to install the application: "`mkdir -p $HOME/code/APP1`"
			1. Install the application following the instructions (`README` or `INSTALL` files).
			1. Create a directory in your HOME directory to create a module file: "`mkdir $HOME/modulefiles/APP1`"
			1. Create a "`.lua`" file that adds the application path to your `$PATH` environment variable and all other requirements (lib or include files).
			1. Load the module as "`ml own; ml APP1`"
 
	#software-building-compilers
		:markdown
			### [Compilers and recommendations](#software-building-compilers)

			The Rockfish cluster provides three different compilers for compute nodes, GNU, Intel and PGI. There are also MPI libraries (openmpi, Intelmpi and Mvapich2). Most applications have been built using GNU compilers version 9.3.0. Users should evaluate which compiler gives the best performance for their applications.

			The intel compilers and intel-mpi libraries can be loaded by executing the following command:

			<pre class="cmd-line">login1$ <b>ml intel intel-mpi intel-mkl</b></pre>

			A standard command to compile a Fortran or C-code will look like:  (add as many flags as needed)

			<pre class="cmd-line">login1$ <b>ifort (icc) -O3 -xHOST -o code.x code.f90 (or code.c) </b></pre>

 
			For GNU compilers you may want to use this sequence:
 
			<pre class="cmd-line">login1$ <b>g++ -O3 -march=native -mtune=native -march=cascadelake-avx2</b></pre>
 
#running
	:markdown
		# [Running Jobs](#running)

	#running-accounting
		:markdown
			## [Job Accounting](#running-accounting)
 
			Rockfish allocations are made in core-hours. The recommended method for estimating your resource needs for an allocation request is to perform benchmark runs. The core-hours used for a job are calculated by multiplying the number of processor cores used by the wall-clock duration in hours. Rockfish core-hour calculations should assume that all jobs will run in the regular queue. 
 
			For example: if you request one core on one node for an hour your allocation will be charged one core-hour. If you request 24 cores on one node, and the job runs for one hour, your account will be charged 24 core-hours.  For parallel jobs, compute nodes are dedicated to the job. If you request 2 compute nodes and the job runs for one hour, your allocation will be charged 96 core-hours. 
 
			Job accounting is independent of the number of processes you run on compute nodes.  You can request 2 cores for your job for one hour. If you run only one process, your allocation will be charged for 2 core-hours.  

				Charge = Number of cores x wall-time.

	#running-computenodes
		:markdown
			## [Accessing the Compute Nodes](#running-computenodes)

			* **Batch jobs**: Jobs can be submitted to the scheduler by writing a script and submitting it via the "sbatch" command:

				<pre class="cmd-line">login1$ <b>sbatch script-file-name</b></pre>
		
				where `script-file-name` is a file that contains a set of keywords used by the scheduler to set variables and the parameters for the job. It also contains a set of Linux commands to be executed. See [Job Scripts](#scripts) below.
		 
			* **Interactive sessions**: Users may need to connect to a compute node in interactive mode by using a internal script called "interact".  See "`interact -usage`" will provide examples and a list of parameters. For example:
 
				<pre class="cmd-line">login1$ <b>interact -p defq-n 1 -c 1 -t 120</b></pre>

				Will request an interactive session on the defq queue with one core for 2 hours.
		
				Alternatively users can use the full command:

				<pre class="cmd-line">login1$ <b>alloc -J interact -N 1 -n 12 --time=120 --mem=48g -p defq srun --pty bash</b></pre>

				This command will request an interactive session with 12 cores for 120 minutes and 48GB memory for the job (4GB per core).

			* **`ssh` from a login node directly to a compute node**. Users may `ssh` to a compute node where their jobs are running to check or monitor the status of their jobs. This connection will last a few minutes.

	#running-slurm
		:markdown
			## [Slurm Job Scheduler](#running-slurm)

			Rockfish uses Slurm (Simple Linux Universal Resource Manager) to manage resource scheduling and job submission. Slurm is an open source application with active developers and an increasing user community. It has been adopted by many HPC centers and universities. All users must submit jobs to the scheduler for processing, that is "interactive" use of login nodes for job processing is not allowed. Users who need to interact with their codes while these are running can request an interactive session using the script "interact", which will submit a request to the queuing system that will allow interactive access to the node.

			Slurm uses "partitions" to divide types of jobs (partitions are called queues on other schedulers). Rockfish defines a few partitions that will allow sequential/shared computing and parallel (dedicated or exclusive nodes), GPU jobs and large memory jobs. The default partition is "`defq`".

	#running-queues
		:markdown
			## [Queues on Rockfish](#running-queues)

			Queue limits are subject to change. Rockfish will use partitions and resources associated with them to create different types of allocations.

			Regular memory allocations will allow the use of all the regular compute nodes (currently the `defq` partition).  All jobs submitted to the `defq` partition will account against this partition.

			Large memory (LM) allocations will allow the use of the large memory nodes. If a user submits a job to this partition then the LM allocation is charged by default.

			Likewise, there is a GPU partition that will allow the use of an GPU nodes.

	#table5
		:markdown
			Table 5. Rockfish Production Queues](#table5)

	%table(border="1" cellpadding="3" cellspacing="5")
		%tr
			%th Queue Name	
			%th Max Nodes per Job (assoc'd cores)*	
			%th Max Duration	
			%th Max number of cores (running)	
			%th Max number Running + queued	
			%th Charge Rate (per node-hour)
		%tr
			%td <code>defq</code>
			%td 368 nodes, 48 cores per node	
			%td 72 hours	
			%td 4800	
			%td 9600
			%td 1 Service Unit (SU)
		%tr
			%td <code>bigmem</code>
			%td 10 nodes (1524GB per node)	
			%td 48 hrs	
			%td 144 	
			%td 288	
			%td 1 SU
		%tr
			%td <code>a100</code> 
			%td 10 nodes, 192GB RAM, 4 Nvidia A100	
			%td 48	
			%td 144	
			%td 288	
			%td 1SU

	#running-jobmanagement
		:markdown
			## [Job Management](#running-jobmanagement)

			Users can **monitor** their jobs with the "`squeue`" command. In this example user test345 is running two jobs: JobID: 31559 is a parallel job using 4 nodes. JobId: 31560 is a large memory job running on node bigmem01.

			<pre class="cmd-line">login1$ <b>squeue -l -u $USER</b>
			JOBID PARTITION     NAME     USER    STATE  TIME NODES NODELIST(REASON)
			31559      defq Parallel  test345  RUNNING  1:55     4 c[399-402]
			31560    lrgmem       LM  test345  RUNNING  0:31     1 bigmem01</pre>

			Users can also invoke a script, "`sqme`", to monitor jobs:

			<pre class="cmd-line">login1$ <b>sqme</b></pre>

			To **cancel** a job, sse the "`scancel`" command followed by the jobid. For example "`scancel 31560`" will cancel the LM job for user test345 in the example above.

#jobscripts
	:markdown
		# [Sample Job Scripts](#jobscripts)

		The following scripts are examples for different workflows. Users can modify them according to the resources needed to run their applications.

	#jobscripts-mpi
		:markdown
			## [MPI Jobs](#jobscripts-mpi)
 
			This job will run on 5 nodes each with 48 processes/cores. Total 240 MPI processes.

			<pre class="job-script">
			#!/bin/bash
			#SBATCH –job-name=MPI-job
			#SBATCH –time=5:0:0
			#SBATCH –partition=defq
			#SBATCH -N 5
			#SBATCH –ntasks-per-node=48
			#SBATCH -A My-Account
 
			module purge 		
			ml intel intel-mpi #load Intel compiler and Intel MPI libraries
 
			mpirun ./my-mpi-code.x < my-input-file > My-output.log</pre>
 
	#jobscripts-openmp
		:markdown
			## [OpenMP/Threaded Jobs](#jobscripts-openmp)
 
			This script will run a small job that creates 8 threads. It will use the default time of 1:00:00 (one hour). 

			<pre class="job-script">
			#!/bin/bash
			#SBATCH –job-name=my-openmp-job
			#SBATCH -P defq
			#SBATCH -N 1
			#SBATCH –ntasks-per-node=8
			#SBATCH –mem-per-cpu=4GB
			#SBATCH -A My-Account
			#SBATCH –export=ALL
 
			ml purge
			ml gcc
			export OMP_NUM_THREADS=8
			time ./a.out &gt; My-output.log</pre>
 
 
	#jobscripts-hybrid
		:markdown
			## [Hybrid (MPI + OpenMP)](#jobscripts-hybrid)
 
			This script will run a hybrid jobs (Gromacs) on two nodes, each node will have 8 MPI processes each with 6 threads

			<pre class="job-script">
			#!/bin/bash
			#SBATCH –Job-name=Hybrid
			#SBATCH –time=4:0:0
			#SBATCH –partition=defq
			#SBATCH -N 2
			#SBATCH –ntasks-per-node=8
			#SBATCH –cpus-per-task=6
			#SBATCH -A My-Account
			#SBATCH -o Hybrid-%J.log
			#SBATCH –export=ALL
 
			ml purge
			ml gcc openmpi
			ml hwloc boost
			ml gromacs/2016-mpi-plumed
 
			mpirun -np 8 bin/gmx_mpi mdrun -deffnm [options…]</pre>
 
	#jobscripts-gnuparallel
		:markdown
			## [GNU parallel ](#jobscripts-gnuparallel)

			This sample will run 48 serial jobs on one node using GNU parallel. This job directs output to the local scratch file system.
			<pre class="cmd-line">
			#!/bin/bash -l
			#SBATCH --time=2:0:0
			#SBATCH --nodes=1
			#SBATCH --ntasks-per-node=48
			#SBATCH --partition=defq          
			#SBATCH --export=ALL
 
			module load gaussian
			mkdir -p /scratch16/$USER/$SLURM_JOBID
			export GAUSS_SCRDIR=/scratch16/$USER/$SLURM_JOBID
 
			ml parallel

			# create a file that contains the names of the 48 input files.
			cat my-list | parallel -j 20 --joblog LOGS "g09 {}"</pre>
 
 
	#jobscripts-htc
		:markdown
			## [Parametric / Array / HTC jobs](#jobscripts-htc)

			This script is an example to run a set of 5,000 jobs. Only 480 jobs will run at a time. The input files are in a directory (`$workdir`). A temporary directory (`$tmpdir`) will be created in "`scratch`" where all the jobs will be run. At the end of each run the temporary directory is deleted.

			<pre class="job-script">
			#!/bin/bash -l
			#SBATCH –job-name=small-array
			#SBATCH –time=4:0:0
			#SBATCH –partition=defq
			#SBATCH –nodes=1
			#SBATCH –ntasks-per-node=1
			#SBATCH –mem-per-cpu=4G
			#SBATCH –array=1-5000%480

			# load modules and check
			ml purge
			module load intel
			ml

			# set variable "file" to read all the files in $workdir 
			# (zmatabcde where "abcde" goes from 00001 to 05000) and 
			# assign them to the job array
			file=$(ls zmat* | sed -n ${SLURM_ARRAY_TASK_ID}p)
			echo $file

			# get the number for each file (abcde)
			newstring="${file:4}"
			export basisdir=/scratch16/jcombar1/LC-tests
			export workdir=/scratch16/jcombar1/LC-tests
			export tmpdir=/scratch16/jcombar1/TMP/$SLURM_JOBID
			export PATH=/scratch16/jcombar1/LC/bin:$PATH
			export OMP_NUM_THREADS=1
			export MKL_NUM_THREADS=1
			mkdir -p $tmpdir
			cd $tmpdir

			# run your job
			cp $workdir/$file   ZMAT
			cp $basisdir/GENBAS GENBAS
			&nbsp;./a.out > $workdir/out.$newstring
			cd ..
			rm -rf $tmpdir</pre>

	#jobscripts-bigmem
		:markdown
			## [Bigmem (LM) Jobs](#jobscripts-bigmem)

			This script will run a job that needs large amounts of memory. Users need a special resource allocation (bigmem). It will use the default time 1:00:00 (one hour).

			<pre class="job-script">
			#!/bin/bash
			#SBATCH –job-name=my-bigmem-job
			#SBATCH -P bigmem
			#SBATCH -N 1
			#SBATCH –ntasks-per-node=48
			#SBATCH -A My-Account_bigmem    ###   this flag is required
			#SBATCH –export=ALL

			ml purge
			ml intel

			time ./big-mem.x  > My-output.log</pre>

	#jobscripts-a100
		:markdown
			## [GPU (LM) Jobs (a100 partition)](#jobscripts-a100)

			This script will run a job that uses all 4 Nvidia a100 gpus. Users need a special resource allocation (gpu). It will use the default time 1:00:00 (one hour).

			<pre class="job-script">
			#!/bin/bash
			#SBATCH –job-name=my-bigmem-job
			#SBATCH -P a100
			#SBATCH -N 1
			#SBATCH –ntasks-per-node=48
			#SBATCH --gres=gpu:4
			#SBATCH -A My-Account_gpu    ###   this flag is required
			#SBATCH –export=ALL

			ml purge
			ml intel

			time ./gpu-code.x  &gt; My-output.log</pre>


#help
	:markdown
		# [Help](#help)

		Please visit the [XSEDE help desk](/help-desk) for important contact information. When [submitting a support ticket](XSEDEHELPDESK), please include: 

		* a complete description of the problem with accompanying screenshots if applicable 
		* include any paths to job script or input/output files.
		* if you are having problems while on a login node please include the login node name
 
