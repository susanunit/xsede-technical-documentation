#!/bin/sh

inputfile=$1
outputfile=$2

echo "building PSC Bridges User Guide"



#revision history arrows
revhistoryrightarrow="\"\/documents\/10308\/0\/small-right-arrow.png\/a9ed7d76-796c-4b08-b7bd-a59452bf0da1?t=1480702749000\""
revhistorydownarrow="\"\/documents\/10308\/0\/small-down-arrow.png\/3f6dbc1d-4bc5-4903-9f7f-ec9926d7d1eb?t=1480702747000\""

#XSEDE images
logincomputenodes="<img src=\"broken\">"
stockyard="<img src=\"broken\">"
knlmemorymodes="<img src=\"broken\">"
knlclustermodes="<img src=\"broken\">"



xsedeuserportal="http:\/\/portal.xsede.org\/"
xsedeusernews="http:\/\/portal.xsede.org\/news\/"

stylegreen="<span\ style=\"color:green;font-style:italic\">"
stylered="<span\ style=\"color:red\">"
styleyellow="<span\ style=\"background-color:yellow\">"
stylenowrap="<span\ style=\"white-space:\ nowrap;\">"
endspan="<\/span>"



sed	-e "s/GREEN/$stylegreen/g" \
	-e "s/STYLERED/$stylered/g" \
	-e "s/YELLOW/$styleyellow/g" \
	-e "s/NOWRAP/$stylenowrap/g" \
	-e "s/ESPAN/$endspan/g" \
	\
	-e "s/XSEDEUSERPORTAL/$xsedeuserportal/g" \
	-e "s/XSEDEUSERNEWS/$xsedeusernews/g" \
	\
	-e "s/SMALLDOWNARROW/$revhistorydownarrow/g" \
	-e "s/SMALLRIGHTARROW/$revhistoryrightarrow/g" \
	< $inputfile > $outputfile

 
