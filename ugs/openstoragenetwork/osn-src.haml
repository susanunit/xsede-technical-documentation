<style>.help{box-sizing:border-box}.help *,.help *:before,.help *:after{box-sizing:inherit}.row{margin-bottom:10px;margin-left:-15px;margin-right:-15px}.row:before,.row:after{content:" ";display:table}.row:after{clear:both}[class*="col-"]{box-sizing:border-box;float:left;position:relative;min-height:1px;padding-left:15px;padding-right:15px}.col-1-5{width:20%}.col-2-5{width:40%}.col-3-5{width:60%}.col-4-5{width:80%}.col-1-4{width:25%}.col-1-3{width:33.3%}.col-1-2,.col-2-4{width:50%}.col-2-3{width:66.7%}.col-3-4{width:75%}.col-1-1{width:100%}article.help{font-size:1.25em;line-height:1.2em}.text-center{text-align:center}figure{display:block;margin-bottom:20px;line-height:1.42857143;border:1px solid #ddd;border-radius:4px;padding:4px;text-align:center}figcaption{font-weight:bold}.lead{font-size:1.7em;line-height:1.4;font-weight:300}.embed-responsive{position:relative;display:block;height:0;padding:0;overflow:hidden}.embed-responsive-16by9{padding-bottom:56.25%}.embed-responsive .embed-responsive-item,.embed-responsive embed,.embed-responsive iframe,.embed-responsive object,.embed-responsive video{position:absolute;top:0;bottom:0;left:0;width:100%;height:100%;border:0}</style>

<span style="font-size:225%; font-weight:bold;">Open Storage Network User Guide</span><br>
<em>Last update: April 28, 2021</em></p>

#top
	:markdown
		<br>The Open Storage Network (OSN) is a distributed data sharing and transfer service intended to facilitate exchanges of active scientific data sets between research organizations, communities and projects, providing easy access and high bandwidth delivery of large data sets to researchers.
		
		The OSN serves two principal purposes: (1) enable the smooth flow of large data sets between resources such as instruments, campus data centers, national supercomputing centers, and cloud providers; and (2) facilitate access to long tail data sets by the scientific community. Examples of data currently available on the OSN include synthetic data from ocean models; the widely used Extracted Features Set from the [Hathi Trust Digital Library](https://www.hathitrust.org); open access earth sciences data from [Pangeo](https://pangeo.io); and Geophysical Data from [BCO-DMO](https://www.bco-dmo.org). These data sets are being used by researchers to machine learning models, validate simulations, and perform statistical analysis of live data. 
		
		
#overview
	:markdown
		# [System Overview](#overview)

		OSN data is housed in storage pods interconnected by national, high-performance networks creating well-connected, cloud-like storage that is easily accessible at high data transfer rates comparable to or exceeding the public cloud storage providers, where users can temporariy park data, for retrieval by a collaborator or create a repository of active research data .

		This user guide is designed for the following categories of OSN user:

		* End Users who wish to view metadata and retrieve data.
		* Data Curators who maintain data sets
		* Data Managers who grant access to data sets for Curators and End Users

	#overview-configuration
		:markdown
			## [Configuration](#overview-configuration)
		
			Key characteristics of OSN storage are:
		
			* Ability to access data from anywhere via a RESTful interface that follows S3 conventions.
			* Federated identity management, allowing access to protected information with existing identity via InCommon or commercial services.
			* High speed access and transfer via national research and education networks
			* Security and data integrity
		
			OSN storage pods are located in science DMZs at Big Data Hub sites, interconnected by national, high-performance networks. 5 petabytes of storage are currently available for allocation. 

	#figure1
		:markdown
			<figure><img alt="OSN Pod Deployment at six sites as of January, 2021" src="/documents/10308/2640738/image3.png/11007c11-0f7c-40a3-977c-57732b696a21?t=1610740756313" style="width: 600px; border-width: 1px; border-style: solid; height: 315px;" /><figcaption> Figure 1. OSN Pod Deployment at six sites as of January, 2021 </figcaption> </figure>
		
	#figure2
		:markdown
			<figure><img alt="OSN Storage Pod" src="/documents/10308/2640738/image11.png/b36bc0a8-5a0b-49f9-9a96-b5680b7faf6b?t=1610740984397" style="width: 400px; height: 229px; border-width: 1px; border-style: solid;" /><figcaption> Figure 2. OSN Storage Pod </figcaption></figure>
		
	#overview-filesystems
		:markdown
			## [File Systems](#overview-filesystems)
		
			OSN Storage is disk based and primarily intended to house active data sets. OSN storage is allocated from the pod(s) closest to the requestor with capacity to fulfill the request. Allocations of a minimum 10 terabytes and a maximum of 50 terabytes can be requested through the XRAS process. If your project needs more than 50 terabytes, please <a href="mailto:jma@mghpcc.org?subject=OSN%20Request">contact the OSN team</a> directly to discuss before you submit your request. 

			The OSN supports two types of data sets:

			1. Open Access Data Sets that are readable by anyone and writable by Curators and Data Managers.
			1. Protected Access Data Sets that are readable by invitation from a data manager and writable by Curators and Data Managers.

			Every data set is a collection of objects that are individually and uniquely accessible from anywhere. For Open Access data sets, an S3 RESTful interface allows users to manipulate storage objects simply by issuing commands in the form of Uniform Resource Identifiers. For example issuing the following URI via your web browser will download a copy of this document: <a href="https://mghp.osn.xsede.org/osndemo1/AHM18_OSN_Poster.pdf">https://mghp.osn.xsede.org/osndemo1/AHM18_OSN_Poster.pdf</a> For Protected Access Data Sets, the user first obtains an access key which is then embedded into the access command. Examples of each are provided below. 

			**Coming soon**: Consistent with <a href="https://www.nature.com/articles/sdata201618">FAIR</a> principles, every OSN data set will have a landing page that makes it easy to "visit" a data set from a browser, search engine, or data catalog. The landing page contains metadata that describes the data set, along with the links to preconfigured, downloadable tools for accessing the data. 

			An active research data set can remain in OSN storage up to five years and usage must comply with the OSN Acceptable Use Policy. 


#allocations
	:markdown
		# [Allocations](#allocations)
		
		<p class="portlet-msg-alert">Storage on the OSN is allocated in standalone buckets independent of HPC allocations. There is a one-to-one mapping between buckets and allocations. This User Guide uses "Allocation" when referring to outward-facing operations such as Allocation requests, and "Bucket" when referring to inward-facing operations such as Bucket creation.</pre>

		OSN storage is allocated from the resources at the location(s) closest to the requestor with capacity to fulfill the request. Allocations of a minimum 10 terabytes and max of 50 terabytes supporting up to 1.6 million files can be requested through the XRAS process. Larger allocations can be accommodated with additional review. If your project needs more than more than 50 terabytes or more than 1.6 million files, please <a href="mailto:jma@mghpcc.org?subject=OSN%20Request">contact the OSN team</a> directly to discuss before you submit your request. 

		An active research dataset can remain in OSN storage up to five years. 
		
#access-datasets
	:markdown
		# [Accessing Datasets](#access-datasets)
		
		OSN supports a RESTful API that is compatible with the basic data access model of the <a href="http://docs.aws.amazon.com/AmazonS3/latest/API/APIRest.html">Amazon S3 API</a>. Any software that complies with that API can access data stored on the OSN.
		
		There are three common methods for connecting to and using OSN resources: OSN portal built-in web tools, third party desktop applications and third party data management server applications.

		
	#datasets-webtools
		:markdown
			## [OSN Portal Built-in Web Tools](#datasets-webtools)

			The OSN portal (`portal.osn.xsede.org`) supports a simple UI that allows end users to browse allocations and to upload and download objects via the browser. This mode of access is most appropriate for browsing a dataset and uploading/downloading smaller files (typically &lt;100G). 
		
			To use the built-in browser, a user logs onto the OSN portal and clicks on one of the allocations that they have been granted access to. This brings the user to a searchable/sortable table listing of the allocation and its subdirectories. Clicking on any of the objects shown initiates a download of the object to the local disk.

			To upload a file, the user locates the file on their local filesystem and drags the file to the browser window. This initiates an upload to the bucket location that the user is currently browsing.
		
		#datasets-webtools-bucketbrowser
			:markdown
				### [OSN Basic Bucket Browser](#datasets-webtools-bucketbrowser)

				<figure><img alt="" src="/documents/10308/2640738/image9.png/efdf4ddc-3636-44f1-93af-bdab08c980bc?t=1610740924752" style="width: 600px; border-width: 1px; border-style: solid; height: 466px;" />  <figcaption>Figure. OSN Basic Bucket Explorer</figcaption></figure>

	#datasets-thirdparty
		:markdown
			## [Third Party Desktop Applications](#datasets-thirdparty)

			There are numerous commercial and open source software tools for moving files to and from S3 buckets. These tools provide more sophisticated capabilities than the built-in browser tool including transfer management, multi-upload management and provide configuration options that can help optimize data transfer for a given computer/network environment. 

			To use these tools, you will need to retrieve a pair of keys that are used to access the buckets stored on OSN. To retrieve these keys, you can contact your data manager and she will either give you keys or create an account for you on the OSN portal where you can retrieve these keys. If your data manager creates a portal account for you and gives you access to the keys you can visit <a href="https://portal.osn.xsede.org">https://portal.osn.xsede.org</a> to retrieve them; the allocations you have access to and their associated keys will be listed on your home page.

			<figure><img alt="OSN Portal User Home page" src="/documents/10308/2640738/image5.png/22c3cae2-6ffc-412b-9eec-7b34db832d7c?t=1610740795041" style="width: 600px; border-width: 1px; border-style: solid; height: 445px;" /><figcaption>Figure. OSN Portal User Home page</figcaption></figure>

			Note that the "Bucket" information displayed in the portal has two components (this will be important when you configure third party tools). The bucket information contains the OSN site/pod location and the specific allocation on that pod.

		#datasets-thirdparty-cyberduck
			:markdown
				### [Cyberduck](#datasets-thirdparty-cyberduck)

				Cyberduck is a popular file transfer tool that supports the S3 API. The following describes how to configure Cyberduck to connect to an OSN resource.  Cyberduck is a "cloud storage browser" for Mac and Windows that supports multiple storage providers/protocols. The software may be downloaded at: <a href="https://cyberduck.io/download/">https://cyberduck.io/download/</a>

				Using Cyberduck with OSN is straightforward.

				1. Visit the osn portal and retrieve your allocation keys or retrieve them from the data manager for your project.
				1. Open Cyberduck and select the bookmarks icon (see image 1)
				1. Click the add icon at the bottom left of the screen to create the bookmark (image 1)
				1. Edit the new bookmark to point at the desired OSN pod using the allocation key pair retrieved in step #1
		
				<figure><img alt="Cyberduck" src="/documents/10308/2640738/image1.png/5252631f-98f0-4824-ba88-27c08f2e0136?t=1610740714601" style="border-width: 1px; border-style: solid; height: 119px; width: 300px;" /><figcaption>Figure. Cyberduck</figcaption></figure>

				<figure><img alt="Image 1: Selecting the bookmarks page and adding new bookmark" src="/documents/10308/2640738/image6.png/3ec39840-955a-40b3-848f-22108832e95b?t=1610740870000" style="border-width: 1px; border-style: solid; height: 393px; width: 600px;" /><figcaption>Image 1: Selecting the bookmarks page and adding new bookmark</figcaption></figure>


				When specifying the server, use the hostname portion of the location (i.e. if the location is <a href="https://mghp.osn.xsede.org">https://mghp.osn.xsede.org</a> the hostname is "`mghp.osn.xsede.org`").

				When specifying "Port", use 443 if the location starts with "`https://`"; use 80 if the location starts with "`http://`".

				<figure><img alt="Image 2: Adding OSN pod and user information to bookmark" src="/documents/10308/2640738/image10.png/00819be6-36ad-4ebc-a92b-07deacb201a4?t=1610740945895" style="width: 400px; height: 487px; border-width: 1px; border-style: solid;" /><figcaption> Image 2: Adding OSN pod and user information to bookmark </figcaption></figure>
		
			#datasets-thirdparty-cyberduck-anonymous
				:markdown
					#### [Anonymous Access Data Sets](#datasets-thirdparty-cyberduck-anonymous)

					Some datasets provide anonymous read access; if you are accessing buckets anonymously, type "anonymous" into the Access ID portion and Cyberduck will then select the grayed out anonymous access box in the window.

					<figure><img alt="Image 2.1: Using anonymous access as your user" src="/documents/10308/2640738/image7.png/18e9b4a8-168b-4e66-9565-8ff671636014?t=1610740889515" style="width: 400px; height: 550px; border-width: 1px; border-style: solid;" /><figcaption> Image 2.1: Using anonymous access as your user </figcaption></figure>

					Exit the window for the bookmark to save.

			#datasets-thirdparty-cyberduck-browsing
				:markdown
					#### [Browsing, Uploading and Downloading](#datasets-thirdparty-cyberduck-browsing)

					Once a bookmark is created, you can use it to access data by double-clicking the bookmark. This logs your user in and lists the contents of the dataset.

					Note: If your buckets have large object counts, you will need to increase the Timeout settings for connections.

					Go to Preference&gt;Connection and change the box next to Timeout for opening connections (seconds) and change the setting to 90 seconds.

					<figure><img alt="Image 4: Directory listing within bucket" src="/documents/10308/2640738/image4.png/ab336aa3-2f00-47ca-b291-bc1589884a73?t=1610740775704" style="width: 600px; height: 387px; border-width: 1px; border-style: solid;" /><figcaption> Image 4: Directory listing within bucket </figcaption></figure>

					Cyberduck client is a full-fledged transfer client so desktop up/downloads can be easily performed for data sets.


					<figure><img alt="" src="/documents/10308/2640738/image8.png/18e9b4a8-168b-4e66-9565-8ff671636014?t=1610740889515" style="width: 400px; height: 550px; border-width: 1px; border-style: solid;" /><figcaption> Image 7: Drag-and-drop file copy from desktop to OSN bucket </figcaption></figure>

					The tool supports multiple upload/download streams, chunking, pausing and restarting.

		#datasets-rclone
			:markdown
				### [Rclone](#datasets-rclone)

				Rclone is an open source command line utility that functions similarly to rsync and is able to communicate with numerous cloud-based storage providers. The application and documentation may be found at <a href="https://rclone.org">https://rclone.org</a>. Download and install the application per the instructions at the rclone website. 

			#datasets-rclone-config
				:markdown
					#### [Rclone Configuration](#datasets-rclone-config)

					The most straightforward way to configure Rclone for OSN is to edit the rclone configuration file. This file may be found by typing the command "`rclone config file`". The command will return the path to the rclone config file. Open this file with a text editor and add the following stanza to the end of the file:

					<pre class="syntax">
					[&lt;alias&gt;]
					type = s3
					provider = Ceph
					access_key_id = &lt;access key&gt;
					secret_access_key =&lt;secret key&gt;
					endpoint = &lt;location&gt; </pre>

					Where:

					* &lt;alias&gt;      &ndash; nickname of your choice for the allocation
					* &lt;access key&gt; &ndash; the access key from the data manager or from the portal
					* &lt;secret key&gt; &ndash; the secret key from the data manager of the portal
					* &lt;location&gt;   &ndash; the location information provided by the data manager or portal

					An example of a configuration stanza might look like:

					<pre class="job-script">
					[ocean-data]
					type = s3
					provider = Ceph
					access_key_id = ASasd8KJHDAKH**&amp;asd
					secret_access_key =asd(*&amp;Adskj*(*(&amp;868778
					endpoint = <a href="https://mghp.osn.xsede.org">https://mghp.osn.xsede.org</a></pre>

			#datasets-rclone-commands
				:markdown
					#### [Rclone commands](#datasets-rclone-commands)

					`Rclone` commands are of the form:

					<pre class="syntax">rclone command alias:/bucket</pre>

					So, using the example config file entry described above and assuming a bucket named "phytoplankton" one would list the content of the bucket using the following command:

					<pre class="cmd-line">rclone ls ocean-data:/phytoplankton</pre>
		
					You could copy a local file to the bucket with the command

					<pre class="cmd-line">rclone cp my-local-file.dat ocean-data:/phytoplankton</pre>

					Rclone offers a wide range of commands for performing typical unix file operations (ls, cp, rm, rsync, etc.) Details on these commands can be found <a href="https://rclone.org/docs/">here</a>.

	#datasets-thirdparty
		:markdown
			## [Third Party Data Management Applications](#datasets-thirdparty)

			OSN users may also choose to layer more sophisticated data management applications on top of the S3 API services that OSN provides. Two applications that have been used with OSN include Globus (using the Globus S3) connector and iRods. Both packages have detailed descriptions on how to connect the service with a S3 storage provider.
		
#landingpages
	:markdown
		# [Landing Pages](#landingpages)
		
		<font color="red"><b>Coming Soon!</b></font> The data set owner may also create a landing page that follows DOI landing page conventions, making it easy to visit from a browser or data catalog. The landing page contains metadata that describes the data set and links to preconfigured, downloadable tools for accessing the data set.
		
		OSN provides tools to create a Basic landing page that may be overridden by more sophisticated landing pages depending upon the needs of the End User community. The landing page has the following generic template.

		<figure><img alt="" src="/documents/10308/2640738/image12.png/6dee5c85-890c-4682-9451-cb5801c7347c?t=1610741004310" style="width: 400px; height: 236px; border-width: 1px; border-style: solid;" /><figcaption> Sample landing page </figcaption></figure>

		A completed template example is shown below.
		
		<figure><img alt="" src="/documents/10308/2640738/image2.png/61bca794-1b2b-4e50-bdc7-ea52032a0c5c?t=1610740735763" style="width: 400px; border-width: 1px; border-style: solid; height: 252px;" /><figcaption> </figcaption></figure>

#protected
	:markdown
		# [Open Access &amp; Protected Data Sets ](#protected)
		
		OSN datasets can be either open access or protected. In the former case, keys are only needed to write new objects to the dataset otherwise, read access can be accomplished anonymously (e.g. as shown earlier for the anonymous cyberduck configuration).
		
		**Protected datasets require keys for both reading and writing**. When an open access dataset is created only one pair of keys are created which are accessible to the data manager for a project and are used to upload data to the allocation. No keys are needed by users to download/read the data stored in the allocation.
		
		Protected datasets have two sets of keys. One set allows writing to the dataset and is identical in function to the on previously described for open access datasets. The second set of keys provides read access to the dataset. Anonymous access is not allowed on protected datasets.

		Keys are shared with users in two ways. Data managers can choose to share keys with other users "out of band" by simply sending other users keys that they are interested in via whatever secure mechanisms the project uses to store and communicate project-specific secrets (e.g. username/passwords, certificates, PKI material, access keys, etc.). Keys can also be managed using the OSN portal. 
		
		A project in the OSN portal may have multiple allocations associated with it. Data managers for a project have access to all keys for a project's allocations. A data manager may share all project keys with another portal user by:

		1. adding the portal user to the data manager's group
		1. Assigning that group member the data manager role
		
		Once another portal user has been added to a group and given the data manager role for that group, she will have access to all the keys (and have the same privileges in the portal) as the original data manager for the group.
		
		Data managers may also add users to a group without assigning the user the data manager role. When this happens the user will only have access to keys that the data manager has made "visible". Visible keys are available to all group members whereas non-visible keys are only available to group members in the data manager role.

		In the image below, <a href="mailto:culbertj@mit.edu">culbertj@mit.edu</a> and <a href="mailto:jtgoodhue@mghpcc.org">jtgoodhue@mghpcc.org</a> have access to all the keys in the project because they are both data managers for the project "JIMTEST", <a href="mailto:dsimmel@psc.edu">dsimmel@psc.edu</a> will only have access to the two keys shown with the "visible" checkbox checked. 

		<figure><img alt="" src="/documents/10308/2640738/image13.png/87828656-bcba-4ef4-a9e6-e5063ac35315?t=1610741035947" style="width: 500px; height: 370px; border-width: 1px; border-style: solid;" /> <figcaption>what is this caption</figcaption></figure>


#files
	:markdown
		# [Managing Files and Data](#files)
		
		There are four roles associated with an OSN allocation: 

		* Principal Investigator - Responsible for the allocation and serves as either the Data Manager or the Alternate Data Manager for the allocation. 

		* Data Manager: 
		
			* Adds/removes data curators and data managers
			* Adds/removes end users for protected data
			* Maintain Data Set Landing Page Information
			* Monitors capacity vs utilization and requests allocation changes when needed

			The OSN Portal is used by PIs/Data managers to manage their allocations. The Portal uses CiLogon for authentication, and provides bucket administration tools to the PI/Data Manager who requested the allocation. When requesting an allocation, the PI provides an identity that is recognized by CILogon. After the bucket is created, the PI can log in to the OSN Portal and administer access to the bucket. 

		* Data Curator - Maintains the data set

		* End User:
		
			* Has read access to all of the data in the bucket. Public-access buckets allow access to anyone who has the name of the pod and bucket. Authenticated access buckets allow access to anyone who has the READ key.
			* Registers via any identity service that is trusted by the data manager (InCommon, ORCID, Github, Google, Amazon, etc.
			* Logs in after receiving an invitation from a Data Manager or OSN Operations 

#transferring
	:markdown
		# [Transferring Data to the OSN](#transferring)

		OSN data sets are comprised of <a href="https://docs.ceph.com/en/latest/start/intro/">Ceph Objects</a> accessible from anywhere, via a <a href="https://en.wikipedia.org/wiki/Representational_state_transfer">RESTful</a> protocol that follows S3 conventions. All end user access is via S3 put and get requests, mediated by <a href="https://docs.ceph.com/docs/master/radosgw/bucketpolicy/">Bucket Policies</a>. 

		All put requests to OSN buckets must include an <a href="https://docs.ceph.com/docs/master/radosgw/s3/authentication/">Authorization String</a>. 
		There are two modes of get request:

		* Protected Dataset buckets -- All requests must include an Authorization String.
		* Open access buckets -- Get requests do not include an Authorization String, making the bucket accessible to anyone who has the name of the bucket and pod. The Ceph Object Gateway documentation refers to requests of this type as coming from an anonymous user. 

		Unlike many cloud object stores, OSN Ceph Object stores contain no information about individual users. Access to an allocation is mediated by the keys that are assigned to authorized end users. Some of the implications of this approach include: 
		
		* Object-level access control is not supported
		* There is no audit trail that identifies the originator of any request
		* Keys are unique to a bucket, and buckets are unique to a pod 
		* For example, if a data set has been replicated across two pods, each instance has a different set of keys and separately maintained access control
		* Since there is one READ key per bucket, the origin of a Get request may only be distinguished by source IP address

		
#policies
	:markdown
//		# [Policies](#policies)
		
//		Usage must comply with the [OSN Acceptable Use Policy](xx).

#help
	:markdown
		# [Help](#help)
		
		Users may always contact [XSEDE User Support](mailto:help@xsede.org) for help.


