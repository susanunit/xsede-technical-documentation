#horizon
	:markdown
		#[Horizon](#horizon)

	#horizon:overview
		:markdown
			##[Overview](#horizon:overview)

			Horizon is a web-based GUI for interacting with the Openstack API. It is likely the most complete GUI for working with Openstack but isn't the fastest or most user-friendly. Almost all functionality that is available from the CLI is available in Horizon. Exosphere and Cacao both provide subsets of Horizon functionality with more focus on ease of use.

			That said, there are times you may need the more complete features of Horizon instead of the other GUI interfaces. This documentation will cover the basics of launching, using, and managing instances, using various storage options in Horizon, using container orchestration engines, and other aspects of using Openstack.

	#horizon:login
		:markdown
			###[Logging into Horizon](#horizon:login)

			To login to Horizon and view the Dashboard, follow the steps in [Using the Horizon Dashboard to Generate openrc.sh](#cli:openrc).

			<img width="600" src="/documents/10308/2947537/CLI+openrc+fig+5/5c8ec583-f170-4dfc-ae48-676ebfb4bee5?t=1647908168000"/>

			If you are on multiple XSEDE allocations, you'll want to verify you're using the correct one and change to the correct one if you are not.

			You do that by clicking at the top left next to the Jetstream2 logo where it has "XSEDE * TG-XXXXXXXXX * IU". That will show allocations under "Projects".

			<img width="600" src="/documents/10308/2947537/CLI+openrc+fig+6/a18956b1-3ee7-463f-be96-9635d49bea2f?t=1647908144000"/>

	#horizon:getstarted
		:markdown
			###[Getting started with Horizon](#horizon:getstarted)

			* [Creating and managing security groups](#horizon:security)
			* [Creating networks](#horizon:networks)
			* [Launching an instance](#horizon:launch)
			* [Managing instances](#horizon:manageinst)
			* [Using volumes](#horizon:volumes)
			* [Using Manila shares](#horizon:manila)
			* [Storage](#horizon:storage)
			* [Troubleshooting in Horizon](#horizon:trouble)

	#horizon:security
		:markdown
			##[Security Group Management](#horizon:security)

			Security Group Management in Horizon

			Security groups can be thought of like firewalls. They ultimately control inbound and outbound traffic to your virtual machines. Under the CLI and Horizon, access defaults to all outbound allowed and __NO__ inbound allowed.

			To allow access to your VM for things like SSH, you will need to create a security group and add rules to it.

			<div class="portlet_msg_info">You can reuse a security group many times, so a best practice is to create groups by related services. For instance, you might create a basic group for ssh and icmp (which is what we will show as an example) and then a separate security group for http and https access if you're running a web service on your instance.</div>

	#horizon:security-groups
		:markdown
			##[Creating Security Groups and Rules](#horizon:security-groups)

			This will walk you through creating a basic security group in Horizon and adding a couple of simple access rules.

			Login to the <a target="_blank" href="https://js2.jetstream-cloud.org">Horizon dashboard</a> and make sure you've selected the correct allocation. Select the "Network" tab on the sidebar and click "Security Groups".
			<img width="600" src="/documents/10308/2947537/horizon+security+fig+1/836afdc3-3dac-40d7-884a-8ba93acb8d78?t=1647908673000"/>

			Once you're on the security group page, you'll need to click the "Create Security Group" button (noted with a red arrow on the screenshot)
			<img width="600" src="/documents/10308/2947537/horizon+security+fig+2/599d954e-b32f-4e3a-985d-86845875a5ff?t=1647908642000"/>

			In the popup box that comes up, you'll give your new security group a name (we suggest something like *my-username-ssh-and-icmp*) and optional description. We recommend giving a meaningful name and noting in the description what your intended purpose is.

			<img width="600" src="/documents/10308/2947537/horizon+security+fig+3/787aa3c9-c77a-4931-ba14-e6bb213ba447?t=1647908617000"/>

			When the creation is successful, it will bring you back to the security group page and note the success in the corner with a green status message. You'll see your new group name at the top where it says **Manage Security Group Rules: your-rule-name**.

			You'll then want to click "Add Rule" (noted with a red arrow on the screenshot)
			<img width="600" src="/documents/10308/2947537/horizon+security+fig+4/c1df4937-7262-4d6b-b7eb-ef82c854af1c?t=1647908585000"/>

			This will bring up a new dialog box where you can select the parameters for your security group rule.

			<img width="600" src="/documents/10308/2947537/horizon+security+fig+5/2e28ef4d-d077-4330-883e-4f34285a01fe?t=1647908547000"/>

			If you click the "Rule" dropdown at the top, you'll see a list of common rule types as well as the option for custom rules. For this example, we'll select "SSH" to allow inbound port 22/SSH access.
			<img width="600" src="/documents/10308/2947537/horizon+security+fig+6/3f7d64c3-8855-46ef-b734-51aa01d5f814?t=1647908509000"/>

			We'll fill in the other details needed. We do recommend putting in a description with what the rule does so it's easy to see at a glance. We'll also select CIDR as the remote type and then set *0.0.0.0/0* as the CIDR. This allows all traffic to the SSH port.

			You can make that be a single IP or a specific CIDR block.

			In general, limiting access to specific CIDR blocks or IPs is best.
			<img width="600" src="/documents/10308/2947537/horizon+security+fig+7/718b34d3-39c6-4d53-86ca-3d2309abb6ca?t=1647908478000"/>

			When the creation is successful, it will bring you back to the security group page and note the success in the corner with a green status message. You'll see your new rule now on the page.
			<img width="600" src="/documents/10308/2947537/horizon+security+fig+8/020f939d-31c0-4a46-992c-7fcc90f5f245?t=1647908454000"/>

			You'll need to click the "Create Security Group" button and second time and we'll create a second rule for "All ICMP". This will allow things like incoming ping to check the status of your virtual machine. You'll select *Ingress*, *CIDR*, and set the CIDR to *0.0.0.0/0* to allow all hosts to ping your virtual machine.
			<img width="600" src="/documents/10308/2947537/horizon+security+fig+9/d6d28508-94f9-4454-9fcd-38b05c72d27a?t=1647908430000"/>

			As before, when the creation is successful, it will bring you back to the security group page and note the success in the corner with a green status message. You'll see your new rule now on the page.
			<img width="600" src="/documents/10308/2947537/horizon+security+fig+10/5774175b-95da-4926-9391-4e365bdee723?t=1647908383000"/>

			You can then add additional rules or additional security groups. This will allow the most basic of access to your VMs.

			<div class="portlet_msg_info">__We do recommend limiting access as much as possible for best security practices.__</div>

	#horizon:networks
		:markdown
			##[Networks in Horizon](#horizon:networks)

	#horizon:launch
		:markdown
			##[Launching Instances](#horizon:launch)

	#horizon:manageinst
		:markdown
			##[Instance Management](#horizon:manageinst)

	#horizon:volumes
		:markdown
			##[Volumes Under Horizon](#horizon:volumes)

	#horizon:manila
		:markdown
			##[Manila Shares in Horizon](#horizon:manila)

	#horizon:manila
		:markdown
			##[Manila via Horizon](#horizon:manila-horizon)

			__To use Manila via Horizon__

	#horizon:manila-create
		:markdown
			###[Create the Share](#horizon:manila-create)

			1. Click on: Project -> Share -> Shares -> Create Share

				<img width="600" src="/documents/10308/2947537/manila+fig+1/f08209a2-3a43-4461-942d-01e15f07a374?t=1647907954000"/>

			1. Create a share with the following settings:

				* share name: a name of your choosing
				* share protocol: CephFS
				* size: the size of your manila share
				* share type: cephnfsnativetype

				<img width="600" src="/documents/10308/2947537/manila+fig+2/19cd0a80-51f7-43ac-8713-1109b492216f?t=1647907918000"/>

	#horizon:manila-edit
		:markdown
			###[Edit the Share Rule](#horizon:manila-edit)

			1. Once your share is available, you can select `Edit Share` and `Manage Rules` and `Add Rule`: 

				<img width="600" src="/documents/10308/2947537/manila+fig+3/902eee9e-6d7d-4836-9658-5aabf3826018?t=1647907880000"/>

				* access type: cephx
				* access level: read-write
				* access to: an arbitrary unique name

				In the example above the accessTo name is `manilashare`. The name assigned must be globally unique, if you use a name that is already in use you will see an error state.

				<img width="600" src="/documents/10308/2947537/manila+fig+4/58f4c198-be84-47ac-941f-911f4d2f5b7b?t=1647907850000"/>

			1. If you now go back to the share page (Project/Share/Shares) and click on the share you created, you should see your share's metadata.

				Important things to note here are:

				* Path: ips:ports followed by volume path (/volume/_no-group/...)
				* Access Key

				<img width="600" src="/documents/10308/2947537/manila+fig+5/8aac6d05-a355-4fcd-a0ed-1845b5311265?t=1647907814000"/>

	#horizon:manila-vmshare
		:markdown
			###[Using Manila Share on a VM](#horizon:manila-vmshare)

			This is the same whether you're using Horizon or the CLI. Please refer to [Configuring a VM to use Manila Shares](#storage:manila-share)

	#horizon:trouble
		:markdown
			##[Troubleshooting](#horizon:trouble)

			Coming Soon!

