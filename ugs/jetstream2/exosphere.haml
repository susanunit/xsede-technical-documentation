#exosphere
	:markdown
		#[Exosphere](#exosphere)

	#exosphere:overview
		:markdown
			##[Overview](#exosphere:overview)

			Exosphere strives to be the most user-friendly interface for research clouds. If you are new to Jetstream2 and unsure which interface to use, Exosphere is a great place to start. Whether you are exploring new software tools, running compute-intensive jobs, teaching a course/workshop, or building a science gateway, Exosphere can likely help you reach your goals. Use the <a target="_blank" href="https://jetstream2.exosphere.app">Exosphere interface for Jetstream2</a> in your web browser. 

			<img width="600" src="/documents/10308/2947537/exo-overview.png/fee3cad9-c090-4aee-bd6b-1bf5803fb70b?t=1649101835546"/>

			Right now, Exosphere supports creating and managing instances. Instances are virtual computers (a.k.a. servers) that run your code, containers, software, and services. When you create an instance, you get helpful features like:

			* A one-click web shell (terminal) in your browser
			* Optionally, a one-click desktop environment for running graphical software
			* A browser-based file upload/download tool
			* Resource usage graphs that show how hard your instance is working
			* Easy passphrase-based SSH access, if you want it

			You can also use volumes to store large data sets, and manage persistent IP addresses for servers and science gateways. More powerful features, like data science workbenches and workflow sharing, are in experimental status now.

			With Exosphere, there is no requirement to learn about advanced cloud technologies like virtual networks or SSH keypairs. If your use of Jetstream2 becomes more sophisticated, and you need to reach for more complex tools like the OpenStack CLI or APIs, Exosphere does not get in your way.

	#exosphere:compare-horizon
		:markdown
			###[How Exosphere compares with the Horizon dashboard, OpenStack CLI, and APIs](#exosphere:compare-horizon)

			Exosphere supports users who wish to mix their use of Exosphere with other OpenStack interfaces like Horizon dashboard, the OpenStack command-line interface, and the APIs. Generally (and with a few limitations), resources that you create in one interface will show up in other interfaces. They are merely different ways to manage the same infrastructure.

			The other OpenStack interfaces support more features of OpenStack that Exosphere doesn't (yet), like Heat for cluster orchestration and Swift for object storage. So, they may better support some advanced cloud use cases than Exosphere, but they are generally less accessible to newer users. The Horizon dashboard, the OpenStack CLI, and the APIs were all built for use by IT engineers, not by researchers and data scientists. For example, in any of these tools you must create a network, subnet, router, security group, and SSH keypair before you can create an instance and connect to it (using an SSH client program and your private SSH key). If your use cases grow sophisticated enough, you may need this lower-level control, but using the Horizon dashboard is sort of like driving a car with a manual transmission. Using the CLI feels somewhat like using Horizon, but now you're shifting gears by typing shell commands instead of clicking buttons. Using the OpenStack APIs directly is like building your own transmission for the car.

			Instances created via these other tools do not get a one-click shell, desktop, data upload/download tool, or any of the other interactions that Exosphere sets up for you. If you want these with the other OpenStack interfaces, you must set them up yourself with varying degrees of difficulty.

			###[How Exosphere compares with Atmosphere2](#exosphere:compare-atmos)

			This section will be populated once Atmosphere2 is ready enough to explore and compare.

	#exosphere:help
		:markdown
			###[Getting Help](#exosphere:help)

			In addition to the XSEDE ticketing system, there is an #exosphere-user-support channel in the Jetstream Slack workspace. To request access, please open a ticket from the XSEDE user portal. This support option includes no promise of immediate, real-time assistance, but the Exosphere core developers monitor it and help when they can. Sometimes it's easier to chat with them than wait for ticket notifications.

	#exosphere:contrib
		:markdown
			###[Contributing](#exosphere:contrib)

			If you'd like to help build Exosphere or request a new feature, visit the <a target="_blank" href="https://gitlab.com/exosphere/exosphere">project on GitLab</a>. The Exosphere maintainers strive to provide a welcoming experience for new contributors. At a broader level than Jetstream, the Exosphere project has a <a target="_blank" href="https://riot.im/app/#/room/#exosphere:matrix.org">chat room on Matrix/Element</a> which is used to coordinate development work, but community members are also welcome to join. Further, the Exosphere team discusses project progress and priorities on a weekly video call on Mondays at 16:00 UTC. You can <a target="_blank" href="https://meet.jit.si/exosphere">join the call online</a> or dial in at +1.512.647.1431, PIN: 3037 7824 88#. (The <a target="_blank" href="https://c-mart.sandcats.io/shared/wfRsWBVmJZ3maUn7HMFqNj_MR_Bzy1vob9CzWu1n7QI">agenda and notes</a> from previous meetings are available.)

	#exosphere:login
		:markdown
			##[Logging In](#exosphere:login)

	#exosphere:login-sso
		:markdown
			###[Using XSEDE Account (single sign-on)](#exosphere:login-sso)

			The default login method uses your XSEDE account credentials. Note that this may require multi-factor (Duo) authentication. (If you need help setting up or changing your multi-factor authentication method, please open an XSEDE ticket, as Jetstream2 staff cannot fix this for you directly.)

	#exosphere:login-regions
		:markdown
			###[Choosing Allocations and Regions](#exosphere:login-regions)

			After you log in with your XSEDE credentials, Exosphere will prompt you to select from the allocations that you are a member of. Any un-selected allocations will not be added the Exosphere interface, so select all that you may want to use. If you are granted access to a Jetstream2 regional cloud, you will be logged into those allocations in both the main (Indiana University) region and any other regions.

	#exosphere:login-cloud
		:markdown
			###[Logging into non-Jetstream Clouds in Exosphere](#exosphere:login-cloud)

			The Exosphere interface for Jetstream2 also allows you to manage resources on other OpenStack-based research clouds alongside Jetstream2. In order for this to work, these third-party clouds must expose their OpenStack APIs publicly, and you must have OpenStack credentials (or an OpenRC file) to provide. To add other clouds, choose "Add allocation", select "Other login methods", and pick the "OpenStack" login method.

			If you encounter difficulty adding non-Jetstream2 clouds to Exosphere, Jetstream2 staff will have limited ability to troubleshoot and help, so this capability is not guaranteed to work.

	#exosphere:createinst
		:markdown
			##[Creating an Instance](#exosphere:createinst)

	#exosphere:create
		:markdown
			###[Creating an Instance with Exosphere](#exosphere:create)

			Once you have logged in and selected an allocation, select "Create" and then "Instance".

			<img width="600" src="/documents/10308/2947537/exo-create-instance-fig-1/fbe0ccf1-a884-4ad7-9ed2-75ca562d4bab?t=1648751828000"/>

	#exosphere:choose
		:markdown
			###[Choosing an Instance Source](#exosphere:choose)

			Next, choose an instance source. If you are a new user, select your desired operating system "By Type".

			__Choose by Type__

			Select your preferred operating system, or if you don't have a preference, pick the newest Ubuntu version. Exosphere selects the latest official image for your chosen type automatically.

			<img width="600" src="/documents/10308/2947537/exo+create+instance+fig+2/b09ceb69-2ce5-418a-ac98-cae66e293297?t=1648751852000"/>

			__Choose by Image__

			Alternatively, if you want to specify a particular image to create an instance from, select the "By Image" tab. Here, you can browse the entire Jetstream2 image catalog.

			<img width="600" src="/documents/10308/2947537/exo+create+instance+fig+3/ccdf171f-9bf1-474f-a299-26e8f2c2011c?t=1648751886000"/>

	#exosphere:configure
		:markdown
			###[Configure Instance](#exosphere:configure)

			Next, you can select options your new instance. If you're unsure what to choose for any of these, you can leave it at the default. When you're done choosing options, click the "Create" button at the bottom.

			__Choose a Name__

			If you will use the instance for anything important, resist the urge to accept the randomly-generated name. Give it a descriptive name to distinguish it from other instances later.

			<img width="600" src="/documents/10308/2947537/exo+create+instance+fig+4/bb667237-a52d-46d7-a286-32ebf28ae74b?t=1648751915000"/>

			__Choose a Flavor__

			Exosphere selects the smallest flavor by default. This is good for exploring and development work, because it conserves your Jetstream2 allocation.

			Select a larger-size flavor if the smallest flavor proves too small for your explorations, or if you are ready to scale a workload up to many CPU cores / many GB of RAM. Note that larger flavors will consume your allocation (SUs) more quickly. Select a GPU flavor if you require a GPU (and your Jetstream2 allocation provides GPU access). You can always resize your instance to a different flavor later.

			<img width="600" src="/documents/10308/2947537/exo+create+instance+fig+5/07c3cb38-9bf7-4b6c-8d1e-c585ad204710?t=1648751937000"/>

			__Choose a Root Disk Size__

			If the default size for the selected flavor is too small for your needs, you can specify a larger custom disk size. This will create a volume-backed instance.

			<img width="600" src="/documents/10308/2947537/exo+create+instance+fig+6/b908126f-ff56-4714-943c-274bacd7fe0b?t=1648751966000"/>

			__Choose a Quantity__

			You can create multiple instances simultaneously, up to the maximum quantity your quota limits support.

			When you create multiple instances at a time, and "X of Y" will be appended to the name of each. Otherwise, they will all will receive the same configuration.

			<img width="600" src="/documents/10308/2947537/exo+create+instance+fig+7/653ef11b-1a19-48de-80ac-3b3c1d7cf3fe?t=1648751992000"/>

			__Decide Whether to Enable Web Desktop__

			Enable Web Desktop if you need to use graphical software on your instance, or if you prefer working in a graphical environment instead of a text-based shell (terminal). Note that the graphical desktop environment consumes slightly more resources (CPU and RAM) than a shell, so consider using at least the `m3.small` flavor for a more responsive experience.

			<img width="600" src="/documents/10308/2947537/exo+create+instance+fig+8/d785ec3d-a587-4e0d-850c-4cc276388a7a?t=1648752026000"/>

	#exosphere:advanced
		:markdown
			###[Advanced Options](#exosphere:advanced)

			Most people can skip these advanced options, and just click the "Create" button at the bottom. Advanced options are intended for power users and less-common use cases.

			__Install Operating System Updates__

			By default, new instances install operating system security and bug-fix updates when they first power on. This takes a couple of minutes. So, only skip these if you *really* want the fastest possible setup and you do *not* care about the security of your instance. This option does not disable <a target="_blank" href="https://packages.ubuntu.com/focal/unattended-upgrades">unattended upgrades</a>, which may still run in the background shortly after your instance powers on.

			<img width="600" src="/documents/10308/2947537/exo+create+instance+fig+9/8336e89e-1d69-4315-8206-b007a32581d7?t=1648752061000"/>

			__Deploy Guacamole for Easy Remote Access__

			By default, Exosphere instances provide easy remote access via a <a target="_blank" href="https://guacamole.apache.org">Guacamole</a>-based terminal (Web Shell), and optionally a graphical environment (Web Desktop). If you *don't* want these for some reason, and you are comfortable using native SSH to connect to your instance, then you can disable setup of the Guacamole components.

			<img width="600" src="/documents/10308/2947537/exo+create+instance+fig+10/9adb68b0-e49c-48b8-a7db-353c0dc4332c?t=1648752227000"/>

			__Choose a Network__

			By default, Exosphere will use the network topology that OpenStack automatically creates for your allocation (and ask OpenStack to create it if needed). If you want your instance connected to a different OpenStack network, you can choose that here. If you change this without knowing what you're doing, your instance may end up with broken network connectivity.

			<img width="600" src="/documents/10308/2947537/exo+create+instance+fig+11/d3f2af7c-4f24-4439-a33f-627359ea1851?t=1648752272000"/>

			__Assign a Public IP Address__

			By default, Exosphere assigns a public (floating) IP address to your instances. Right now, a public IP is required for the Guacamole-based interactions (Web Shell and Desktop) to work. It is also required for you to make a direct native SSH connection to your instance from your workstation (or from anywhere else outside Jetstream2).

			Still, you may be creating a cluster of instances, and you may not want all of the cluster nodes to have public IP addresses. In this and similar cases, you can disable the assignment of a public IP address here. If you disable the public IP address for an instance, the only way to connect to that instance will be via native SSH, from one of your other instances that does have a public IP address.

			<img width="600" src="/documents/10308/2947537/exo+create+instance+fig+12/930b2f51-23b8-4b4c-b37c-a36200905b92?t=1648752305000"/>

			__SSH Public Key__

			If you want to make a native SSH connection to your instance using <a target="_blank" href="https://kb.iu.edu/d/aews">public key-based authentication</a>, you can select your public SSH key here, and upload a new one if needed.

			Note that you do not need to use key-based authentication to connect to an instance via native SSH. Regardless of your choice here, Exosphere will also set a strong passphrase for the exouser user. You can view this passphrase (and use it to SSH in) after instance setup is complete.

			<img width="600" src="/documents/10308/2947537/exo+create+instance+fig+13/3b5f5d1e-fc06-499d-9a50-3d8ff0a307df?t=1648752329000"/>

			__Boot Script__

			Here you can see how the sausage is made! The term "boot script" is a slight over-simplification: this text area contains <a target="_blank" href="https://en.wikipedia.org/wiki/YAML">cloud-init</a> configuration represented in <a target="_blank" href="https://en.wikipedia.org/wiki/YAML">YAML</a> format, which Exosphere passes to the new instance when it is first powered on.

			It is generally best to leave this text area alone, create the instance, then log into it and make further configuration changes as needed (e.g. installing software and downloading data).

			Most of what you see here is important for Exosphere's interactions and other features to work with the new instance. So, if you make changes without knowing what you're doing, the instance setup may not complete. This will leave the instance in a partially working, partially broken state from Exosphere's perspective. In this case, Jetstream2 support staff would likely advise you to delete it and create another instance with this field left un-modified.

			Still, if you are not afraid of editing YAML, you can modify this configuration before clicking the "Create" button. Note that Exosphere templates out a few important variables which are enclosed in single curly braces (`{` and `}`) in this configuration data.

			<img width="600" src="/documents/10308/2947537/exo+create+instance+fig+14/72587cf6-b647-407a-9630-2e5188421140?t=1648752354000"/>

			Finally, click the "Create" button at the bottom, and Exosphere will set up your instance.

	#exosphere:manageinst
		:markdown
			##[Instance Management](#exosphere:manageinst)

			In Exosphere, to see available management actions for an instance and choose one if desired, click the "Actions" drop-down menu on the instance details page.

			<img width="600" src="/documents/10308/2947537/exo+manage+fig+1/ce7a8a81-5257-481d-8976-5ac5b611a354?t=1648752399000"/>

	#exosphere:storage
		:markdown
			##[Storage Under Exosphere](#exosphere:storage)

	#exosphere:volumes
		:markdown
			###[Volumes](#exosphere:volumes)

			In Exosphere, you can create volumes and attach them to instances. Exosphere will display the volume mount point in the user interfaces.

			Please note that attached volumes are mounted the first time they are accessed inside the instance (e.g. with a `cd` command).

	#exosphere:fileshares
		:markdown
			###[File Shares](#exosphere:fileshares)

			Support for shared filesystems (using OpenStack Manila) is coming soon.

	#exosphere:clusters
		:markdown
			##[Push-button Clusters](#exosphere:clusters)

	#exosphere:clusters-pushbutton
		:markdown
			###[Push-button Clusters using Exosphere](#exosphere:clusters-pushbutton)

			<div class="portlet-msg-info">This guide assumes that you are comfortable creating instances using Exosphere. If you are new to Exosphere, see [Exosphere: Overview](#exosphere:overview).</div>

			Exosphere makes it simple to create a virtual Slurm cluster, complete with automatic, elastic scaling of compute nodes. This helps you get started with parallel computing, and to scale up your cluster as your needs grow.

			To learn more about virtual clusters, as well as other ways to create them, see [Advanced Capabilities: Virtual Clusters on Jetstream2](#vclusters).

	#exosphere:clusters-enable
		:markdown
			###[Enable Experimental Features in Settings](#exosphere:clusters-enable)

			Open the Settings page (top menu). Ensure that the "Experimental Features" option is enabled.

	#exosphere:cluster-createhead
		:markdown
			###[Create a Cluster Head Node Instance](#exosphere:cluster-createhead)

			Select "Create" and then "Instance".

			<img width="600" src="/documents/10308/2947537/exo+push+button+fig+1/229491e4-1d5e-4b89-ad08-66cb24451f83?t=1648752431000"/>

			For instance source type select __Rocky Linux 8__.

			Choose a unique name (preferably a short name with no spaces or special characters) for your instance. Ensure there is no existing instance with the same name by looking at the Exosphere instance list page and pressing the "Clear filters" button.

			This will be the base name for all resources created for the cluster, including networks, SSH keys, compute nodes, etc.

			For flavor choose "m1.small".

			Expand "Advanced Options".

			Select an SSH key (highly recommended).

			You should see an option "Create your own Slurm cluster with this instance as the head node". Select "Yes" for this option.

			<img width="600" src="/documents/10308/2947537/exo+push+button+fig+2/b34af909-b616-4c65-a58e-5f61ca72bcb7?t=1648752450000"/>

			Click the "Create" button underneath the Boot Script text input area.

			Wait until the instance is ready; this could take up to half-an-hour.

	#exosphere:cluster-run
		:markdown
			###[Run a Job on the Cluster](#exosphere:cluster-run)

			SSH into the new instance as `exouser`. (Alternatively use the web shell.)

			Switch to the `rocky` user:

				sudo su - rocky

			Go to the cluster repository directory:

				cd ~/CRI_Jetstream_Cluster/

			Submit a test batch job:

				sbatch slurm_test.job

			View the live SLURM logs:

				sudo tail -f /var/log/slurm/slurm_elastic.log /var/log/slurm/slurmctld.log

			You should see messages indicating a new compute node being created as an OpenStack instance.

			Confirm the new compute node instance exists by refreshing Exosphere's instance list page. Its name should begin with the same name as your head node instance, and end with `-compute-0`.

			Once the job is finished confirm that the compute node is gone by refreshing Exosphere's instance list page.

			Check the output of the test batch job by finding a new file ending with .out in the directory where you ran sbatch and viewing its contents. The file should contain two copies of the hostname of the compute node. For example:

			<pre>
			ls -alt *.out
			cat nodes_1.out
			</pre>

			You should seem something like this:

			<pre>
			$ ls -alt *.out
			-rw-rw-r--. 1 exouser exouser 72 Jan 28 19:10 nodes_1.out
			$ cat nodes_1.out
			yourclustername-compute-0.novalocal
			yourclustername-compute-0.novalocal
			</pre>

			You now have your very own working Slurm cluster. Congratulations!

			###[Clean-up steps](#exosphere:cluster-cleanup)

			Once you're done with your cluster, and you want to get rid of the head node instance as well as all the OpenStack resources created for the cluster, run the following commands on the head node instance (in an SSH session or web shell):

			<pre>
			sudo su - rocky
			cd ~/CRI_Jetstream_Cluster
			./cluster_destroy_local.sh -d
			</pre>

			Your SSH or web shell session on the head node will be terminated, and you will be disconnected.

			Confirm that the head node instance is gone by refreshing Exosphere's instance list page.

	#exosphere:trouble
		:markdown
			##[Troubleshooting](#exosphere:trouble)

			*Coming soon!*

