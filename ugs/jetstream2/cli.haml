#cli
	:markdown
		#[Command Line Interface (CLI)](#cli)

	#cli:overview
		:markdown
			##[CLI Overview](#cli:overview)

			There are many options and tools for using the OpenStack API from the command line. Follow the instructions in the table below to set up a security policy and network, launch and manage a VM and then remove the entire structure.

			__*Note*__: There is also an API tutorial that the Jetstream team uses here: <a target="_blank" href="https://github.com/jlf599/JetstreamAPITutorial">Jetstream API Tutorial</a> - this tutorial goes into greater detail on some topics and may be of value to those learning the Openstack CLI.

			<div class="portlet-msg-info">Please note that the tutorial above presently reflects using the API on Jetstream1. It will be updated soon for Jetstream2.</div>

	#cli:notes
		:markdown
			###[Important Notes](#cli:notes)

			* It is possible to create entities with the same name; e.g. you could create two networks with the same name; however, they will have different __Universally Unique Indentifiers (UUIDs)__. When this occurs, you may get a cryptic error message that entity may not exist or that there are multiple entities with that name. In this case, you must address the entity by its UUID.
			* It is important to understand that everything is owned by the project. Other users in your project can see and manipulate entities that you have created. *Be careful in your naming and pay attention to the things you are manipulating.*

	#cli:start
		:markdown
			###[Getting Started](#cli:start)

			You should be running the latest version of the clients. We recommend using python-openstack >= 5.0 as it uses Python3 and removes the dependencies on the now deprecated Python2. See [Installing Openstack clients](#cli:openstack) for more information.

			The next thing you'll need to do before being able to do any CLI commands is have an appropriate *openrc* file.

			<span style="color: red">Please note that openrc files on Jetstream2 require *application credentials*. Please refer to [Openrc setup](#cli:openrc) for information on generating an application credential and openrc file.</span>

			__Source the openrc:__

				source openrc-file-name

			You can also make the output look nicer in your terminal with the `-fit-width` option:

				openstack image show Featured-Ubuntu20 --fit-width

			You can make that permanent by adding the following to your environment.

				export CLIFF_FIT_WIDTH=1

			You'll then need to create a security group and network before launching your first VMs. More information may be found here:

			* [Setting up a security group](#cli:openstack-cl-security)
			* [Create a network](#cli:openstack-cl-network)
			* [Create and launch a VM](#cli:openstack-cl-vmlaunch)
			* [Instance management](#cli:openstack-cl-manage)

	#cli:openstack-install
		:markdown
			##[Installing Openstack Clients](#cli:openstack-install)

			* [Mac-specific steps](#)
			* [Windows-specific steps](#)
			* [Common/Linux-specific steps](#)

	#cli:openstack-install-mac
		:markdown
			###[Mac-specific Steps](#cli:openstack-install-mac)

			This will help you get the Openstack clients working on Mac OS X 10.11.x and higher. It may work on recent older versions of Mac OS X but it has not been tested.

			*At this time, Python 3 is still not shipping on OS X. The latest Openstack clients require it.*

			__Follow the instructions below at your own risk.__

		%table(border="1" cellpadding="3" style="caption-side: bottom")
			%caption Mac-specific Steps
			%thead
				%tr
					%th Task
					%th Command
			%tbody
				%tr
					%td Install <a target="_blank" href="https://brew.sh">Homebrew</a> (this might take a few minutes):
					%td <code>/usr/bin/ruby -e "$(curl -fsSL<br />https://raw.githubusercontent.com/Homebrew/install/master/install)"</code>
				%tr
					%td Using brew we're going to install Python 3:
					%td <code>brew install python</code>
				%tr
					%td Now Python 3 is installed we can install the OpenStack command line tools:
					%td <code>sudo pip3 install python-openstackclient</code>

	#cli:openstack-install-windows
		:markdown
			###[Windows-specific Steps](#cli:openstack-install-windows)

			We recommend that Windows users install Windows Subsystem for Linux and install Ubuntu within it. Microsoft has a learning module for this. The page <a target="_blank" href="https://docs.microsoft.com/en-us/learn/modules/get-started-with-windows-subsystem-for-linux/2-enable-and-install">Enable Windows Subsystem for Linux and install a distribution</a> can walk you through that.

			Once installed, you can verify python3 is installed by doing:

				which python3

			If you get an error, you may need to install Python3 by doing:

				sudo apt install python3 python3-pip

			Then you should be able to proceed to the Linux/common steps below.

	#cli:openstack-install-linux
		:markdown
			###[Common Linux Steps](#cli:openstack-install-linux)

			<div class="portlet-msg-info">Note: *Python3 is required*. This should already be installed by your operating system. Openstack CLI clients MUST be installed with Python3's pip/pip3!</div>

			__Install the OpenStack clients:__

				pip install python-openstackclient

			__Additional clients that may also be useful depending on your custom needs are:__

				python-swiftclient python-heatclient python-magnumclient python-manilaclient

			__For current users, clients that you likely no longer need to install are:__

				python-keystoneclient python-novaclient python-neutronclient python-cinderclient python-glanceclient

			__Set up your OpenStack credentials (see [Setting up openrc.sh for details](#cli:openrc)):__

				source openrc.sh

			__Test an Open Stack command:__

				openstack flavor list

			__Following future OpenStack updates, all installed pip modules can be updated with this command:__

				pip3 list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 pip3 install -U

	#cli:openstack-install-options
		:markdown
			###[Optional Steps](#cli:openstack-install-options)

			Though not strictly necessary, we recommend using `virtualenv` to increase the stability of the openstack cli tools.

			__cd to your preferred directory; create a directory for the project:__

				mkdir <project_name>

			__Change to the project directory__

				cd <project_name>

			__Install the venv packages__

				sudo python3 -m pip install --user virtualenv

			__Start the VirtualEnvironment software__

				python3 -m venv env <project_name>

			__Activate the VirtualEnvironment for the project__

				source <project_name>/bin/activate

	#cli:openrc
		:markdown
			##[openrc.sh](#cli:openrc)

	#cli:openrc-setup
		:markdown
			###[Setting up openrc.sh](#cli:openrc-setup)

			Setting up application credentials and openrc.sh for the Jetstream2 CLI

			####New openrc format for Jetstream2 CLI!

			One of the key changes to using Jetstream2's command line interface (CLI) is that it uses XSEDE credentials for authentication. To do that, you have to create an application credential via the Horizon interface. This will require a different sort of openrc than Jetstream1. This page will walk you through that process.

			<div class="portlet-msg-info">Please make sure to source the new Jetstream2 openrc in a fresh terminal session. If you invoke it in a session that's had another openrc sourced, you'll get an error like this: Error authenticating with application credential: Application credentials cannot request a scope.</div>

	#cli:openrc-allocation
		:markdown
			###[Openrc Files are Allocation-specific](#cli:openrc-allocation)

			Each allocation you wish to use from the command line will need its own application credential and openrc file.

			__You CANNOT use the openrc generator like in Jetstream1__

			The openrc generator on the Horizon right side (username) menu will NOT work properly with Jetstream2! Please use the process below to get your application credential based openrc file.

	#cli:openrc-generate
		:markdown
			###[Using the Horizon Dashboard to Generate openrc.sh](#cli:openrc-generate)

			1. Navigate to <https://js2.jetstream-cloud.org>

				Make sure it says "XSEDE Globus Auth" in the Authenticate Using box.

				<img width="600" src="/documents/10308/2947537/CLI+openrc+fig+1/261c9244-cdda-463c-93b0-8c3866a0f9ae?t=1647908286000"/>

			1. The first time you log in you'll be directed to a Globus page to permit authorization.

				If you have linked institutional, Google, Orcid, or other credentials, you'll be able to use those to authenticate.

				We know XSEDE credentials work correctly so we will show that in our example.
				<img width="600" src="/documents/10308/2947537/CLI+openrc+fig+2/0cf5763e-8109-47d6-8538-0b600942e29a?t=1647908254000"/>

			1. The next page should be the login screen for your credentials. We're showing the XSEDE login screen as our example.

				<img width="600" src="/documents/10308/2947537/CLI+openrc+fig+3/c881539f-e045-4591-95a3-9ca1c68a82c0?t=1647908227000"/>

			1. If you're using two-factor auth with your credentials as XSEDE does, you'll likely get a Duo or Authenticator screen here.

				<img width="600" src="/documents/10308/2947537/CLI+openrc+fig+4/f71fcf18-e79a-4cfc-b6fd-f6c18688568a?t=1647908197000"/>

			1. You should be at the Horizon Dashboard home now.

				<img width="600" src="/documents/10308/2947537/CLI+openrc+fig+5/5c8ec583-f170-4dfc-ae48-676ebfb4bee5?t=1647908168000"/>

			1. As application credentials are unique to each allocation, if you are on multiple XSEDE allocations, you'll want to verify you're using the correct one and change to the correct one if you are not.

				You do that by clicking at the top left next to the Jetstream2 logo where it has "XSEDE * TG-XXXXXXXXX * IU". That will show allocations under "Projects".

				<img width="600" src="/documents/10308/2947537/CLI+openrc+fig+6/a18956b1-3ee7-463f-be96-9635d49bea2f?t=1647908144000"/>

			1. From here, you'll select Identity and then Application Credentials from the sidebar menu on the left

				Once on that page, you'll click "Create Application Credential" towards the top right (noted by the red arrow)

				<img width="600" src="/documents/10308/2947537/CLI+openrc+fig+7/172710e9-f59e-4791-adbd-89d057df29e8?t=1647908118000"/>

			1. This will bring up the application credential creation screen.

				The sidebar has descriptions if you need help.

				We recommend using a descriptive name and to put details in the description so you can easily see what it is for.

				The Secret is the password or passphrase. We recommend using a strong password here or multi-word passphrase. As the page notes, you will not be able to retrieve it later if you forget it or delete the openrc file you generate.

				Set the expiration date and time. If you do not set a date, it will default to TODAY as noted on the sidebar.

				We do not recommend setting the roles, access rules, or selecting unrestricted unless you are an advanced user and understand the full implications of altering these.

				<img width="600" src="/documents/10308/2947537/CLI+openrc+fig+8/45611c34-4694-4377-98f0-20958cf37cb7?t=1647908090000"/>

			1. When you hit "Create Application Credential" it will then generate the credential and bring up a confirmation box. Please make sure to save the credential ID and secret if you need them for things other than the openrc.

				To get the openrc for CLI access, please click the "Download openrc file" button referenced by the red arrow in the screenshot. That will download a plain text file for use with the Openstack CLI client

				We recommend giving your new openrc file a descriptive name (e.g. openrc-TG-TRA111111.sh, using the XSEDE project name or some other meaningful description.)

				<img width="600" src="/documents/10308/2947537/CLI+openrc+fig+9/0eb316a0-27a0-4c02-b1d9-7cd9a767db70?t=1647908043000"/>

