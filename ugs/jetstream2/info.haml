#info
	:markdown
		#[Jetstream2 Info](#info)

	#info:overview
		:markdown
			##[System Overview](#info:overview)

			Jetstream2 is a transformative update to the NSF's science and engineering cloud infrastructure, will provide 8 petaFLOPS of virtual supercomputing power to simplify data analysis, boost discovery, and increase availability of AI resources. It is an NSF-funded, user-friendly cloud environment designed to allow *always on* research infrastructure and to give researchers access to interactive computing and data analysis resources on demand, whenever and wherever they want to analyze their data. While it shares many common features and abilities with other research computing resources, it is not a traditional High Performance Computing (HPC) or High Throughput Computing (HTC) environment.

			It provides a library of __virtual machines__ and shared software designed to create research infrastructure or to perform discipline-specific scientific analysis. Software creators and researchers will be able to create their own customized virtual machines (VM) or their own private computing system within Jetstream2.

			Jetstream2 features multiple user interfaces, including the following __web-based user interfaces__:

			* [CACAO](#cacao:overview)
			* [Exosphere](https://docs.jetstream-cloud.org/ui/exo/exo)
			* [Horizon](https://docs.jetstream-cloud.org/ui/horizon/intro)

			as well as the [OpenStack Command Line Interface (CLI)](#cli:overview) and the [OpenStack Software Development Kit (SDK)](https://docs.openstack.org/openstacksdk/latest).

			The operational software environment is based on [OpenStack](https://www.openstack.org).

	#info:overview-access
		:markdown
			###[Access](#info:overview-access)

			Jetstream2 is accessible primarily through either the Exosphere or Cacao web interfaces using XSEDE credentials via [Globus Auth](https://www.globus.org/tags/globus-auth).

			Jetstream2 is __not__ accessible via XSEDE's [Single Sign-On Login Hub](https://portal.xsede.org/single-sign-on-hub).

			Newly created XSEDE accounts must be added to a Jetstream2-specific allocation by the PI or Resource Manager in order to access Jetstream2.

			Jetstream2 is meant primarily for:

			* __interactive__ research
			* small-scale, __on-demand__ processing
			* as a backend for __Science Gateways__ distributing jobs to other HPC or HTC resources
			* for general research infrastructure or research-related development.

			Jetstream2 may be used for __prototyping__, for creating tailored __workflows__ to either use at smaller scale with a handful of CPUs or to port to larger environments after doing your __proof of concept__ work at a smaller level.

	#info:overview-about
		:markdown
			###[About](#info:overview-about)

			Consisting of five computational systems, Jetstream2's primary system will be located at Indiana University, with four smaller regional systems deployed nationwide at partners Arizona State University, Cornell University, the University of Hawai'i, and the Texas Advanced Computing Center. The 8 petaFLOPS cloud computing system will use next-generation AMD EPYC processors and NVIDIA A100 GPUs, and 17.2 petabytes of storage.

			Within the Pervasive Technology Institute the project will be led by UITS Research Technologies with key contributions from the Cyberinfrastructure Integration Research Center and the Center for Applied Cybersecurity Research. Additional Jetstream2 partners include the University of Arizona, Johns Hopkins University, and the University Corporation for Atmospheric Research, with Dell Inc. as the primary supplier.

			For more information, please see:

			* <a target="_blank" rel="noopener" href="https://www.nsf.gov/awardsearch/showAward?AWD_ID=2005506">NSF Award 2005506 - Jetstream 2: Accelerating Science and Engineering On-Demand</a>
			* [Jetstream2 System Specifications](#info:config)

	#info:keydiffs
		:markdown
			##[Key Differences with Jetstream1](#info:keydiffs)

			Jetstream1 utilized two primary clouds, geographically dispersed and powered by <a target="_blank" href="https://www.openstack.org/">OpenStack</a>, to provide thousands of concurrent virtual machines to researchers, educators, and students from hundreds of institutions across the United States. Jetstream1 featured Atmosphere, a user-friendly graphical user environment, as the means of access for the majority of users. Jetstream1 also allowed API access for infrastructure-driven projects like science gateways.

			There will be quite a few similarities between Jetstream1 and Jetstream2. Jetstream2 will be powered by a more recent version of OpenStack and will still have a primary mission of providing virtual machine services to research and education faculty, staff, and students. Jetstream2, however, will build on what Jetstream1 delivered and provide a number of improvements and new services. This page will try to identify the major differences.

			The biggest difference is that Jetstream2 will consist of one primary cloud and multiple regional clouds. As you can see by the image below, the primary cloud will be hosted at Indiana University in Bloomington, IN with regional clouds at various institutions across the United States.

			<div class="portlet-msg-info">Another huge distinction is that instead of different domains: default (Atmosphere) and tacc (API) domains, there is one namespace for Jetstream2. You can change between the CLI, Horizon, Exosphere, and Cacao as you see fit to manage your resources.</div>
			<img height="292" width="600" src="/documents/10308/2947537/keydiff+fig+1/7d52ee42-e6e3-432c-a209-c6731f09b24e?t=1648752682000"/>

			Allocations will only be awarded on the primary cloud by default.

	#info:keydiffs-hardware
		:markdown
			###[Hardware](#info:keydiffs-hardware)

			Jetstream2 will also bring [multiple classes of research computing hardware](#info:config). Jetstream2 will still have hundreds of CPU-based compute nodes for general purpose virtual machines/computing. Jetstream2 will also feature a small number of large memory nodes with up to 1 terabyte of RAM. Jetstream2 will also make available 90 nodes of GPU-enabled nodes with four <a target="_blank" href="https://www.nvidia.com/en-us/data-center/a100">NVIDIA A100 GPUs. These will be subdivided using <a target="_blank" href="https://www.nvidia.com/en-us/data-center/virtual-solutions">NVIDIA virtual GPU (vGPU)</a> to allow Jetstream2 allocations to utilize from 1/8th of a GPU to an entire GPU in their instances to allow everything from educational use requiring a minimal amount of GPU processing power to a full GPU for research workloads.

	#info:keydiffs-interfaces
		:markdown
			###[Interfaces](#info:keydiffs-interfaces)

			Jetstream2 will also have multiple user interfaces. Atmosphere has evolved into a new tool called [Containerized Atmosphere for Continuous Analysis Orchestration (CACAO or simply Cacao)](#uis:cacao), which is built on the principles of Atmosphere (abstracting complicated functions such as firewalls and virtual networking). Jetstream2 will also provide API services utilizing both the OpenStack Horizon GUI and a robust command line interface (CLI). Because Jetstream2 will no longer have separate operating domains for Cacao and API operations, those utilizing Jetstream2 can switch between interfaces easily, seeing all virtual machines and other assets created in any interface. This single namespace also allows for third-party interfaces that can manage any OpenStack created resource to be used with Jetstream2. At the time of deployment, Jetstream2 will feature one such third-party interface called [Exosphere](#uis:exosphere).

	#info:keydiffs-containers
		:markdown
			###[Containers and Orchestration](#info:keydiffs-containers)

			Jetstream2 will bring support for containers to the forefront of services. It will also support managing and scaling container-based workloads via the cloud-native functionality of OpenStack Magnum as has been demonstrated with Jetstream1. Users will be able to deploy Docker Swarm, Apache Mesos, or Kubernetes container orchestration engines to manage and run their container-based research workloads. In addition, the features of Cacao will provide similar functionality to individuals who have no desire to access the OpenStack API directly. Both approaches will allow researchers and educators to scale their workloads dynamically according to their needs.

	#info:keydiffs-services
		:markdown
			###[Additional Services](#info:keydiffs-services)

			Services such as OpenStack Heat will be available for researchers and developers, as well. OpenStack Heat is a service that allows individuals to instantiate complex resources with dependencies via a declarative YAML-based language. Similar to Magnum, other OpenStack services such as Trove and Sahara also leverage Heat to provide relational and non-relational databases and to provision data-intensive application clusters. Further, tools such as HashiCorp's <a target="_blank" href="https://www.terraform.io">Terraform</a> programmable infrastructure, with the ability to deploy on Jetstream2, private clouds, and commercial clouds easily and consistently, will allow developers and researchers to have their environements where they need and when they need it. These capabilities build on one of the fundamental aspects of cloud computing that was demonstrated in abundance with Jetstream1: the ability of users to create, manage, and orchestrate use of tools autonomously, based on need, without involving sysadmins to install or enable new software.

	#info:keydiffs-clusters
		:markdown
			###[Virtual Clusters](#info:keydiffs-clusters)

			In addition to the ability for individuals to control their infrastructure programmatically, Jetstream2 will provide the capability to spin up <a target="_blank" href="https://xcri-docs.readthedocs.io/en/latest/toolkits/vc-installation">elastic HPC virtual clusters (VCs)</a> at the push of a button. These have been tested extensively on Jetstream1, with about thirty VCs running in production at different times. These Slurm-powered virtual clusters allow individuals to transition easily between cloud and HPC resources, acting as both a test-bed environment for custom software, and a highly-available production resource for projects with modest computational needs. The deployment process for these resources in Jetstream2 will be streamlined, allowing individuals to deploy an instance, acting as a head node, that is ready to accept jobs. Once jobs are submitted, worker instances will be automatically created and destroyed as needed. The Singularity/Apptainer container runtime environment will be built into these VCs, allowing individuals to use containerized scientific software without lengthy installation processes.

	#info:config
		:markdown
			##[Configuration and Specifications](#info:config)

	#info:config-cloud
		:markdown
			###[Primary Cloud Specifications](#info:config-cloud)

	#info:config-cloud-cpu
		:markdown
			###[Compute Nodes (384 nodes)](#info:config-cloud-cpu)

	%table(border="1" cellpadding="3" style="caption-side: bottom")
		%caption Compute Nodes (384 nodes)
		%thead
			%tr
				%th System Configuration
				%th Aggregate information
				%th Per Node (Compute Node)
		%tbody
			%tr
				%td Machine type
				%td Dell
				%td Dell PowerEdge C6525
			%tr
				%td Operating system
				%td Ubuntu
				%td Ubuntu
			%tr
				%td Processor cores
				%td 49,152
				%td 128
			%tr
				%td CPUs
				%td 768 (AMD Milan 7713)
				%td 2
			%tr
				%td RAM
				%td 192 TiB
				%td 512 GiB
			%tr
				%td Network
				%td 100 Gbps x 4 to Internet2
				%td 100 Gpbs to switch
			%tr
				%td Storage
				%td 14 PB Total Ceph Storage
				%td 240 GB SSD

	#info:config-cloud-lm
		:markdown
			###[Large Memory Nodes (32 nodes)](#info:config-cloud-lm)

	%table(border="1" cellpadding="3" style="caption-side: bottom")
		%caption Large Memory Nodes (32 nodes)
		%thead
			%tr
				%th System Configuration
				%th Aggregate information
				%th Per Node (Large Memory Node)
		%tbody
			%tr
				%td Machine type
				%td Dell
				%td Dell PowerEdge R7525
			%tr
				%td Operating system
				%td Ubuntu
				%td Ubuntu
			%tr
				%td Processor cores
				%td 4,096
				%td 128
			%tr
				%td CPUs
				%td 64 (AMD Milan 7713)
				%td 2
			%tr
				%td RAM
				%td 32 TiB
				%td 1024 GiB
			%tr
				%td Network
				%td 100 Gbps x 4 to Internet2
				%td 100 Gpbs to switch
			%tr
				%td Storage
				%td 14 PB Total Ceph Storage
				%td 480 GB SSD


	#info:config-cloud-gpu
		:markdown
			###[GPU Nodes (90 nodes)](#info:config-cloud-gpu)

	%table(border="1" cellpadding="3" style="caption-side: bottom")
		%caption GPU Nodes (90 nodes)
		%thead
			%tr
				%th System Configuration
				%th Aggregate information
				%th Per Node (GPU Node)
		%tbody
			%tr
				%td Machine type
				%td Dell
				%td Dell PowerEdge XE8545
			%tr
				%td Operating system
				%td Ubuntu
				%td Ubuntu
			%tr
				%td Processor cores
				%td 11,520
				%td 128
			%tr
				%td CPUs
				%td 180 (AMD Milan 7713)
				%td 2
			%tr
				%td RAM
				%td 45 TiB
				%td 1024 GiB
			%tr
				%td GPUs
				%td 360 (NVIDIA A100 SXM4 40GB)
				%td 4
			%tr
				%td Network
				%td 100 Gbps x 4 to Internet2
				%td 100 Gpbs to switch
			%tr
				%td Storage
				%td 14 PB Total Ceph Storage
				%td 960 GB SSD

	#info:network
		:markdown
			##[Network Configuration and Considerations](#info:network)

	#info:network-summary
		:markdown
			###[Summary of Network Configuration and Policies](#info:network-summary)

			####Hardware Configuration

			The Jetstream2 primary cloud configuation features:

			* 100 Gbps network connectivity from the compute hosts to the cloud's internal network infrastructure
			* 4x40 Gbps uplinks from the cloud infrastructure to the data center infrastructure (2x100 planned)
			* 100 Gbps connectivity from the site infrastructure to the Internet2 backbone
			* 100 Gbps connectivity to the XSEDE research network via virtualized link
			* Individual instances have full access to this infrastructure with no added speed limits.

			*It is important to note that connections coming from commercial/commodity internet will likely not be as fast as those coming from Internet2 sites.*

			####Persistent IPs:

			A key difference between Jetstream1 and Jetstream2 is that no special or additional access is required to get a persistent IP address. Some of the GUI interfaces like [Exosphere](#uis:exosphere) and [Cacao](#uis:cacao) release IPs by default when a VM is deleted. [Horizon](#uis:horizon) and the Jetstream2 [CLI](#cli:openstack) require you to explicitly release the IP address.

			We do ask that you release any unused IPs back to the public pool. There are a finite number of IPs available and allocations hoarding them may inadvertently cause issues for other Jetstream2 researchers.

			The Jetstream administration team reserves the right to release any IP addresses not associated with a VM as needed.

			####Network Security Policies:

			In general, Jetstream2 does not restrict inbound or outbound access to virtual machines. There are a handful of blocks at the instutional level that are outside of the control of the Jetstream2 staff. In general, though, the most common Unix service ports (eg. 22/ssh, 80/http, 443/https, etc) are not restricted in any way. Whether they are open by default will be dependent on which user interface you're launching your VM with.

			Please refer to the [Security FAQ](#faq:security) for additional information.


	#info:network-detail
		:markdown
			###[In-depth Exploration of Jetstream2 Networking](#info:network-detail)

			This section describes the network architecture of Jetstream2's primary cloud at Indiana University. Regional sites may be configured differently.

			There are three kinds of networks used on Jetstream2.

			* The __Cluster Network__, which carries all of the tenant/workload/data traffic. It underlays the Neutron overlay networks for cloud workloads. It also carries all storage traffic and OpenStack control plane traffic.
			* The __Neutron overlay networks__, an entirely software-defined (virtual) networking stack which runs on top of the cluster network.
			* The __Management Network__, which is fully physically separate from the cluster network. It is used for managing hosts and their <a target="_blank" href="https://en.wikipedia.org/wiki/Dell_DRAC">iDRACs</a>.

			This document primarily looks at the Cluster Network and may delve into the Neutron overlay networks. The Management Network is not in the scope of user-facing documentation.

	#info:network-cluster
		:markdown
			###[Cluster Network](#info:network-cluster)

			<a target="_blank" href="https://docs.jetstream-cloud.org/attachments/bgp-in-the-data-center.pdf">BGP in the Data Center</a> is recommended reading. It provides background and orientation to Clos networks and BGP the way that it is used here. It is a short, accessible read for someone with general IT networking knowledge. For a quicker version, read chapters 1, 2, 4, and 6. Skim chapters 3 and 5.

			The Jetstream2 network is a two-tier spine-and-leaf network in a <a target="_blank" href="https://en.wikipedia.org/wiki/Fat_tree">fat tree</a> or <a target="_blank" href="https://en.wikipedia.org/wiki/Clos_network">Clos</a> topology.

			<img width="600" src="/documents/10308/2947537/network+fig+1/0573f0b4-d19f-4a5c-a5af-36aea08da6b9?t=1648752510000"/>

			There are no inter-switch links (ISLs). Traffic that transits the network will generally traverse a leaf switch, a spine switch, and another leaf switch.

			Two of the leaf switches connect the Cluster network to the campus WAN and internet. These happen to be the two leaf switches that the control plane nodes are also connected to.

			####Physical Switches

			Switches use the <a target="_blank" href="https://www.nvidia.com/en-us/networking/ethernet-switching/cumulus-linux">Cumulus Linux</a> operating system.

		%table(border="1" cellpadding="3" style="caption-side: bottom")
			%caption Switch Types in the Cluster Network
			%thead
				%tr
					%th Model
					%th Topology Layer
					%th Switch Quantity
					%th Port Speed
					%th Port Qty per Switch
			%tbody
				%tr
					%td Mellanox SN4600
					%td Spine
					%td 6
					%td 100 GbE
					%td 64
				%tr
					%td Mellanox SN2700
					%td Leaf
					%td 37
					%td 100 GbE
					%td 32

		:markdown
			####Dual-attached Servers

			Each host (server) is dual-attached to the Cluster network via two separate leaf switches, using two 100 GbE interfaces. Dual-attaching serves both purposes of redundancy and load-balancing (via equal-cost multipathing).

			Redundancy is particularly important here, because many workloads that run on Jetstream2 will not be 'cloud-native' or tolerant of partial-cluster network connectivity failure. Many users will hand-configure their analyses and services on a single instance (virtual machine) that runs on a single host in a single cabinet. Dual-attaching every physical server means that a single network switch failure will not cause a complete or prolonged outage for these non-cloud-native workloads. The physical compute node will quickly notice that one of its network links is offline, and re-route all network traffic to use the remaining functional link.

			####Server cross-wiring

			Throughout the cluster, a given host may be variously connected to:

			* 2 switches in the same cabinet, or
			* 2 switches in an adjacent cabinet, or
			* 1 switch in the same cabinet and 1 in an adjacent cabinet.

			The benefit of this cross-wiring between cabinets is increased utilization of switch ports, and reduction of the number of switches needed. This is especially true in Jetstream2 because different types of hosts (e.g. compute, GPU, storage) are different physical sizes, i.e. different densities in a cabinet. The cost of cross-wiring is limited cabinet mobility, because hosts in one cabinet may have many connections to a switch in the next cabinet over, but cabinets are not expected to be moved over the lifetime of Jetstream2.

			####Border Gateway Protocol

			Jetstream2 uses Border Gateway Protocol (BGP) to route and deliver packets across the entire cluster network. BGP was originally created to handle traffic between large network service providers (a.k.a. carriers), but is also adapted for use within data centers. In this application, BGP replaces use of MLAG to route traffic over redundant physical paths between switches. Jetstream1 used MLAG, but Jetstream2 does not, for the following reasons:

			* MLAG is fragile when making config changes
			* "Rigorous and conservative interface state management needed. Temporary loops or duplicates not acceptable" (source)
			* MLAG does not maximize use of aggregate throughout supported by redundant physical links.
			* MLAG requires inter-switch links (ISLs), which would require many (about 96) additional links between switches, which would require more cabling and purchasing switches with more physical ports.

			Use of BGP solves all of these drawbacks with MLAG. (See also <a target="_blank" href="https://web.archive.org/web/20210924222054/https://people.netfilter.org/pablo/netdev0.1/slides/MLAG-on-Linux-Lessons-Learned.pdf">MLAG on Linux: Lessons Learned</a>. For a comparison of BGP and other internal routing protocols like Open Shortest Path First (OSPF), see pages 22 and 24 of BGP in the Data Center (labeled as pages 14 and 16)).

			Some more configuration details:

			* On the Cluster network, peering between all nodes uses eBGP (external BGP), because each host and each leaf (but not spine) switch has its own individually-assigned private autonomous system number (ASN). iBGP (internal BGP) is not used.
			* The spine switches all share a common ASN, 64512. This avoids path hunting problems as described in pages 26-29 of BGP in the Data Center (labeled as pages 18-21).
			* Equal-cost multipathing allows packet forwarding to a single destination to be load-balanced across multiple network paths.

			####BGP Unnumbered

			In the cluster network, there is no network bridging or explicitly-defined shared subnets between connected switches (or between switches and hosts). There is only routing of packets between hosts. BGP requires underlying TCP/IP connectivity in order to create a connection, so how can that happen if we have no explicitly-defined subnets or bridges?

			In very short, each physical 100 GbE interface on each node (host or switch) has only a <a target="_blank" href="https://en.wikipedia.org/wiki/Link-local_address#IPv6">link-local IPv6 address</a> assigned. IPv6 Router Advertisement (RA) is a link-level protocol that periodically announces an interface's IPv6 addresses, including the link-local address. This is how each node identifies the IP of its peer. RFC 5549 allows a system to advertise IPv4 routes, and route IPv4 packets, over a pure IPv6 network. It also adds an "extended nexthop" capability to BGP, which negotiates use of RFC 5549 over a BGP peering session.

			This strategy is described in greater detail in chapter 4 of BGP in the Data Center. The result is that there is no need to define a shared subnet between any nodes. There is also no need to explicitly define neighbor-specific information in a node's configuration. Two peers connected with a network cable will discover each others' link-local addresses, exchange IPv6 route advertisements, and initiate a BGP route exchange.

			<img width="600" src="/documents/10308/2947537/network+fig+2/2b747afe-e4de-4f94-ae9a-5e33bc7a0049?t=1648752533000"/>

			####Routing on the Host

			In traditional enterprise networking, packet routing (at layer 3 in the OSI model) is generally performed *off the host*, i.e., by router hardware that is physically separate from servers running the workloads. Sometimes there is purpose-dedicated router hardware, and sometimes routing is a role performed by the ethernet switches. In either case, servers do not make most packet routing decisions. Servers are *bridged* to routers at layer 2, on a common subnetwork. Servers determine where to send packets using ARP (Address Resolution Protocol). __This is not how the Jetstream2 Cluster network operates__.

			Instead, the BGP routing fabric is extended all the way to each server. Each host has its own BGP autonomous system number (ASN), and runs the same FRRouting software as the switches. The host's primary IPv4 address (in private, RFC 1918 space) will appear on the loopback interface, and it is configured as a /32 in CIDR notation as its very own single-IP subnetwork, not bridged anywhere else!

			A server advertises BGP routes for its own internal IPv4 address. These routes are advertised to both switches that the server is attached to, via IPv6 link-local connectivity. This overall strategy is called "Routing on the Host", and it is described in chapter 6 of BGP in the Data Center.

			Again, in the cluster network there is no bridging between physical nodes, only routing.

			Some more background reading (if you don't like the book): - Linux Routing On The Host With FRR (archived version) - Independence from L2 Data Centers - Border Gateway Protocol (archived version)

			####Conclusion

			All of this BGP unnumbered architecture supports the 'Abstraction layer' of one or more IPv4 addresses on each host, and the ability to route packets from any host to any other host by its IPv4 address. At this level of abstraction and above, there is no concern with (or visibility of) the BGP routing layer or link-local IPv6 addresses. The Neutron overlay networks do not see it, or know about its complexity.

	#info:network-neutron
		:markdown
			###[Neutron Overlay Network](#info:network-neutron)

			Areas of future discussion:

			####Neutron Overlay Networks

			TODO

			####Layer 2 (Data Link)

			VXLAN / Open vSwitch / Linux Bridge?

			####Layer 3 (Network)

			Neutron Distributed Virtual Routing (DVR)

			Typically with DVR, egress traffic leaves a compute node directly to the WAN, while ingress traffic passes through a network node.

			See also: - https://docs.openstack.org/neutron/latest/admin/config-service-subnets.html
