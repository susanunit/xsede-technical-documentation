<span style="font-size:225%; font-weight:bold;">Open Science Grid User Guide</span><br>
<em>Last update: December 10, 2021</em>

#overview
	:markdown
		#[System Overview](#overview)
	#overview
		:markdown
			##[The OSG's Open Science Pool](#overview:ospool)

			The OSG provides technologies and services for supporting pools of distributed high-throughput computing (dHTC) and data capacity that serve a number of research communities, worldwide. Among these services, the OSG operates the Open Science Pool (OSPool), which includes capacity contributed from clusters at campuses and national labs across and beyond the US, organized as a single virtual cluster. 

			_The OSPool delivered more than 280 million compute hours in 2021_, with individual users achieving more than 30 million hours without fee or allocation requirement (see OSG's <a target="_blank" href="https://gracc.opensciencegrid.org/d/000000077/open-science-pool-all-usage?orgId=1">Grid Accounting system</a>).

	#overview:accelerations
		:markdown
			##[Computations Accelerated by the OSPool](#overview:accelerations)

			Given the distributed and opportunistic (excess backfill) nature of this capacity, the OSPool is a tremendous resource for work that can be run with a _high-throughput computing (HTC) approach_, in the form of numerous independent and self-contained tasks, each with input and output data. On the OSPool, _a single user with single-core jobs can achieve up to 1000s of concurrent cores in use within hours of submitting_, and more than _100,000 hours of usage per day_. The OSG uses the HTCondor compute scheduling software, which includes first-class support for submitting and managing large numbers of jobs, via a single submit file or multi-step workflow. Read more about applicable computations and throughput in the <a target="_blank" href="https://support.opensciencegrid.org/support/solutions/articles/5000632058-is-the-open-science-pool-for-you-">OSG User Documentation</a>.

			Features of computational work that scales well across the OSPool:

			* _individual jobs that complete (or checkpoint) within 20 hours on one or few cores_; jobs requiring large numbers of cores and/or inter-node communication do not run (or run well) on the OSPool, though manager-worker applications may be supported
			* _software that can be distributed across each job in the form of static binaries, self-contained installations (e.g. directories, tarballs), or containers_ (OSG staff are happy to help you pursue the ‘right’ approach); most commercially-licensed software is not suitable, per license limitations
			* _'open' data and software_; the OSPool is not suitable for proprietary or secure data requirements, given that jobs run on a variety of clusters, each with their own administrative access policies
			* _less than 20GB of input and output, *per-job*_; multiple TBs of total data can be supported across an entire batch of jobs, and as long as each job’s input and output can be handled separately from other jobs (no shared filesystem required when each job executes)

			A wide range of computational research tasks can be executed with an HTC approach, often with modest changes to a user's code and/or by splitting datasets. Examples include:

			* image analysis (including MRI, GIS, etc.)
			* text-based analysis, including any/all DNA read mapping, quality control/trimming, and other bioinformatics tasks
			* high-throughput drug or materials screening, including simulations and energy calculations
			* parameter sweeps, Monte Carlo methods, model optimization, and ensembles of simulations
			* machine learning, especially with hyperparameter treatments or multi-model training
			* these and other GPU-dependent computations that can be run with an HTC approach

			Get in touch with an OSG Research Computing Facilitator to discuss what’s possible for your own work!: <support@opensciencegrid.org>

	#overview:avail
		:markdown
			##[Who Can Use the OSPool](#overview:avail)

			Access to OSPool capabilities is available for free and without an allocation via the <a target="_blank" href="https://www.osgconnect.net">OSG Connect Service</a> for individuals and projects associated with a US-based academic, government, or non-profit organization. Additionally, *OSG* allocations for OSPool computing resources are available, including for those who may not meet the requirements for OSG Connect but who *do* meet XSEDE requirements. Users and groups with allocations also receive higher fair-share priority, which may be another reason for pursuing an allocation via XSEDE.

			Interested users should also consult the <a target="_blank" href="https://support.opensciencegrid.org/support/solutions/articles/12000074852-policies-for-using-osg-connect-and-the-ospool">OSG Connect and OSPool Policies</a>.

	#overview:workloads
		:markdown
			##[Integrate Workloads and Allocations across Resources](overview:workloads)

			One advantage of using an OSG access point (i.e. *login node* or *submit server*, including the OSG Connect access points) is that users can run work not only on the OSPool resources (best suited to work described above), but also on other XSEDE-allocated resources, commercial cloud (credits or paid), and other computing systems. This may be particularly useful for projects with computational requirements outside of those suitable for the OSPool, or that consist of a workflow of tasks that need to run across differently-suited computing systems, all through a single interface and submission point. Contact the OSG via <support@opensciencegrid.org> if you'd like to discuss options.

#help
	:markdown
		#[Get Help](#help)

		Any user of the OSPool and/or OSG Connect services can contact the OSG Research Facilitation team by emailing <support@opensciencegrid.org>.

#access
	:markdown
		#[System Access](#access)

		As of December 2021, users with XSEDE allocations (startup or regular) will be requested to 'Sign Up' for an account via the <a target="_blank" href="https://www.osgconnect.net">OSG Connect service</a>, after which an OSG Research Computing Facilitator will meet with them in order to provide personalized guidance and set up appropriate accounts. Given differences between the OSPool and other XSEDE-participating resources, login via the XSEDE single sign-on portal is not functional.

#features
	:markdown
		#[System Features](#features)

		The Open Science Pool includes capacity contributed by dozens of campus, national labs, and non-profit organizations, often opportunistically via backfill of a cluster local to the site. These resources are organized into a single pool (*virtual cluster*) that is dynamically sized based upon demand (the number of jobs in the queue across configured access points) and the supply of resources currently available at contributing sites.		

	#features:config
		:markdown
			##[System Configuration](#features:config)

			On average, there are more than 40,000 total cores available across the pool, with peaks of more than 70,000 cores, and with hundreds of <a target="_blank" href="https://support.opensciencegrid.org/support/solutions/articles/5000653025-gpu-jobs">GPUs available</a>. Nodes from each contributing cluster may differ in CPU and/or GPU models, number of cores, RAM, etc. However, each job requests the computing resources it needs (usually one or few cores, memory, disk, GPU features, etc.), such that multiple jobs run on each contributed node, and even sub-node units of computing are contributed. Compute *slots* are created dynamically (multiple per node) to server job requirements that fit within available resources on each node, and are the unit of computing for the purpose of matching jobs.

			There are a number of access points into the OSPool, including access points operated as part of the OSG Connect services, where users can log in, stage data, and submit jobs. These have sufficient CPU, memory, and storage resources to support job submission and data transfer. Users are expected to refrain from running their software on the OSG Connect access points and should consult with OSG Research Computing Facilitators about relevant HTCondor features in lieu of running user scripts to automate job submission/resubmission or ordered workflows, as these are likely to cause performance issues on the access point.

			One important difference between the OSPool and most other XSEDE resources is that there is no shared file system across access points and execute servers. This means that each job needs to bring along necessary executables, other software, and input data, which are supported features of the HTCondor scheduling software; users specify data and software to-be-transferred as part of the submit file. See [Job Submission](#using:jobsubmit), [Data Management](#using:datamgt), and [Software Solutions](#using:software) within the below section on [Using the OSPool](#using).

	#features:sharing
		:markdown
			##[Fair-Share and Computational Throughput](#features:sharing)

			While the HTCondor scheduler includes tools for viewing bulk numbers of nodes and cores in the pool, as well as per-node data about memory, disk, and CPU/GPU features, the dynamic scaling of the pool means that resources are always changing and always near-fully occupied, by design. Because most jobs use only a single core, there are slots of computing capacity coming available constantly (as other jobs finish), meaning that queue wait times for a first job to start are generally within seconds or minutes (not hours).

			Jobs are matched and executed based upon the user's fair-share priority and per-job resource requirements. A user's priority will be highest when they haven’t executed any work recently, and will decrease as they accumulate usage relative to other users, and with a decay factor such that usage from more than a week ago counts near-zero toward the users at-the-moment priority calculation. In general, though, jobs requiring fewer and/or less specialized (less scarce) resources are able to match to more resources, to achieve a larger number of concurrently-running cores, and to reach that peak sooner, even with a low fair-share priority. This also means that a user with batches of jobs differing in resource requirements will see different throughput between the batches, such that batches with different resource requirements will run concurrently, rather than following a first-in-first-out scheduling.

			Users running work as part of an XSEDE allocation will experience a higher priority factor than other users, until their allocation runs out. Jobs submitted without identifying an XSEDE allocation will be scheduled with a standard fair-share priority factor, but may be scheduled ahead of or alongside jobs associated with an allocation, depending on resource requirements. While rough estimates of computational throughput (as number of concurrent cores in use) are provided in the <a target="_blank" href="https://support.opensciencegrid.org/support/solutions/articles/5000632058-is-the-open-science-pool-for-you-">OSG User Documentation</a>, throughput metrics will be best informed by running scale tests, as is the case for performance-based testing on other XSEDE resources.

	#features:performance
		:markdown
			##[Performance and Scaling](#features:performance)

			For the above reasons - and with HTC computing in general - the performance of individual jobs will vary significantly across jobs running in the OSPool and matters less for scaling, while the number of concurrently-running jobs matters much more for overall completion, i.e. *computational throughput*. Especially because multi-core and otherwise large jobs will match to far fewer total resources, optimal throughput in the OSPool is achieved for single-core jobs, or with jobs running with as few cores as possible (which also generally corresponds to the best per-core performance). Therefore, traditional code performance and scaling calculations are less relevant to allocation resource justifications than the overall computational throughput, which is demonstrable via the concurrent cores achievable across jobs, jobs completed per day, and total core hours required based upon the number cores requested per-job, based upon the scale achieve with test batches.

#using
	:markdown
		#[Using the OSPool](#using)

	#using:jobsubmit
		:markdown
			##[Job Submission](#using:jobsubmit)

			<a target="_blank" href="https://support.opensciencegrid.org/support/home">Details about logging in and submitting jobs are available via the OSG User Documentation</a>, including a <a target="_blank" href="https://support.opensciencegrid.org/support/solutions/articles/12000081596-roadmap-to-htc-workload-submission-via-osg-connect">Roadmap for building and scaling an HTC workload</a>.

			Users submit jobs to the OSPool using the HTCondor jobs scheduler, which has first-class features for submitting highly numerous jobs via a single submit file, and with customizable variables to specify filenames, arguments, data transfer, and/or other features that differ across a set of jobs. Guides in the OSG User Documentation include detailed information about the most helpful HTCondor features and integration of HTCondor learning resources, which OSG's Research Computing Facilitators contribute to, directly.

	#using:datamgt
		:markdown
			##[Data Management](#using:datamgt)

			<a target="_blank" href="https://support.opensciencegrid.org/support/solutions/articles/12000002985">Policies and how-tos regarding data storage and per-job data transfer are included in the OSG User Documentation</a>.

			Data for jobs can be staged via the user's OSPool-associated access point (likely an OSG Connect login node) for transfer to/from jobs performed by the HTCondor scheduler, or can be transferred to/from each job from via web-accessible storage services (e.g. HTTP, S3) using HTCondor’s built-in file transfer features. The OSPool's distributed resources take advantage of OSG's Open Science Data Federation, which automatedly and regionally caches data, where possible, for enhanced transfer performance. 

			While the data locations accessible via OSG Connect access points are not backed up and should be treated as temporary (*scratch*) space only used for actively-running jobs, individual users may use up to TBs of storage space, with justified quota. 

	#using:software
		:markdown
			##[Software Solutions](#using:software)

			Given the distributed and heterogeneous nature of the resources in the OSPool, <a target="_blank" href="https://support.opensciencegrid.org/support/solutions/articles/5000634395-using-software-on-the-open-science-pool">software can be distributed to jobs with one of just a few options described in the OSG user documentation</a>.

These include distribution of software in the form of static binaries or self-contained installation directories (delivered to jobs in the form of tarballs), according to a software’s documented installation procedures. System-level support includes the execution of jobs within automatedly-deployed containers, which are used both to provide the user with a consistent OS environment for non-containerized software. For software with complex dependencies, the same container deployment can be triggered to run each job within a user-specified container available via a community repository (e.g. DockerHub) or staged via the user's access point (e.g. Singularity .sif files).

OSG's Research Computing Facilitators are happy to help you identify and implement the most appropriate and scalable solution for your software.

