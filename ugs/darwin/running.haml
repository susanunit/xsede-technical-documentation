#running
	:markdown
		# [Running Applications](#running)

	#running-intro
		:markdown
			The Slurm workload manager (job scheduling system) is used to manage and control the resources available to computational tasks.  The job scheduler considers each job&#039;s resource requests (memory, disk space, processor cores) and executes it as those resources become available.  As a cluster workload manager, Slurm has three key functions: (1) It allocates exclusive and/or non-exclusive access to resources (compute nodes) to users for some duration of time so they can perform work. (2) It provides a framework for starting, executing, and monitoring work (normally a parallel job) on the set of allocated nodes. (3) It arbitrates contention for resources by managing a queue of pending work.
	
			Without a job scheduler, a cluster user would need to manually search for the resources required by his or her job, perhaps by randomly logging-in to nodes and checking for other users&#039; programs already executing thereon.  The user would have to “sign-out” the nodes he or she wishes to use in order to notify the other cluster users of resource availability<sup><a href="#fn__1" id="fnt__1">1)</a></sup>.  A computer will perform this kind of chore more quickly and efficiently than a human can, and with far greater sophistication.
	
			Slurm is an open source, fault-tolerant, and highly scalable cluster management and job scheduling system for large and small Linux clusters.  Documentation for the current version of Slurm provided by SchedMD <a href="https://slurm.schedmd.com/documentation.html" title="https://slurm.schedmd.com/documentation.html">SchedMD Slurm Documentation</a>.
	
			You may find it helpful when migrating from one scheduler to another such as GridEngine to Slurm to refer to SchedMD&#039;s <a href="https://slurm.schedmd.com/rosetta.pdf" title="https://slurm.schedmd.com/rosetta.pdf">rosetta</a> showing equivalent commands across various schedulers and their <a href="https://slurm.schedmd.com/pdfs/summary.pdf" title="https://slurm.schedmd.com/pdfs/summary.pdf">command/option summary (two pages)</a>. 
	
			<p class="portlet-msg-alert">It is a good idea to periodically check in <code>/opt/shared/templates/slurm/</code> for updated or new <a href="https://docs.hpc.udel.edu/technical/slurm/darwin/templates/start">templates</a> to use as job scripts to run generic or specific applications designed to provide the best performance on DARWIN.</p>
	
			Need help? See <a href="http://www.hpc.udel.edu/presentations/intro_to_slurm/" title="http://www.hpc.udel.edu/presentations/intro_to_slurm/">Introduction to Slurm</a> in <abbr title="University of Delaware">UD</abbr>&#039;s HPC community cluster environment.
	
			<sup><a href="#fnt__1" id="fn__1">1)</a></sup> Historically, this is actually how some clusters were managed!  
	
		#running-intro-whatsajob
			:markdown
				## [What is a Job?](#running-intro-whatsajob)
	
				In this context, a <em>job</em> consists of:
	
				* a sequence of commands to be executed
				* a list of resource requirements and other properties affecting scheduling of the job
				* a set of environment variables
	
				For an <em><a href="#scheduling-salloc">interactive job</a></em>, the user manually types the sequence of commands once the job is eligible for execution.  If the necessary resources for the job are not immediately available, then the user must wait; when resources are available, the user must be present at his/her computer in order to type the commands.  Since the job scheduler does not care about the time of day, this could happen anytime, day or night.
	
				By comparison, a <em><a href="#scheduling-sbatch">batch job</a></em> does not require the user be awake and at his or her computer:  the sequence of commands is saved to a file, and that file is given to the job scheduler.  A file containing a sequence of shell commands is also known as a <em>script</em>, so in order to run batch jobs a user must become familiar with <em>shell scripting</em>.  The benefits of using batch jobs are significant:
	
				* a <em>job script</em> can be reused (versus repeatedly having to type the same sequence of commands for each job)
	
				* when resources are granted to the job it will execute immediately (day or night), yielding increased job throughput
	
				An individual&#039;s increased job throughput is good for all users of the cluster!
	
				<p class="portlet-msg-alert">All resulting <strong><em>executables</em></strong> (created via your own compilation) and other <strong><em>applications</em></strong> (commercial or open-source) should only be run on the compute nodes.</p>
	
	
		#running-intro-queues
			:markdown
				## [Queues](#running-intro-queues)
	
				At its most basic, a <em>queue</em> represents a collection of computing entities (call them nodes) on which jobs can be executed.  Each queue has properties that restrict what jobs are eligible to execute within it:  a queue may not accept interactive jobs; a queue may place an upper limit on how long the job will be allowed to execute or how much memory it can use; or specific users may be granted or denied permission to execute jobs in a queue.
	
				<p class="portlet-msg-info">Slurm uses a <em>partition</em> to embody the common set of properties that define what nodes they include, and general system state. A <em>partition</em> can be considered job queues representing a collection of computing entities each of which has an assortment of constraints such as job size limit, job time limit, users permitted to use it, etc. Priority-ordered jobs are allocated nodes within a partition until the resources (nodes, processors, memory, etc.) within that partition are exhausted. Once a job is assigned a set of nodes, the user is able to initiate parallel work in the form of job steps in any configuration within the allocation. The term <em>queue</em> will most often imply a <em>partition</em>.</p>
	
				When submitting a job to Slurm, a user must set their workgroup prior to submitting a job <strong>and</strong> explicitly request a single partition as part of the job submission doing so will place that partitions&#039;s resource restrictions (e.g. maximum execution time) on the job, even if they are not appropriate.
	
		#running-intro-slurm
			:markdown
				## [Slurm](#running-intro-slurm)
	
				The Slurm workload manager is used to manage and control the computing resources for all jobs submitted to a cluster. This includes load balancing, reconciling requests for memory and processor cores with availability of those resources, suspending and restarting jobs, and managing jobs with different priorities. 
	
				In order to schedule any job (interactively or batch) on a cluster, you must set your <a href="#appdev-compenv-workgroup">workgroup</a> to define your allocation workgroup <strong>and</strong> explicitly request a single partition.
	
	
		#running-intro-runtimeenv
			:markdown
				## [Runtime Environment](#running-intro-runtimeenv)
	
				Generally, your runtime environment (path, environment variables, etc.) should be the same as your compile-time environment. Usually, the best way to achieve this is to put the relevant <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> commands in shell scripts. You can reuse common sets of commands by storing them in a shell script file that can be <em>sourced </em>from within other shell script files.
	
				If you are writing an executable script that does not have the <strong>-l</strong> option on the <strong>bash</strong> command, and you want to include <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> commands in your script, then you should include the line:
	
				<pre class="cmd-line"><b>source /etc/profile.d/valet.sh</b></pre>
	
				You do not need this command when you:
	
				1. type commands, or source the command file,
				1. include lines in the file to be submitted with `sbatch`.
	
		#running-intro-help
			:markdown
				## [Getting Help](#running-intro-help)
	
				Slurm includes man pages for all of the commands that will be reviewed in this document.  When logged-in to a cluster, type
	
				<pre class="cmd-line">&#91;bjones@login00.darwin ~&#93;$ <b>man squeue</b></pre>
	
				to learn more about a Slurm command (in this case, <code>squeue</code>).  Most commands will also respond to the <code>-help</code> command-line option to provide a succinct usage summary:
	
				<pre class="cmd-line">[bjones@darwin ~]$ <b>squeue -help</b>
				Usage: squeue [OPTIONS]
				  -A, --account=account(s)        comma separated list of accounts
				                                  to view, default is all accounts
				  -a, --all                       display jobs in hidden partitions
				      --array-unique              display one unique pending job array
				                                  element per line
				      --federation                Report federated information if a member
				                                  of one
				&nbsp;
				           :</pre>
	
