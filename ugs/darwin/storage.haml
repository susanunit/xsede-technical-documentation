#files
	:markdown
		# [File Systems](#files)

	#files-home
		:markdown
			## [Home](#files-home)

			The 13.5 TiB file system uses 960 GiB enterprise class SSD drives in a triple-parity RAID configuration for high reliability and availability. The file system is accessible to all nodes via IPoIB on the 100 Gbit/s InfiniBand network.

		#files-home-storage
			:markdown
				### [Storage](#files-home-storage)

			Each user has 20 GB of disk storage reserved for personal use on the home file system. Users&#039; home directories are in /home  (e.g., <code>/home/1005</code>), and the directory name is put in the environment variable <code>$HOME</code> at login.

	#files-lustre
		:markdown
			## [High-Performance Lustre](#files-lustre)

			Lustre is designed to use parallel I/O techniques to reduce file-access time. The Lustre file systems in use at UD are composed of many physical disks using RAID technologies to give resilience, data integrity, and parallelism at multiple levels. There is approximately 1.1 PiB of Lustre storage available on DARWIN. It uses high-bandwidth interconnects such as Mellanox HDR100. Lustre should be used for storing input files, supporting data files, work files, and output files associated with computational tasks run on the cluster.

			Consult [All About Lustre](https://docs.hpc.udel.edu/abstract/darwin/filesystems/lustre) for more detailed information. 

		#files-lustre-storage
			:markdown
				### [Workgroup Storage](#files-lustre-storage)

				Allocation workgroup storage is available on a <a href="https://docs.hpc.udel.edu/abstract/darwin/filesystems/lustre">high-performance Lustre-based file system</a> having almost 1.1 PB of usable space. Users should have a basic understanding of the concepts of Lustre to take full advantage of this file system. The default stripe count is set to 1 and the default striping is a single stripe distributed across all available OSTs on Lustre. See <a href="https://www.nas.nasa.gov/hecc/support/kb/lustre-best-practices_226.html">Lustre Best Practices</a> from Nasa.

				Each allocation will have at least 1 TiB of shared (<a href="#compenv-workgroup">workgroup</a>) storage in the <code>/lustre/</code> directory identified by the «<em>allocation_workgroup</em>» (e.g., <code>/lustre/it_css</code>) accessbile by all users in the allocation workgroup, and is referred to as your workgroup directory (<code>$WORKDIR</code>), if the allocation workgroup has been set. 

				Each user in the allocation workgroup will have a <code>/lustre/«<em>workgroup</em>»/users/«<em>uid</em>»</code> directory to be used as a personal workgroup storage directory for running jobs, storing larger amounts of data, input files, supporting data files, work files, output files and source code. It can be referred to as <code>$WORKDIR_USERS</code>, if the allocation workgroup has been set. 

				Each allocation will also have a <code>/lustre/«<em>workgroup</em>»/sw</code> directory to allow users to install software to be shared for the allocation workgroup. It can be referred to as <code>$WORKDIR_SW</code>, if the allocation workgroup has been set. In addition a <code>/lustre/«<em>workgroup</em>»/sw/valet</code>) directory is also provided to store VALET package files to shared for the allocation workgroup. 

				Please see <a href="#compenv-xxxusing-workgroup-and-directories">workgroup</a> for complete details on environment variables.

				<strong>Note</strong>: A full file system inhibits use for everyone preventing jobs from running.

	#files-local
		:markdown
			## [Local/Node File System](#files-local)

		#files-local-storage
			:markdown
				### [Temporary Storage](#files-local-storage)

				Each compute node has its own 2 TB local hard drive, which is needed for time-critical tasks such as managing virtual memory.  The system usage of the local disk is kept as small as possible to allow some local disk for your applications, running on the node. 

	#files-quotas
		:markdown
			## [Quotas and Usage](#files-quotas)

			To help users maintain awareness of quotas and their usage on the <code>/home</code> file system, the command <code>my_quotas</code> is available to display a list of all quota-controlled file systems on which the user has storage space. 

			For example, the following shows the amount of storage available and in-use for user <code>bjones</code> in workgroup <code>it_css</code> for their home and workgroup directory.

			<pre class="cmd-line">$ <b>my_quotas</b>
			Type  Path           In-use / kiB Available / kiB  Pct
			----- -------------- ------------ --------------- ----
			user  /home/1201          7497728        20971520  36%
			group /lustre/it_css          228      1073741824   0%</pre>

		#files-quotas-home
			:markdown
				### [Home](#files-quotas-home)

				Each user&#039;s home directory has a hard quota limit of 20 GB. To check usage, use

				<pre class="cmd-line">$ <b>df -h $HOME</b></pre>

				The example below displays the usage for the home directory (<code>/home/1201</code>) for the account <code>bjones</code> as 7.2 GB used out of 20 GB which matches the above example provide by <code>my_quotas</code> command.

				<pre class="cmd-line">$ <b>df -h $HOME</b>
				Filesystem                 Size  Used Avail Use% Mounted on
				nfs0-ib:/beagle/home/1201   20G  7.2G   13G  36% /home/1201</pre>

		#files-quotas-workgroup
			:markdown
				### [Workgroup](#files-quotas-workgroup)

				All of Lustre is available for allocation workgroup storage.  To check Lustre usage for all users, use <code>df -h /lustre</code>.

				The example below shows 25 TB is in use out of 954 TB of usable Lustre storage.

				<pre class="cmd-line">$ <b>df -h /lustre</b>
				Filesystem                             Size  Used Avail Use% Mounted on
				10.65.2.6@o2ib:10.65.2.7@o2ib:/darwin  978T   25T  954T   3% /lustre </pre>

				To see your allocation workgroup usage, please use the <code>my_quotas</code> command. Again the the following example shows the amount of storage available and in-use for user <code>bjones</code> in allocation workgroup <code>it_css</code> for their home and allocation workgroup directories.

				<pre class="cmd-line">$ <b>my_quotas</b>
				Type  Path           In-use / kiB Available / kiB  Pct
				----- -------------- ------------ --------------- ----
				user  /home/1201          7497728        20971520  36%
				group /lustre/it_css          228      1073741824   0% </pre>

		#files-quotas-local
			:markdown
				### [Node](#files-quotas-local)

				The node temporary storage is mounted on <code>/tmp</code> for all nodes.  There is no quota, and if you exceed the physical size of the disk you will get disk failure messages.  To check the usage of your disk, use the <code>df -h</code> command <strong>on the compute node</strong> where your job is running. 

				We strongly recommend that you refer to the node scratch by using the environment variable, <code>$TMPDIR</code>, which is defined by Slurm when using <code>salloc</code> or <code>srun</code>or <code>sbatch</code>.

				For example, the command

				<pre class="cmd-line">$ <b>ssh r1n00 df -h /tmp</b></pre>

				shows size, used and available space in M, G or T units. 

				<pre class="cmd-line">Filesystem      Size  Used Avail Use% Mounted on
				/dev/sda3       1.8T   41M  1.8T   1% /tmp</pre>

				This node <code>r1n00</code> has a 2 TB disk, with only 41 MB used, so 1.8 TB is available for your job.

				<p class="portlet-msg-alert">There is a physical disk installed on each node that is used for time critical tasks, such as swapping memory. Most of the compute nodes are configured with a 2 TB disk, however, the <code>/tmp</code> file system will never have the total disk.  Larger memory nodes will need to use more of the disk for swap space.</p>

	#files-recovering
		:markdown
			## [Recovering Files](#files-recovering)

			<p class="portlet-msg-info">While all file systems on the DARWIN cluster utilize hardware redundancies to protect data, there is <strong>no</strong> backup or replication and <strong>no</strong> recovery available for the home or Lustre file systems. All backups are the responsibility of the user. DARWIN&#039;s systems administrators are not liable for any lost data.</p>

	#files-usage
		:markdown
			## [Usage Recommendations](#files-usage)

			<strong>Home directory</strong>: Use your <a href="#files-home">home</a> directory to store private files. Application software you use will often store its configuration, history and cache files in your home directory. Generally, keep this directory free and use it for files needed to configure your environment. For example, add <a href="http://en.wikipedia.org/wiki/Symbolic_link#POSIX_and_Unix-like_operating_systems">symbolic links</a> in your home directory to point to files in any of the other directory.

			<strong>Workgroup directory</strong>: Use the personal allocation <a href="#files-workgroup">workgroup</a> directory for running jobs, storing larger amounts of data, input files, supporting data files, work files, output files and source code in `$WORKDIR_USERS` as an extension of your home direcory.  It is also appropriate to use the software allocation <a href="#files-workgroup" title="abstract:darwin:filesystems:filesystems">workgroup</a> directory to build applications for everyone in your allocation group in `$WORKDIR_SW` as well as create a VALET package for your fellow researchers to access applications you want to share in `$WORKDIR_SW/valet`. 

			<strong>Node scratch directory</strong>: Use the <a href="#node-scratch" title="abstract:darwin:filesystems:filesystems">node scratch</a> directory for temporary files. The job scheduler software (Slurm) creates a temporary directory in /tmp specifically for each job&#039;s temporary files. This is done on each node assigned to the job. When the job is complete, the subdirectory and its contents are deleted. This process automatically frees up the local scratch storage that others may need. Files in node scratch directories are not available to the head node, or other compute nodes. 

