#progenv
	:markdown
		# [Programming Environment](#progenv)

	#progenv-models
		:markdown
			## [Programming Models](#progenv-models)
	
			There are two memory models for computing: distributed-memory and shared-memory. In the former, the message passing interface (MPI) is employed in programs to communicate between processors that use their own memory address space. In the latter, open multiprocessing (<abbr title="OpenMP">OMP</abbr>) programming techniques are employed for multiple threads (light weight processes) to access memory in a common address space. When your job spans several compute nodes, you must use an <abbr title="Message Passing Interface">MPI</abbr> model.
	
			Distributed memory systems use single-program multiple-data (SPMD) and multiple-program multiple-data (MPMD) programming paradigms. In the SPMD paradigm, each processor loads the same program image and executes and operates on data in its own address space (different data). It is the usual mechanism for <abbr title="Message Passing Interface">MPI</abbr> code: a single executable is available on each node (through a globally accessible file system such as `$WORKDIR`), and launched on each node (through the <abbr title="Message Passing Interface">MPI</abbr> wrapper command, <strong>mpirun</strong>).
	
			The shared-memory programming model is used on Symmetric Multi-Processor (SMP) nodes such as a single typical compute node (20 or 24 cores, 64 <abbr title="Gigabyte">GB</abbr> memory). The programming paradigm for this memory model is called Parallel Vector Processing (PVP) or Shared-Memory Parallel Programming (SMPP). The former name is derived from the fact that vectorizable loops are often employed as the primary structure for parallelization. The main point of SMPP computing is that all of the processors in the same node share data in a single memory subsystem. There is no need for explicit messaging between processors as with <abbr title="Message Passing Interface">MPI</abbr> coding.
	
			The SMPP paradigm employs compiler directives (as pragmas in C/C++ and special comments in Fortran) or explicit threading calls (e.g. with Pthreads). The majority of science codes now use OpenMP directives that are understood by most vendor compilers, as well as the GNU compilers.
	
			In cluster systems that have SMP nodes and a high-speed interconnect between them, programmers often treat all CPUs within the cluster as having their own local memory. On a node, an <abbr title="Message Passing Interface">MPI</abbr> executable is launched on each processor core and runs within a separate address space. In this way, all processor cores appear as a set of distributed memory machines, even though each node has processor cores that share a single memory subsystem.
	
			Clusters with SMPs sometimes employ hybrid programming to take advantage of higher performance at the node-level for certain algorithms that use SMPP (<abbr title="OpenMP">OMP</abbr>) parallel coding techniques. In hybrid programming, <abbr title="OpenMP">OMP</abbr> code is executed on the node as a single process with multiple threads (or an <abbr title="OpenMP">OMP</abbr> library routine is called), while <abbr title="Message Passing Interface">MPI</abbr> programming is used at the cluster-level for exchanging data between the distributed memories of the nodes.
	
	#progenv-compiling
		:markdown
			## [Compiling Code](#progenv-compiling)
	
			Fortran, C, C++, Java and Matlab programs should be compiled on the login node, however if lengthy compiles or extensive resources needed, you may need to schedule a job for compilation using <code>salloc</code> or <code>sbatch</code> which will be <a href="#jobaccounting">billed</a> to your allocation. <strong><em>All resulting executables should only be run on the compute nodes.</em></strong>
	
	#progenv-compiling
		:markdown
			## [The Compiler Suites](#progenv-compiling)
	
			There are three 64-bit compiler suites that IT generally installs and supports: PGI CDK (Portland Group Inc.&#039;s Cluster Development Kit), Intel Composer XE 2011, and GNU. In addition, IT has installed OpenJDK (Open Java Development Kit), which must only be used on the compute nodes. (Type <strong>vpkg_info openjdk</strong> for more information on OpenJDK.)
	
			The PGI compilers exploit special features of AMD processors. If you use open-source compilers, we recommend the GNU collection.
	
			You can use a <a href="#appdev-compenv-valet">VALET</a> <strong>vpkg_require</strong> command to set the UNIX environment for the compiler suite you want to use. After you issue the corresponding <strong>vpkg_require</strong> command, the compiler path and supporting environment variables will be defined.
	
			A general command for basic source code compilation is:
	
			&lt;<em>compiler</em>&gt; &lt;<em>compiler_flags</em>&gt; &lt;<em>source_code_filename&gt;</em> -o &lt;<em>executable_filename</em>&gt;
	
			For each compiler suite, the table below displays the compiler name, a link to documentation describing the compiler flags, and the appropriate filename extension for the source code file. The executable will be named <strong>a.out</strong> unless you use the <strong>-o</strong> <strong>&lt;<em>executable_filename</em></strong><strong>&gt;</strong> option.
	
			To view the compiler option flags, their syntax, and a terse explanation, execute a compiler command with the <strong>-help</strong> option. Alternatively, read the compiler&#039;s <strong>man</strong> pages.
	
		%table(border="1" cellspacing="5" cellpadding="3")
			%tr
				%th PGI 
				%th <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> command 
				%th Reference manuals 
				%th User guides 
			%tr
				%th 
				%td <strong>vpkg_require pgi</strong> 
				%td <a href="http://www.pgroup.com/doc/pgiref.pdf">C, Fortran</a> 
				%td <a href="http://www.pgroup.com/doc/pgiug.pdf">C, Fortran</a> 
			%tr
				%td  
				%th Compiler  
				%th Language 
				%th Common filename extensions 
			
			%tr
				%td  
				%td pgfortran 
				%td F90, F95, F2003 
				%td .f, .for, .f90, .f95 
			%tr
				%td  
				%td pgf77 
				%td F77 
				%td .f 
			%tr
				%td  
				%td pgCC 
				%td C++ 
				%td .C, .cc 
			%tr
				%td  
				%td pgcc 
				%td C 
				%td .c 
			%tr
				%th Intel 
				%th <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> command 
				%th Reference manuals 
				%th User guides 
			%tr
				%td  
				%td <strong>vpkg_require intel</strong> 
				%td <a href="http://software.intel.com/sites/products/documentation/hpc/compilerpro/en-us/cpp/lin/main_cls_lin.pdf">C</a>, <a href="http://software.intel.com/sites/products/documentation/hpc/compilerpro/en-us/fortran/lin/main_for_lin.pdf">Fortran</a> 
				%td <a href="http://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=2&amp;ved=0CDAQFjAB&amp;url=http%3A%2F%2Fsoftware.intel.com%2Ffile%2F6320&amp;ei=lPfUTuutOKTX0QHZsJmKAg&amp;usg=AFQjCNEk20j03jlsNspzRyYvAEXOeT7aTA">C</a>, <a href="http://software.intel.com/sites/products/documentation/hpc/compilerpro/en-us/fortran/lin/main_for_lin.pdf">Fortran</a> 
			%tr
				%td  
				%th Compiler  
				%th Language 
				%th Common filename extensions 
			%tr
				%th 
				%td ifort 
				%td F77, F90, F95 
				%td .f, .for, .f90, .f95 
			%tr
				%th 
				%td icpc  
				%td C++ 
				%td .C, .c, .cc, .cpp, .cxx, .c++, .i, .ii 
			%tr
				%th 
				%td icc   
				%td C 
				%td .c 
			%tr
				%th GCC 
				%th <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> command 
				%th Reference manuals 
				%th User guides 
			%tr
				%th 
				%td <strong>vpkg_require gcc</strong> 
				%td <a href="http://gcc.gnu.org/onlinedocs/">C</a>, <a href="http://gcc.gnu.org/onlinedocs/gcc-4.0.4/gfortran/">Fortran</a> 
				%td <a href="http://gcc.gnu.org/onlinedocs">C</a>, <a href="http://gcc.gnu.org/onlinedocs/gcc-4.0.4/gfortran/">Fortran</a> 
			%tr
				%td  
				%th Compiler  
				%th Language 
				%th Common filename extensions 
			%tr
				%td  
				%td gfortran, f95  
				%td F77, F90, F95 
				%td .f, .f90, .f95 
			%tr
				%td  
				%td g++ 
				%td C++ 
				%td .C, .c, .cc, .cpp, .cxx, .c++, .i, .ii 
			%tr
				%td  
				%td gcc 
				%td C 
				%td .c 
	
		#progenv-compiling-serial
			:markdown
				### [Compiling Serial Programs](#progenv-compiling-serial)
		
				This section uses the PGI compiler suite to illustrate simple Fortran and C compiler commands that create an executable. For each compiler suite, you must first set the UNIX environment so the compilers and libraries are available to you. <a href="#appdev-compenv-valet">VALET</a> commands provide a simple way to do this.
		
				The examples below show the compile and link steps in a single command. These illustrations use source code files named fdriver.f90 (Fortran 90) or cdriver.c (C). They all use the <strong>-o</strong> option to produce an executable named &#039;driver.&#039; The optional <strong>-fpic</strong> PGI compiler flag generates position-independent code and creates smaller executables. You might also use code optimization option flags such as <strong>-fast</strong> after debugging your program.
		
				You can use the <strong>-c</strong> option instead to create a <strong>.o</strong> object file that you would later link to other object files to create the executable.
		
				Some people use the UNIX <strong>make</strong> command to compile source code. There are many good online tutorials on the <a href="http://www.eng.hawaii.edu/Tutor/Make" class="urlextern" title="http://www.eng.hawaii.edu/Tutor/Make">basics of using make</a>. Also available is a cross-platform makefile generator, <strong>cmake</strong>. You can set the UNIX environment for <strong>cmake</strong> by typing the <strong>vpkg_require cmake</strong> command. <strong></strong>
		
				<u>Using the PGI suite to illustrate:</u>
		
				First use a <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> command to set the environment:
		
				<pre class="code"> vpkg_require pgi</pre>
		
				Then use that compiler suite&#039;s commands:
		
				<u>Fortran 90 example:</u>
				<pre class="code"> pgfortran -fpic fdriver.f90 -o driver</pre>
		
				<u>C example:</u>
				<pre class="code"> pgcc -fpic cdriver.c -o driver</pre>
		
		#progenv-compiling-parallel
			:markdown
				### [Compiling Parallel Programs with OpenMP](#progenv-compiling-parallel)
		
				If your program only uses OpenMP directives, has <em class="u">no</em> message passing, and your target is a single SMP node, you should add the <a href="https://www.openmp.org/resources/openmp-compilers-tools/" class="urlextern" title="https://www.openmp.org/resources/openmp-compilers-tools/">OpenMP</a> compiler flag to the serial compiler flags.
		
		%table(border="1" cellspacing="5" cellpadding="3")
			%tr
				%th Compiler suite 
				%th OpenMP compiler flag 
			%tr
				%td <strong>PGI</strong> 
				%td -mp 
			%tr
				%td <strong>Open64</strong> 
				%td -mp 
			%tr
				%td <strong>Intel</strong> 
				%td -openmp 
			%tr
				%td <strong>Intel-2016</strong> 
				%td -qopenmp 
			%tr
				%td <strong>GCC</strong> 
				%td -fopenmp 
			
			:markdown
				Instead of using OpenMP directives in your program, you can add an OpenMP-based library. You will still need the OpenMP compiler flag when you use the library.
		
		#progenv-compiling-parallelmpi
			:markdown
				### [Compiling Parallel Programs with MPI](#progenv-compiling-parallelmpi)
		
		#progenv-compiling-parallelmpi-implementations
			:markdown
				#### [MPI Implementations](#progenv-compiling-parallelmpi-implementations)
		
				In the distributed-memory model, the message passing interface (MPI) allows programs to communicate between processors that use their own node&#039;s memory address space.<strong> </strong>It is the most commonly used library and runtime environment for building and executing distributed-memory applications on clusters of computers.
		
				<strong>OpenMPI is the most desirable <abbr title="Message Passing Interface">MPI</abbr> implementation to use</strong>.<strong> </strong>It is the only one that works for job suspension, checkpointing, and task migration to other processors. These capabilities are needed to enable opportunistic use of idle nodes as well as to configure short-term and long-term queues.
		
				Some software comes packaged with other <abbr title="Message Passing Interface">MPI</abbr> implementations that IT cannot change. In those cases, their <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> configuration files use the bundled <abbr title="Message Passing Interface">MPI</abbr> implementation. However, we recommend that you use OpenMPI whenever you need an <abbr title="Message Passing Interface">MPI</abbr> implementation.
		
		#progenv-compiling-parallelmpi-wrappers
			:markdown
				#### [MPI Compiler Wrappers](#progenv-compiling-parallelmpi-wrappers)
		
				The <a href="http://www.openmpi.org/" class="urlextern" title="http://www.openmpi.org/">OpenMPI</a> implementation provides OpenMPI library compilers for C, C++, Fortran 77, 90, and 95. These <em>compiler wrappers</em> add <abbr title="Message Passing Interface">MPI</abbr> support to the actual compiler suites by passing additional information to the compiler. You simply use the <abbr title="Message Passing Interface">MPI</abbr> compiler wrapper in place of the compiler name you would normally use.
			
				The compiler suite that&#039;s used depends on your UNIX environment settings. Use <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> commands to simultaneously set your environment to use the OpenMPI implementation and to select a particular compiler suite. The commands for the four compiler suites are:
			
				<pre class="code">  vpkg_require openmpi/1.4.4-pgi
					vpkg_require openmpi/1.4.4-open64
					vpkg_require openmpi/1.4.4-intel64
					vpkg_require openmpi/1.4.4-gcc</pre>
			
				(Type<em> </em><strong><em>vpkg_versions openmpi</em></strong><em> </em>to see if newer versions are available<em>.</em>)
			
				The <strong>vpkg_require</strong> command selects the <abbr title="Message Passing Interface">MPI</abbr> and compiler suite combination, and then you may use the compiler wrapper commands repeatedly. The wrapper name depends only on the language used, not the compiler suite you choose: mpicc (C), mpicxx or mpic++ (C++), mpi77 (Fortran 77), and mpif90 (Fortran 90 and 95).
			
			
				<u>Fortran example:</u>
				<pre class="cmd-line">vpkg_require openmpi/1.4.4-pgi
				mpif90 -fpic fdriver.f90 -o driver</pre>
			
			
				<u>C example:</u>
				<pre class="cmd-line">vpkg_require openmpi/1.4.4-pgi
				mpicc -fpic cdriver.c -o driver</pre>
			
				You may use other compiler flags listed in each <a href="#the-compiler-suites" title="abstract:darwin:app_dev:prog_env ↵" class="wikilink1">compiler suite&#039;s documentation</a>.
			
				To modify the options used by the <abbr title="Message Passing Interface">MPI</abbr> wrapper commands, consult the <a href="http://www.open-mpi.org/faq/?category=mpi-apps" class="urlextern" title="http://www.open-mpi.org/faq/?category=mpi-apps">FAQ section</a> of the OpenMPI web site.
			
	#progenv-libraries
		:markdown
			## [Programming Libraries](#progenv-libraries)
	
			IT installs high-quality math and utility libraries that are used by many applications. These libraries provide highly optimized math packages and functions. To determine which compilers IT used to prepare a library version, use the <strong>vpkg_versions</strong> <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> command. 
		
			Here is a representative sample of installed libraries. Use the <strong>vpkg_list</strong><strong> </strong>command to see the most current list of libraries.<strong></strong>
		
		#progenv-libraries-opensource
			:markdown
				### [Open-source libraries](#progenv-libraries-opensource)
				
				* <a href="http://math-atlas.sourceforge.net/" class="urlextern" title="http://math-atlas.sourceforge.net/">ATLAS</a>: Automatically Tuned Linear Algebra Software (portable)
				* <a href="http://www.fftw.org/">FFTW</a>: Discrete Fast Fourier Transform library
				* <a href="https://portal.tacc.utexas.edu/tutorials/blaslapack">BLAS and LAPACK at TACC</a>: Enhanced <abbr title="Basic Linear Algebra Subprograms">BLAS</abbr> routines from the Texas Advanced Computing Center (TACC)
				* <a href="http://www.hdfgroup.org/products/hdf4/" class="urlextern" title="http://www.hdfgroup.org/products/hdf4/">HDF4</a> and <a href="http://www.hdfgroup.org/HDF5/" class="urlextern" title="http://www.hdfgroup.org/HDF5/">HDF5</a>: Hierarchical Data Format suite (file formats and libraries for storing and organizing large, numerical data collections)
				* <a href="http://acts.nersc.gov/hypre/#Documentation" class="urlextern" title="http://acts.nersc.gov/hypre/#Documentation">HYPRE</a>: High-performance preconditioners for linear system solvers (from LLNL)
				* <a href="http://www.netlib.org/lapack" class="urlextern" title="http://www.netlib.org/lapack">LAPACK</a>: Linear algebra routines
				* <a href="http://matplotlib.sourceforge.net/" class="urlextern" title="http://matplotlib.sourceforge.net/">Matplotlib</a>: Python-based 2D publication-quality plotting library 
				* <a href="http://www.unidata.ucar.edu/software/netcdf/" class="urlextern" title="http://www.unidata.ucar.edu/software/netcdf/">netCDF</a>: network Common Data Form for creation, access and sharing of array-oriented scientific data
				* <a href="http://netlib.org/scalapack/scalapack_home.html" class="urlextern" title="http://netlib.org/scalapack/scalapack_home.html">ScaLAPACK</a> - Scalable <abbr title="Linear Algebra PACKage">LAPACK</abbr>: Subset of <abbr title="Linear Algebra PACKage">LAPACK</abbr> routines redesigned for distributed memory MIMD parallel computers using <abbr title="Message Passing Interface">MPI</abbr>
				* <a href="http://www.vtk.org/" class="urlextern" title="http://www.vtk.org/">VTK</a> – Visualization ToolKit: A platform for 3D computer graphics and visualization
				
				
		#progenv-libraries-opensource
			:markdown
				### [Commercial Libraries](#progenv-libraries-commericial)
		
				<!-- * <a href="http://developer.amd.com/libraries/acml/pages/default.aspx">ACML</a>: AMD&#039;s Core Math Library (See <a href="http://developer.amd.com/libraries/acml/onlinehelp/Documents/BestLibrary.html#BestLibrary">AMD&#039;s guide on library selection</a>.) -->
				* <a href="http://www.roguewave.com/products/imsl" class="urlextern" title="http://www.roguewave.com/products/imsl">IMSL</a>: RogueWave&#039;s mathematical and statistical libraries
				* <a href="http://software.intel.com/en-us/articles/intel-mkl/">MKL</a>: Intel&#039;s Math Kernel Library
				* <a href="http://www.nag.com/numeric/numerical_libraries.asp">NAG</a>: Numerical Algorithms Group&#039;s numerical libraries
				
				The libraries will be optimized a given cluster architecture. Note that the calling sequences of some of the commercial library routines differ from their open-source counterparts.
				
				
		#progenv-libraries-using
			:markdown
				### [Using Libraries](#progenv-libraries-using)
		
				This section shows you how to link your program with libraries you or your colleagues have created or with centrally installed libraries such as <abbr title="AMD Core Math Library">ACML</abbr> or FFTW. The examples introduce special environment variables (FFLAGS, CFLAGS, CPPFLAGS and LDFLAGS) whose use simplifies a command&#039;s complexity. The <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> commands <strong>vpkg_require</strong> and <strong>vpkg_devrequire</strong> can easily define the working environment for your compiler suite choice.
		
				Joint use of <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> and these environment variables will also prepare your UNIX environment to support your use of <strong>make</strong> for program development. <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> will accommodate using one or several libraries, and you can extend its functionality for software you develop or install.
		
		
		#progenv-libraries-intel
			:markdown
				### [Intel Compiler Suite](#progenv-libraries-intel)
		
				You should use Intel <abbr title="Intel Math Kernel Library">MKL</abbr> — it&#039;s a highly-optimized <abbr title="Basic Linear Algebra Subprograms">BLAS</abbr>/<abbr title="Linear Algebra PACKage">LAPACK</abbr> library.
				
				If you use the Intel compilers, you can add <code>-mkl</code> to your link command, e.g.
				
				<pre class="cmd-line">    ifort -o program -mkl=sequential &#91;...&#93;
				&nbsp;
				    ifort -o program -qopenmp -mkl=parallel &#91;...&#93;</pre>
				
				The former uses the serial library, the latter uses the threaded library that respects the OpenMP runtime environment of the job for multithreaded <abbr title="Basic Linear Algebra Subprograms">BLAS</abbr>/<abbr title="Linear Algebra PACKage">LAPACK</abbr> execution.
				
				If you&#039;re not using the Intel compilers, you&#039;ll need to generate the appropriate compiler directives using <a href="https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor" class="urlextern" title="https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor">Intel&#039;s online tool</a>.
				
				Please use “dynamic linking” since that allows <abbr title="Intel Math Kernel Library">MKL</abbr> to adjust the underlying kernel functions at runtime according to the hardware on which you&#039;re running.  If you use static linking, you&#039;re tied to the lowest common hardware model available and you will usually not see as good performance.
				
				You&#039;ll need to load a version of Intel into the environment before compiling/building and also at runtime using <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> such as
				
				<pre class="cmd-line">    vpkg_require intel/2019</pre>
				
				Among other things, this will set <code>MKLROOT</code> in the environment to the appropriate path, which the link advisor references.  The <abbr title="Intel Math Kernel Library">MKL</abbr> version (year) matches that of the compiler version (year).
				
				To determine the available versions of Intel installed use
				
				<pre class="code">$ vpkg_versions intel</pre>
				
		#progenv-libraries-pgi
			:markdown
				### [PGI Compiler Suite](#progenv-libraries-pgi)
		
		#progenv-libraries-pgi-fortran
			:markdown
				#### [Fortran Examples illustrated with the PGI compiler suite](#progenv-libraries-pgi-fortran)
		
				<u>Reviewing the basic compilation command</u>
				
				The general command for compiling source code:
				
				«<em>compiler</em>» «<em>compiler_flags</em>»  «<em>source_code_filename</em>»  -o «<em>executable_filename</em>»
				
				For example:
				
				<pre class="cmd-line">vpkg_require pgi
				pgfortran -fpic fdriver.f90 -o driver</pre>
				
				
				<u>Using user-supplied libraries</u>
				
				To compile fdriver.f90 and link it to a shared F90 library named lib<strong><em>fstat</em></strong>.so stored in `$HOME/lib`, add the library location and the library name (<strong><em>fstat</em></strong>) to the command:
				
				<pre class="cmd-line">pgfortran -fast -fpic -L$HOME/lib fdriver.f90 -lfstat -o driver</pre>
				
				The <strong>-L</strong> option flag is for the shared library directory&#039;s name; the <strong>-l</strong> flag is for the specific library name.
				
				You can simplify this compiler command by creating and exporting two special environment variables. FFLAGS represents a set of Fortran compiler option flags; LDFLAGS represents the location and choice of your library.
				
				<pre class="cmd-line">vpkg_require pgi
				export FFLAGS='-fpic'
				export LDFLAGS='-L$HOME/lib'
				export LDLIBS='-lfstat'
				pgfortran $FFLAGS $LDFLAGS fdriver.f90 $LDLIBS -o driver</pre>
				
				Extending this further, you might have several libraries in one or more locations. In that case, list all of the &#039;-l&#039; flags in the LDLIBS statement, for example,
				
				<pre class="cmd-line">export LDLIBS='-lfstat -lfpoly'</pre>
				
				and all of the &#039;-L&#039; flags in the LDFLAGS statement. (The order in which the &#039;-L&#039; directories appear in LDFLAGS determines the search order.)
				
				
				<u>Using centrally supplied libraries (ACML, MKL, FFTW, etc.)</u>
				
				This extends the previous section&#039;s example by illustrating how to use <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr>&#039;s <strong>vpkg_</strong><strong>devrequire</strong> command to locate and link a centrally supplied library such as AMD&#039;s Core Math Library, <abbr title="AMD Core Math Library">ACML</abbr>. Several releases (versions) of a library may be installed, and some may have been compiled with several compiler suites.
				
				To view your choices, use <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr>&#039;s <strong>vpkg_versions</strong> command:
				
				<pre class="cmd-line">vpkg_versions acml</pre>
				
				The example below uses the acml/5.0.0-pgi-fma4 version, the single-threaded, <abbr title="AMD Core Math Library">ACML</abbr> 5.0.0 FMA4 library compiled with the PGI 11 compilers. Since that version depends on the PGI 11 compiler suite,
				
				<pre class="cmd-line">vpkg_devrequire acml/5.0.0-pgi-fma4 pgi</pre>
				
				jointly sets the UNIX environment for both <abbr title="AMD Core Math Library">ACML</abbr> and the PGI compiler suite. Therefore, you <em class="u">should not also issue</em> a <strong>vpkg_require pgi</strong> command.
				
				Unlike <strong>vpkg_require</strong>, <strong>vpkg_devrequire</strong> also modifies key environment variables including LDFLAGS.
				
				Putting it all together, the complete example using the library named <strong>acml</strong> is:
				
				<pre class="cmd-line">vpkg_devrequire acml/5.0.0-pgi-fma4 pgi
				export FFLAGS='-fpic'
				export LDLIBS='-lacml'
				pgfortran $FFLAGS $LDFLAGS fdriver.f90 $LDLIBS -o driver</pre>
				
				Note that <strong>$LDFLAGS</strong> must be in the compile statement but does not need an explicit <strong>export</strong> command here. The <strong>vpkg_devrequire</strong> command above defined and exported LDFLAGS and its value.
				
				
				<u>Using user-supplied libraries and centrally supplied libraries together</u>
				
				This final example illustrates how to use your <strong>fstat</strong> and <strong>fpoly</strong> libraries (both in `$HOME/lib`) with the acml5.0.0 library:
				
				<pre class="cmd-line">vpkg_devrequire acml/5.0.0-pgi-fma4 pgi
				export FFLAGS='-fpic'
				export LDFLAGS='-L$HOME/lib $LDFLAGS'
				export LDLIBS='-lacml -lfstat -lfpoly'
				pgfortran $FFLAGS $LDFLAGS fdriver.f90 $LDLIBS -o driver</pre>
				
				Remember that the library search order depends on the order of the LDFLAGS libraries.
				
				
		#progenv-libraries-pgi-c
			:markdown
				#### [C Examples illustrated with the PGI compiler suite](#progenv-libraries-pgi-c)
		
				<u>Reviewing the basic compilation command</u>
				
				The general command for compiling source code:
				
				«<em>compiler</em>» «<em>compiler_flags</em>»  «<em>source_code_filename</em>»  -o «<em>executable_filename</em>»
				
				For example,
				
				<pre class="cmd-line">vpkg_require pgi
				pgcc -fpic cdriver.c -o driver</pre>
				
				
				<u>Using user-supplied libraries</u>
				
				
				To compile cdriver.c and link it to a shared C library named lib<strong><em>cstat</em></strong>.so stored in `$HOME/lib` and include header files in `$HOME/inc`, add the library location and the library name (<strong><em>cstat</em></strong>) to the command.
				
				<pre class="cmd-line">pgcc -fpic –I$HOME/inc –L$HOME/lib cdriver.c –lcstat -o driver</pre>
				
				The <strong>-I</strong> option flag is for the include library&#039;s location; the <strong>-L</strong> flag is for the shared library directory&#039;s name; and the <strong>-l</strong> flag is for the specific library name.
				
				You can simplify this compiler command by creating and exporting two special environment variables. CFLAGS represents a set of C compiler option flags; CPPFLAGS represents the C++ preprocessor flags; and LDFLAGS represents the location and choice of your shared library.
				
				<pre class="cmd-line">pkg_require pgi
				export CFLAGS='-fpic'
				export CPPFLAGS='$HOME/inc'
				export LDFLAGS='-L$HOME/lib'
				export LDLIBS='-lcstat'
				pgcc $CFLAGS $CPPFLAGS $LDFLAGS cdriver.c $LDLIBS -o driver</pre>
				
				
				Extending this further, you might have several libraries in one or more locations. In that case, list all of the &#039;-l&#039; flags in the LDLIBS statement, for example,
				
				<pre class="cmd-line">export LDLIBS='-lcstat -lcpoly'</pre>
				
				
				and all of the &#039;-L&#039; flags in the LDFLAGS statement. (The order in which the &#039;-L&#039; directories appear in LDFLAGS determines the search order.)
				
				<u>Using centrally supplied libraries (ACML, MKL, FFTW, etc.)</u>
				
				This extends the previous section&#039;s example by illustrating how to use <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr>&#039;s <strong>vpkg_devrequire</strong> command to locate and link a system-supplied library, such as AMD&#039;s Core Math Library, <abbr title="AMD Core Math Library">ACML</abbr>. Several releases (versions) of a library may be installed, and some may have been compiled with several compiler suites.
				
				To view your choices, use <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr>&#039;s <strong>vpkg_versions</strong> command:
				
				<pre class="cmd-line">vpkg_versions acml</pre>
				
				
				The example below uses the acml/5.0.0-pgi vpkg_devrequire acml/5.0.0-pgi-fma4 version, the single-threaded, <abbr title="AMD Core Math Library">ACML</abbr> 5.0.0 FMA4 library compiled with the PGI 11 compilers. Since that version depends on the PGI 11 compiler suite,<strong></strong>
				
				<pre class="cmd-line">vpkg_devrequire acml/5.0.0-pgi-fma4 pgi</pre>
				
				
				jointly sets the UNIX environment for both <abbr title="AMD Core Math Library">ACML</abbr> and the PGI compiler suite. Therefore, you <em class="u">should not also issue</em> a <strong>vpkg_require pgi</strong> command.
				
				
				Unlike <strong>vpkg_require</strong>,<strong> </strong><strong>vpkg_devrequire</strong> also modifies key environment variables including LDFLAGS and CPPFLAGS.
				
				Putting it all together, the complete example using the library named <strong>acml</strong>, is:
				
				<pre class="cmd-line">vpkg_devrequire acml/5.0.0-pgi-fma4 pgi
				export CFLAGS='-fpic'
				export LDLIBS='-lacml'
				pgcc $CFLAGS $CPPFLAGS $LDFLAGS cdriver.c $LDLIBS -o driver</pre>
				
				
				Note that, `$CPPFLAGS` and `$LDFLAGS` must be in the compile statement even though the <strong>export CPPFLAGS</strong> and <strong>export LDFLAGS</strong> statement didn&#039;t appear above. The <strong>vpkg_devrequire</strong> command above defined and exported CPPFLAGS and LDFLAGS and their values.
				
				
				<u>Using user-supplied libraries and centrally supplied libraries together</u>
				
				The final example illustrates how to use your <strong>cstat </strong>and <strong>cpoly</strong> libraries (both in `$HOME/lib`) with the <strong>acml</strong> library:
				
				<pre class="cmd-line">vpkg_devrequire acml/5.0.0-pgi-fma4 pgi
				export CFLAGS='-fpic'
				export CPPFLAGS='$CPPFLAGS $HOME/inc'
				export LDFLAGS='-L$HOME/lib $LDFLAGS'
				export LDLIBS='-lacml -lcstat -lcpoly'
				pgcc $CFLAGS $CPPFLAGS $LDFLAGS cdriver.c $LDLIBS -o driver</pre>
				
				
				Remember that the library search order depends on the order of the LDFLAGS libraries.
				
				
