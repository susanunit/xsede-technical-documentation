#software
	:markdown
		# [Software Installation on DARWIN](#software)

		First see if your software is installed by using <code>vpkg_list</code> on DARWIN, and then check the <a href="https://docs.hpc.udel.edu/software/software">Software</a> documentation for all software which is organized in alphabetical order on the sidebar to see if there are any specific instructions provided for use and/or installation on DARWIN.

		The HPC team has a set of standards and technology used to reduce complexity and bring consistency to the process of software installation and management. 

		Software is generally built, installed, and accessed using the <a href="https://docs.hpc.udel.edu/software/valet/valet">VALET</a> system (not Modules but similar) developed by Dr. Jeffrey Frey. <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> provides the ability to modify your environment without modifying your startup files like <code>.bashrc</code> and <code>.bash_profile</code> as it prevents jobs from failing by keeping a clean login environment.

		This process of basic software and management is described in <a href="http://docs.hpc.udel.edu/technical/recipes/software-managment">Software Management</a> and <a href="#software-workgroup">Workgroup Directory</a>.

		With that said, you may need to use other methodologies for installing your software such as a <a href="https://docs.hpc.udel.edu/software/python/python">Python</a> virtualenv or a container technology like <a href="https://docs.hpc.udel.edu/software/singularity/singularity">Singularity</a> on the cluster along with creating a <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> package to properly add it to your environment.

	#software-workgroup
		:markdown
			## [Workgroup Software Installs](#software-workgroup)
	
			Each workgroup receives its own dedicated storage space.  If a workgroup uses software that IT does not maintain, then its likely that software is present within that storage space.  Often times every individual workgroup member might have his or her own copy that he or she maintains – this may be necessary if frequent redimensioning and recompiling is necessary to the job workflow.
	
			This document assumes that there is software necessary to a workgroup&#039;s productivity that do not require per-job recompilation.  While the scheme presented here works best when individuals do not require their own copy of the software, the scheme can be extended to include that situation, as well.
	
			<p class="portlet-msg-info">For the sake of examples, the workgroup name used in this document will be <em>it_nss</em>.</p>
	
		#software-workgroup-dirstructure
			:markdown
				### [Directory Structure](#software-workgroup-dirstructure)
	
				After using the <code>workgroup</code> command (either explicitly or within your login files) the <code>WORKDIR</code>, <code>WORKDIR_USERS</code> and <code>WORKDIR_SW</code> environment variables are set based the allocation workgroup directory. Create a directory to contain all workgroup-specific software:
	
				<pre class="cmd-line">
				$ <b>workgroup -g it_nss</b>
				$ <b>ls -ld $WORKDIR_SW</b>
				drwxrws--- 4 root it_nss 4096 Aug  1  2013 sw</pre>
	
	
				The privileges assigned to the <em>sw</em> directory allow any member of the workgroup to create directories/files inside it.
		
				To streamline <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> usage, a directory named <em>valet</em> has been created inside <em>sw</em> to contain any <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> package definition files representing the software you&#039;ve installed.  The <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> software will automatically check this directory if present:
		
				<pre class="cmd-line">$ <b>ls -la $WORKDIR_SW</b>
				total 130
				drwxrws--- 4 root   it_nss 33280 Feb  3 14:37 .
				drwxrws--- 6 root   it_nss 33280 Feb  4 12:59 ..
				drwxrws--- 2 root   it_nss 33280 Feb  4 10:37 valet</pre>
		
				Again, the directory is made writeable by all workgroup members.  
		
				The IT-managed software organized under <code>/opt/shared</code> is a good example of how individual software packages will be installed inside <code>$WORKDIR_SW</code>.  Each package gets its own subdirectory, named in lowercase.  Individual versions of that software package are installed in subdirectories of that:
		
				<pre>$WORKDIR_SW
				|- supercfd/
				|- attic/
				|- 1.0
				|- 1.2
				|- 2.1
				|- valet/</pre>
		
				You may have seen an <code>attic</code> directory in some of the packages in <code>/opt/shared</code> – it is used to hold the original source/installer components for a software package.
	
			#software-workgroup-dirstructure-build
				:markdown
					#### [&quot;Buildmeister&quot;](#software-workgroup-dirstructure-build)
	
					Some workgroups may elect to have one individual maintain their software – call him or her the <em>buildmeister</em>.  A workgroup may have several buildmeisters who each maintain some subset of the software.  At the total opposite extreme, every group member acts as buildmeister for his/her own software.  However your workgroup decides to divide that responsibility, it is best to leave package version directories and their contents ONLY writable by the buildmeister for that package (or version of a package).
	
		#software-workgroup-building
			:markdown
				### [Building from Source](#software-workgroup-building)
	
				Let&#039;s say that version 2.2 of “SuperCFD” has just been released and I&#039;ve logged-in and downloaded the Unix/Linux source code to my desktop.  I copy the source package to DARWIN using <code>scp</code> (or <code>sftp</code>) and the remote directory <code>/lustre/it_nss/sw/supercfd/attic</code>.  Note that this is the value of <code>$WORKDIR_SW</code> plus paths created in the previous section.
	
				On DARWIN, I change to the SuperCFD software directory and prepare for the build of version 2.2:
	
				<pre class="cmd-line">
				$ <b>cd $WORKDIR_SW/supercfd</b>
				$ <b>mkdir 2.2</b>
				$ <b>cd 2.2</b></pre>
	
				With the source code uploaded to the <code>attic</code> directory (or you could use <code>wget «<em><abbr title="Uniform Resource Locator">URL</abbr></em>»</code> to download into the <code>attic</code> directory), I can unpack inside the version 2.2 directory I just created:
	
				<pre class="cmd-line">
				$ <b>pwd</b>
				/lustre/it_nss/sw/supercfd/2.2
				$ <b>tar --bzip -xf ../attic/supercfd-2.2.tar.bz2</b>
				$ <b>ls</b>
				supercfd-2.2
				$ <b>mv supercfd-2.2 src</b></pre>
	
				In this case, the source was a bzip-compressed tar file, so it was unpacked using the <code>tar</code> command.  Other software might be packed as a gzip-compressed tar file or a ZIP file, so the actual command will vary.
	
				The authors of SuperCFD organize their source code along the GNU autoconf guidelines.  The source code directory was renamed from <em>supercfd-2.2</em> to <em>src</em>; when running <code>./configure</code> the install prefix will be set to <code>$WORKDIR_SW/supercfd/2.2</code> which will then have e.g. <code>bin</code>, <code>etc</code>, <code>share</code>, <code>lib</code> directories accompanying the <code>src</code> directory.  For any software package that uses autoconf or CMake, this is the organizational strategy IT has adopted.
	
			#software-workgroup-building-autoconf
				:markdown
					#### [Building for Autoconf/CMake](#software-workgroup-building-autoconf)
	
					The SuperCFD package requires Open <abbr title="Message Passing Interface">MPI</abbr> 1.6 and NetCDF:
		
					<pre class="cmd-line">$ <b>vpkg_require openmpi/1.6.1-intel64</b>
					Adding dependency `intel/2011-8.273-64bit` to your environment
					Adding package `openmpi/1.6.1-intel64` to your environment
					&nbsp;
					$ <b>vpkg_devrequire netcdf/4.1.3-intel64</b>
					Adding dependency `hdf5/1.8.8-intel64` to your environment
					Adding package `netcdf/4.1.3-intel64` to your environment</pre>
		
		
					The <code>vpkg_devrequire</code> command is used for NetCDF because it will set the <code>CPPFLAGS</code> and <code>LDFLAGS</code> environment variables to include the appropriate <code>-I</code> and <code>-L</code> arguments, respectively.  For source code that is configured for build using GNU autoconf or CMake this is usually good enough.
		
					<p class="portlet-msg-alert">Some autoconf or CMake packages may require that the actual location of required libraries be specified.  In such cases, use <code>vpkg_devrequire</code> as above and examine the value of <code>CPPFLAGS</code> or <code>LDFLAGS</code> e.g. using <code>echo $CPPFLAGS</code>.</p>
		
					Some software packages will require the buildmeister to pass arguments to the <code>./configure</code> script or set environment variables.  Keeping track of what parameters went into a build can be difficult.  It&#039;s often a good idea to make note of the commands involved in a text file:
		
					setup.sh</dt>
					<pre class="job-script">vpkg_require openmpi/1.6.1-intel64
					vpkg_devrequire netcdf/4.1.3-intel64
					&nbsp;
					export SUPERCFD_UNITS=METRIC
					&nbsp;
					&nbsp;./configure --prefix=$WORKDIR_SW/supercfd/2.2 \ --solver=auto</pre>
		
		
					In the future the nature of the build is easily determined by looking at this file, versus examining build logs, etc.  The process with CMake would be similar to what is outlined above.
		
					After the software is built (e.g. using <code>make</code> and possibly a <code>make check</code> or <code>make test</code>) and installed (probably using <code>make install</code>) the software is ready for use:
		
					<pre class="cmd-line">
					$ <b>cd ..</b>
					$ <b>pwd</b>
					/lustre/it_nss/sw/supercfd/2.2
					&nbsp;
					$ <b>ls -l</b>
					total 16
					drwxr-sr-x 2 user it_nss 4096 Sep 12  2012 bin
					drwxr-sr-x 2 user it_nss  145 Dec 18  2012 etc
					drwxr-sr-x 4 user it_nss 4096 Sep 12  2012 include
					drwxr-sr-x 4 user it_nss 4096 Sep 12  2012 lib
					drwxr-sr-x 5 user it_nss   48 Sep 12  2012 share
					drwxr-sr-x 9 user it_nss 4096 Sep 12  2012 src</pre>
		
	#udbuild
		:markdown
			## [UDBUILD Software Deployment](#udbuild)
	
			The software installed and deployed on the DARWIN cluster each has it&#039;s own methods for compiling and installing.  To manage this process, the HPC team has a set of standards and technology used to reduce complexity and bring consistency to the process.
	
			Software is built, installed, and accessed using the <a href="#appdev-compenv-valet">VALET</a> system developed by Dr. Jeffrey Frey.  IT developed a set of software helper functions which can be access using <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> by importing the <code>udbuild</code> vpkg.
	
			This page describes the file system layout used by IT, and the anatomy of the <code>udbuild</code> file used to build and deploy software.  Throughout this process, it is helpful to have an understanding of how to use <a href="https://docs.hpc.udel.edu/software/valet/valet">VALET</a> to add and remove software packages from your environment.
	
		#udbuild-filesystem
			:markdown
				### [File System](#udbuild-filesystem)
		
				Software is deployed to <code>/opt/shared</code>.  The <code>udbuild</code> system defaults to this <code>/opt/shared</code> location.  However, this can be changed by setting the <code>UDBUILD_HOME</code> environment variable before initializing the ubuild environment.  A good value for this environment variable for workgroup software is <code>$WORKDIR/sw</code>, and for personal software installation is <code>$HOME/sw</code>. Refer to <a href="#software-workgroup">workgroup software installs</a> for help setting up your directories for workgroup storage.
		
				Beneath this directory should be an <code>attic</code> sub-directory for downloaded software bundles, optionally an <code>add-ons</code> directory for software with optional add-ons, and one sub-directory for each package installed.  These sub-directories should always be in all lower-case letter.  One more layer down should be a directory for each version of the software installed.  It is important understand that on a complex cluster like DARWIN, the same release of a software package may have multiple installations due to various compiler and dependency package requirements.  These directories are the software installation roots.
		
				Underneath the installation root should be a directory called <code>src</code>, which is the un-packed source bundle.  Next to <code>src</code> should be any <code>bin</code>, <code>lib</code>, <code>share</code>, etc. directories neccessary for the final deployment.
		
				An illustrated example of the software directory structure is as such:
		
				* opt
					* shared
						* atlas
							* 3.10.3
							* 3.10.3-intel
							* attic
								* <code>udbuild</code>  <sup><em> - build and install script for atlas</em></sup>
							* python
								* 2.7.8
								* 3.2.5
								* add-ons
									 python2.7.15
										* mpi
											* 20180613
										* python3.2.5
								* attic
									* <code>udbuild</code>   <sup><em> - build and install script for python</em></sup>
			
			
		#udbuild-building-yyy
			:markdown
				### [Building](#udbuild-building)
		
				When building software, the base directory structure (including the <code>attic</code> directory) should be created by you before proceeding further.  You should download the software source bundle into <code>attic</code>.  Then, unpack the software bundle and rename the directory to <code>src</code> as above.  This provides consistency in finding the source bundle and the <code>udbuild</code> file.
		
				Examples of builds are provided below (after the udbuild function documentation).
		
			#udbuild-building-yyy-zzz
				:markdown
					#### [udbuild functions](#udbuild-building-yyy-zzz)
			
					**init_udbuildenv**
			
					This function initializes the udbuild environment. It ensures that you have the required <code>PKGNAME</code> and <code>VERSION</code> environment variables defined, you do not have <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> packages loaded before <code>udbuild</code> in your <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> history (these might affect your build), sets compiler variables like CC, FC, etc., then finally sets your <code>PREFIX</code> and <code>VERSION</code> variables based on it&#039;s command-line.  These command-line options affect <code>init_udbuildenv</code>:
			
					* none - This is equivalent to not supplying any parameters
					* python-addon - Ensure a python <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> package is loaded, and set PREFIX \ appropriately for that python version&#039;s add-on installation path
					* r-addon - Ensure an R <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> package is loaded, and set PREFIX \ appropriately for that R version&#039;s add-on installation path
			
					* node-addon - Ensure a Node.JS version is loaded and set the PREFIX \ appropriately for that Node.JS versions&#039; add-on installation path
					* Any other arguments are treated as the names of <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> packages which are \ loaded and added to the <code>VERSION</code> environment variable.
			
					After all of this, your <code>PREFIX</code> variable will be set to
			
					<pre class="code">${UDBUILD_HOME:-/opt/shared}/$PKGNAME/$VERSION</pre>
			
					**debug**
			
					Drop into a debug shell.  If the debug shell is exited cleanly, then the udbuild script will continue from there.  This is a useful routine to use when creating a udbuild script.  You may want to build the script based on documentation, and run a <code>debug</code> function between a configure and make step to verify the environment looks sane.  After the first successful compile, you can then remove the debug line.
			
					**download**
			
					The <code>download</code> function takes a mandatory and optional argument.  The mandatory, first, argument is the <abbr title="Uniform Resource Locator">URL</abbr> to download.  The resulting file will be named after the last part of the <abbr title="Uniform Resource Locator">URL</abbr> unless the optional second argument is specified.  In this case, the resulting file will be named after the second argument.
			
					If a file with the same name already exists, the download exits successfully without doing anything.  If you wish to re-download the archive, delete or rename the existing one.
			
					**unpack**
			
					The <code>unpack</code> function takes a mandatory and optional argument.  The mandatory, first, argument is the name of an archive file (tar.gz, tar.bz2, tar.xz, zip, etc.) to extract.  The archive will be unpacked into a directory named <code>src</code> under the install prefix (versioned directory for installation) unless the optional second argument is specified, then it will be used in place of the name <code>src</code>.  This directory, and its parents if necessary, will be created prior to extraction.  Source archives using the <code>tar</code> format customarily have a single top-level directory entry which contains the package name and version, this is automatically stripped from the extracted archive.  After completing the extraction process, the <code>unpack</code> function places the udbuild script into the newly created directory to prepare the script for configure and make steps.
			
					If the <code>src</code> (or alternately specified) directory exists, then the archive is not extracted over it.  In this case, the function returns successfully without doing anything.  If you wish to force a new extraction, remove or rename the existing <code>src</code> directory.
			
					**create_valet_template**
			
					Create a YAML based valet package file template and place it in the <code>attic</code> directory if one can be found, and the same directory as the udbuild script if one cannot.  This template is helpful, but usually cannot just be copied into place.  For example, it only knows about the version of the software it is installing, and copying the file blindly would remove entries for all other versions.  Furthermore, the “dependencies” entry is filled with all loaded valet packages, even if they are dependencies of dependencies, and not needed to be explicitly listed.
			
					**valet**
			
			
					This function takes either the name of a package (e.g. <code>openmpi</code>), or a package name/version pair (e.g. <code>openmpi/1.8.2</code>) and return true if there is a <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> package loaded to satisfy this dependency, and false otherwise.
			
					This function can be used along with any other shell constructs, such as <code>if</code> … <code>else</code> … <code>fi</code>, to modify the behaviour of a build.
			
			
					**version**
			
			
					This function takes a string and validates that it exists as a complete entry (i.e. starts, stops, or is bounded by hyphens) in the VERSION string.
			
					This function can be used along with any other shell constructs, such as <code>if</code> … <code>else</code> … <code>fi</code>, to modify the behavior of a build.
			
					**package**
			
			
					This function takes a string and validates that it exists as part of the final package name, which may include features.  This is useful for matching features which are specified to configure a software build but don&#039;t show up in the version string or require a valet package be available, the string “threads” for OpenMP is a good example of using this feature.
			
			
					**ifvalet**
			
					This function is shorthand for <code>if valet “$1”; then shift; eval “$@”; fi</code> to make udbuild scripts simple to read and code.
			
					**ifversion**
			
					This function is shorthand for <code>if version “$1”; then shift; eval “$@”; fi</code> to make udbuild scripts simple to read and code.
			
					**ifpackage**
			
					This function is shorthand for <code>if package “$1”; then shift; eval “$@”; fi</code> to make udbuild scripts simple to read and code.
			
					**udbuildcapture**
			
					Put all screen output into a capture file.  The main purpose of this is to log questions answered during an interactive isntall, to document what choices were made.
			
					**udbuildmon**
			
					This script is helpful to be run during the install phase of a build, for example:
			
					<pre class="code">udbuildmon make install</pre>
			
					It will log all <code>open</code> for write and <code>mkdir</code> system calls and log them to a file named <code>udbuildmon.log</code>.  You can use this log file to verify the build did not write any files to unknown locations.  This function should not be necessary with <a href="http://www.cmake.org" class="urlextern" title="http://www.cmake.org" rel="ugc nofollow">cmake</a> builds, as they normally store this information in an <code>install_manifest.txt</code> file.
			
					**apath**
			
					Append a path to a variable and ensure that variable is exported.  The required first argument is the environment variable name, all remaining arguments are paths to append to the end of the environment variable.  A colon (:) character is used as the delimiter, as is standard in path environment variables.
			
					**ppath**
			
					Prepend a path to a variable similar to <code>apath</code>, but instead of adding the path to the end, add it to the beginning.  Arguments are the same as <code>apath</code>
			
					**rpath**
			
					Remove a path from an environment variable.  The required first argument is the environment variable name.  All remaining arguments are removed from the environment variable.  If an entry exists multiple times, all instances are removed.
			
					**aflag**
			
					Append a flag to an environment variable.  The required first argument is the environment variable name.  All remaining arguments are added to the environment variable.  The <code>aflag</code> variable works under two contexts, which depend on the status of the first argument.  If it is an already defined bash array variable type, then the remaining arguments are added as new elements in the array.  In all other cases, the remaining arguments are added to the string using a space character as a delimiter.
			
					Using a bash array has the advantage of allowing flags which contain whitespace characters.  If this is a requirement, the following steps should be undertaken:
			
					<pre class="code">CONFIG=()                            #Declare CONFIG as a bash array
					aflag CONFIG --prefix=&quot;$PREFIX&quot;      #Specify flags, this one is regular
					aflag CONFIG --title=&quot;My Software&quot;   #Specify flags, his one with spaces
					./configure  &quot;${CONFIG[@]}&quot;          #bash syntax for arrays as arguments</pre>
			
			
					**pflag**
			
					Prepend flags to an environment variable.  This is the same as the <code>aflag</code> function, but it puts its arguments at the beginning of the variable.  Its arguments are identical.
			
					**rflag**
			
					Remove a flag from an environment variable.  This works similar to the <code>rpath</code> variable and also supports bash arrays.
			
					**udexpect**
			
					This is a wrapper around the TCL <code>expect</code> utility to simplify the process of answering questions for interactive builds.  This function accepts an expect script as STDIN (the normal method is via HERE-DOC) and provides all the basics of running <code>expect</code>.  Some standard responses are provided to simplify the process:
			
					* enter - Send a carrige return as if the user pressed their “Enter” key
					* yes - Send the string “yes” as if the user typed “yes” and pressed “Enter”
					* no - Send “no” and press “Enter”
					* y - Send “y” and press “Enter”
					* n - Send “n” and press “Enter”
					* respond text - Send <em>text</em> and press “Enter”
					* keypress c - Send the character <em>c</em> and DO NOT press “Enter”
					* user - Prompt the person at the keyboard for a respone, and send it, press “Enter”
			
					**makeflags_set**
			
					Update a file (presumably a Makefile and specified as the first argument) which uses the syntax “key=value” and update the value of the second argument to be that of the third argument.  This is a simple helper function to make it simple to edit basic information in a Makefile.
			
					**makeflags_prepend**
			
					Update a file similar to <code>makeflags_set</code>, except prepend the third argument to the existing value, instead of replacing it.
			
					**makeflags_append**
			
					Update a file similar to <code>makeflags_set</code>, except append the third argument to the existing value, instead of replacing it.
			
			#udbuild-building-yyy-zzz
				:markdown
					#### [udbuild script examples](#udbuild-building-yyy-zzz)
			
					**simple**
			
					In this example, an easy-to-install software package called cmake is built and isntalled.  It has no software dependencies, and uses the standard <code>configure</code>, <code>make</code>, <code>make install</code> procedure used by very many open source software packages.
			
					To prepare for this build, you would want to create the following directories:
			
					<dl class="file">
					<dt>udbuild</dt>
					<dd><pre class="code file sh">#/bin/bash -l
					&nbsp;
					PKGNAME=cmake                       #These are required variables and must
					VERSION=3.11.3                      #be set before calling 'init_udbuildenv'
					#Setting the &quot;SITEURL&quot; is not required, but is
					#helpful later.
					SITEURL=https://cmake.org/files/v${VERSION%.*}
					&nbsp;
					PKGINFO='Cross-Platform Make'       #Helpful for the ''create_valet_template'' function,
					URLINFO=http://www.cmake.org        #but not required at all
					&nbsp;
					vpkg_devrequire udbuild/2           #Use VALET to load the udbuild v2 environment
					init_udbuildenv                     #Initialize the udbuild environment
					&nbsp;
								#Download the source file if it doesn't already exist
					download $SITEURL/$PKGNAME-$VERSION.tar.gz
					unpack $PKGNAME-$VERSION.tar.gz     #Unpack the source file into $PREFIX/src and cd there
					&nbsp;
					create_valet_template               #Create a template file for valet in the attic (you
								#can't just copy this into place)
					&nbsp;
					./configure --prefix=$PREFIX        #Run your normal configure, without
								#having to define your own PREFIX
								#variable, because 'init_udbuildenv' did
								#that for you.
					&nbsp;
					make                                #normal make commands
					&nbsp;
					udbuildmon make install             #wrap your 'make install' with the
								#'udbuildmon' function to log what files
								#and directories were changed.</pre>
					</dd></dl>
			
					It is imperitive to start udbuild scripts with the string <code>#!/bin/bash -l</code> because this instructs bash to setup the <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> system.
			
					**medium**
			
					<dl class="file">
					<dt>udbuild</dt>
					<dd><pre class="code file sh">#!/bin/bash -l
					&nbsp;
					PKGNAME=cdo
					VERSION=1.9.4
					SITEURL=https://code.mpimet.mpg.de/attachments/download/17374/
					&nbsp;
					vpkg_devrequire udbuild/2 eccodes/2.8.0:threads proj/5.1.0
					vpkg_devrequire netcdf/4.6.1 udunits/2.2.26 fftw/3.3.8
					init_udbuildenv
					&nbsp;
					create_valet_template
					&nbsp;
					download $SITEURL/$PKGNAME-$VERSION.tar.gz
					unpack $PKGNAME-$VERSION.tar.gz
					&nbsp;
					aflag CONFIG --with-szlib=&quot;$SZIP_PREFIX&quot;                
					aflag CONFIG --with-hdf5=&quot;$HDF5_PREFIX&quot;                 
					aflag CONFIG --with-netcdf=&quot;$NETCDF_PREFIX&quot;             
					aflag CONFIG --with-eccodes=&quot;$ECCODES_PREFIX&quot;   
					aflag CONFIG --with-proj=&quot;$PROJ_PREFIX&quot;                 
					aflag CONFIG --with-udunits2=&quot;$UDUNITS_PREFIX&quot;  
					aflag CONFIG --with-threads=yes                         
					aflag CONFIG --with-curl=yes                    
					aflag CONFIG --with-libxml=yes
					&nbsp;
					./configure --prefix=&quot;$PREFIX&quot; $CONFIG
					&nbsp;
					sed -i 's/#include &lt;string&gt;/#include &lt;string.h&gt;/' src/modules.cc
					&nbsp;
					make
					&nbsp;
					udbuildmon make install</pre>
					</dd></dl>
			
			
					In this example, we use <code>vpkg_devrequire</code> to specify additional dependencies needed to build the <code>cdo</code> package.  <code>PREFIX</code>, however, will still be set to <code>/opt/shared/cdo/1.6.4</code>.
			
			
					**complex**
			
					<dl class="file">
					<dt>udbuild</dt>
					<dd><pre class="code file sh">#!/bin/bash -l
					&nbsp;
					PKGNAME=hdf4
					VERSION=4.2.13
					SITEURL=https://support.hdfgroup.org/ftp/HDF/HDF_Current/src
					&nbsp;
					PKGINFO='HDF4: Data Model, Library, &amp; File Format'
					URLINFO=http://www.hdfgroup.org/products/hdf4/
					&nbsp;
					vpkg_devrequire udbuild szip/2.1.1
					init_udbuildenv
					&nbsp;
					create_valet_template
					&nbsp;
					download $SITEURL/hdf-$VERSION.tar.bz2
					unpack hdf-$VERSION.tar.bz2
					&nbsp;
					aflag CFLAGS -fPIC                           
					if valet intel; then   
								aflag CFLAGS -qopt-jump-tables=large
					fi             
					&nbsp;
					aflag CONFIG --disable-netcdf
					aflag CONFIG --with-szlib=$SZIP_PREFIX
					&nbsp;
					# Make shared libraries (sans fortran support):
					./configure --prefix=&quot;$PREFIX&quot; --enable-shared --disable-fortran $CONFIG
					&nbsp;
					make install
					&nbsp;
					make clean
					&nbsp;
					# Make fortran enabled HDF4:
					./configure --prefix=&quot;$PREFIX&quot; --disable-shared --enable-fortran $CONFIG
					&nbsp;
					make install</pre>
					</dd></dl>
			
			
					In this more complicated example, we still need dependencies, but this time one of them will affect the <code>PREFIX</code> variable.  The Intel64 compiler will be used, and PREFIX will be set to <code>/opt/shared/hdf4/4.2.10-intel64</code>.
			
					Furthermore, specific <code>CFLAGS</code> changes will be made for this compiler.  This example also illustrates how the <code>VERSION</code> string can be used.  Here, we would set additional flags for the <code>./configure</code> script if the <code>VERSION</code> string were set to <code>4.2.10-sansnetcdf</code>.  These options allow one build file to build multiple versions of a package, and with only minor changes near the top of the script (namely to the <code>VERSION</code> variable and the <code>init_udbuildenv</code> command-line.
			
					Another interesting thing we do here is to make sure the installation is as complete as possible.  HDF4 does not support shared object files for fortran libraries.  So, first we build the shared objects which are possible, then we enable fortran and ensure the full compliment of archive <code>.a</code> files are present.
			
					**python**
					<dl class="file">
					<dt>udbuild-cdf</dt>
					<dd><pre class="code file sh">#!/bin/bash -l
					&nbsp;
					PKGNAME=cdf
					VERSION=20180613
					PY_VER=3.6.5
					NETCDF_VER=4.6.1
					&nbsp;
					PKGINFO='Python NetCDF, HDF5, HDF4, and dependent Modules'
					URLINFO=https://pypi.org/
					&nbsp;
					vpkg_devrequire udbuild python/$PY_VER netcdf/$NETCDF_VER
					init_udbuildenv python-addon
					&nbsp;
					create_valet_template
					&nbsp;
					ppath PATH        &quot;$PREFIX/bin&quot;
					apath LD_RUN_PATH &quot;$PREFIX/lib&quot;
					&nbsp;
					pip_install CDF cdflib netCDF4 h5netCDF wera2netcdf Puppy nco
					pip_install python-hdf4 hdf5able hdf5pickle hdf5storage
					pip_install dtt2hdf ascii2hdf5 h5json h5pyd hdf5_matlab_reader
					pip_install HDFconvert mrr2c hdfdict LazyHDF5 simpletraj mdtraj
					pip_install mriqc ncagg
					pip_install 'MDAnalysis[analysis, AMBER]'</pre>
					</dd></dl>
					
					
					The python example above is used to install the cdf and related modules as an add-on for python version 3.6.5. It is also considered a complex example since it displays the use of the option <code>python-addon</code> for <code>init_udbuildenv</code> to initialize the udbuild environment and ensure a python <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> package is loaded, sets <code>PREFIX</code> appropriately for python version&#039;s add-on installation path, and utilizes the udbuild <code>pip_install</code> function to call pip with using the correct <code>PREFIX</code>. To use the python add-on module bundle, you can setup a <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> package.  Below is an example <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> yaml package created from the <code>create_valet_template</code> function.  It should be trimmed, as szip, hdf4, and hdf5 are all dependencies of netcdf.  The dependencies section should look like the <code>vpkg_devrequire</code> calls of the udbuild script.
			
					<dl class="file">
					<dt>python-cdf.vpkg_yaml</dt>
					<dd><pre class="code file sh">python-cdf:
								description:          Python NetCDF, HDF5, HDF4, and dependent Modules
								url:                  https://pypi.org/
								prefix:               /opt/shared/python/add-ons
					&nbsp;
								default-version:      &quot;3.6.5:20180613&quot;
					&nbsp;
								versions:
								&quot;3.6.5:20180613&quot;:
								description:      cdf compiled with system compilers
								prefix:           python3.6.5/cdf/20180613
								dependencies:
								-               python/3.6.5
								-               szip/2.1.1
								-               hdf4/4.2.13
								-               hdf5/1.10.2
								-               netcdf/4.6.1</pre>
					</dd></dl>
			
