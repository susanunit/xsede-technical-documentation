#queues
	:markdown
		# [Queues/Partitions](#queues)

		The DARWIN cluster has several partitions (queues) available to specify when running jobs.  These partitions correspond to the various node types available in the cluster:

	%table(border="1" cellspacing="5" cellpadding="3")
		%tr
			%th Partition Name
			%th Description
			%th Node Names
		%tr
			%td standard
			%td Contains all 48 standard memory nodes (64 cores, 512 <abbr title="Gibibtye">GiB</abbr> memory per node)
			%td r1n00 - r1n47
		%tr
			%td large-mem
			%td Contains all 32 large memory nodes (64 cores, 1024 <abbr title="Gibibtye">GiB</abbr> memory per node)
			%td r2l00 - r2l10
		%tr
			%td xlarge-mem
			%td Contains all 11 extra-large memory nodes (64 cores, 2048 <abbr title="Gibibtye">GiB</abbr> memory per node)
			%td r2x00 - r2x10
		%tr
			%td extended-mem
			%td Contains the single extended memory node (64 cores, 1024 <abbr title="Gibibtye">GiB</abbr> memory + 2.73 <abbr title="Tebibtye">TiB</abbr> NVMe swap)
			%td r2e00
		%tr
			%td gpu-t4
			%td Contains all 9 NVIDIA Tesla T4 GPU nodes (64 cores, 512 <abbr title="Gibibtye">GiB</abbr> memory, 1 T4 GPU per node)
			%td r1t00 - r1t07, r2t08
		%tr
			%td gpu-v100
			%td Contains all 3 NVIDIA Tesla V100 GPU nodes (48 cores, 768 <abbr title="Gibibtye">GiB</abbr> memory, 4 V100 GPUs per node)
			%td r2v00 - r2v02
		%tr
			%td gpu-mi50
			%td Contains the single AMD Radeon Instinct MI50 GPU node (64 cores, 512 <abbr title="Gibibtye">GiB</abbr> memory, 1 MI50 GPU)
			%td r2m00
		%tr
			%td idle
			%td Contains all nodes in the cluster, jobs on this partition can be preempted but are not charged against your allocation
			%td  

	#queues-requirements
		:markdown
			## [Requirements](#queues-requirements)

			All partitions on DARWIN have two requirements for submitting jobs:

			1. You must set an allocation workgroup prior to submitting a job by using the <strong>workgroup</strong> command (e.g., <code>workgroup -g it_nss</code>).  This ensures jobs are billed against the correct account in Slurm.

			1. You must explicitly request a single partition in your job submission using <code>--partition</code> or <code>-p</code>.


	#queues-defaults
		:markdown
			## [Defaults and Limits](#queues-defaults)

			All partitions on DARWIN except <code>idle</code> have the following defaults:

			* Default run time of 30 minutes
			* Default resources of 1 node, 1 CPU, and 1 <abbr title="Gibibtye">GiB</abbr> memory
			* Default <strong>no</strong> preemption

			All partitions on DARWIN except <code>idle</code> have the following limits:

			* Maximum run time of 7 days
			* Maximum of 400 jobs per user per partition

			The <code>idle</code> partition has the same defaults and limits as above with the following differences:

			* <strong>Preemption is enabled for all jobs</strong>
			* Maximum of 320 jobs per user
			* Maximum of 640 CPUs per user (across all jobs in the partition)


	#queues-extendedmem
		:markdown
			## [The `extended-mem` Partition](#queues-extendedmem)

			Because access to the swap cannot be limited via Slurm, the <code>extended-mem</code> partition is configured to run all jobs in exclusive user mode.  This means only a single user can be on the node at a time, but that user can run one or more jobs on the node.  All jobs on the node will have access to the full amount of swap available, so care must be taken in usage of swap when running multiple jobs.

	#queues-gpu
		:markdown
			## [The GPU Partitions](#queues-gpu)

			Jobs that will run in one of the GPU partitions must request GPU resources using ONE of the following flags:

		%table(border="1" cellspacing="5" cellpadding="3")
			%tr
				%th Flag
				%th Description
			%tr
				%td <code>--gpus=&lt;count&gt;</code>
				%td &lt;count&gt; GPUs total for the job, regardless of node count
			%tr
				%td <code>--gpus-per-node=&lt;count&gt;</code>
				%td &lt;count&gt; GPUs are required on each node allocated to the job
			%tr
				%td <code>--gpus-per-socket=&lt;count&gt;</code>
				%td &lt;count&gt; GPUs are required on each socket allocated to the job
			%tr
				%td <code>--gpus-per-task=&lt;count&gt;</code>
				%td &lt;count&gt; GPUs are required for each task in the job


		:markdown
			If you do not specify one of these flags, your job will not be permitted to run in the GPU partitions.

			<p class-"portlet-msg-alert">On DARWIN the <code>--gres</code> flag should <strong>NOT</strong> be used to request GPU resources.  The GPU type will be inferred from the partition to which the job is submitted if not specified.</p>

	#queues-idle
		:markdown
			## [The `idle` Partition](#queues-idle)

			The <code>idle</code> partition contains all nodes in the cluster.  Jobs submitted to the <code>idle</code> partition <strong>can be preempted</strong> when the resources are required for jobs submitted to the other partitions.  Your job should support <a href="#scheduling-batch-signals">checkpointing</a> to effectively use the <code>idle</code> partition and avoid lost work.

			<p class="portal-msg-alert">Be aware that implementing checkpointing is highly dependent on the nature of your job and the ability of your code or software to handle interruptions and restarts.  For this reason, we can only provide limited support of the idle partition.</p>

			Jobs in the <code>idle</code> partition that have been running for less than 10 minutes are not considered for preemption by Slurm.  Additionally, there is a 5 minute grace period between the delivery of the initial preemption signal (SIGCONT+SIGTERM) and the end of the job (SIGCONT+SIGTERM+SIGKILL).  This means jobs in the <code>idle</code> partition will have a minimum of 15 minutes of execution time once started.  Jobs submitted using the <code>–requeue</code> flag automatically return to the queue to be rescheduled once resources are available again.

			Jobs that execute in the <code>idle</code> partition do not result in charges against your allocation(s).  However, they do accumulate resource usage for the sake of scheduling priority to ensure fair access to this partition.  If your jobs can support <a href="#scheduling-batch-signals">checkpointing</a>, the <code>idle</code> partition will enable you to continue your research even if you exhaust your allocation(s).

		#queues-idle-requesting
			:markdown
				### [Requesting a Specific Resource Type](#queues-idle-requesting)

				Since the <code>idle</code> partition contains all nodes in the cluster, you will need to request a specific GPU type if your job needs GPU resources.  The three GPU types are:

			%table(border="1" cellspacing="5" cellpadding="3")
				%tr
					%th Type
					%th Description
				%tr
					%td <code>tesla_t4</code>
					%td NVIDIA Tesla T4
				%tr
					%td <code>tesla_v100</code>
					%td NVIDIA Tesla V100
				%tr
					%td <code>amd_mi50</code>
					%td AMD Radeon Instinct MI50

		:markdown
			<p>To request a specific GPU type while using the <code>idle</code> partition, include the <code>--gpus=&lt;type&gt;:&lt;count&gt;</code> flag with your job submission.  For example, <code>--gpus=tesla_t4:4</code> would request 4 NVIDIA Telsa T4 GPUs.</p>


