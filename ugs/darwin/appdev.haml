#appdev
	:markdown
		# [Application Development](#appdev)

		There are three 64-bit compiler suites on DARWIN with Fortran, C and C++:
		
	%table(border="1" cellspacing="5" cellpadding="3")
		%tr
			%th PGI 
			%td Portland Compiler Suite 
		%tr
			%th Intel 
			%td Parallel Studio XE 
		%tr
			%th GCC 
			%td GNU Compiler Collection

	:markdown		
		In addition, multiple versions of OpenJDK are available for compiling java applications on the login node.
		
		DARWIN is based on AMD EPYC processors, please review the following documents if you are planning to compile and install your own software
		
		* <a href="https://www.amd.com/system/files/documents/amd-epyc-7002-tg-hpc-56827.pdf">High Performance Computing (HPC) Tuning Guide for AMD EPYC™ 7002 Series Processors</a> guide for getting started tuning AMD 2nd Gen EPYC™ Processor based systems for HPC workloads.  This is not an all-inclusive guide and some items may have similar, but different, names in specific OEM systems (e.g. OEM-specific BIOS settings). Every HPC workload varies in its performance characteristics.  While this guide is a good starting point, you are encouraged to perform your own performance testing for additional tuning.  This guide also provides suggestions on which items should be the focus of additional, application-specific tuning (November 2020). 
		
		* <a href="http://developer.amd.com/wp-content/resources/56420.pdf">HPC Tuning Guide for AMD EPYC™ Processors</a> guide intended for vendors, system integrators, resellers, system managers and developers who are interested in EPYC system configuration details. There is also a discussion on the AMD EPYC software development environment, and we include four appendices on how to install and run the HPL, HPCG, DGEMM, and STREAM benchmarks. The results produced are ‘good’ but are not necessarily exhaustively tested across a variety of compilers with their optimization flags (December 2018). 
		
		* <a href="https://developer.amd.com/wordpress/media/2020/04/Compiler%20Options%20Quick%20Ref%20Guide%20for%20AMD%20EPYC%207xx2%20Series%20Processors.pdf">AMD EPYC™ 7xx2-series Processors Compiler Options Quick Reference Guide</a>, however we do not have the AOCC compiler (with Flang - Fortran Front-End) installed on DARWIN.
		

	#appdev-compenv
		:markdown
			## [Computing Environment](#appdev-compenv)
	
		#appdev-compenv-unixshell
			:markdown
				### [UNIX Shell](#appdev-compenv-unixshell)
	
				The <em>UNIX shell</em> is the interface to the UNIX operating system. The HPC cluster allows use of the enhanced Bourne shell <em>bash</em>, the enhanced C shell <em>tcsh</em>, and the enhanced Korn shell <em>zsh</em>. IT will primarily support <em>bash</em>, the default shell.
	
				For most Linux systems, the <em>sh</em> shell is the <em>bash</em> shell and the <em>csh</em> shell is the <em>tcsh</em> shell. The remainder of this document will use only <em> bash</em> commands.
	
		#appdev-compenv-envvars
			:markdown
				### [Environment Variables](#appdev-compenv-envvars)
	
				Environment variables store dynamic system values that affect the user environment. For example, the <code>PATH</code> environment variable tells the operating system where to look for executables. Many UNIX commands and tools, such as the compilers, debuggers, profilers, editors, and applications with graphical user interfaces, often look at environment variables for information they need to function.  The man pages for these programs typically have an ENVIRONMENT VARIABLES section with a list of variable names which tells how the program uses the values.
	
				This is why we encourage users to use <a href="#appdev-compenv-valet">VALET</a> to modify your environment versus explicitly setting environment variables.
	
				In bash, a variable must be exported to be used as an environment variable. By convention, environment variables are all uppercase.  You can display a list of currently set environment variables by typing
	
				<pre class="cmd-line">$ <b>env</b></pre>
	
				The "<strong>`echo`</strong>" and "<strong>`export`</strong>" commands will display and set environment variables.
	
			%table(border="1" cellspacing="5" cellpadding="3")
				%tr
					%th  Command 
					%th  Results 
				%tr
					%td <code>  echo $<em>varName</em> </code>
					%td Display specific environment variable 
				%tr
					%td <code>  export <em>varName</em><code>=</code><em>varValue</em> </code>
					%td To set an environment variable to a value 
	
			:markdown
				You can display specific environment variables by typing for example:
	
				<pre class="cmd-line">$ <b>echo $HOME </b>
				$ <b>export FFLAGS='-g -Wall'</b></pre>
	
				The variable <code>FFLAGS</code> will have the value <code>-g -Wall</code> in the shell and exported to programs run from this shell.
	
				<p class="portlet-msg-alert">Spaces are important. Do not put spaces around the equal sign. If the value has spaces, enclose the value in quotes.</p>
	
				<p class="portlet-msg-info">If you see instructions that refer the <code>setenv</code> command, replace it with the <code>export</code> <strong>`bash`</strong> command.  Make sure you use equal signs, with no spaces.  The <code>setenv</code> "<strong>`csh`</strong>" command uses spaces instead of one equal.</p>
	
		#appdev-compenv-scripts
			:markdown
				### [Startup and Logout Scripts](#appdev-compenv-scripts)
	
				All UNIX systems set up a default environment and provide users with the ability to execute additional UNIX commands to alter the environment. These commands are automatically sourced (executed) by your shell and define the normal and environmental variables, command aliases, and functions you need. Additionally, there is a final system-wide startup file that automatically makes global environment changes that IT sets for all users.
	
				You can modify the default environment by adding lines **at the end** of the <code>~/.bash_profile</code> file and the <code>~/.bashrc</code> file. These modifications affect shells started on the login node **and** the compute nodes. In general we recommend that you **should not** modify these files especially when software documentation refers to changing the <code>PATH</code> environment variable, instead use <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> to load the software.
	
				* The <code>~/.bash_profile</code> file&#039;s commands are executed once at login. Add commands to this file to set your login environment and to run startup programs.
				* The <code>~/.bashrc</code> file&#039;s commands are executed by each new shell you start (spawn). Add lines to this file to create aliases and bash functions. Commands such as "<strong>`xterm`</strong>" and "<strong><a href="#appdev-compenv-workgroup">`workgroup`</a></strong>" automatically start a new shell and execute commands in the <code>~/.bashrc</code> file.  The "<strong>`salloc`</strong>" command starts a shell on a compute node and will execute the <code>~/.bashrc</code> file from your home directory, but it does not execute the commands in the <code>~/.bash_profile</code> file. 
	
				You may modify the IT-supplied <code>~/.bash_udit</code> file to be able to use several IT-supplied aliases (commands) and environment settings related to your <a href="#appdev-compenv-workgroup">workgroup</a> and <a href="#appdev-compenv-workgroup">work directory</a> . Edit <code>.bash_udit</code> and follow the directions in the file to activate these options. This is the ONLY way you should set your default workgroup at login.  **DO NOT** add the workgroup command to your <code>.bashrc</code> or <code>.bash_profile</code> as this will likely prevent you from logging in and will cause file transfer programs like WinSCP, sftp or Fetch to break.
	
				Exiting the login session or typing the "<strong>`logout`</strong>" command executes your <code>~/.bash_logout</code> file and terminates your session. Add commands to <code>~/.bash_logout</code> that you want to execute at logout.
	
				To restore the <code>.bash_profile</code>, <code>.bashrc</code>, <code>.bash_udit</code> and <code>.bash_logout</code> files in your home directory to their original state, type from the login node:
	
				<pre class="cmd-line">$ <b>cp /opt/shared/templates/homedir/.bash* $HOME</b></pre>
	
				<strong>Where to put startup commands:</strong> You can put bash commands in either <code>~/.bashrc</code> or <code>~/.bash_profile</code>. Again we do not recommend modifying these files unless you really know what you are doing. Here are general suggestions:
	
				* Even if you have favorite commands from other systems, start by using the supplied files and <strong>only</strong> modify <code>.bash_udit</code> for customization.
	
				* Add essential commands that you fully understand, and keep it simple.  Quoting rules can be complicated.
				* Do not depend on the order of command execution.  Do not assume your environment, set in <code>.bash_profile</code>, will be available when the commands in <code>.bashrc</code> are executed.
				* Do not include commands that spawn new shells, such as <code>workgroup</code>.
				* Be very careful of commands that may produce output.  If you must, only execute them after a test to make sure there is a terminal to receive the output.  Keep in mind using any commands that produce output may break other applications like file transfer (sftp, scp, WinSCP).
	
				* Do not include <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> commands as they produce output and will be a part of every job submitted which could cause conflicts with other applications you are trying to run in your job script.
	
				* Keep a session open on the cluster, so when you make a change that prevents you from logging on you can reverse the last change, or copy the original files from  <code>/opt/shared/templates/homedir/</code> to start over.
	
		#appdev-compenv-workgroup
			:markdown
				### [Using `workgroup` and Directories](#appdev-compenv-workgroup)
	
				There are some key environment variables that are set for you, and are important for your work on any cluster. They are used to find directories for your projects.  These environment variables are set on initial connection to a cluster, and will be changed if you
	
				* set your workgroup (allocation group <em>allocation_group</em> name) with the "<strong>`workgroup`</strong>" command,
				* change to your project directory with the "<strong>`cd`</strong>" command,
				* connect to the compute node resources with "<strong>`salloc`</strong>" (or "<strong>`sbatch`</strong>") command specifying a single partition your allocation workgroup has access based on resources requested for your allocation.
	
			#appdev-compenv-workgroup-loginnode
				:markdown
					#### [Connecting to Login Node](#appdev-compenv-workgroup-loginnode)
	
				The system&#039;s initialization scripts set the values of some environment variables to help use the file systems.   
	
			%table(border="1" cellspacing="5" cellpadding="3")
				%tr
					%th  Variable        
					%th  Value                              
					%th  Description  
				%tr
					%td <code>HOSTNAME</code>        
					%td <em>hostname</em>                   
					%td Host name  
				%tr
					%td <code>USER</code>            
					%td <em>login_name</em>                 
					%td Login name  
				%tr
					%td <code>HOME</code>            
					%td <code>/home/</code><em>uid</em>              
					%td Your home directory  
	
			:markdown
	
				The initialization scripts also set the <em>standard prompt</em> with your login name and a shortened host name.
				For example, if your <em>hostname</em> is <code>darwin.hpc.udel.edu</code> and your <em>login_name</em> is <code>bjones</code>, then the <em>standard prompt</em> will be
	
				<code>[bjones@login00.darwin ~]$ </code>
	
				Clusters may be configured to have multiple login nodes, with one common name for connecting.  For example, on the DARWIN cluster, the hostname may be set to <code>login00</code> or <code>login01</code>, and the standard prompt and window title bar will indicate which login node on <em>darwin</em>. 
	
			#appdev-compenv-workgroup-setting
				:markdown
					#### [Setting Workgroup](#appdev-compenv-workgroup-setting)
	
					To use the compute node resources for a particular allocation group (workgroup), you need to use the "<strong>`workgroup`</strong>" command. 
	
					For example,
	
					<pre class="cmd-line">$ <b>workgroup –g it_css</b></pre>
	
					starts a new shell for the workgroup <code>it_css</code>, and sets the environment variables:
	
				%table(border="1" cellspacing="5" cellpadding="3")
					%tr
						%th  Variable        
						%th  Example Value                
						%th  Description  
					%tr
						%td <code>WORKDIR</code>         
						%td <code>/lustre/it_css</code>    
						%td Allocation workgroup directory, this is not writeable 
					%tr
						%td <code>WORKGROUP</code>       
						%td <code>it_css</code>                   
						%td Current allocation workgroup name 
					%tr
						%td <code>WORKDIR_USER</code>       
						%td <code>/lustre/it_css/users/<em>uid</em></code>                   
						%td Allocation workgroup user directory 
					%tr
						%td <code>WORKDIR_SW</code>       
						%td <code>/lustre/it_css/sw</code>                   
						%td Allocation workgroup software directory 
	
				:markdown
					Use specific environment variables such as `$WORKDIR_USERS` when referring to your allocation workgroup user directory and `$WORKDIR_SW` when referring to your allocation workgroup software directory.  This will improve portability. 
	
					It is always important to be aware of your current allocation workgroup name.  The standard prompt includes the allocation workgroup name, added to your username and host.  You must have an allocation workgroup name in your prompt to use that allocation group&#039;s compute node resources to <a href="#scheduling-batch-submitting">submit jobs</a> using <code>sbatch</code> or <code>salloc</code>. An example prompt after the "<strong>`workgroup`</strong>" command,
	
					<code>[(it_css:bjones)@login01.darwin ~]$ </code>
	
			#appdev-compenv-workgroup-changing
				:markdown
					#### [Changing Directory](#appdev-compenv-workgroup-changing)
	
					When you first connect to the login node, all your commands are executed from your home directory (<code>~</code>). Most of your work will be done in your allocation workgroup directory.  The "<strong>`workgroup`</strong>" command has an option to start you in the allocation workgroup work directory. For example,
	
					<pre class="cmd-line">$ <b>workgroup -cg it_css</b></pre>
	
					will spawn a new shell in the workgroup directory for <code>it_css</code>.
	
					You can always use the <code>cd</code> bash command. 
	
					For example,
	
					<pre class="cmd-line">$ <b>cd users/1201/project/fuelcell</b>
					&nbsp;&nbsp;&nbsp;cd /lustre/it_css/users/1201/project/fuelcell
					&nbsp;&nbsp;&nbsp;cd $WORKDIR/users/1201/project/fuelcell
					&nbsp;&nbsp;&nbsp;cd $WORKDIR_USERS/project/fuelcell</pre>
				
					The first is using a path name relative to the current working directory (implied `./`).  The second, third and fourth use the full path (`$WORKDIR` and `$WORKDIR_USERS` always begins with a `/`). In all cases the directory is changed, and the `$PWD` environment variable is set:
				
			%table(border="1" cellspacing="5" cellpadding="3")
				%tr
					%th  Variable    
					%th  Example Value                           
					%th  Description  
				%tr
					%td <code>PWD</code>         
					%td <code>/lustre/it_css/users/1201/project/fuelcell</code>    
					%td Print (current) working directory 
	
			:markdown
	
				It is always important to be aware of your current working directory.  The standard prompt ends with the basename of `PWD`.  In these two examples the basename is the same, `1201`, but the standard bash `PROMPT_COMMAND`, which is executed every time you change directories, will put the full path of your current working directory in your window title. For example,
	
				<code>bjones@login00.darwin:/lustre/it_css/users/1201</code>
	
			#appdev-compenv-workgroup-connecting
				:markdown
					#### [Connecting to a Compute Node](#appdev-compenv-workgroup-connecting)
	
					To run a job on the compute nodes, you must submit your job script using `sbatch` or start an interactive session using `salloc`.  In both cases, you will be connected to one of your allocation compute nodes based on the partition (queue) specified with a clean environment.  Do not rely on the environment you set on the login node. The variables `USER`, `HOME`, `WORKGROUP`, `WORKDIR`, `WORKDIR_USERS` and `PWD` are all set on the compute node to match the ones you had on the login node, but two variables are set to node-specific values:
	
				%table(border="1" cellspacing="5" cellpadding="3")
					%tr
						%th  Variable        
						%th  Example Value    
						%th  Description  
					%tr
						%td <code>HOSTNAME</code>    
						%td <code>r00n17</code>       
						%td compute node name  
					%tr
						%td <code>TMPDIR</code>      
						%td <code>/tmp</code>         
						%td temporary disk space 
	
	
				:markdown
					An empty directory is created by the SLURM job scheduler that is associated with your job and defined as `TMPDIR`. This is a safe place to store temporary files that will not interfere with other jobs and tasks you or other members of your group may be executing.  This directory is automatically emptied on normal termination of your job. This way the usage on the node scratch file system will not grow over time.
	
					<p class="portlet-msg-alert">Before submitting jobs you <strong>must</strong> first use the "<strong><code>workgroup</code></strong>" command. Type <code>workgroup -h</code> for additional information.  Both "<strong><code>sbatch</code></strong>" and "<strong><code>salloc</code></strong>" will start in the same project directory you set on the login node and will require a single <a href="#queues">partition</a> to be specified to be able to submit a batch or interactive session.</pre>
	
	
		#appdev-compenv-valet
			:markdown
				### [Using VALET](#appdev-compenv-valet)
	
				The <abbr title="University of Delaware">UD</abbr>-developed <em><abbr title="VALET Automates Linux Environment Tasks">VALET</abbr></em> system facilitates your use of compilers, libraries, programming tools and application software. It provides a uniform mechanism for setting up a package&#039;s required UNIX environment. <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> is a recursive acronym for <strong><em>V</em></strong><em>ALET </em><strong><em>A</em></strong><em>utomates </em><strong><em>L</em></strong><em>inux </em><strong><em>E</em></strong><em>nvironment </em><strong><em>T</em></strong><em>asks</em>. It provides functionality similar to the <strong>Modules</strong> package used at other HPC sites.
	
	
				<abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> commands set the basic environment for software. This may include the <code>PATH</code>, <code>MANPATH</code>, <code>INFOPATH</code>, <code>LDPATH</code>, <code>LIBPATH</code> and <code>LD_LIBRARY_PATH</code> environment variables, compiler flags, software directory locations, and license paths. This reduces the need for you to set them or update them yourself when changes are made to system and application software. For example, you might find several versions for a single package name, such as Mathematica/8 and Mathematica/8.0.4. You can even apply <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> commands to packages that you install or alter its actions by customizing <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr>&#039;s configuration files. Type <strong>`man valet`</strong> for instructions or see the <a href="https://docs.hpc.udel.edu/software/valet/valet">VALET software documentation</a> for complete details.
	
				The table below shows the basic informational commands for <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr>. In subsequent sections, <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> commands are illustrated in the contexts of application development (e.g., compiling, using libraries) and running IT-installed applications.
	
		%table(border="1" cellspacing="5" cellpadding="3")
			%tr
				%th  Command        
				%th  Function     
			%tr
				%td <code>vpkg_help</code>  
				%td <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> help.  
			%tr
				%td <code>vpkg_list</code>  
				%td List the packages that have <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> configuration files.  
			%tr
				%td <code>vpkg_versions <em>pkgid</em>  </code>
				%td List versions available for a single package.  
			%tr
				%td <code>vpkg_info <em>pkgid</em>  </code>
				%td Show information for a single package (or package version).  
			%tr
				%td <code>vpkg_require <em>pkgid</em>  </code>
				%td Configure environment for one or more <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> packages.  
			%tr
				%td <code>vpkg_devrequire <em>pkgid</em> </code>
				%td Configure environment for one or more <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> packages including software development variables such as <code>CPPFLAGS</code> and <code>LDFLAGS</code>.  
			%tr
				%td <code>vpkg_rollback <em># or all</em> </code>
				%td Each time <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> changes the environment, it makes a snapshot of your environment to which it can return.<br/> <code>vpkg_rollback</code> attempts to restore the UNIX environment to its previous state. You can specify a number (<code>#</code>) to revert one or more prior changes to the environment or <code>all</code> to remove all changes.  
			%tr
				%td <code>vpkg_history</code>  
				%td List the versioned packages that have been added to the environment.  
			%tr
				%td <code>man valet</code>  
				%td Complete documentation of <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> commands.  
		
