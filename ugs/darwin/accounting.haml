#jobaccounting
	:markdown
		# [Job Accounting on DARWIN](#jobaccounting)

		Accounting for jobs on DARWIN varies with the type of node used within a given allocation type.  There are two types of allocations:

		1. Compute - for CPU based nodes with 512 GiB, 1024 GiB, or 2048 GiB of RAM
		1. GPU - for GPU based nodes with NVIDIA Tesla T4, NVIDIA Tesla V100, or AMD Radeon Instinct MI50 GPUs

		For all allocations and node types, usage is defined in terms of a Service Unit (SU).  The definition of an SU varies with the type of node being used.

		<strong>IMPORTANT:</strong> When a job is submitted, the SUs will be calculated and pre-debited based on the resources requested thereby putting a hold on and deducting the SUs from the allocation credit for your project/workgroup. However, once the job completes the amount of SUs debited will be based on the actual time used. Keep in mind that if you request 20 cores and your job really only takes advantage of 10 cores, then the job will still be billed based on the requested 20 cores.  And specifying a time limit of 2 days versus 2 hours may prevent others in your project/workgroup from running jobs as those SUs will be unavailable until the job completes. On the other hand, if you do not request enough resources and your job fails (i.e. did not provide enough time, enough cores, etc.), you will still be billed for those SUs. See <a href="#scheduling-sbatch-options">Scheduling Jobs Command options</a> for help with specifying resources.

		<p class="portlet-msg-info"><strong>Moral of the story:</strong> Request only the resources needed for your job.  Over or under requesting resources results in wasting your allocation credits for everyone in your project/workgroup.</p>

	#jobaccounting-compute
		:markdown
			## [Compute Allocations](#jobaccounting-compute)

			A Compute allocation on DARWIN can be used on any of the four compute node types.  Each compute node has 64 cores but the amount of memory varies by node type.  The available resources for each node type are below:

		%table(border="1" cellspacing="5" cellpadding="3")
			%tr
				%th Compute Node         
				%th Number of Nodes 
				%th Total Cores 
				%th Memory per Node     
				%th Total Memory
			%tr
				%td Standard             
				%td 48
				%td 3,072
				%td 512 GiB
				%td 24 TiB
			%tr
				%td Large Memory         
				%td 32
				%td 2,048
				%td 1024 GiB
				%td 32 TiB
			%tr
				%td Extra-Large Memory   
				%td 11
				%td 704
				%td 2,048 GiB
				%td 22 TiB
			%tr
				%td Extended Memory      
				%td 1
				%td 64
				%td 1024 GiB + 2.73 TiB<sup><a href="#fn__1">1)</a></sup>
				%td 3.73 TiB
			%tr
				%td <strong>Total</strong>            
				%td 92
				%td 5,888
				%td 
				%td 81.73 TiB
		
		:markdown
			<sup><a href="#fnt__1">1)</a></sup> 1024 GiB of system memory and 2.73 TiB of swap on high-speed Intel Optane NVMe storage

			A Service Unit (SU) on compute nodes corresponds to the use of one compute core for one hour.  The number of SUs charged for a job is based on the fraction of total cores or fraction of total memory the job requests, whichever is larger.  This results in the following SU conversions:

		%table(border="1" cellspacing="5" cellpadding="3")
			%tr
				%th Compute Node         
				%th SU Conversion                              
			%tr
				%td Standard             
				%td 1 unit = 1 core + 8 GiB RAM for one hour   
			%tr
				%td Large Memory         
				%td 1 unit = 1 core + 16 GiB RAM for one hour  
			%tr
				%td Extra-Large Memory   
				%td 1 unit = 1 core + 32 GiB RAM for one hour  
			%tr
				%td Extended Memory      
				%td 64 units = 64 cores + 1024 GiB RAM + 2.73 TiB swap for one hour<sup><a href="#fn__2" id="fnt__2">2)</a></sup>  

		:markdown
			<sup><a href="#fnt__2" id="fn__2">2)</a></sup> always billed as the entire node

			See the examples below for illustrations of how SUs are billed by the intervals in the conversion table:

		%table(border="1" cellspacing="5" cellpadding="3")
			%tr
				%th Node Type          
				%th Cores          
				%th Memory                
				%th SUs billed per hour  
			%tr
				%td Standard           
				%td 1
				%td 1 GiB to 8 GiB
				%td 1 SU  
			%tr
				%td Standard           
				%td 1
				%td 504 GiB to 512 GiB
				%td 64 SUs<sup><a href="#fn__3" id="fnt__3">3)</a></sup>  
			%tr
				%td Standard           
				%td 64
				%td 1 GiB to 512 GiB
				%td 64 SUs  
			%tr
				%td Standard           
				%td 2
				%td 1 GiB to 16 GiB
				%td 2 SUs  
			%tr
				%td Large Memory       
				%td 2
				%td &gt; 32 GiB and ≤ 48 GiB
				%td 3 SUs<sup><a href="#fn__4" id="fnt__4">4)</a></sup>  
		
		:markdown
			<sup><a href="#fnt__3" id="fn__3">3)</a></sup> 512 GiB RAM on a standard node is equivalent to using all 64 cores, so you are charged as if you used 64 cores  
			<sup><a href="#fnt__4" id="fn__4">4)</a></sup> RAM usage exceeds what is available with 2 cores on a large memory node, so you are charged as if you used 3 cores

			Note that these are estimates based on nominal memory.  Actual charges are based on available memory which will be lower than nominal memory due to the memory requirements for the OS and system daemons.
		

	#jobaccounting-gpu
		:markdown
			## [GPU Allocations](#jobaccounting-gpu)

			A GPU allocation on DARWIN can be used on any of the three GPU node types.  The NVIDIA-T4 and AMD MI50 nodes have 64 cores each, while the NVIDIA-V100 nodes have 48 cores each.  The available resources for each node type are below:

		%table(border="1" cellspacing="5" cellpadding="3")
			%tr
				%th GPU Node             
				%th Number of Nodes 
				%th Total Cores 
				%th Memory per Node     
				%th Total Memory  
				%th Total GPUs
			%tr
				%td nvidia-T4            
				%td 9
				%td 576
				%td 512 GiB
				%td 4.5 TiB
				%td 9
			%tr
				%td nvidia-V100          
				%td 3
				%td 144
				%td 768 GiB
				%td 2.25 TiB
				%td 12
			%tr
				%td AMD-MI50             
				%td 1
				%td 64
				%td 512 GiB
				%td .5 TiB
				%td 1
			%tr
				%td <strong>Total</strong>            
				%td 13
				%td 784
				%td 
				%td 7.25 TiB
				%td 22
		
		:markdown
			A Service Unit (SU) on GPU nodes corresponds to the use of one GPU device for one hour.  The number of SUs charged for a job is based on the fraction of total GPUs, fraction of total cores, or fraction of total memory the job requests, whichever is larger.  Because the NVIDIA T4 and AMD MI50 nodes only have 1 GPU each, you have access to all available memory and cores for 1 SU.  The NVIDIA V100 nodes have 4 GPUs each, so the available memory and cores per GPU is 1/4 of the total available on a node.  This results in the following SU conversions:
		
		%table(border="1" cellspacing="5" cellpadding="3")
			%tr
				%th GPU Node        
				%th SU Conversion                                          
			%tr
				%td nvidia-T4       
				%td 1 unit = 1 GPU + 64 cores + 512 GiB RAM for one hour   
			%tr
				%td AMD-MI50        <td class="col1 leftalign">1 unit = 1 GPU + 64 cores + 512 GiB RAM for one hour   
			%tr
				%td nvidia-V100     
				%td 1 unit = 1 GPU + 12 cores + 192 GiB RAM for one hour   
		
		<p>See the examples below for illustrations of how SUs are billed by the intervals in the conversion table:</p>

		%table(border="1" cellspacing="5" cellpadding="3")
			%tr
				%th Node Type      
				%th GPUs 
				%th Cores          
				%th Memory                  
				%th SUs billed per hour  
			%tr
				%td nvidia-T4      
				%td 1
				%td 1 to 64
				%td 1 GiB to 512 GiB
				%td 1 SU   
			%tr
				%td nvidia-T4      
				%td 2
				%td 2 to 128
				%td 2 GiB to 1024 GiB
				%td 2 SUs  
			%tr
				%td AMD-MI50       
				%td 1
				%td 1 to 64
				%td 1 GiB to 512 GiB
				%td 1 SU   
			%tr
				%td nvidia-V100    
				%td 1
				%td 1 to 12
				%td 1 GiB to 192 GiB
				%td 1 SU   
			%tr
				%td nvidia-V100    
				%td 2
				%td 1 to 24
				%td 1 GiB to 384 GiB
				%td 2 SUs  
			%tr
				%td nvidia-V100    
				%td 1
				%td 25 to 48
				%td 1 GiB to 192 GiB
				%td 2 SUs<sup><a href="#fn__5" id="fnt__5" class="fn_top">5)</a></sup>  
			%tr
				%td nvidia-V100    
				%td 1
				%td 1 to 24
				%td &gt; 192 GiB and ≤ 384 GiB
				%td 2 SUs<sup><a href="#fn__6" id="fnt__6" class="fn_top">6)</a></sup>  
		
		
		:markdown
			<sup><a href="#fnt__5" id="fn__5">5)</a></sup> billed as if you were using 2 GPUs due to the proportion of CPU cores used  
			<sup><a href="#fnt__6" id="fn__6">6)</a></sup> billed as if you were using 2 GPUs due to the proportion of memory used

			Note that these are estimates based on nominal memory.  Actual charges are based on available memory which will be lower than nominal memory due to the memory requirements for the OS and system daemons.
		
	#jobaccounting-idle
		:markdown
			## [The `idle` Partition](#jobaccounting-idle)

			Jobs that execute in the <a href="#queues-idle">idle partition</a> do not result in charges against your allocation(s).  If your jobs can support <a href="#scheduler-handling">checkpointing</a>, the idle partition will enable you to continue your research even if you exhaust your allocation(s).  However, jobs submitted to the other partitions which do get charged against allocations <strong>will take priority</strong> and may cause <code>idle</code> partition jobs to be <strong>preempted</strong>.

			<p class="portlet-msg-info">Since jobs in the <code>idle</code> partition do not result in charges you will not see them in the output of the <code>sproject</code> command documented below.  You can still use <a href="#checking-job-status">standard Slurm commands to check the status</a> of those jobs.</p>


#allocationusage
	:markdown
		# [Monitoring Allocation Usage](#allocationusage)

	#allocationusage-sproject
		:markdown
			## [The `sproject` Command](#allocationusage-sproject)

			UD IT has created the <code>sproject</code> command to allow various queries against allocations (UD and XSEDE) on DARWIN.  You can see the help documentation for <code>sproject</code> by running <code>sproject -h</code> or <code>sproject --help</code>.  The <code>-h/--help</code> flag also works for any of the subcommands:  <code>sproject allocations -h</code>, <code>sproject projects -h</code>, <code>sproject jobs -h</code>, or <code>sproject failures -h</code>.

			For all <code>sproject</code> commands you can specify an <em>output-format</em> of <code>table</code>, <code>csv</code>, or <code>json</code> using the <code>--format &lt;<em>output-format</em>&gt;</code> or <code>-f &lt;<em>output-format</em>&gt;</code> options.

		#allocationusage-sproject-allocations
			:markdown
				### [`sproject allocations`](#allocationusage-sproject-allocations)

				The <code>allocations</code> subcommand shows information for resource allocations granted to projects/workgroups on DARWIN to which you are a member. To see a specific workgroup&#039;s allocations, use the <code>-g &lt;<em>workgroup</em>&gt;</code> option as in this example for workgroup <code>it_css</code>:

				<pre class="cmd-line">$ <b>sproject allocations -g it_css</b>
				Project id Alloc id Alloc descr Category RDR Start date                End date
				---------- -------- ----------- -------- --- ------------------------- -------------------------
				2&nbsp;3 it_css::cpu startup  cpu 2021-07-12 00:00:00-04:00 2021-07-25 23:59:59-04:00
				2&nbsp;4 it_css::gpu startup  gpu 2021-07-12 00:00:00-04:00 2021-07-25 23:59:59-04:00
				2&nbsp;43 it_css::cpu startup  cpu 2021-07-26 00:00:00-04:00 2022-07-31 23:59:50-04:00
				2&nbsp;44 it_css::gpu startup  gpu 2021-07-26 00:00:00-04:00 2022-07-31 23:59:50-04:00</pre>


				The <code>--detail</code> flag will show additional information reflecting the credits, running + completed job charges, debits, and balance of each allocation:

				<pre class="cmd-line">$ <b>sproject allocations -g it_css --detail</b>
				Project id Alloc id Alloc descr Category RDR Credit Run+Cmplt Debit Balance
				---------- -------- ----------- -------- --- ------ --------- ----- -------
				2&nbsp;3 it_css::cpu startup  cpu 108500         0 -1678  106822
				2&nbsp;4 it_css::gpu startup  gpu    417         0   -16     401
				2&nbsp;43 it_css::cpu startup  cpu  33333         0     0   33333
				2&nbsp;44 it_css::gpu startup  gpu  33333         0     0   33333</pre>


				The <code>--by-user</code> flag is helpful to see detailed allocation usage broken out by project user:

				<pre class="cmd-line">$ <b>sproject allocations -g it_css --by-user</b>
				Project id Alloc id Alloc descr Category RDR User     Transaction Amount Comments
				---------- -------- ----------- -------- --- -------- ----------- ------ -------------------------
				2        3 it_css::cpu startup  cpu          credit         167 Request approved jnhuffma
				3 it_css::cpu                       credit      108333 Testing allocation
				3 it_css::cpu              jnhuffma debit        -1678
				2        4 it_css::gpu startup  gpu jnhuffma debit          -16
				4 it_css::gpu                       credit         417 Testing allocation
				2       43 it_css::cpu startup  cpu          credit       33333
				2       44 it_css::gpu startup  gpu          credit       33333</pre>

		#allocationusage-sproject-projects
			:markdown
				### [`sproject projects`](#allocationusage-sproject-projects)

				The <code>projects</code> subcommand shows information (such as the project id, group id, name, and creation date) for projects/workgroups on DARWIN to which you are a member. To see a specific project/workgroup, use the <code>-g &lt;<em>workgroup</em>&gt;</code> option as in this example for workgroup <code>it_css</code>:

				<pre class="cmd-line">$ <b>sproject projects -g it_css</b>
				Project id Account Group id Group name  Creation date
				---------- ------- -------- ----------  -------------------------
				2 it_css      1002 it_css      2021-07-12 14:51:57-04:00</pre>


				Adding the <code>--detail</code> flag will also show each allocation associated with the project.

				<pre class="cmd-line">$ <b>sproject projects -g it_css --detail</b>
				Project id Account Group id Group name Allocation id Category RDR Start date                End date                   Creation date
				---------- ------- -------- ---------- ------------- -------- --- ------------------------- -------------------------  -------------------------
				2 it_css      1002 it_css                 3 startup  cpu 2021-07-12 00:00:00-04:00 2021-07-25 23:59:59-04:00  2021-07-12 15:00:46-04:00
				4 startup  gpu 2021-07-12 00:00:00-04:00 2021-07-25 23:59:59-04:00  2021-07-12 15:00:54-04:00
				43 startup  cpu 2021-07-26 00:00:00-04:00 2022-07-31 23:59:50-04:00  2021-07-26 14:47:56-04:00
				44 startup  gpu 2021-07-26 00:00:00-04:00 2022-07-31 23:59:50-04:00  2021-07-26 14:47:56-04:00</pre>

		#allocationusage-sproject-jobs
			:markdown
				### [`sproject jobs`](#allocationusage-sproject-jobs)

				The <code>jobs</code> subcommand shows information (such as the Slurm job id, owner, and amount charged) for individual jobs billed against resource allocations for projects/workgroups on DARWIN to which you are a member. Various options are available for sorting and filtering, use <code>sproject jobs -h</code> for complete details. To see jobs associated with a specific project/workgroup, use the <code>-g &lt;<em>workgroup</em>&gt;</code> option as in this example for workgroup <code>it_css</code>:

				<pre class="cmd-line">$ <b>sproject jobs -g it_css</b>
				Activity id Alloc id Alloc descr  Job id Owner    Status    Amount  Creation date            
				----------- -------- ------------ ------ -------- --------- ------  -------------------------
				25000       43 it_css::cpu   80000 bjones   executing  -3966  2021-08-10 18:27:59-04:00
				25001       43 it_css::cpu   80001 bjones   executing  -3966  2021-08-10 18:52:11-04:00
				25002       43 it_css::cpu   80002 bjones   executing  -4200  2021-08-11 13:06:26-04:00
				25003       43 it_css::cpu   80003 bjones   executing  -1200  2021-08-11 16:07:42-04:00</pre>


				Jobs that complete execution will be displayed with a status of <code>completed</code> and the actual billable amount used by the job.  At the top and bottom of each hour, completed jobs are <em>resolved</em> into per-user debits and disappear from the <code>jobs</code> listing (see the <strong>sproject allocations</strong> section above for the display of resource allocation credits, debits, and pre-debits).

		#allocationusage-sproject-failures
			:markdown
				### [`sproject failures`](#allocationusage-sproject-failures)

				The <code>failures</code> subcommand shows information (such as the Slurm job id, owner, and amount charged) for all jobs that failed to execute due to insufficient allocation balance on resource allocations for projects/workgroups on DARWIN to which you are a member. Various options are available for sorting and filtering, use <code>sproject failures -h</code> for complete details. To see failures associated with jobs run as a specific project/workgroup, use the <code>-g &lt;<em>workgroup</em>&gt;</code> option as in this example for workgroup <code>it_css</code>:

				<pre class="cmd-line">$ <b>sproject failures -g it_css</b>
				Job id Error message
				------ -------------------------------------------------------------
				60472 Requested allocation has insufficient balance: 3641 &lt; 7680
				60473 Requested allocation has insufficient balance: 3641 &lt; 7680
				60476 Requested allocation has insufficient balance: 3641 &lt; 7680
				60474 Requested allocation has insufficient balance: 3641 &lt; 7680
				60475 Requested allocation has insufficient balance: 3641 &lt; 7680
				60471 Requested allocation has insufficient balance: 3641 &lt; 7680
				60478 Requested allocation has insufficient balance: 3641 &lt; 7680
				60477 Requested allocation has insufficient balance: 3641 &lt; 7680
				60487 Requested allocation has insufficient balance: 12943 &lt; 368640
				60488 Requested allocation has insufficient balance: 12943 &lt; 276480
				60489 Requested allocation has insufficient balance: 12943 &lt; 184320
				60490 Requested allocation has insufficient balance: 12943 &lt; 76800
				60491 Requested allocation has insufficient balance: 12943 &lt; 76800
				60492 Requested allocation has insufficient balance: 12943 &lt; 61440
				60493 Requested allocation has insufficient balance: 12943 &lt; 15360</pre>


				Adding the <code>--detail</code> flag provides further information such as the owner, amount, and creation date.  

				<pre class="cmd-line">$ <b>sproject failures -g it_css --detail</b>
				Activity id Alloc id Alloc descr Job id Owner    Amount Error message                                                  Creation date
				----------- -------- ----------- ------ -------- ------ -------------------------------------------------------------  -------------------------
				75        3 it_css::cpu  60472 jnhuffma    128 Requested allocation has insufficient balance: 3641 &lt; 7680     2021-07-13 08:57:10-04:00
				76        3 it_css::cpu  60473 jnhuffma    128 Requested allocation has insufficient balance: 3641 &lt; 7680     2021-07-13 08:57:10-04:00
				77        3 it_css::cpu  60476 jnhuffma    128 Requested allocation has insufficient balance: 3641 &lt; 7680     2021-07-13 08:57:10-04:00
				78        3 it_css::cpu  60474 jnhuffma    128 Requested allocation has insufficient balance: 3641 &lt; 7680     2021-07-13 08:57:10-04:00
				79        3 it_css::cpu  60475 jnhuffma    128 Requested allocation has insufficient balance: 3641 &lt; 7680     2021-07-13 08:57:10-04:00
				80        3 it_css::cpu  60471 jnhuffma    128 Requested allocation has insufficient balance: 3641 &lt; 7680     2021-07-13 08:57:10-04:00
				81        3 it_css::cpu  60478 jnhuffma    128 Requested allocation has insufficient balance: 3641 &lt; 7680     2021-07-13 08:57:13-04:00
				82        3 it_css::cpu  60477 jnhuffma    128 Requested allocation has insufficient balance: 3641 &lt; 7680     2021-07-13 08:57:13-04:00
				92        3 it_css::cpu  60487 jnhuffma   6144 Requested allocation has insufficient balance: 12943 &lt; 368640  2021-07-13 11:09:00-04:00
				93        3 it_css::cpu  60488 jnhuffma   4608 Requested allocation has insufficient balance: 12943 &lt; 276480  2021-07-13 11:09:24-04:00
				94        3 it_css::cpu  60489 jnhuffma   3072 Requested allocation has insufficient balance: 12943 &lt; 184320  2021-07-13 11:09:40-04:00
				95        3 it_css::cpu  60490 jnhuffma   1280 Requested allocation has insufficient balance: 12943 &lt; 76800   2021-07-13 11:15:08-04:00
				96        3 it_css::cpu  60491 jnhuffma   1280 Requested allocation has insufficient balance: 12943 &lt; 76800   2021-07-13 11:16:00-04:00
				97        3 it_css::cpu  60492 jnhuffma   1024 Requested allocation has insufficient balance: 12943 &lt; 61440   2021-07-13 11:19:16-04:00
				98        3 it_css::cpu  60493 jnhuffma    256 Requested allocation has insufficient balance: 12943 &lt; 15360   2021-07-13 11:20:01-04:00</pre>

	#allocationusage-xsede
		:markdown
			## [XSEDE Allocations](#allocationusage-xsede)

			For XSEDE allocations on DARWIN, you may use the <a href="https://portal.xsede.org/my-xsede">XSEDE user portal</a> to check allocation usage, however keep in mind using the <a href="#allocationusage-sproject">sproject</a> command available on DARWIN will provide the most up-to-date allocation usage information since the XSEDE Portal will only be updated nightly.

	#allocationusage-storage
		:markdown
			## [Storage Allocations](#allocationusage-storage)

			Every DARWIN Compute or GPU allocation has a storage allocation associated with it on the DARWIN Lustre file system.  These allocations are measured in tebibytes and the default amount is 1 TiB.  There are no SUs deducted from your allocation for the space you use, but you will be limited to a storage quota based on your awarded allocation.

			Each project/workgroup has a folder associated with it referred to as <a href="#files-quotas-workgroup">workgroup storage</a>. Every file in that folder will count against that project/workgroup&#039;s allocated quota for their workgroup storage.

			You can use the <a href="#files-quotas">my_quotas</a> command to check storage usage.  

			
