<span style="font-size:225%; font-weight:bold;">University of Delaware DARWIN User Guide<br></span>
<i>Last update: July 27, 2022</i>

#gettingstarted
	:markdown
		# [Getting started on DARWIN](#gettingstarted)

		DARWIN (Delaware Advanced Research Workforce and Innovation Network) is a big data and high performance computing system designed to catalyze Delaware research and education funded by a $1.4 million grant from the National Science Foundation (NSF). This award will establish the DARWIN computing system as an XSEDE Level 2 Service Provider in Delaware contributing 20% of DARWIN&#039;s resources to XSEDE: Extreme Science and Engineering Discovery Environment. DARWIN has 105 compute nodes with a total of 6672 cores, 22 GPUs, 100TB of memory, and 1.2PB of disk storage. See <a href="#gettingstarted-config-compute">compute nodes</a> and <a href="#gettingstarted-storage">storage</a> for complete details on architecture. 

	%figure
		<img alt="DARWIN Data Center" src="/documents/10308/2983571/darwin-data_center.jpg/8585d2ea-f1eb-4ba5-a2f0-84bf674e1054?t=1654096921520" style="width: 600px; border-width: 1px; border-style: solid; height: 346px;" />  
		%figcaption
			Figure 1. Fish-eye front view of DARWIN in the computing center  


	#gettingstarted-config
		:markdown
			## [Configuration](#gettingstarted-config)
	
			The DARWIN cluster is being set up to be very similar to the existing Caviness cluster, and will be familiar to those currently using Caviness.  However DARWIN is a NSF funded HPC resource available via committee reviewed allocation request process similar to XSEDE allocations.
	
			An HPC system always has one or more public-facing systems known as <em>login nodes</em>. The login nodes are supplemented by many <em>compute nodes</em> which are connected by a private network.  One or more <em>head nodes</em> run programs that manage and facilitate the functioning of the cluster. (In some clusters, the head node functionality is present on the <em>login nodes</em>.) Each compute node typically has several multi-core processors that share memory. Finally, all the nodes share one or more filesystems over a high-speed network.
	
	%figure
		<img alt="" src="/documents/10308/2983571/DARWIN+topology/e3b41b2f-653b-4eae-8fc1-57d337630c6c?t=1652194932792" style="border: 1px; width: 683px; height: 798px;" /></p>
		%figcaption
			Figure 2. DARWIN Configuration


		#gettingstarted-config-login
			:markdown
				### [Login nodes](#gettingstarted-config-login)
		
				Login (head) nodes are the gateway into the cluster and are shared by all cluster users.  Their computing environment is a full standard variant of Linux configured for scientific applications.  This includes command documentation (man pages), scripting tools, compiler suites, debugging/profiling tools, and application software.  In addition, the login nodes have several tools to help you move files between the HPC filesystems and your local machine, other clusters, and web-based services.
		
				<p class="portlet-msg-info"> Login nodes should be used to set up and submit job workflows and to compile programs. You should generally use compute nodes to run or debug application software or your own executables.</p>
		
				If your work requires highly interactive graphics and animations, these are best done on your local workstation rather than on the cluster.  Use the cluster to generate files containing the graphics information, and download them from the HPC system to your local system for visualization.
		
				When you use SSH to connect to <code>darwin.hpc.udel.edu</code> your computer will choose one of the login (head) nodes at random.  The default command line prompt clearly indicates to which login node you have connected: for example, <code>[bjones@login00.darwin ~]$</code> is shown for account <code>bjones</code> when connected to login node <code>login00.darwin.hpc.udel.edu</code>.
		
				<p class="portlet-msg-alert">Only use SSH to connect to a specific login node if you have existing processes present on it.  For example, if you used the <code>screen</code> or <code>tmux</code> utility to preserve your session after logout.</p>
		
		#gettingstarted-config-compute
			:markdown
				### [Compute nodes](#gettingstarted-config-compute)
		
				There are many compute nodes with different configurations.  Each node consists of multi-core processors (CPUs), memory, and local disk space.  Nodes can have different <abbr title="Operating System">OS</abbr> versions or <abbr title="Operating System">OS</abbr> configurations, but this document assumes all the compute nodes have the same <abbr title="Operating System">OS</abbr> and almost the same configuration.  Some nodes may have more cores, more memory, GPUs, or more disk.
		
				All compute nodes are now available and configured for use.  Each compute node has 64 cores, so the compute resources available at initial early access will be the following:
		
		%table(border="1" cellspacing="5" cellpadding="3")
			%tr
				%th Compute Node         
				%th Number of Nodes 
				%th Node Names 
				%th Total Cores 
				%th Memory per Node  
				%th Total Memory  
				%th Total GPUs
			%tr
				%td Standard             
				%td 48
				%td r1n00 - r1n47  
				%td 3,072
				%td 512 <abbr title="Gibibtye">GiB</abbr>
				%td 24 <abbr title="Tebibtye">TiB</abbr>
				%td        
			%tr
				%td Large Memory         
				%td 32
				%td r2l00 - r2l31  
				%td 2,048
				%td 1024 <abbr title="Gibibtye">GiB</abbr>
				%td 32 <abbr title="Tebibtye">TiB</abbr>
				%td        
			%tr
				%td Extra-Large Memory   
				%td 11
				%td r2x00 - r2x10  
				%td 704
				%td 2,048 <abbr title="Gibibtye">GiB</abbr>
				%td 22 <abbr title="Tebibtye">TiB</abbr>
				%td        
			%tr
				%td nVidia-T4            
				%td 9
				%td r1t00 - r1t07, r2t08  
				%td 576
				%td 512 <abbr title="Gibibtye">GiB</abbr>
				%td 4.5 <abbr title="Tebibtye">TiB</abbr>
				%td 9
			%tr
				%td nVidia-V100          
				%td 3
				%td r2v00 -  02  
				%td 144
				%td 768 <abbr title="Gibibtye">GiB</abbr>
				%td 2.25 <abbr title="Tebibtye">TiB</abbr>
				%td 12
			%tr
				%td AMD-MI50             
				%td 1
				%td r2m00  
				%td 64
				%td 512 <abbr title="Gibibtye">GiB</abbr>
				%td .5 <abbr title="Tebibtye">TiB</abbr>
				%td 1
			%tr
				%td Extended Memory      
				%td 1
				%td r2e00  
				%td 64
				%td 1024 <abbr title="Gibibtye">GiB</abbr> + 2.73 <abbr title="Tebibtye">TiB</abbr><sup><a href="#fn__1">1)</a></sup>
				%td 3.73 <abbr title="Tebibtye">TiB</abbr>
				%td        
			%tr
				%td <strong>Total</strong>            
				%td 105
				%td 
				%td 6,672
				%td 
				%td 88.98 <abbr title="Tebibtye">TiB</abbr>
				%td 22
				
			:markdown
				<sup><a href="#fnt__1">1)</a></sup> 1024 <abbr title="Gibibtye">GiB</abbr> of system memory and 2.73 <abbr title="Tebibtye">TiB</abbr> of swap on high-speed NVMe storage.

				The standard Linux on the compute nodes are configured to support just the running of your jobs, particularly parallel jobs.  For example, there are no man pages on the compute nodes.  All compute nodes will have full development headers and libraries.
				
				Commercial applications, and normally your programs, will use a layer of abstraction called a <em>programming model</em>.  Consult the cluster specific documentation for advanced techniques to take advantage of the low level architecture. 
				
	
	#gettingstarted-storage
		:markdown
			## [Storage](#gettingstarted-storage)
		
		#gettingstarted-storage-home
			:markdown
				### [Home filesystem](#gettingstarted-storage-home)
		
		Each DARWIN user receives a home directory (<code>/home/&lt;<em>uid</em>&gt;</code>) that will remain the same during and after the early access period. This storage has slower access with a limit of 20 <abbr title="Gibibtye">GiB</abbr>. It should be used for personal software installs and shell configuration files. 
		
		#gettingstarted-storage-lustre
			:markdown
				### [Lustre high-performance filesystem](#gettingstarted-storage-lustre)
		
				Lustre is designed to use parallel I/O techniques to reduce file-access time. The Lustre filesystems in use at <abbr title="University of Delaware">UD</abbr> are composed of many physical disks using RAID technologies to give resilience, data integrity, and parallelism at multiple levels. There is approximately 1.1 PiB of Lustre storage available on DARWIN. It uses high-bandwidth interconnects such as Mellanox HDR100. Lustre should be used for storing input files, supporting data files, work files, and output files associated with computational tasks run on the cluster.
				
				* Each allocation will be assigned a workgroup storage in the Lustre directory (<code>/lustre/«<em>workgroup</em>»/</code>).
				* Each workgroup storage will have a users directory (<code>/lustre/«<em>workgroup</em>»/users/«<em>uid</em>»</code>) for each user of the workgroup to be used as a personal directory for running jobs and storing larger amounts of data.
				* Each workgroup storage will have a software and <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> directory (<code>/lustre/«<em>workgroup</em>»/sw/</code> and <code>/lustre/«<em>workgroup</em>»/sw/valet</code>) all allow users of the workgroup to install software and create <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> package files that need to be shared by others in the workgroup.
				* There will be a quota limit set based on the amount of storage approved for your allocation for the workgroup storage.
				
				<p class="portlet-msg-info">While all filesystems on the DARWIN cluster utilize hardware redundancies to protect data, there is <strong>no</strong> backup or replication and <strong>no</strong> recovery available for the home or Lustre filesystems.</p>
				
		#gettingstarted-storage-local
			:markdown
				### [Local filesystems](#gettingstarted-storage-local)
		
				Each node has an internal, locally connected disk. Its capacity is measured in terabytes. Each compute node on DARWIN has a 1.75 <abbr title="Tebibtye">TiB</abbr> SSD local scratch filesystem disk. Part of the local disk is used for system tasks such memory management, which might include cache memory and virtual memory.  This remainder of the disk is ideal for applications that need a moderate amount of scratch storage for the duration of a job&#039;s run. That portion is referred to as the <em>node scratch</em> filesystem. 
				
				Each node scratch filesystem disk is only accessible by the node in which it is physically installed. The job scheduling system creates a temporary directory associated with each running job on this filesystem. When your job terminates, the job scheduler automatically erases that directory and its contents.
				
		
	#gettingstarted-software
		:markdown
			## [Software](#gettingstarted-software)
				
			There will <strong>not</strong> be a full set of software during early access and testing, but we will be continually installing and updating software.  Installation priority will go to compilers, system libraries, and highly utilized software packages. Please DO let us know if there are packages that you would like to use on DARWIN, as that will help us prioritize user needs, but understand that we may not be able to install software requests in a timely manner. 
				
			Users will be able compile and install software packages in their home or workgroup directories. There will be very limited support for helping with user compiled installs or debugging during early access. Please reference <a href="https://docs.hpc.udel.edu/technical/recipes/software-managment">basic software building and management</a> to get started with software installations utilizing <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> (versus Modules) as suggested and used by IT RCI staff on our HPC systems.
				
				
			Please review the following documents if you are planning to compile and install your own software
				
			* <a href="https://www.amd.com/system/files/documents/amd-epyc-7002-tg-hpc-56827.pdf">High Performance Computing (HPC) Tuning Guide for AMD EPYC™ 7002 Series Processors</a> guide for getting started tuning AMD 2nd Gen EPYC™ Processor based systems for HPC workloads.  This is not an all-inclusive guide and some items may have similar, but different, names in specific OEM systems (e.g. OEM-specific BIOS settings). Every HPC workload varies in its performance characteristics.  While this guide is a good starting point, you are encouraged to perform your own performance testing for additional tuning.  This guide also provides suggestions on which items should be the focus of additional, application-specific tuning (November 2020). 
			* <a href="http://developer.amd.com/wp-content/resources/56420.pdf">HPC Tuning Guide for AMD EPYC™ Processors</a> guide intended for vendors, system integrators, resellers, system managers and developers who are interested in EPYC system configuration details. There is also a discussion on the AMD EPYC software development environment, and we include four appendices on how to install and run the HPL, HPCG, DGEMM, and STREAM benchmarks. The results produced are ‘good’ but are not necessarily exhaustively tested across a variety of compilers with their optimization flags (December 2018). 
			* <a href="https://developer.amd.com/wordpress/media/2020/04/Compiler%20Options%20Quick%20Ref%20Guide%20for%20AMD%20EPYC%207xx2%20Series%20Processors.pdf">AMD EPYC™ 7xx2-series Processors Compiler Options Quick Reference Guide</a>, however we do not have the AOCC compiler (with Flang - Fortran Front-End) installed on DARWIN.
				
				
	#gettingstarted-scheduler
		:markdown
			## [Scheduler](#gettingstarted-scheduler)
	
			DARWIN will being using the Slurm scheduler like Caviness, and is the most common scheduler among XSEDE resources. Slurm on DARWIN is configured as fair share with each user being given equal shares to access the current HPC resources available on DARWIN.
	
		#gettingstarted-scheduler-queues
			:markdown
				### [Queues (Partitions)](#gettingstarted-scheduler-queues)
		
				Partitions have been created to align with allocation requests moving forward based on different node types. There will be no default partition, and must only specify one partition at a time.  It is not possible to specify multiple partitions using Slurm to span different node types.
		
		
		#gettingstarted-scheduler-running
			:markdown
				### [Run Jobs](#gettingstarted-scheduler-running)
		
				In order to schedule any job (interactively or batch) on the DARWIN cluster, you must set your workgroup to define your cluster group. Each research group has been assigned a unique workgroup. Each research group should have received this information in a welcome email. For example,
				
				<pre class="cmd-line"># <b>workgroup -g it_css</b></pre>
				
				
				will enter the workgroup for <code>it_css</code>. You will know if you are in your workgroup based on the change in your bash prompt.  See the following example for user <code>bjones</code>
				
				<pre class="cmd-line">&#91;bjones@login00.darwin ~&#93;$ <b>workgroup -g it_css</b>
				&#91;&#40;it_css:bjones&#41;@login00.darwin ~&#93;$ printenv USER HOME WORKDIR WORKGROUP WORKDIR_USER
				bjones
				/home/1201
				/lustre/it_css
				it_css
				/lustre/it_css/users/1201
				&#91;&#40;it_css:bjones&#41;@login00.darwin ~&#93;$</pre>
				
				
				Now we can use <code>salloc</code> or <code>sbatch</code> as long as a <a href="#queues">partition</a> is specified as well to submit an interactive or batch job respectively.  See DARWIN <a href="#running">Run Jobs</a>, <a href="#scheduling">Schedule Jobs</a> and <a href="#managing">Managing Jobs</a> wiki pages for more help about Slurm including how to specify resources and check on the status of your jobs.
				
				<p class="portlet-msg-alert">All resulting <strong><em>executables</em></strong> (created via your own compilation) and other <strong><em>applications</em></strong> (commercial or open-source) should only be run on the compute nodes.<p> It is a good idea to periodically check in <code>/opt/shared/templates/slurm/</code> for updated or new <a href="https://docs.hpc.udel.edu/technical/slurm/darwin/templates/start">templates</a> to use as job scripts to run generic or specific applications designed to provide the best performance on DARWIN.</p>
				
				
	#gettingstarted-help
		:markdown
			## [Help](#gettingstarted-help)
	
		#gettingstarted-help-xsede
			:markdown
				### [XSEDE allocations](#gettingstarted-help-xsede)
		
				To report a problem or provide feedback, submit a <a href="https://portal.xsede.org/group/xup/help-desk">help desk ticket on the XSEDE Portal</a> and complete the form selecting <code>darwin.udel.xsede.org</code> as the system and your problem details in the description field to route your question more quickly to the research support team. Provide enough details (including full paths of batch script files, log files, or important input/output files) that our consultants can begin to work on your problem without having to ask you basic initial questions. 
		
		
		#gettingstarted-help-community
			:markdown
				### [Ask or tell the HPC community](#gettingstarted-help-community)
		
				<a href="https://groups.google.com/a/udel.edu/d/forum/hpc-ask?hl=en-US">hpc-ask</a> is a Google group established to stimulate interactions within UD’s broader HPC community and is based on members helping members. This is a great venue to post a question about HPC, start a discussion, or share an upcoming event with the community. Anyone may request membership. Messages are sent as a daily summary to all group members. This list is archived, public, and searchable by anyone.
		
		#gettingstarted-help-resources
			:markdown
				### [Publication and Grant Writing Resources](#gettingstarted-help-resources)
		
				Please refer to the <a href="https://nsf.gov/awardsearch/showAward?AWD_ID=1919839">NSF award</a> information for a proposal or publication to acknowledge use of or describe DARWIN resources. 
		
		
	
