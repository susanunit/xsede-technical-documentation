#transferring
	:markdown
		# [Transferring Files](#transferring)

		<p class="portlet-msg-alert">Be careful about modifications you make to your startup files (e.g. <code>.bash*</code>). Commands that produce output such as <abbr title="VALET Automates Linux Environment Tasks">VALET</abbr> or workgroup commands may cause your file transfer command or application to fail. Log into the cluster with <code>ssh</code> to check what happens during login, and modify your startup files accordingly to remove any commands which are producing output and try again. See computing environment <a href="#appdev-compenv-scripts">startup and logout scripts</a> for help.</p>

	#transferring-clients
		:markdown
			## [Common Clients](#transferring-clients)

			You can move data to and from the cluster using the following supported clients:

		#tablea
			:markdown
				[ Table. Command-line Clients](#tablea)

		%table(border="1" cellspacing="5" cellpadding="3")
			%tr
				%td <strong>sftp</strong>
				%td Recommended for interactive, command-line use. 
			%tr
				%td <strong>scp</strong>
				%td Recommended for batch script use. 
			%tr
				%td <strong>rsync</strong>
				%td Most appropriate for synchronizing the file directories of two systems when only a small fraction <br/> of the files have been changed since the last synchronization. 
			%tr
				%td <strong>Rclone</strong>
				%td <a href="https://rclone.org/" title="https://rclone.org/" rel="ugc nofollow">Rclone</a> is a command line program to sync files and directories to and from popular cloud storage services. 

		:markdown
			If you prefer a non-command-line interface, then consult this table for GUI clients.

		#tableb
			:markdown
				[Table. Graphical-User-Interface Clients](#tableb)

		%table(border="1" cellspacing="5" cellpadding="3")
			%tr
				%td <a href="http://udeploy.udel.edu/software/winscp/" title="http://udeploy.udel.edu/software/winscp/" rel="ugc nofollow">winscp</a>
				%td Windows only 
			%tr
				%td <a href="http://udeploy.udel.edu/software/fetch-ftp-client/" title="http://udeploy.udel.edu/software/fetch-ftp-client/" rel="ugc nofollow">fetch</a> 
				%td Mac <abbr title="Operating System">OS</abbr> X only 
			%tr
				%td <a href="http://filezilla-project.org/download.php?type=client" title="http://filezilla-project.org/download.php?type=client" rel="ugc nofollow">filezilla</a> 
				%td Windows, Mac <abbr title="Operating System">OS</abbr> X, UNIX, Linux 
			%tr
				%td <a href="http://cyberduck.ch/" title="http://cyberduck.ch/" rel="ugc nofollow">cyberduck</a> 
				%td Windows, Mac <abbr title="Operating System">OS</abbr> X (command line version for Linux) 

		:markdown
			**For Windows clients**: editing files on Windows desktops and then transferring them back to the cluster, you may find that your file becomes "`corrupt`" during file transfer process.  The symptoms are very subtle because the file appears to be okay, but in fact contains <code>CRLF</code> line terminators.  This causes problems when reading the file on a Linux cluster and generates very strange errors. Some examples might be a file used for submitting a batch job such as <code>submit.qs</code> and one you have used before and know is correct, will no longer work. Or an input file used for ABAQUS like <code>tissue.inp</code> which has worked many times before produces an error like <code>Abaqus Error: Command line option "`input`" must have a value.</code>. 

			Use the "<code>file</code>" utility to check for <code>CRLF</code> line terminators and <code>dos2unix</code> to fix it, like this below

			<pre class="cmd-line">[bjones@login01 ABAQUS]$ <b>file tissue.inp</b>
			tissue.inp: ASCII text, with CRLF line terminators
			[bjones@login01 ABAQUS]$ dos2unix tissue.inp
			dos2unix: converting file tissue.inp to UNIX format ...
			[bjones@login01 ABAQUS]$ <b>file tissue.inp</b>
			tissue.inp: ASCII text</pre>

	#transferring-copyto
		:markdown
			## [Copying Files to the Cluster](#transferring-copyto)

			To copy a file over an SSH connection from a Mac/UNIX/Linux system to any of the cluster&#039;s file systems, type the generic command:

			<code>scp [options] <i>local_filename</i> <i>HPC_username</i>@<em>HPC_hostname</em>:<em>HPC_filename</em></code>

			Begin the *`HPC_filename`* with a "`/`" to indicate the full path name. Otherwise the name is relative to your home directory on the HPC cluster.
			
			Use <code>scp -r</code> to copy an entire directory, for example:

			<pre class="cmd-line">$ <b>scp -r fuelcell bjones@darwin.hpc.udel.edu:/lustre/it_css/users/1201/projects</b></pre>

			copies the <code>fuelcell</code> directory in your local current working directory into the <code>/lustre/it_css/users/1201/projects</code> directory on the DARWIN cluster. The <code>/lustre/it_css/users/1201/projects</code> directory on the DARWIN cluster must exist, and <code>bjones</code> must have write access to it.

	#transferring-copyfrom
		:markdown
			## [Copying files from the cluster](#transferring-copyfrom)

			To copy a file over an SSH connection to a Mac/UNIX/Linux system from any of the cluster&#039;s files systems type the generic command:

			<code>scp [options] <i> HPC_username@HPC_hostname:HPC_filename local_filename</i></code>

			Begin the *`HPC_filename`* with a "`/`" to indicate the full path name. Otherwise, the name is relative to your home directory.

			Use <code>scp -r</code> to copy the entire directory.  For example:

			<pre class="cmd-line">$ <b>scp -r bjones@darwin.hpc.udel.edu:/lustre/it_css/users/1201/projects/fuelcell  .</b></pre>

			will copy the directory <code>fuelcell</code> on the DARWIN cluster into a new <code>fuelcell</code> directory in your local system&#039;s current working directory. (Note the final period in the command.)

	#transferring-copybetween
		:markdown
			## [Copying Files Between Clusters](#transferring-copybetween)

			You can use <abbr title="Graphical User Interface">GUI</abbr> applications to transfer small files to and from your PC as a way to transfer between clusters, however this is highly inefficient for large files due to multiple transfers and slower disk speeds. As a result, you do not benefit from the <em>arcfour</em> encoding.

			The command tools work the same on any Unix cluster.  To copy a file over an SSH connection, first logon the file cluster1 and then use the <code>scp</code> command to copy files to cluster1. Use the generic commands:

			<code>ssh [options] <i>HPC_username1@HPC_hostname1</i></code><br/>
			<code>scp [options] <i>HPC_filename1 HPC_username2@HPC_hostname2:HPC_filename2</i></code>

			Login to `HPC_hostname1` and in the <code>scp</code> command begin both `HPC_filename1` and `HPC_filename2` with a "`/`" to indicate the full path name. The clusters will most likely have different full path names.

			Use <code>ssh -A</code> to enable agent forwarding and <code>scp -r</code> to copy the entire directory.<sup><a href="#fn__1" id="fnt__1">1)</a></sup> For example:

			<pre class="cmd-line">$ <b>ssh -A bjones@caviness.hpc.udel.edu</b>
			cd archive/it_css/project
			scp -r fuelcell bjones@darwin.hpc.udel.edu:/lustre/it_css/users/1201/projects/fuelcell</pre>
			
			will copy the directory <code>fuelcell</code> from Farber to a new <code>fuelcell</code> directory on DARWIN.

			<sup><a href="#fnt__1" id="fn__1">1)</a></sup> If you are using PuTTY, skip the <code>ssh</code> step and connect to the cluster you want to copy from.

