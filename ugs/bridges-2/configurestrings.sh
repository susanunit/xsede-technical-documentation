#!/bin/sh

# modified for Bridges-2 (partially). The conditional execution can probably be removed, this is likely only to be run for XSEDE

if [ "$1" = "t" ]  
then
	echo "building Bridges-2 user guide for TACC"
	outputfile="tbridges-2.html"

	ugtitle="<h1\>Bridges-2\ User\ Guide<\/h1\>"
	hostname="bridges-2.tacc.utexas.edu"

	# Bridges-2 images on TACC
	jstopo="<img alt=\"\" src=\"\/documents\/10157\/1310756\/Jetstream-topology-1.jpg\/08f0ff9b-2907-495b-8e9a-ee51505ceaee\" style=\"width: 600px; height: 450px;\">"

elif [ "$1" = "x" ] 
then
	echo "building Bridges-2 User Guide for XSEDE"
	outputfile="bridges-2.html"

	ugtitle=""
	hostname="bridges-2.psc.edu"

	# Example format for images on XSEDE
	jstopo="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/Jetstream-topology.jpg\/cc8b9ae6-4b06-4eb1-8d75-71c5edcadd9a\"\ style=\"width:\ 600px;\ height:\ 450px;\">"

else
	echo "invalid command line argument"
	exit 1
fi


sed	-e "s/UGTITLE/$ugtitle/" \
	-e "s/HOSTNAME/$hostname/" \
	-e "s/JSTOPO/$jstopo/" < js.html > $outputfile

