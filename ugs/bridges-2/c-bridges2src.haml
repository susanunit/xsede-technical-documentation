#ondemand
	:markdown
		# [OnDemand](#ondemand)

		The OnDemand interface allows you to conduct your research on Bridges-2 through a web browser. You can manage files &ndash; create, edit and move them &ndash; submit and track jobs, see job output, check the status of the queues, run a Jupyter notebook through JupyterHub and more, without logging in to Bridges-2 via traditional interfaces.
		
		OnDemand was created by the Ohio Supercomputer Center (OSC). In addition to this document, you can check the <a target="_blank" rel="noopener" href="https://www.osc.edu/resources/online_portals/ondemand">extensive documentation for OnDemand</a> created by OSC, including many video tutorials, or email <a target="_blank" rel="noopener" href="mailto:help@psc.edu">help@psc.edu</a>. Information about using OnDemand to connect to Bridges-2 is coming soon.

	#ondemand-start
		:markdown
			## [Start OnDemand](#ondemand-start)

			To connect to Bridges-2 via OnDemand, point your browser to <a target="_blank" rel="noopener" href="https://ondemand.bridges2.psc.edu./">https://ondemand.bridges2.psc.edu</a>.

			* You will be prompted for a username and password. Enter your PSC username and password.
			* The OnDemand **Dashboard** will open. From this page, you can use the menus across the top of the page to manage files and submit jobs to Bridges-2.
		
			To end your OnDemand session, choose **Log Out** at the top right of the **Dashboard** window and close your browser.

	#ondemand-files
		:markdown
			## [Manage files](#ondemand-files)

			To create, edit or move files, click on the **Files** menu from the **Dashboard** window. A dropdown menu will appear, listing all your file spaces on Bridges-2: your home directory and the Ocean directories for each of your Bridges-2 grants.

			Choosing one of the file spaces opens the **File Explorer** in a new browser tab. The files in the selected directory are listed. No matter which directory you are in, your home directory is displayed in a panel on the left.

			There are two sets of buttons in the **File Explorer**.

			Buttons on the top left just below the name of the current directory allow you to **View**, **Edit**, **Rename**, **Download**, **Copy** or **Paste** (after you have moved to a different directory) a file, or you can toggle the file selection with **(Un)Select All.**

			<p><img alt="3" src="/documents/10308/2690165/b2-ondemand-1-600.jpg/53e60342-49ae-4ef6-b932-85f151948667?t=1616023005398" style="width: 600px; height: 98px;" /></p>

			Buttons on the top of the window on the right perform these functions:

			**Go To**

			Navigate to another directory or file system

			**Open in Terminal**

			Open a terminal window on Bridges-2 in a new browser tab

			**New File**

			Creates a new empty file

			**New Dir**

			Create a new subdirectory

			**Upload**

			Copies a file from your local machine to Bridges-2

			**Show Dotfiles**

			Toggles the display of dotfiles

			**Show Owner/Mode**

			Toggles the display of owner and permisson settings

			<p><img alt="3" src="/documents/10308/2690165/b2-ondemand-2-600.jpg/78f21ae7-31e9-4657-8951-49e63714b77e?t=1616023160543" style="width: 600px; height: 98px;" /></p>

	#ondemand-jobs
		:markdown
			## [Create and edit jobs](#ondemand-jobs)

			You can create new job scripts, edit existing scripts, and submit those scripts to Bridges-2 through OnDemand.

			From the top menus in the Dashboard window, choose **Jobs &gt; Job Composer**. A **Job Composer** window will open.

			There are two tabs at the top: **Jobs** and **Templates**.

			In the **Jobs** tab, a listing of your previous jobs is given.

			<p><img alt="3" src="/documents/10308/2690165/b2-ondemand-3-600.jpg/c893faa6-2488-43d2-a5e1-3d2cb29308a2?t=1616023693215" style="width: 600px; height: 287px;" /></p>

	#ondemand-jobs-script
		:markdown
			### [Create a new job script](#ondemand-jobs-script)

			To create a new job script:

			1. Select a template to begin with
			1. Edit the job script
			1. Edit the job options

			**Select a template**

			1. Go to the **Jobs** tab in the **Jobs Composer** window. You have been given a default template, named Simple Sequential Job.
			1. To create a new job script, click the blue **New Job** From Default Template** button in the upper left. You will see a green message at the top of the window, "Job was successfully created".

			<p><img alt="3" src="/documents/10308/2690165/b2-ondemand-4-600.jpg/3e02f6cd-46c3-45e2-b6c7-717ffc8dd56b?t=1616023702204" style="width: 600px; height: 299px;" /></p>
			At the right of the **Jobs** window, you will see the Job Details, including the location of the script and the script name (by default, `main_job.sh`). Under that, you will see the contents of the job script in a section titled Submit Script.

			<p><img alt="3" src="/documents/10308/2690165/b2-ondemand-5-600.jpg/a1dc7194-99bc-402b-a260-6470ff4304ee?t=1616023710402" style="width: 600px; height: 269px;" /></p>
			**Edit the job script**

			Edit the job script so that it has the commands and workflow that you need.

			If you do not want the default settings for a job, you must include options to change them in the job script. For example, you may need more time or more than one node. For the GPU partitions, you must specify the number of GPUs per node that you want. Use an SBATCH directive in the job script to set these options.

			* [See the default settings for all partitions](#partitions)
			* [See how to use SBATCH directives](#sbatch)

			There are two ways to edit the job script: using the **Edit Files** button or the **Open Editor** button. First, go to the **Jobs** tab in the **Jobs Composer** window.

			Find the blue **Edit Files** tab at the top of the window

			<p><img alt="3" src="/documents/10308/2690165/b2-ondemand-6-600.jpg/9224d2d8-a99d-4f26-ad06-93edb04f1f10?t=1616023721221" style="width: 600px; height: 281px;" /></p>
		:markdown

			Find the **Submit Script** section at the bottom right. Click the blue **Open Editor** button.

			<p><img alt="3" src="/documents/10308/2690165/b2-ondemand-7-600.jpg/ff399418-4e36-44b4-af1b-edc7e8023bc1?t=1616023730574" style="width: 600px; height: 302px;" /></p>
		:markdown

			In either case, an **Editor** window opens. Make the changes you want and click the blue **Save** button. 

			<p><img alt="3" src="/documents/10308/2690165/b2-ondemand-8-600.jpg/93ce9120-8d7e-44bb-8498-cf116d4bcfde?t=1616023742843" style="width: 600px; height: 136px;" /></p>
			After you save the file, the editor window remains open, but if you return to the **Jobs Composer** window, you will see that the content of your script has changed.
			**Edit the job options**
		
			In the **Jobs** tab in the **Jobs Composer** window, click the blue **Job Options** button.

			<p><img alt="3" src="/documents/10308/2690165/b2-ondemand-9-600.jpg/4353e149-7dcb-4648-a3e8-49c49eaeae21?t=1616023756042" style="width: 600px; height: 281px;" /></p>
			The options for the selected job such as name, the job script to run, and the account to run it under are displayed and can be edited. Click **Reset** to revert any changes you have made. Click **Save** or **Back** to return to the job listing (respectively saving or discarding your edits).

			<p><img alt="3" src="/documents/10308/2690165/b2-ondemand-10-600.jpg/16313c50-c4a9-4643-830d-1c748f684e56?t=1616023763815" style="width: 600px; height: 260px;" /></p>
	#ondemand-jobs-submit
		:markdown
			### [Submit jobs to Bridges-2](#ondemand-jobs-submit)

			Select a job in the **Jobs** tab in the **Jobs Composer** window. Click the green **Submit** button to submit the selected job. A message at the top of the window shows whether the job submission was successful or not. If it is not, you can edit the job script or options and resubmit. When the job submits successfully, the status of the job in the **Jobs Composer** window will change to Queued or Running. When the job completes, the status will change to Completed.

			<p><img alt="3" src="/documents/10308/2690165/b2-ondemand-11-600.jpg/819eb059-a914-4332-bc69-f9dcf273049a?t=1616023772464" style="width: 600px; height: 415px;" /></p>

#gpunodes
	:markdown
		# [GPU nodes](#gpunodes)

		Bridges-2's GPU nodes provide substantial, complementary computational power for deep learning, simulations and other applications.
		
		A standard NVIDIA accelerator environment is installed on Bridges-2's GPU nodes. If you have programmed using GPUs before, you should find this familiar. Please contact <help@psc.edu> for more help.
		
		The GPU nodes on Bridges-2 are available to those with a Bridges-2 "GPU" or "GPU-AI" allocation. You can see which of Bridges-2's resources that you have been allocated with the `projects` command. See the [Account Administration](#accounts) section of this User Guide for more information.

	#gpunodes-hardware
		:markdown
			## [Hardware description](#gpunodes-hardware)

			See the [System configuration](#system) section for hardware details for all GPU node types. Note that soon after Bridges is decommissioned, its GPU-AI resources will be migrated to Bridges-2. Watch for an announcement.

	#gpunodes-files
		:markdown
			## [File Systems](#gpunodes-files)

			The `$HOME` (`/jet/home`) and Ocean file systems are available on all of these nodes. See the [File Spaces](#filespaces) section for more information on these file systems.

	#gpunodes-jobs
		:markdown
			## [Compiling and Running jobs](#gpunodes-jobs)
		
			After your codes are compiled, use the GPU partition, either in batch or interactively, to run your jobs. See the [Running Jobs](#running) section for more information on Bridges-2's partitions and how to run jobs.

	#gpunodes-cuda
		:markdown
			## [CUDA](#gpunodes-cuda)

			More information on using CUDA on Bridges-2 can be found in the <a target="_blank" rel="noopener" href="https://www.psc.edu/resources/software/cuda/">CUDA document</a>.

			To use CUDA, first you must load the CUDA module. To see all versions of CUDA that are available, type:

				module avail cuda

			Then choose the version that you need and load the module for it.

				module load cuda

			loads the default CUDA. To load a different version, use the full module name.

				module load cuda/8.0

	#gpunodes-openacc
		:markdown
			## [OpenACC](#gpunodes-openacc)

			Our primary GPU programming environment is OpenACC.

			The NVIDIA compilers are available on all GPU nodes. To set up the appropriate environment for the NVIDIA compilers, use the module command:

				module load nvhpc

			<a target="_blank" rel="noopener" href="https://www.psc.edu/resources/software/module/">Read more about the module command at PSC</a>.

			If you will be using these compilers often, it will be useful to add this command to your shell initialization script.

			There are many options available with these compilers. See the <a target="_blank" rel="noopener" href="https://docs.nvidia.com/hpc-sdk/compilers/index.html">NVIDIA documentation</a> for detailed information. You may find these basic OpenACC options a good place to start:

				nvcc -acc yourcode.c
				nvfortran -acc yourcode.f90

			Adding the `-Minfo=accel` flag to the compile command (whether nvfortran, nvcc or nvc++) will provide useful feedback regarding compiler errors or success with your OpenACC commands.

				nvfortran -acc -Minfo=accel yourcode.f90

	#gpunodes-openacc-hybrid
		:markdown
			### [Hybrid MPI/GPU Jobs](#gpunodes-openacc-hybrid)

			To run a hybrid MPI/GPU job use the following commands for compiling your program:

			<pre>
			module load cuda
			module load openmpi/<i>version</i> -nvhpc-<i>version</i>
			mpicc -acc yourcode.c</pre>

			When you execute your program you must first issue the above two module load commands.

	#gpunodes-openacc-debug
		:markdown
			### [Profiling and Debugging](#gpunodes-openacc-debug)

			For CUDA codes, use the command line profiler nvprof. See the <a target="_blank" rel="noopener" href="https://www.psc.edu/resources/software/cuda/">CUDA document</a> for more information.

			For OpenACC codes, the environment variables `NV_ACC_TIME`, `NV_ACC_NOTIFY` and `NV_ACC_DEBUG` can provide profiling and debugging information for your job. Specific commands depend on the shell you are using (bash or cshell).

	#gpunodes-openacc-debug-profiling
		:markdown
			#### [Performance profiling](##gpunodes-openacc-debug-profiling)

			To enable runtime GPU performance profiling (bash shell):

				export NV_ACC_TIME=1

			or (cshell):

				setenv NV_ACC_TIME 1

	#gpunodes-openacc-debug-debugging
		:markdown
			#### [Debugging](##gpunodes-openacc-debug-debugging)

			Basic debugging (bash):

				export NV_ACC_NOTIFY=1

			or (cshell):

				setenv NV_ACC_NOTIFY 1

			For data transfer information, set `NV_ACC_NOTIFY` to 3.

			More detailed debugging (bash):

				export NV_ACC_DEBUG=1

			or (cshell):

				setenv NV_ACC_DEBUG 1


#containers
	:markdown
		# [Containers](#containers)

		Containers are stand-alone packages holding the software needed to create a very specific computing environment. If you need a very specialized environment, you can create your own container or use one that is already installed on Bridges-2. 
		<a target="_blank" rel="noopener" href="https://www.psc.edu/resources/software/singularity/">Singularity </a>is the only type of container supported on Bridges-2.

	#containers-create
		:markdown
			## [Creating a container](#containers-create)

			Singularity is the only container software supported on Bridges-2. You can create a Singularity container, copy it to Bridges-2 and then execute your container on Bridges-2, where it can use Bridges-2's compute nodes and filesystems. In your container you can use any software required by your application: a different version of CentOS, a different Unix operating system, any software in any specific version needed. You can install your Singularity container without any intervention from PSC staff.

			See the <a target="_blank" rel="noopener" href="https://www.psc.edu/resources/software/singularity/">PSC documentation on Singularity</a> for more details on producing your own container and Singularity use on Bridges-2.

			However, Bridges-2 may have all the software you will need. Before creating a container for your work, check the extensive list of software that has been installed on Bridges-2. While logged in to Bridges-2, you can also get a list of installed packages by typing

				module avail
		
			If you need a package that is not available on Bridges-2 you can request that it be installed by emailing <a target="_blank" rel="noopener" href="mailto:help@psc.edu">help@psc.edu</a>. You can also install software packages in your own file spaces and, in some cases, we can provide assistance if you encounter difficulties.

	#containers-public
		:markdown
			## [Publicly available containers on Bridges-2](#containers-public)

			We have installed many containers from the NVIDIA GPU Cloud (NGC) on Bridges-2. These containers are fully optimized, GPU-accelerated environments for AI, machine learning and HPC. They can only be used on the Bridges-2 GPU nodes.
			These include containers for:

			* Caffe and Caffe2
			* Microsoft Cognitive Toolkit
			* DIGITS
			* Inference Server
			* MATLAB
			* MXNet
			* PyTorch
			* Tensorflow
			* TensorRT
			* Theano
			* Torch

			See the <a target="_blank" rel="noopener" href="https://www.psc.edu/resources/software/singularity">PSC documentation on Singularity</a> for more details on Singularity use on Bridges-2.


#datasets
	:markdown
		# [Public Datasets](#datasets)

		A community dataset space allows Bridges-2's users from different grants to share data in a common space. Bridges-2 hosts both community (public) and private datasets, providing rapid access for individuals, collaborations and communities with appropriate protections.

		If there is a dataset which you need for your research on Bridges-2 and you believe it would be of widespread interest, you can request that it be installed for public use by using the <a target="_blank" rel="noopener" href="https://www.psc.edu/resources/dataset-request-form/">Dataset Request form</a>.

	#datasets-available
		:markdown
			## [Available datasets](#datasets-available)

			Some datasets are available to anyone with a Bridges-2 account. They include:

	#datasets-available-2019ncovr
		:markdown
			### [2019nCoVR: 2019 Novel Coronavirus Resource](#datasets-available-2019ncovr)

			The *2019 Novel Coronavirus Resource* concerns the outbreak of novel coronavirus in Wuhan, China since December 2019. Visit the <a target="_blank" rel="noopener" href="https://bigd.big.ac.cn/ncov/?lang=en">China National Center for Bioinformation</a> website for more details about the statistics, metadata, publications and visualizations of the data.

			Available on Bridges-2 at `/ocean/datasets/community/genomics/2019nCoVR`.

	#datasets-available-coco
		:markdown
			### [COCO ](#datasets-available-coco)

			COCO (Common Objects in Context) is a large scale image dataset designed for object detection, segmentation, person keypoints detection, stuff segmentation and caption generation. Please visit <a target="_blank" rel="noopener" href="https://cocodataset.org/">COCO website</a> for more information, including details about the data, paper and tutorials.

			Available on Bridges-2 at `/ocean/datasets/community/COCO`.

	#datasets-available-prevent-ad
		:markdown
			### [PREVENT-AD ](#datasets-available-prevent-ad)

			The PREVENT-AD (Pre-symptomatic Evaluation of Experimental or Novel Treatments for Alzheimer Disease) cohort is composed of cognitively healthy participants over 55 years old, at risk of developing Alzheimer Disease (AD) as their parents and/or siblings were/are affected by the disease. These 'at-risk' participants have been followed for a naturalistic study of the presymptomatic phase of AD since 2011 using multimodal measurements of various disease indicators. Two clinical trials intended to test pharmaco-preventive agents have also been conducted. The PREVENT-AD research group is now releasing data openly with the intention to contribute to the community's growing understanding of AD pathogenesis.

			Available on Bridges-2 at `/ocean/datasets/community/prevent_ad`.

	#datasets-available-imagenet
		:markdown
			### [ImageNet](#datasets-available-imagenet)

			ImageNet is an image dataset organized according to WordNet hierarchy. See the ImageNet website at http://image-net.org for information.
		
			Available on Bridges-2 at `/ocean/datasets/community/imagenet`.

	#datasets-available-nltk
		:markdown
			### [Natural Languge Tool Kit Data](#datasets-available-nltk)

			NLTK comes with many corpora, toy grammars, trained models, etc. A complete list of the available data is posted on the <a target="_blank" rel="noopener" href="https://www.nltk.org/nltk_data/">NLT website</a>.

			Available on Bridges-2 at `/ocean/datasets/community/nltk`.

	#datasets-available-mnist
		:markdown
			### [MNIST](#datasets-available-mnist)

			Dataset of handwritten digits used to train image processing systems.

			Available on Bridges-2 at `/ocean/datasets/community/mnist`.

	#datasets-available-blast
		:markdown
			### [BLAST](#datasets-available-blast)

			The <a target="_blank" rel="noopener" href="https://www.psc.edu/resources/software/blast/">BLAST</a> databases can be accessed through the environment variable $BLASTDB after loading the BLAST module.

	#datasets-available-other
		:markdown
			### [Other genomics datasets](#datasets-available-other)

			Other available datasets are typically used with a particular genomics package. These include:

			* **Homer**
			* **MetaPhlAn2**


#gateways
	:markdown
		# [Gateways](#gateways)

		Bridges-2 hosts a number of gateways &ndash; web-based, domain-specific user interfaces to applications, functionality and resources that allow users to focus on their research rather than programming and submitting jobs. Gateways provide intuitive, easy-to-use interfaces to complex functionality and data-intensive workflows.

		Gateways can manage large numbers of jobs and provide collaborative features, security constraints and provenance tracking, so that you can concentrate on your analyses instead of on the mechanics of accomplishing them.


#policies
	:markdown
		# [Security Guidelines and Policies](#policies)

		PSC policies regarding [privacy](#policies-privacy) , [security](#policies-security) and the [acceptable use of PSC resources](#policies-acceptableuse) are documented here. Questions about any of these policies should be directed to <a target="_blank" rel="noopener" href="https://www.psc.edu/psc-contacts/">PSC User Services</a>. See also policies for:

		* <a target="_blank" rel="noopener" href="https://www.psc.edu/password-policies/">Passwords</a>
		* <a target="_blank" rel="noopener" href="https://www.psc.edu/user-account-policies/">User Accounts</a>

	#policies-security
		:markdown
			## [Security Measures](#policies-security)

			Security is very important to PSC. These policies are intended to ensure that our machines are not misused and that your data is secure.

	#policies-security-you
		:markdown
			### [What You Can Do:](#policies-security-you)

			**You play a significant role in security!** To keep your account and PSC resources secure, please:

			* Be aware of and **comply with PSC's policies** on security, use and privacy found in this document
			* Choose **strong passwords** and don't share them between accounts or with others. More information can be found in the <a target="_blank" rel="noopener" href="https://www.psc.edu/password-policies/">PSC password policies</a>.
			* Utilize your **local security team** for advice and assistance
			* Take the <a target="_blank" rel="noopener" href="https://portal.xsede.org/online-training">online XSEDE Cybersecurity Tutorial</a>. Go to Online Training and click on "XSEDE Cybersecurity (CI-Tutor)"
			* Keep your **computer properly patched** and protected
			* Report any **security concerns** to the **PSC help desk ASAP** by calling the PSC hotline at: 412-268-6350 or email <help@psc.edu>.

	#policies-security-wenever
		:markdown
			### [What We Will Never Do:](#policies-security-wenever)

			* PSC will **never** send you unsolicited emails requesting confidential information.
			* We will also **never** ask you for your password via an unsolicited email or phone call.

			Remember that the PSC help desk is always a phone call away to confirm any correspondence at 412-267-6350.

			If you have replied to an email appearing to be from PSC and supplied your password or other sensitive information, **please contact the help desk immediately**.

	#policies-security-expect
		:markdown
			### [What You Can Expect:](#policies-security-expect)

			* We will send you email when we need to communicate with you about service outages, new HPC resources, and the like.
			* We will send you email when your password is about to expire and ask you to change it by using the web-based <a target="_blank" rel="noopener" href="https://apr.psc.edu/">PSC password change utility</a>.

	#policies-security-other
		:markdown
			### [Other Security Policies](#policies-security-other)

			* PSC <a target="_blank" rel="noopener" href="https://www.psc.edu/password-policies/">password policies</a>
			* Users must connect to PSC machines using <a target="_blank" rel="noopener" href="https://www.psc.edu/about-using-ssh">CERT Coordination Project</a> with regard to possible Internet security violations

	#policies-security-incidents
		:markdown
			### [Reporting Security Incidents](#policies-security-incidents)

			To report a security incident you should contact our Hotline at 412-268-6350. To report non-emergency security incidents you can send email to <help@psc.edu>.


	#policies-security-acceptable
		:markdown
			### [PSC Acceptable Use Policy](#policies-security-acceptable)

			PSC's resources are vital to the scientific community, and we have a responsibility to ensure that all resources are utilized in a responsible manner. PSC has legal and other obligations to protect its services, resources, and the intellectual property of users. Users share this responsibility by observing the rules of acceptable use that are outlined in this document. Your on-line assent to this Acceptable Use Policy is your acknowledgment that you have read and understand your responsibilities as a user of PSC Services and Resources, which refers to all computers owned or operated by PSC and all hardware, data, software, storage systems and communications networks associated with these computers. If you have questions, please contact <a target="_blank" rel="noopener" href="https://www.psc.edu/psc-contacts/">PSC User Services</a>.

			By using PSC Services and Resources associated with your allocation, you agree to comply with the following conditions of use:

			1. You will protect any access credentials (e.g., private keys, tokens &amp; passwords) that are issued for your sole use by PSC and not knowingly allow any other person to impersonate or share any of your identities.
			1. You will not use PSC Services and Resources for any unauthorized purpose, including but not limited to:
				1. Financial gain
				1. Tampering with or obstructing PSC operations
				1. Breaching, circumventing administrative, or security controls
				1. Inspecting, modifying, distributing, or copying privileged data or software without proper authorization, or attempting to do so
				1. Supplying, or attempting to supply, false or misleading information or identification in order to access PSC Services and Resources
			1. You will comply with all applicable laws and relevant regulations, such as export control law or HIPAA.
			1. You will immediately report any known or suspected security breach or misuse of PSC access credentials to <help@psc.edu>.
			1. Use of PSC Services and Resources is at your own risk. There are no guarantees that PSC Services and Resources will be available, that they will suit every purpose, or that data will never be lost or corrupted. Users are responsible for backing up critical data.
			1. Logged information, including information provided by you for registration purposes, will be used solely for administrative, operational, accounting, monitoring and security purposes.
			1. Violations of this Acceptable Use Policy and/or abuse of PSC Services and Resources may result in loss of access to PSC Services and Resources. Abuse will be referred to the PSC User Services manager and/or the appropriate local, state and federal authorities, at PSC's discretion.
			1. PSC may terminate or restrict any user's access to PSC Services and Resources, without prior notice, if such action is necessary to maintain computing availability and security for other users of the systems.
			1. Allocations are awarded solely for open research, intended for publication. You will only use PSC Computing Resources to perform work consistent with the stated allocation request goals and conditions of use as defined by your approved PSC project and this Acceptable Use Policy.
			1. PSC is entitled to regulate, suspend or terminate your access, and you will immediately comply with their instructions.

	#policies-privacy
		:markdown
			## [Privacy](#policies-privacy)

			Pittsburgh Supercomputing Center is committed to preserving your privacy. This privacy policy explains exactly what information is collected when you visit our site and how it is used.

			This policy may be modified as new features are added to the site. Any changes to the policy will be posted on this page.

			* Any data automatically collected from our site visitors &ndash; domain name, browser types, etc. &ndash; are used only in aggregate to help us better meet site visitors' needs.
			* There is no identification of individuals from our aggregate data. Therefore, unless you choose otherwise, you are totally anonymous when visiting our site.
			* We do not share data with anyone for commercial purposes.
			* If you choose to submit personally identifiable information to us electronically via the <a target="_blank" rel="noopener" href="https://www.psc.edu/feedback/">PSC feedback page</a>, email, etc., we will treat it with the same respect for privacy afforded to mailed submissions. Submission of such information is always optional.

			PSC respects individual privacy and takes great effort in supporting web site privacy policy outlined above. Please be aware, however, that we may publish URLs of other sites on our web site that may not adhere to the same policy.

#problems
	:markdown
		# [Reporting a Problem](#problems)

		To report a problem on Bridges-2, please email <help@psc.edu>. Please report only one problem per email; it will help us to track and solve any issues more quickly and efficiently.

		Be sure to include:

		* an informative subject line
		* your username

		If the question concerns a particular job, include these in addition:

		* the JobID
		* any error messages you received
		* the date and time the job ran
		* link to job scripts, output and data files
		* the software being used, and versions when appropriate
		* a screenshot of the error or the output file showing the error, if possible


#publications
	:markdown
		# [Acknowledgement in Publications](#publications)

		All publications, copyrighted or not, resulting from an allocation of computing time on Bridges-2 should include an acknowledgement. Please acknowledge both the funding source that supported your access to PSC and the specific PSC resources that you used.

		Please also acknowledge support provided by XSEDE's ECSS program and/or PSC staff when appropriate.

		Proper acknowledgment is critical for our ability to solicit continued funding to support these projects and next generation hardware.

		Suggested text and citations follow.

	#publications-xsede
		:markdown
			## [XSEDE supported research on Bridges-2](#publications-xsede)

			We ask that you use the following text:

			This work used the Extreme Science and Engineering Discovery Environment (XSEDE), which is supported by National Science Foundation grant number ACI-1548562. Specifically, it used the Bridges-2 system, which is supported by NSF award number ACI-1445606, at the Pittsburgh Supercomputing Center (PSC).

			Please include these citations:

			Towns, J., Cockerill, T., Dahan, M., Foster, I., Gaither, K., Grimshaw, A., Hazlewood, V., Lathrop, S., Lifka, D., Peterson, G.D., Roskies, R., Scott, J.R. and Wilkens-Diehr, N. 2014. XSEDE: Accelerating Scientific Discovery. Computing in Science &amp; Engineering. 16(5):62-74. 
			<a target="_blank" rel="noopener" href="https://www.computer.org/csdl/magazine/cs/2014/05/mcs2014050062/13rRUxAASOj">XSEDE: Accelerating Scientific Discovery</a>

	#publications-ecss
		:markdown
			## [XSEDE ECSS Support](#publications-ecss)

			To acknowledge support provided through XSEDE's Extended Collaborative Support Services (ECSS), please use this text:

			We thank [*consultant name(s)*] for [*his/her/their*] assistance with [*describe tasks such as porting code, optimization, visualization, etc*.], which was made possible through the XSEDE Extended Collaborative Support Service (ECSS) program.

			Please include this citation:

			Wilkins-Diehr, N and S Sanielevici, J Alameda, J Cazes, L Crosby, M Pierce, R Roskies. High Performance Computer Applications 6th International Conference, ISUM 2015, Mexico City, Mexico, March 9-13, 2015, Revised Selected Papers Gitler, Isidoro, Klapp, Jaime (Eds.) Springer International Publishing. ISBN 978-3-319-32243-8, 3-13, 2016. 10.1007/978-3-319-32243-8.

	#publications-nonxsede
		:markdown
			## [Research on Bridges-2 not supported by XSEDE](#publications-nonxsede)

			For research on Bridges-2 supported by programs other than XSEDE, we ask that you use the following text:
		
			This work used the Bridges-2 system, which is supported by NSF award number OAC-1928147 at the Pittsburgh Supercomputing Center (PSC).

	#publications-psc
		:markdown
			## [PSC Support](#publications-psc)

			Please also acknowledge any support provided by PSC staff.

			If PSC staff contributed substantially to software development, optimization, or other aspects of the research, they should be considered as coauthors.

			When PSC staff contributions do not warrant coauthorship, please acknowledge their support with the following text:
			We thank [*consultant name(s)* ] for [*his/her/their* ] assistance with [*describe tasks such as porting code, optimization, visualization, etc.*]

