#composable
	:markdown
		# [Composable Subsystem](#composable)

		New usage patterns have emerged in research computing that depend on the availability of custom services such as notebooks, databases, elastic software stacks, and science gateways alongside traditional batch HPC. The Anvil Composable Subsystem is a <a target="_blank" href="https://kubernetes.io">Kubernetes</a> based private cloud managed with <a target="_blank" href="https://rancher.com">Rancher</a> that provides a platform for creating composable infrastructure on demand. This cloud-style flexibility provides researchers the ability to self-deploy and manage persistent services to complement HPC workflows and container-based data analysis tools and applications.

		* [Concepts](#composable:concepts)
		* [Access](#composable:access)
		* [Deployments](#composable:deployments)
		* [Services](#composable:services)
		* [Registry](#composable:registry)
		* [Storage](#composable:storage)
		* [Examples](#composable:examples)

	#composable:concepts
		:markdown
			## [Concepts](#composable:concepts)

	#composable:concepts-containers
		:markdown
			### [Containers & Images](#composable:concepts-containers)

			__Image__ - An image is a simple text file that defines the source code of an application you want to run as well as the libraries, dependencies, and tools required for the successful execution of the application. Images are immutable meaning they do not hold state or application data. Images represent a software environment at a specific point of time and provide an easy way to share applications across various environments. Images can be built from scratch or downloaded from various repositories on the internet, additionally many software vendors are now providing containers alongside traditional installation packages like Windows .exe and Linux rpm/deb.

			__Container__ - A container is the run-time environment constructed from an image when it is executed or run in a container runtime. Containers allow the user to attach various resources such as network and volumes in order to move and store data. Containers are similar to virtual machines in that they can be attached to when a process is running and have arbitrary commands executed that affect the running instance. However, unlike virtual machines, containers are more lightweight and portable allowing for easy sharing and collaboration as they run identically in all environments.

			__Tags__ - Tags are a way of organizing similar image files together for ease of use. You might see several versions of an image represented using various tags. For example, we might be building a new container to serve web pages using our favorite web server: nginx. If we search for the nginx container on Docker Hub image repository we see many options or tags are available for the official nginx container.

			The most common you will see are typically `:latest` and `:number` where number refers to the most recent few versions of the software releases. In this example we can see several tags refer to the same image: `1.21.1`, `mainline`, `1`, `1.21`, and `latest` all reference the same image while the `1.20.1`, `stable`, `1.20` tags all reference a common but different image. In this case we likely want the nginx image with either the `latest` or `1.21.1` tag represented as `nginx:latest` and `nginx:1.21.1` respectively.

			__Container Security__ - Containers enable fast developer velocity and ease compatibility through great portability, but the speed and ease of use come at some costs. In particular it is important that folks utilizing container driver development practices have a well established plan on how to approach container and environment security. Best Practices

			__Container Registries__ - Container registries act as large repositories of images, containers, tools and surrounding software to enable easy use of pre-made containers software bundles. Container registries can be public or private and several can be used together for projects. Docker Hub is one of the largest public repositories available, and you will find many official software images present on it. You need a user account to avoid being rate limited by Docker Hub. A private container registry based on Harbor that is available to use. TODO: link to harbor instructions

			__Docker Hub__ - Docker Hub is one of the largest container image registries that exists and is well known and widely used in the container community, it serves as an official location of many popular software container images. Container image repositories serve as a way to facilitate sharing of pre-made container images that are “ready for use.” Be careful to always pay attention to who is publishing particular images and verify that you are utilizing containers built only from reliable sources.

			__Harbor__ - Harbor is an open source registry for Kubernetes artifacts, it provides private image storage and enforces container security by vulnerability scanning as well as providing RBAC or role based access control to assist with user permissions. Harbor is a registry similar to Docker Hub, however it gives users the ability to create private repositories. You can use this to store your private images as well as keeping copies of common resources like base OS images from Docker Hub and ensure your containers are reasonably secure from common known vulnerabilities.

	#composable:concepts-runtime
		:markdown
			### [Containers Runtime Concepts](#composable:concepts-runtime)

			__Docker Desktop__ - Docker Desktop is an application for your Mac / Windows machine that will allow you to build and run containers on your local computer. Docker desktop serves as a container environment and enables much of the functionality of containers on whatever machine you are currently using. This allows for great flexibility, you can develop and test containers directly on your laptop and deploy them directly with little to no modifications.

			__Volumes__ - Volumes provide us with a method to create persistent data that is generated and consumed by one or more containers. For docker this might be a folder on your laptop while on a large Kubernetes cluster this might be many SSD drives and spinning disk trays. Any data that is collected and manipulated by a container that we want to keep between container restarts needs to be written to a volume in order to remain around and be available for later use.

	#composable:concepts-orchestration
		:markdown
			### [Containers Orchestration Concepts](#composable:concepts-orchestration)

			__Container Orchestration__ - Container orchestration broadly means the automation of much of the lifecycle management procedures surrounding the usage of containers. Specifically it refers to the software being used to manage those procedures. As containers have seen mass adoption and development in the last decade, they are now being used to power massive environments and several options have emerged to manage the lifecycle of containers. One of the industry leading options is Kubernetes, a software project that has descended from a container orchestrator at Google that was open sourced in 2015.

			__Kubernetes (K8s)__ - Kubernetes (often abbreviated as "K8s") is a platform providing container orchestration functionality. It was open sourced by Google around a decade ago and has seen widespread adoption and development in the ensuing years. K8s is the software that provides the core functionality of the Anvil Composable Subsystem by managing the complete lifecycle of containers. Additionally it provides the following functions: service discovery and load balancing, storage orchestration, secret and configuration management. The Kubernetes cluster can be accessed via the Rancher UI or the kubectl command line tool.

			__Rancher__ - Rancher is a "is a complete software stack for teams adopting containers." as described by its website. It can be thought of as a wrapper around Kubernetes, providing an additional set of tools to help operate the K8 cluster efficiently and additional functionality that does not exist in Kubernetes itself. Two examples of the added functionality is the Rancher UI that provides an easy to use GUI interface in a browser and Rancher projects, a concept that allows for multi-tenancy within the cluster. Users can interact directly with Rancher using either the Rancher UI or Rancher CLI to deploy and manage workloads on the Anvil Composable Subsystem.

			__Rancher UI__ - The Rancher UI is a web based graphical interface to use the Anvil Composable Subsystem from anywhere.

			__Rancher CLI__ - The Rancher CLI provides a convenient text based toolkit to interact with the cluster. The binary can be downloaded from the link on the right hand side of the footer in the Rancher UI. After you download the Rancher CLI, you need to make a few configurations Rancher CLI requires:

			* Your Rancher Server URL, which is used to connect to Rancher Server.
			* An API Bearer Token, which is used to authenticate with Rancher. see Creating an API Key.

			After setting up the Rancher CLI you can issue `rancher --help` to view the full range of options available.

			__Kubectl__ - Kubectl is a text based tool for working with the underlying Anvil Kubernetes cluster. In order to take advantage of kubectl you will either need to set up a Kubeconfig File or use the built in kubectl shell in the Rancher UI. You can learn more about kubectl and how to download the kubectl file here.

			__Storage__ - Storage is utilized to provide persistent data storage between container deployments. The __Ceph__ filesystem provides access to Block, Object and shared file systems. File storage provides an interface to access data in a file and folder hierarchy similar to NTFS or NFS. Block storage is a flexible type of storage that allows for snapshotting and is good for database workloads and generic container storage. Object storage is also provided by Ceph, this features a REST based bucket file system providing S3 and Swift compatibility.

	#composable:access
		:markdown
			## [Access](#composable:access)

			How to Access the Anvil Composable Subsystem via the Rancher UI, the command line (kubectl) and the Anvil Harbor registry.

			* [Rancher](#composable:access-rancher)
			* [kubectl](#composable:access-kubectl)
			* [Harbor](#composable:access-harbor)

	#composable:access-rancher
		:markdown
			### [Rancher](#composable:access-rancher)

			Logging in to Rancher

			The Anvil Composable Subsystem Rancher interface can be accessed via a web browser at https://composable.anvil.rcac.purdue.edu. Log in by choosing "log in with shibboleth" and using your XSEDE credentials at the XSEDE login screen.

	#composable:access-kubectl
		:markdown
			### [kubectl](#composable:access-kubectl)

			__Configuring local kubectl access with Kubeconfig file__

			__kubectl__ can be installed and run on your local machine to perform various actions against the Kubernetes cluster using the API server.

			These tools authenticate to Kubernetes using information stored in a <a target="_blank" rel="noopener noreferrer" href="https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig">kubeconfig file</a>.

			<p class="portlet-msg-info">__Note:__ A file that is used to configure access to a cluster is sometimes called a kubeconfig file. This is a generic way of referring to configuration files. It does not mean that there is a file named `kubeconfig`.</p>

			To authenticate to the Anvil cluster you can download a kubeconfig file that is generated by Rancher as well as the `kubectl` tool binary.

			1. From anywhere in the rancher UI navigate to the cluster dashboard by hovering over the box to the right of the cattle and selecting anvil under the "Clusters" banner.

				* Click on kubeconfig file at the top right
				* Click copy to clipboard
				* Create a hidden folder called .kube in your home directory
				* Copy the contents of your kubeconfig file from step 2 to a file called config in the newly create .kube directory

			1. You can now issue commands using kubectl against the Anvil Rancher cluster

				* to look at the current config settings we just set use kubectl config view
				* now let's list the available resource types present in the API with `kubectl api-resources`

			To see more options of kubectl review the <a target="_blank" href="https://kubernetes.io/docs/reference/kubectl/cheatsheet">cheatsheet</a>.

			__Accessing kubectl in the rancher web UI__

			You can launch a kubectl command window from within the Rancher UI by selecting the __Launch kubectl__ button to the left of the __Kubeconfig File__ button. This will deploy a container in the cluster with kubectl installed and give you an interactive window to use the command from.

	#composable:access-harbor
		:markdown
			### [Harbor](#composable:access-harbor)

			__Logging into the Anvil Registry UI with XSEDE credentials__

			Harbor is configured to use XSEDE as an OpenID Connect (OIDC) authentication provider. This allows you to login using your XSEDE credentials.

			To login to the harbor registry using your XSEEDE credentials:

			Navigate to https://registry.anvil.rcac.purdue.edu in your favorite web browser.

			1. Click the Login via OIDC Provider button.

				* This redirects you to the XSEDE portal for authentication.

			1. If this is the first time that you are logging in to Harbor with OIDC, specify a user name for Harbor to associate with your OIDC username.

				* This is the user name by which you are identified in Harbor, which is used when adding you to projects, assigning roles, and so on. If the username is already taken, you are prompted to choose another one.

			1. After the OIDC provider has authenticated you, you are redirected back to the Anvil Harbor Registry.


	#composable:deployments
		:markdown
			## [Deployments](#composable:deployments)

			__Deploy a Container__

			This is a simple example of deploying a container from a Docker image hosted in Docker Hub. Refer to the [Concepts](#composable:concepts) page for more information about images and containers.

			1. In the Rancher web interface, select the "anvil" cluster and your project name.
			1. From the Workloads tab, click the __Deploy__ button.

				* Set a unique __Name__ for your deployment, i.e. "myapp"
				* Set __Docker Image__ to `registry.anvil.rcac.purdue.edu/docker-hub-cache/library/alpine`. We will use alpine for this example. __Alpine__ is small Linux distribution often used as the base for Docker image builds.
				* Instead of pulling directly from Docker Hub, we use the Docker Hub Cache on Anvil's Harbor registry. Use the cache whenever possible as this will prevent all users of Anvil from hitting <a target="_blank" href="https://www.docker.com/increase-rate-limits">pull rate limits</a> imposed by Docker Hub.
				* Select the Namespace for your application or create a new one by selecting "Add to a new namespace"
				* Click Launch

				Wait a couple minutes while your application is deployed. The "does not have minimum availability" message is expected. But, waiting more than 5 minutes for your workload to deploy typically indicates a problem. You can check for errors by clicking your workload name (i.e. "myapp"), then the lower button on the right side of your deployed pod and selecting __View Logs__.

				If all goes well, you will see an __Active__ status for your deployment.

			1. You can then interact with your deployed Alpine container on the command line by clicking the button with three dots on the right side of the screen and choosing "Execute Shell".

	#composable:services
		:markdown
			## [Services](#composable:services)

			A Service is an abstract way to expose an application running on Pods as a network service. This allows the networking and application to be logically decoupled so state changes in either the application itself or the network connecting application components do not need to be tracked individually by all portions of an application.

	#composable:services-resources
		:markdown
			### [Service Resources](#composable:services-resources)

			In Kubernetes, a Service is an abstraction which defines a logical set of Pods and a policy by which to access them (sometimes this pattern is called a micro-service). The set of Pods targeted by a Service is usually determined by a Pod selector, but can also be defined other ways.

		#composable:services-publishing
			:markdown
				### [Publishing Services (ServiceTypes)](#composable:services-publishing)

				For some parts of your application you may want to expose a Service onto an external IP address, that’s outside of your cluster.

				Kubernetes __ServiceTypes__ allow you to specify what kind of Service you want. The default is ClusterIP.

				* __ClusterIP__: Exposes the Service on a cluster-internal IP. Choosing this value makes the Service only reachable from within the cluster. This is the default ServiceType.
				* __NodePort__: Exposes the Service on each Node's IP at a static port (the NodePort). A ClusterIP Service, to which the NodePort Service routes, is automatically created. You’ll be able to contact the NodePort Service, from outside the cluster, by requesting  &lt;NodeIP&gt;:&lt;NodePort&gt;.
				* __LoadBalancer__: Exposes the Service externally using a cloud provider’s load balancer. NodePort and ClusterIP Services, to which the external load balancer routes, are automatically created.

				You can see an example of [exposing a workload using the LoadBalancer type](#composable:examples-database) on Anvil in the Examples section.

				* __ExternalName__: Maps the Service to the contents of the externalName field (e.g. foo.bar.example.com), by returning a CNAME record with its value. No proxying of any kind is set up.

		#composable:services-publishing-ingress
			:markdown
				#### [Ingress](#composable:services-publishing-ingress)

				An __Ingress__ is an API object that manages external access to the services in a cluster, typically HTTP/HTTPS. An Ingress is not a ServiceType, but rather brings external traffic into the cluster and then passes it to an Ingress Controller to be routed to the correct location. Ingress may provide load balancing, SSL termination and name-based virtual hosting. Traffic routing is controlled by rules defined on the Ingress resource.
		
				You can see an example of [a service being exposed with an Ingress](#composable:examples-webserver) on Anvil in the Examples section.

		#composable:services-publishing-ingresscontroller
			:markdown
				#### [Ingress Controller](#composable:services-publishing-ingresscontroller)

				In order for the Ingress resource to work, the cluster must have an ingress controller running to handle Ingress traffic.

				Anvil provides the [nginx](https://github.com/kubernetes/ingress-nginx/blob/main/README.md#readme) ingress controller configured to facilitate SSL termination and automatic DNS name generation under the `anvilcloud.rcac.purdue.edu` subdomain.

				Kubernetes provides additional information [here](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/) about Ingress Controllers in the official documentation.

	#composable:registry
		:markdown
			## [Registry](#composable:registry)

			__Accessing the Anvil Composable Registry__

			The Anvil registry uses Harbor, an open source registry to manage containers and artifacts, it can be accessed at the following URL: https://registry.anvil.rcac.purdue.edu

			__Using the Anvil Registry Docker Hub Cache__

			It's advised that you use the Docker Hub cache within Anvil to pull images for deployments. There's a limit to how many images Docker hub will allow to be pulled in a 24 hour period which Anvil reaches depending on user activity. This means if you're trying to deploy a workload, or have a currently deployed workload that needs migrated, restarted, or upgraded, there’s a chance it will fail.

			To bypass this, use the Anvil cache url registry.anvil.rcac.purdue.edu/docker-hub-cache/ in your image names

			For example if you're wanting to pull a notebook from jupyterhub's Docker Hub repo e.g jupyter/tensorflow-notebook:latest Pulling it from the Anvil cache would look like this registry.anvil.rcac.purdue.edu/docker-hub-cache/jupyter/tensorflow-notebook:latest

			__Using OIDC from the Docker or Helm CLI__

			After you have authenticated via OIDC and logged into the Harbor interface for the first time, you can use the Docker or Helm CLI to access Harbor.

			The Docker and Helm CLIs cannot handle redirection for OIDC, so Harbor provides a CLI secret for use when logging in from Docker or Helm.

			1. Log in to Harbor with an OIDC user account.
			1. Click your username at the top of the screen and select User Profile.
			1. Click the clipboard icon to copy the CLI secret associated with your account.
			1. Optionally click the ... icon in your user profile to display buttons for automatically generating or manually creating a new CLI secret.
				1. A user can only have one CLI secret, so when a new secret is generated or create, the old one becomes invalid.
			1. If you generated a new CLI secret, click the clipboard icon to copy it.

			You can now use your CLI secret as the password when logging in to Harbor from the Docker or Helm CLI.

				docker login -u testuser -p cli_secret jt-test.local.goharbor.io

			__Note: The CLI secret is associated with the OIDC ID token. Harbor will try to refresh the token, so the CLI secret will be valid after the ID token expires. However, if the OIDC Provider does not provide a refresh token or the refresh fails, the CLI secret becomes invalid. In this case, log out and log back in to Harbor via your OIDC provider so that Harbor can get a new ID token. The CLI secret will then work again.__

			__Creating a harbor Registry__

			1. Using a browser login to https://registry.anvil.rcac.purdue.edu with your XSEDE account username and password
			1. From the main page click __create project__, this will act as your registry
			1. Fill in a name and select whether you want the project to be public or private
			1. Click __ok__ to create and finalize

			__Tagging and Pushing Images to Your Harbor Registry__

			1. Tag your image
				`$ docker tag my-image:tag registry.anvil.rcac.purdue.edu/project-registry/my-image:tag`
			1. login to the Anvil registry via command line
				`$ docker login registry.anvil.rcac.purdue.edu`
			1. Push your image to your project registry
				`$ docker push registry.anvil.rcac.purdue.edu/project-registry/my-image:tag`

			__Creating a Robot Account for a Private Registry__

			A robot account and token can be used to authenticate to your registry in place of having to supply or store your private credentials on multi-tenant cloud environments like Rancher/Anvil.

			1. Navigate to your project after logging into https://registry.anvil.rcac.purdue.edu
			1. Navigate to the Robot Accounts tab and click New Robot Account.
			1. Fill out the form.
				1. Name your robot account.
				1. Select account expiration if any, select never to make permanent.
				1. Customize what permissions you wish the account to have.
				1. Click Add.
			1. Copy your information.
				1. Your robot's account name will be something longer than what you specified, since this is a multi-tenant registry, harbor does this to avoid unrelated project owners creating a similarly named robot account.
			1. Export your token as JSON or copy it to a clipboard.

			__Note Harbor does not store account tokens, once you exit this page your token will be unrecoverable.__

			__Adding Your Private Registry to Rancher__

			1. From your project navigate to Resources > secrets
			1. Navigate to the Registry Credentials tab and click Add Registry
			1. Fill out the form
				1. Give a name to the Registry secret (this is an arbitrary name)
				1. Select whether or not the registry will be available to all or a single namespace
				1. Select address as "custom" and provide "registry.anvil.rcac.purdue.edu"
				1. Enter your robot account's long name e.g. `robot$my-registry+robot` as the Username
				1. Enter your robot account's token as the password

			1. Click Save

			__External Harbor Documentation__

			* <a target="_blank" href="https://goharbor.io/docs/2.3.0/working-with-projects">Working with Projects</a>
			* <a target="_blank" href="https://goharbor.io/docs/2.1.0/working-with-projects/working-with-images">Working With Images</a>
			* You can learn more about the Harbor project on the <a target="_blank" href="">Harbor website</a>.


	#composable:storage
		:markdown
			## [Storage](#composable:storage)

			__Storage__ is utilized to provide persistent data storage between container deployments and comes in a few options on Anvil.

			The Ceph software is used to provide block, filesystem and object storage on the Anvil composable cluster. __File storage__ provides an interface to access data in a file and folder hierarchy similar to NTFS or NFS. __Block storage__ is a flexible type of storage that allows for snapshotting and is good for database workloads and generic container storage. __Object storage__ is ideal for large unstructured data and features a REST based API providing an S3 compatible endpoint that can be utilized by the preexisting ecosystem of S3 client tools.

			__Provisioning Block and Filesystem Storage for use in deployments__

			Block and Filesystem storage can both be provisioned in a similar way.

			1. While deploying a Workload, select the Volumes drop down and click Add Volume…
			1. Select "Add a new persistent volume (claim)"
			1. Set a unique volume name, i.e. "<username>-volume"
			1. Select a Storage Class. The default storage class is Ceph for this Kubernetes cluster
			1. Request an amount of storage in Gigabytes
			1. Click Define
			1. Provide a Mount Point for the persistent volume: i.e /data

			__Accessing object storage externally from local machine using Cyberduck__

			__Cyberduck__ is a free server and cloud storage browser that can be used to access the public S3 endpoint provided by Anvil.

			1. Download and install Cyberduck from https://cyberduck.io/download/
			1. Launch Cyberduck
			1. Click __+ Open Connection__ at the top of the UI.
			1. Select __S3__ from the dropdown menu
			1. Fill in __Server, Access Key ID__ and __Secret Access Key__ fields
			1. Click __Connect__
			1. You can now right click to bring up a menu of actions that can be performed against the storage endpoint

			Further information about using Cyberduck can be found on the Cyberduck documentation site: https://docs.cyberduck.io/Cyberduck.

	#composable:examples
		:markdown
			## [Examples](#composable:examples)

			Examples of deploying a database with persistent storage and making it available on the network and deploying a webserver using a self-assigned URL.

			* [Database](#composable:examples-database)
			* [Web Server](#composable:examples-webserver)

	#composable:examples-database
		:markdown
			### [Database](#composable:examples-database)

			__Deploy a postgis Database__

			1. In the Rancher web interface, select the "anvil" cluster and your project name.
			1. From the Workloads tab, click the __Deploy__ button.
			1. Set the __Name__ for your deployment, i.e. "mydb"
			1. Set __Docker Image__ to the postgis Docker image: `registry.anvil.rcac.purdue.edu/docker-hub-cache/postgis/postgis`
			1. Select the Namespace for your application
			1. Set the postgres user password
				1. Select the Environment Variables drop down
				1. Click the Add Variable button `POSTGRES_PASSWORD = <some password>` You will need this password to connect to the database, so don't forget it! Using Kubernetes secrets is the better way to do this.
			1. Create a persistent volume for your database
				1. Select the __Volumes__ drop down and click Add Volume…
				1. Select "Add a new persistent volume (claim)"
				1. Set a unique volume name, i.e. "mydb-volume"
				1. Use the default Storage Class. The default storage class is Ceph block storage.
				1. Request 2 GiB of storage
				1. Leave Single Node Read-Write checked under __Customize__
				1. Click Define
			1. Provide the default postgres data directory as a Mount Point for the persistent volume: `/var/lib/postgresql/data`
			1. Set __Sub Path__ in Volume to `data`
				1. Click __Show advanced options__ (bottom right of the page)
				1. Click __Security & Host Config__
				1. Under __CPU Reservation__ select __Limit to__ 2000 milli CPUs
				1. Click the __Launch__ button

			Wait a couple minutes while your persistent volume is created and the postgis container is deployed. The "does not have minimum availability" message is expected. But, waiting more than 5 minutes for your workload to deploy typically indicates a problem. You can check for errors by clicking your workload name (i.e. "mydb"), then the lower button on the right side of your deployed pod and selecting __View Logs__.

			If all goes well, you will see an __Active__ status for your deployment.

			__Expose the Database to external clients__

			Use a LoadBalancer service to automatically assign an IP address on a private Purdue network and open the postgres port (5432). A DNS name will automatically be configured for your service as `<servicename>.<namespace>.anvilcloud.rcac.purdue.edu`.

			1. Mouse over the __Resources__ menu and select __Workloads__ in the Rancher UI
			1. Click the __Service Discovery__ tab
			1. Click the __Add Record__ button
				1. Provide a Name
					This will get mapped to <servicename> in your DNS record
			1. Select your Namespace
				1. This will get mapped to <namespace> in your DNS record
			1. Select "One or more workloads" for __Resolves To__
			1. Click __Add Target Workload__
			1. Select your postgis workload (i.e. mydb)
			1. Click __Show advanced options__
			1. Select Layer-4 Load Balancer from the __As a__ dropdown
			1. Under __Port Mapping__, click __Add Port__
				1. Name the port, i.e. postgres-port
				1. Enter 5432 (the default postgres port) under __Publish the service port__
			1. Expand the __Labels & Annotations__ section
				1. Click the __Add Annotation__ button
				1. Enter the following key/value pair: `metallb.universe.tf/address-pool = anvil-private-pool`
			1. Click __Create__

			Kubernetes will now automatically assign you an IP address from the Anvil private IP pool. You can check the IP address by hovering over the “5432/tcp” link on the Service Discovery page or by viewing your service via kubectl on a terminal.

				$ kubectl -n <namespace> get services

			Verify your DNS record was created:

				$ host <servicename>.<namespace>.anvilcloud.rcac.purdue.edu

	#composable:examples-webserver
		:markdown
			### [Web Server](#composable:examples-webserver)

			__Nginx Deployment__

			In this example, we will deploy an nginx web server and use a Kubernetes Ingress to define a custom URL for the server.

			1. In the Rancher web interface, select the “anvil” cluster and your project name.
			1. From the Workloads tab, click the Deploy button at the top right of the UI.
			1. Set the Name for your deployment, i.e. “mywebserver”
			1. Set Docker Image to the nginx Docker image: registry.anvil.rcac.purdue.edu/docker-hub-cache/library/nginx
			1. Select the Namespace for your application, or define a new namespace by clicking Add to a new namespace.
			1. Click the Launch button

			Wait a couple minutes while your application is deployed. The "does not have minimum availability" message is expected. But, waiting more than 5 minutes for your workload to deploy typically indicates a problem. You can check for errors by clicking your workload name (i.e. "mywebserver"), then the lower button on the right side of your deployed pod and selecting View Logs

			If all goes well, you will see an __Active__ status for your deployment.

			__Expose the web server to external clients via an Ingress__

			1. Open the Workload page and click the Load Balancing tab
			1. Click the Add Ingress button at the top right of the UI.
			1. Provide a __Name__ (i.e. "myingress") and the __Namespace__ where you deployed nginx.
			1. Select __Specify a hostname to use__ and enter a hostname of your choice, using the anvilcloud subdomain: <hostname>.anvilcloud.rcac.purdue.edu
			1. Put / for __Path__ and select your nginx deployment name (i.e. "mywebserver") as the Target workload.
			1. Use 80 for __Port__
			1. Add any required annotations. If your web application does not exist on the same path in the workload, a rewrite-target annotation is needed. If you specify /myapp as your ingress path and your web application exists at the root of the web server, use this annotation: nginx.ingress.kubernetes.io/rewrite-target: /
			1. The default anvilcloud.rcac.purdue.edu SSL certificate will be used to encrypt traffic.
			1. Click __Save__

				Kubernetes will now automatically provision the DNS name you requested and create an Ingress for your web server. You may have to wait a minute or two for the state to change to "Active". Once the state changes, your web server will be available on the public Internet.

			1. In the __Targets__ column, click the link to the hostname you created on the Load Balancing tab to open your nginx web server. You should see the "Welcome to nginx!" page in your browser.

