#policies
	:markdown
		# [Policies, Helpful Tips and FAQs](#policies)

		Here are details on some ITaP policies for research users and systems.

		* [Software Installation Request Policy](#policies:sw-install)
		* [Helpful Tips](#policies:tips)
		* [Frequently Asked Questions](#policies:faq)

	#policies:sw-install
		:markdown
			## [Software Installation Request Policy](#policies:sw-install)

			The Anvil team will go to every effort to provide a broadly useful set of popular software packages for users. However, many domain-specific packages that may only be of use to single users or small groups of users are beyond the capacity of research computing staff to fully maintain and support. Please consider the following if you require software that is not available via the module command:

			* If your lab is the only user of a software package, Anvil staff may recommend that you install your software privately, either in your home directory or in your allocation project space. If you need help installing software, Anvil support team may be able to provide limited help.
			* As more users request a particular piece of software, Anvil staff may decide to provide the software centrally. Matlab, Python (Anaconda), NAMD, GROMACS, and R are all examples of frequently requested and used centrally-installed software.
			* Python modules that are available through the Anaconda distribution will be installed through it. Anvil staff may recommend you install other Python modules privately.

			If you're not sure how your software request should be handled or need help installing software please contact us at [Help Desk](/help-desk).

	#policies:tips
		:markdown
			## [Helpful Tips](#policies:tips)

			We will strive to ensure that Anvil serves as a valuable resource to the national research community. We hope that you the user will assist us by making note of the following:

			* You share Anvil with thousands of other users, and what you do on the system affects others. Exercise good citizenship to ensure that your activity does not adversely impact the system and the research community with whom you share it. For instance: do not run jobs on the login nodes and do not stress the file system.

			* Help us serve you better by filing informative help desk tickets.  Before submitting a help desk ticket do check what the user guide and other documentation say. Search the internet for key phrases in your error logs; that's probably what the consultants answering your ticket are going to do. What have you changed since the last time your job succeeded?

			* __Describe your issue as precisely and completely as you can:__ what you did, what happened, verbatim error messages, other meaningful output. When appropriate, include the information a consultant would need to find your artifacts and understand your workflow: e.g. the directory containing your build and/or job script; the modules you were using; relevant job numbers; and recent changes in your workflow that could affect or explain the behavior you're observing.

			* __Have realistic expectations.__ Consultants can address system issues and answer questions about Anvil. But they can't teach parallel programming in a ticket, and may know nothing about the package you downloaded. They may offer general advice that will help you build, debug, optimize, or modify your code, but you shouldn't expect them to do these things for you.

			* __Be patient.__ It may take a business day for a consultant to get back to you, especially if your issue is complex. It might take an exchange or two before you and the consultant are on the same page. If the admins disable your account, it's not punitive. When the file system is in danger of crashing, or a login node hangs, they don't have time to notify you before taking action.

			<p class="portlet-msg-info">For GPU jobs, make sure to use `--gpus-per-node` command instead of `--gpu` or `-G`, otherwise your job may not run properly.</p>

	#policies:tools
		:markdown
			## [Helpful Tools](#policies:tools)

			The Anvil cluster provides a list of useful auxiliary tools:

		#table7
			:markdown
				[The following table provides a list of auxiliary tools:](#table7)

		%table(border="1" cellpadding="5" cellspacing="3")
			%tr
				%th Tool
				%th Use
			%tr
				%td myquota
				%td Check the quota of different file systems
			%tr
				%td flost
				%td A utility to recover files from snapshots
			%tr
				%td showpartitions
				%td Display all Slurm partitions and their current usage
			%tr
				%td myscratch
				%td Show the path to your scratch directory 
			%tr
				%td jobinfo
				%td Collates job information from the sstat, sacct and squeue SLURM commands to give a uniform interface for both current and historical jobs
			%tr
				%td sfeatures
				%td Show the list of available constraint feature names for different node types.
			%tr
				%td myproject
				%td print the location of my project directory
			%tr
				%td mybalance 
				%td Check the allocation usage of your project team

	#policies:faq
		:markdown
			## [Frequently Asked Questions](#policies:faq)

			Some common questions, errors, and problems are categorized below. Click the Expand Topics link in the upper right to see all entries at once. You can also use the search box above to search the user guide for any issues you are seeing.

	#policies:faq-aboutanvil
		:markdown
			### [About Anvil](#policies:faq-aboutanvil)

	#policies:faq-maillist
		:markdown
			#### [Can you remove me from the Anvil mailing list?](#policies:faq-maillist)

			Your subscription in the Anvil mailing list is tied to your account on Anvil which was granted to you through an XSEDE allocation. If you are no longer using your account on Anvil, you can contact your PI or allocation manager to remove you from their Anvil allocation.

	#policies:faq-diffs
		:markdown
			#### [How is Anvil different than Purdue Community Clusters?](#policies:faq-diffs)

			Anvil is part of the national XSEDE ecosystem and is not part of Purdue <a target="_blank" rel="noopener noreferrer" href="https://www.rcac.purdue.edu/services/communityclusters">Community Clusters program</a>. There are a lot of similarities between the systems, yet there are also a few differences in hardware, software and overall governance. For Purdue users accustomed to the way Purdue supercomputing clusters operate, the following summarizes key differences between RCAC clusters and Anvil.

	#policies:faq-support
		:markdown
			##### [Support](#policies:faq-support)

			While Anvil is operated by Purdue RCAC, it is an XSEDE resource, and all support requests have to go through XSEDE channels rather than RCAC ones. Please direct your Anvil questions to the XSEDE Help Desk (a.k.a. help@xsede.org) and they will be dispatched to us.

	#policies:faq-allocations
		:markdown
			##### [Resource Allocations](#policies:faq-allocations)

			Two key things to remember on Anvil and other XSEDE resources:

			1. In contrast with Community Clusters, you do not buy nodes on Anvil. To access Anvil, PIs must request an allocation through XSEDE.
			1. Users don't get access to a dedicated "owner" queue with X-number of cores. Instead, they get an allocation for Y-number of core-hours. Jobs can be submitted to any of the predefined partitions.

			More details on these differences are presented below.

			* Access to Anvil is free (no need to purchase nodes), and is governed by XSEDE [allocation policies](/allocations/policies). All allocation requests must be submitted via [XSEDE Resource Allocation System (XRAS)](/submit-request). Note that this process will change under the new NSF ACCESS program, e.g., a simpler and quicker process for smaller requests.

			The recommended general flow for new XSEDE users is to apply for a [Startup allocation](/allocations/startup), and later advance into a full-blown [Research allocation](/allocations/research). Startup allocations have simplified application requirements, quick turn-around time and no fixed schedule (can be submitted at any time). They are designed to put you on the system quickly - and give you time to proceed with a full-scale Research allocation application (including collecting the necessary supporting benchmarking data, etc). Research allocations are much larger (but have a somewhat higher “burden of proof” and more detailed applications), and they are peer-reviewed on a quarterly schedule rather than continuously. [Education allocations](/allocations/education) are distinctly different from Research allocations, and are intended for academic courses or training activities that have specific begin and end dates. Education allocation requests are evaluated continuously throughout the year.

			* Unlike the Community Clusters model (where you "own" certain amount of nodes and can run on them for the lifetime of the cluster), under XSEDE model, you apply for resource allocations on one or more XSEDE systems, and your project is granted certain amounts of Service Units (SUs) on each system. Different XSEDE centers compute SUs differently, but in general SUs are always some measure of CPU-hours or similar resource usage by your jobs. The Anvil [Job Accounting](#running-accounting) page provides more details on how we compute SU consumption on Anvil. Once granted, you can use your allocation's SUs until they are consumed or expired, after which the allocation must be renewed via established XSEDE process (note: no automatic refills, but there are options to extend the time to use up your SUs and request additional SUs as supplements). You can check your allocation balances on XSEDE User Portal, or use a local `mybalance` command in Anvil terminal window.

	#policies:faq-accounts
		:markdown
			##### [Accounts and Passwords](#policies:faq-accounts)

			* Your Anvil account is not the same as your Purdue Career Account. Following XSEDE procedures, you will need to create an XSEDE User Portal (XUP) account (it is these XSEDE user names that your PI or project manager adds to their allocation to grant you access to Anvil). Your Anvil user name will be automatically derived from XSEDE account name, and it will look something similar to `x-XSEDEname`, starting with an `x-`.
			* Anvil does not support password authentication, and there is no “Anvil password”. Nor does it support BoilerKey. The only accepted authentication methods are the standard XSEDE gsissh logins from [XSEDE SSO Hub](/single-sign-on-hub) and SSH with public key-based authentication ("SSH keys"). Please see the user guide for [detailed descriptions](#access:login) and [steps to configure and use your SSH keys](#access:sshkeys).

	#policies:faq-storage
		:markdown
			##### [Storage and Filesystems](#policies:faq-storage)

			* Anvil scratch purging policies (see the [Filesystems](#files-systems) section) are significantly more stringent than on Purdue RCAC systems. Files not accessed for 30 days are deleted instantly and automatically (on the filesystem's internal policy engine level). *Note: there are no warning emails!*
			* Purdue Data Depot is not available on Anvil, but every allocation receives a dedicated project space (`$PROJECT`) shared among allocation members in a way very similar to Data Depot. See the [filesystems section](#files-systems) in the user guide for more details. You can transfer files between Anvil and Data Depot or Purdue clusters using any of the mutually supported methods (e.g. SCP, SFTP, rsync, Globus).
			* Purdue Fortress is available on Anvil, but direct HSI and HTAR are currently not supported. You can transfer files between Anvil and Fortress using any of the mutually supported methods (e.g. SFTP, Globus).
			* Anvil features Globus Connect Server v5 which enables direct HTTPS access to data on Anvil Globus collections right from your browser (both uploads and downloads).

	#policies:faq-partitions
		:markdown
			##### [Partitions and Node Types](#policies:faq-partitions)

			* Anvil consists of several types of compute nodes (regular, large memory, GPU-equipped, etc), arranged into [multiple partitions](#running-queues) according to various hardware properties and scheduling policies. Your are free to direct your jobs and use your SUs in any partition that suits your jobs' specific computational needs and matches your allocation type (CPU vs. GPU). Note that different partitions may "burn" your SUs at a different rate - see Anvil [Job Accounting](#running-accounting) page for detailed description.

			*Corollary:* On Anvil, you need to specify both allocation account and partition for your jobs (`-A allocation` and `-p partition` options), otherwise your job will end up in the default wholenode partition, which may or may not be optimal. See [Partitions](#running-queues) page for details.

			* There are no `standby`, `partner` or `owner-`type queues on Anvil. All jobs in all partitions are prioritized equally.

	#policies:faq-software
		:markdown
			##### [Software Stack](#policies:faq-software)

			* Two completely separate software stacks and corresponding Lmod module files are provided for CPU- and GPU-based applications. Use `module load modtree/cpu` and `module load modtree/gpu` to switch between them. The CPU stack is loaded by default when you login to the system. See [Example Jobs](#running-examples) section for specific instructions and submission scripts templates.

	#policies:faq-composable
		:markdown
			##### [Composable Subsystem](#policies:faq-composable)

			* A [composable subsystem](#composable) alongside of the main HPC cluster is a uniquely empowering feature of Anvil. Composable subsystem is a Kubernetes-based private cloud that enables researchers to define and stand up custom services, such as notebooks, databases, elastic software stacks, and science gateways.

	#policies:faq-else
		:markdown
			##### [Everything Else](#policies:faq-else)

			* The above list provides highlights of major differences an RCAC user would find on Anvil, but it is by no means exhaustive. Please refer to Anvil user guide for detailed descriptions, or reach out to us through XSEDE Help Desk (help@xsede.org)

	#policies:faq-login
		:markdown
			### [Logging In & Accounts](#policies:faq-login)

			* [Can I use browser-based Thinlinc to access Anvil?](#policies:faq-browser)
			* [What is my username and password to access Anvil?](#policies:faq-username)
			* [What if my ThinLinc screen is locked?](#policies:faq-locked)

	#policies:faq-browser
		:markdown
			#### [Can I use browser-based Thinlinc to access Anvil?](#policies:faq-browser)

			__Problem__

			You would like to use browser-based Thinlinc to access Anvil, but do not know what username and password to use.

			__Solution__

			Password based access is not supported at this moment. Please use Thinlinc Client instead. For your first time login to Anvil, you will have to use SSH client to start an SSH session with XSEDE [single sign-on](/single-sign-on-hub) and set up [SSH keys](#access:sshkeys). Then you are able to use your [native Thinlic client](#access:account-thinlinc) to access Anvil with SSH keys.

	#policies:faq-username
		:markdown
			#### [What is my username and password to access Anvil?](#policies:faq-username)

			__Problem__

			You would like to login to Anvil, but do not know what username and password to use.

			__Solution__

			Currently, you can access Anvil through:

			* __SSH client:__

			You can login with [XSEDE single sign-on](/single-sign-on-hub) or use [SSH keys](#access:sshkeys).

			* __Native Thinlinc Client:__

			You can access native [Thinlic client](#access:account-thinlinc) with [SSH keys](#access:sshkeys).

			* __Open OnDemand:__

			You can access [Open OnDemand](#access:ondemand) with your XSEDE portal username and password.
 
	#policies:faq-locked
		:markdown
			#### [What if my ThinLinc screen is locked?](#policies:faq-locked)

			__Problem__

			Your ThinLinc desktop is locked after being idle for a while, and it asks for a password to refresh it, but you do not know the password.

			<img style="margin-top: 0.8em" src="/documents/10308/2983699/faq-1.jpg/956222b9-52de-4291-86b9-e9b4d527e7e6?t=1652203898412"/>

			In the default settings, the "screensaver" and "lock screen" are turned on, so if your desktop is idle for more than 5 minutes, your screen might be locked.
 
			__Solution__

			If your screen is locked, close the ThinLinc client, reopen the client login popup, and select End existing session.

			Select "End existing session" and try "Connect" again.

			<img style="margin-top: 0.8em" src="/documents/10308/2983699/faq-2.jpg/46fb9af9-134f-443e-9be0-8d0980e0f038?t=1652203913324"/>
 
			To permanently avoid screen lock issues, right click desktop and select Applications, then settings, and select Screensaver.

			<img style="margin-top: 0.8em" src="/documents/10308/2983699/faq-3.jpg/22d8d49e-9fb4-4173-a280-fcb3db612c2f?t=1652203920436"/>

			Select "Applications", then "settings", and select "Screensaver".
 
			Under Screensaver, turn off the Enable Screensaver, then under Lock Screen, turn off the Enable Lock Screen, and close the window.

			<img style="margin-top: 0.8em" src="/documents/10308/2983699/faq-4.jpg/b3eb6deb-25b4-4138-8934-c258b35ce8a0?t=1652203927503"/>

			Under the "Screensaver" tab, turn off the "Enable Screensaver" option.

			<img style="margin-top: 0.8em" src="/documents/10308/2983699/faq-5.jpg/8e37c3e3-96dc-4b4f-9979-b69020db2fa1?t=1652203935444"/>

			Under "Lock Screen" tab, turn off the "Enable Lock Screen" option.

	#policies:faq-errors
		:markdown
			### [Jobs/Errors](#policies:faq-errors)

			* [Close Firefox / Firefox is already running but not responding](#policies:faq-errors-firefox)
			* [Jupyter: database is locked / can not load notebook format](#policies:faq-errors-jupyter)

	#policies:faq-errors-firefox
		:markdown
			#### [Close Firefox / Firefox is already running but not responding](#policies:faq-jobs-firefox)

			__Problem__

			You receive the following message after trying to launch Firefox browser inside your graphics desktop:

			<pre>Close Firefox
			
			Firefox is already running, but not responding.  To open a new window,
			you  must first close the existing Firefox process, or restart your system.
			
			Link to section 'Solution' of 'Close Firefox / Firefox is already running but not responding'</pre>

			__Solution__

			When Firefox runs, it creates several lock files in the Firefox profile directory (inside `~/.mozilla/firefox/` folder in your home directory). If a newly-started Firefox instance detects the presence of these lock files, it complains.

			This error can happen due to multiple reasons:

			1. Reason: You had a single Firefox process running, but it terminated abruptly without a chance to clean its lock files (e.g. the job got terminated, session ended, node crashed or rebooted, etc).
				* Solution: If you are certain you do not have any other Firefox processes running elsewhere, please use the following command in a terminal window to detect and remove the lock files:
					$ unlock-firefox

			1. Reason: You may indeed have another Firefox process (in another Thinlinc or Gateway session on this or other cluster, another front-end or compute node). With many clusters sharing common home directory, a running Firefox instance on one can affect another.
				* Solution: Try finding and closing running Firefox process(es) on other nodes and clusters.
				* Solution: If you must have multiple Firefoxes running simultaneously, you may be able to create separate Firefox profiles and select which one to use for each instance.


	#policies:faq-errors-jupyter
		:markdown
			#### [Jupyter: database is locked / can not load notebook format](#policies:faq-errors-jupyter)

			__Problem__

			You receive the following message after trying to load existing Jupyter notebooks inside your JupyterHub session:

			<pre>Error loading notebook
			
			An unknown error occurred while loading this notebook.  This version can load notebook formats or earlier. See the server log for details.
			
			Alternatively, the notebook may open but present an error when creating or saving a notebook:
			
			Autosave Failed!
			
			Unexpected error while saving file:  MyNotebookName.ipynb database is locked</pre>

			__Solution__

			When Jupyter notebooks are opened, the server keeps track of their state in an internal database (located inside `~/.local/share/jupyter/` folder in your home directory). If a Jupyter process gets terminated abruptly (e.g. due to an out-of-memory error or a host reboot), the database lock is not cleared properly, and future instances of Jupyter detect the lock and complain.

			Please follow these steps to resolve:

			1. Fully exit from your existing Jupyter session (close all notebooks, terminate Jupyter, log out from JupyterHub or JupyterLab, terminate OnDemand gateway's Jupyter app, etc).
			1. In a terminal window (SSH, Thinlinc or OnDemand gateway's terminal app) use the following command to clean up stale database locks:
			<pre>$ unlock-jupyter</pre>
			1. Start a new Jupyter session as usual.

