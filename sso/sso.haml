<p class="portlet-msg-alert">The Single Sign-On Hub (SSO) will be retired on August 31, 2022. If you use SSO to login into remote systems, such as XSEDE Resource Providers like Bridges 2, Expanse, and others, <a href="https://portal.xsede.org/group/xup/help-desk">contact the helpdesk</a> for information on how to log in directly.  </p>

#top
	:markdown
		XSEDE's Single Sign On (SSO) login hub, <strong><code>login.xsede.org</code></strong>, is a single point-of-entry to the XSEDE cyberinfrastructure. Upon logging into the hub with your [XSEDE User Portal](http://portal.xsede.org) (XUP) username and password, a 12 hour [proxy certificate](http://toolkit.globus.org/toolkit/docs/4.0/security/key-index.html) is automatically generated, allowing you to access XSEDE resources for the duration of the proxy.  You may then `gsissh` to any XSEDE compute resource where you have an account without the need for a resource-specific username and password.  

		<p class="portlet-msg-info">The SSO hub now requires you to authenticate using Multi-Factor Authorization (MFA) with the Duo app. Consult <a href="http://portal.xsede.org/mfa">Multi-Factor Authentication with Duo</a> for account setup instructions.</p>

		You may view a complete listing of your [XSEDE resource accounts](https://portal.xsede.org/group/xup/accounts) in the XSEDE User Portal under the MyXSEDE->Accounts menu item.  For any and all account related questions and problems, please [submit an XSEDE helpdesk ticket](https://portal.xsede.org/help-desk).

		The XSEDE SSO hub only accepts standard SSH incoming connections.  Use of SSH keys is not allowed as this would interfere with the authentication mechanism.

		<p class="portlet-msg-alert">It is your responsibility to verify each system's host key fingerprint prior to connecting to the system for the first time.  See the [XSEDE Resource SSH Keys](https://www.xsede.org/ecosystem/operations/ssh-keys) page for a full listing.</p>

		You are limited to 100MB of storage on the hub. Your directories and data on the SSO Hub are NOT backed up, so use your XSEDE allocated [storage resources](https://www.xsede.org/storage) for data storage.  

#login
	:markdown
		### [Login to the SSO Hub](#login)

		To login to the XSEDE SSO Hub, use your SSH client to start an SSH session on **`login.xsede.org`** with your XSEDE User Portal username and password:

		<pre>localhost$ <b>ssh -l <i>XUPusername</i> login.xsede.org</b></pre>

		XSEDE now requires that you use the XSEDE Duo service for additional authentication, you will be prompted to authenticate yourself further using Duo and your Duo client app, token or other contact method.

		Once logged into the hub, then use the "`gsissh`" utility to login to any XSEDE HPC system where you have an account.  To simplify access to XSEDE HPC systems, host aliases and settings have been defined in the default gsissh client configuration on the SSO Hub. This means that you can use gsissh to login to XSEDE HPC systems by referring only to their short Host alias name, e.g.,

		<pre>[XUPusername@ssohub ~]$ <b>gsissh bridges</b></pre>

		A current list of these gsissh host aliases is provided in the "message of the day" (`/etc/motd`) file displayed upon login, or by executing the "`xsede-gsissh-hosts`" command on the XSEDE SSO Hub. (See [example](#example) below.)

#status
	:markdown
		### [Checking the Status of and Renewing your X.509 Credential on the SSO Hub](#status)

		The X.509 credential obtained on your behalf upon SSH login to the SSO Hub has a 12-hour validity period by default. You can check the remaining validity of this credential with the "`grid-proxy-info`" command.

		To renew your X.509 credential while logged into the SSO Hub, use the "`myproxy-logon`" command; when prompted, enter your XSEDE User Portal password. 

#example
	:markdown
		### [SSO Hub Example Work Session](#example)

		In the example session below, Dr. Jane User, uses the command-line `ssh` utility to log in to the XSEDE SSO Hub, also authenticating with Duo. From there she uses the `gsissh` utility to login to her account on Bridges at PSC. After exiting from Bridges and returning to the SSO Hub, she examines the status of her X.509 credential using `grid-proxy-info` and then renews it using `myproxy-logon`. After listing the currently available `gsissh` host aliases using the `xsede-gsissh-hosts` command, Dr. Jane uses `gsissh` to login to her account on the Maverick subsystem at TACC. Finally Dr. Jane exits the Maverick system, returning to the SSO Hub, and then exits the SSO Hub to return to her local workstation and ending the session.

		<pre>
		Script started on Thu Apr 20 12:00:46 2017
		[my-local-workstation:~ DrJanePhD]$ <b>ssh JaneXUPUser@login.xsede.org</b>
		Please login to this system using your XSEDE username and password:
		password:
		Duo two-factor login for janexupuser

		Enter a passcode or select one of the following options:

		1. Duo Push to XXX-XXX-9999
		2. Phone call to XXX-XXX-9999

		Passcode or option (1-2): <b>1</b>
		Success. Logging you in...
		Last login: Thu Apr 20 12:55:35 2017 from 70.114.204.80

		#  Welcome to the XSEDE Single Sign-On (SSO) Hub!</pre>

		The X.509 credential displays time left in this session: 11 hours and 58 minutes.  

		<pre>
		[JaneXUPuser@ssohub ~]$ <b>grid-proxy-info</b>
		subject  : /C=US/O=National Center for Supercomputing Applications/CN=Susan Lindsey
		issuer   : /C=US/O=National Center for Supercomputing Applications/OU=Certificate Authorities/CN=MyProxy CA 2013
		identity : /C=US/O=National Center for Supercomputing Applications/CN=Susan Lindsey
		type     : end entity credential
		strength : 2048 bits
		path     : /tmp/x509up_u7121
		timeleft : <font color="blue"><b>11:58:38</b></font></pre>


		Jane User connects to PSC's [Bridges](http://portal.xsede.org/psc-bridges), does science, then exits:

		<pre>
		[JaneXUPuser@ssohub ~]$ <b>gsissh bridges</b>
		Last login: Mon Feb 20 14:11:23 2017 from ssohub.iu.xsede.org
		...
		You have connected to br006.pvt.bridges.psc.edu 
		...
		... do science ...
		...
		[janeuser@br006 ~]$ <b>exit</b>
		logout
		Connection to bridges.psc.edu closed.</pre>

		After exiting Bridges, display a list of the available XSEDE resources.  Note that you must have an account on that resource in order to log in. 

		<pre>
		[JaneXUPuser@ssohub ~]$ <b>xsede-gsissh-hosts</b>
		bridges
		comet
		gordon
		mason
		maverick
		osg
		stampede
		supermic
		wrangler-iu
		wrangler-tacc
		xstream</pre>

		Jane User then connects to TACC's [Maverick](http://portal.xsede.org/tacc-maverick) subsystem for more science.  After finishing work and exiting Maverick, she exits the SSO hub, ending the session.

		<pre>
		[JaneXUPuser@ssohub ~]$ <b>gsissh maverick</b>
		...
		Welcome to the Maverick Ivy Bridge/K40 Linux Cluster
		...
		
		login2.maverick(1)$ ... do science ...
		login2.maverick(1)$ <b>exit</b>
		logout
		Connection to maverick.tacc.utexas.edu closed.
		[JaneXUPuser@ssohub ~]$ <b>exit</b>
		logout
		Connection to login.xsede.org closed.
		[my-local-workstation:~ DrJanePhD]$ </pre>

#refs
	:markdown
		### [References](#refs)
	
		* [Multi-Factor Authentication with Duo](http://portal.xsede.org/mfa)
		* Globus' "[`grid-proxy-info`](http://toolkit.globus.org/toolkit/docs/4.0/security/prewsaa/rn01re07.html)" command
		* [SSO Hub with MEG PAM Module and OpenSSH](http://grid.ncsa.illinois.edu/myproxy/ssohub/)
 
		*Last update: August 3, 2022*



