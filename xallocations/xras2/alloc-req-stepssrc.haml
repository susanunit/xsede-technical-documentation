<span style="font-size:225%; font-weight:bold;">XRAS Submit Allocation Request<br />Step-by-Step Guide</span><br />
<em>Last update: March 16, 2022</em>

#top
	:markdown
		# [Summary of Process](#top)
		The [XSEDE Resource Allocation System](https://portal.xsede.org/allocations/about-xras) (XRAS) allows XSEDE users to submit project allocation requests for the use of XSEDE compute, visualization and storage resources. These requests are evaluated and granted by the XSEDE Resource Allocation Committee (XRAC). XSEDE PIs may submit Startup, Education, and Campus Champions allocation requests year-round. Research allocation requests are evaluated by the XRAC at [quarterly meetings](/allocations/research#schedule).

		Users may also submit [Supplemental and Transfer Requests](#transfer) to add units to an existing allocation or move allocation units from one resource to another.

#overview:initial-page
	:markdown
		# [Submit and Edit Allocation Requests](#overview:initial-page)
		The [initial submission page](/submit-request) presents two columns to submitters, <strong>Available Opportunities</strong> and <strong>Requests</strong>. The Available Opportunities column lists currently open submission opportunities for allocation requests of all types. Educational, Startup and Campus Champions opportunities are available year-round, while Research opportunities are available quarterly according to the schedule on the [Research Allocations](/allocations/research#schedule) page. The Requests column displays the user's allocation requests, all those submitted previously and those currently in progress.

		SCREENSHOT 1: Initial Submission Page

		<img alt="submit home page" src="/documents/10308/1774652/xras-shot-1.jpg/6ce7f67b-3f23-4dfe-9a40-91ea9c35f605?t=1517530077305" style="width: 600px; height: 483px; border-width: 1px; border-style: solid;" />

		To begin a new allocation request, select the "Start a New Submission" button under the desired allocation type. For each allocation type, you'll be presented with a list of required items and have the choice of starting a Guided Submission or Advanced Submission. The Guided Submission interface takes the user through seven steps, one page per step as outlined [below](#guided), while the Advanced Submission interface displays a single form on one page. New XSEDE users should make use of the Guided Submission. Experienced users and those familiar with the allocation requirements will find the Advanced Submission quicker.

		SCREENSHOT 2: Choose Guided or Advanced Submission Interface

		<img alt="choose interface" src="/documents/10308/1774652/xras-shot-2.jpg/4da9845d-9779-49a5-879e-1337e0853167?t=1517530077919" style="width: 600px; height: 139px; border-width: 1px; border-style: solid;" />

#guided-submission
	:markdown
		# [Guided Submission](#guided-submission)

		The Guided Submission interface takes the user through seven pages of required and optional information depending on the type of allocation request. At any time users may click on the navigation bar at the top of each page to switch between pages. And also, a user may switch to the "Advanced" view to see all information on one page.
			**Edits are saved automatically. You may exit the submission at any time, returning later to finish any unsubmitted requests.**

		SCREENSHOT 3: Submit Request Navigation

		<img alt="submit request navigation" src="/documents/10308/1774652/xras-shot-3.jpg/06a77bb7-7493-43c8-851f-55e2555ccbbc?t=1517530078139" style="width: 600px; height: 139px; border-width: 1px; border-style: solid;" />

	#guided-submission:personnel
		:markdown
			## [Step 1: Personnel](#guided-submission:personnel)

			Enter personnel associated with an allocation request here. All requests must have a PI. Co-PIs and Allocation Managers are optional. You must know your users' XSEDE Portal usernames. For security reasons XSEDE does not allow username lookups.

			SCREENSHOT 4: Enter Personnel Associated with This Allocation Request

			<img alt="personnel" src="/documents/10308/1774652/xras-shot-4.jpg/55668424-83e7-4d8d-a962-4c2df457bf64?t=1517530078296" style="width: 600px; height: 126px; border-width: 1px; border-style: solid;" />

	#guided-submission:title
		:markdown
			## [Step 2: Title / Abstract](#guided-submission:title)

			Required information on this page includes the title of your request, an abstract, keywords and field of science.

			SCREENSHOT 5: Title and Abstract Information

			<img alt="submit title abstract" src="/documents/10308/1774652/xras-shot-5.jpg/a257e6a6-3ddf-4bc0-89b9-1288ec759a5b?t=1517530078530" style="width: 600px; height: 567px; border-width: 1px; border-style: solid;" />

	#guided-submission:resources
		:markdown
			## [Step 3: Resources](#guided-submission:resources)

			Select one or more resources needed for your allocation request. Please visit the [Resource Monitor](/resource-monitor) and click on the machine name to view each resource's specifications and recommended use.

			Resources are grouped according to type: Compute, Visualization and Storage. Note that some resources require additional companion resource requests, usually in the case of compute resources being accompanied by storage resources.

			SCREENSHOT 6: Select XSEDE-allocated Resources

			<img alt="selecting resources" src="/documents/10308/1774652/xras-shot-6.jpg/a126ce3b-a9ff-4193-a469-0e508e009196?t=1517530078857" style="width: 600px; height: 565px;" />

			Compute resource allocations are requested in a variety of [Service Units (SUs)](/allocations#overview-metrics) while storage allocations are requested in Gigabytes. See the documentation for each resource for clarification on calculating your allocation amounts appropriately.

			SCREENSHOT 7: Compute and Storage Resources

			<img alt="selecting resources" src="/documents/10308/1774652/xras-shot-7.jpg/a2bec65b-4314-42cb-b121-3601f8be7597?t=1517531289798" style="width: 600px; height: 565px; border-width: 1px; border-style: solid;" />

	#guided-submission:documents
		:markdown
			## [Step 4: Documents](#guided-submission:documents)

			Upload all documents pertaining to this allocation request in this section. Documentation requirements differ per type of request. Please see the [Required and Optional Documents](/allocations/policies#632) section in the [Allocations Policies](/allocations/policies), and the sections on [Startup and Education](/allocations/policies#50) and [Research](/allocations/policies#60) allocations for details of submission document requirements.

			SCREENSHOT 8: Upload Supporting Documents

			<img alt="submit supporting documents" src="/image/image_gallery?uuid=befade08-c066-4f67-8063-36cfea59b588&amp;groupId=10746&amp;t=1407865333874" style="width: 600px; height: 180px; border-width: 1px; border-style: solid;" />

	#guided-submission:grants
		:markdown
			## [Step 5: Grants](#guided-submission:grants)

			Supporting grant information informs the reviewers that the science has been reviewed and that sufficient funding (for staff and student time, etc.) is available to complete the work proposed. The Grants section is required for Research requests and optional for all other types of requests. For Startup and Education allocations, supporting grant information helps XSEDE in its reports to NSF. _All current research funding affected by or benefitting from the use of the allocation requested in your research request should be listed._ If your project has one or more supporting grants, complete all required fields for each grant for final submission.

			SCREENSHOT 9: Required Grant Information

			<img alt="required supporting grant information" src="/image/image_gallery?uuid=8fb6bce5-d44c-4a47-9d74-d30d7b1cb352&amp;groupId=10746&amp;t=1407954459820" style="width: 600px; height: 329px; border-width: 1px; border-style: solid;" />

			Most of the information required for each grant is self-explanatory; additional information for some fields is provided below.

			* **PI**: Enter the name of PI on the supporting grant. This is not necessarily the same PI on this allocation request.
			* **Grant Number and Awarded Amount**: Please enter the grant number in the agency-defined format. For grants spanning several years, enter the Award Amount for a 12-month period. For grants covering less than 12 months, enter the full award amount. The amount should be in US dollars, no punctuation or extra characters.
			* **Percentage Support**: Please enter the percentage of the grant amount entered above that will support the work described in this allocation request. Reasonable rounding is acceptable.

	#guided-submission:publications
		:markdown
			## [Step 6: Publications](#guided-submission:publications)

			Users may associate publications to each allocation request. The publications data is pulled from the user's XSEDE User Portal profile. To add publications for your allocation request, proceed to [My XSEDE -> Profile](/group/xup/profile) and click on "Add a Publication". Users can add a publication and associate it with multiple existing projects or with a generic Allocation Request.

			SCREENSHOT 10: Choose Publications to Attach to Request

			<img alt="submit add publications" src="/image/image_gallery?uuid=d5c8fc67-3826-4e42-9c85-332c8f276593&amp;groupId=10746&amp;t=1407954459816" style="border-width: 1px; border-style: solid; width: 600px; height: 283px;" />

	#guided-submission:submit
		:markdown
			## [Step 7: Submit](#guided-submission:submit)

			Submit your allocation request from this page. At this point all entered information is validated, any errors or omissions are flagged and users may use the interface to navigate between sections or use the Advanced interface to access all fields on one page. You'll receive an email confirmation upon successfully finalizing your allocation request.

			SCREENSHOT 11: Correct Any Errors

			<img alt="submit fix errors" src="/image/image_gallery?uuid=24f3e0b9-2176-4999-9e1e-4baf981119aa&amp;groupId=10746&amp;t=1407791801342" style="border-width: 1px; border-style: solid; width: 650px; height: 225px;" />

			Once you've successfully submitted your allocation request, it will appear at the upper right of the [Submission Form](/submit-request) under the "Submitted" heading. Users may edit both incomplete and submitted requests. Approved requests cannot be edited.

#transfer
	:markdown
		# [Supplemental and Transfer Requests](#transfer)

		Allocation managers may also request Supplemental computing time or storage for an existing allocation, or request a Transfer of computing time or storage from one resource to another.

		From the initial submission page, click on "Actions" under the desired allocation, and select either "Transfer" or "Supplement."

		SCREENSHOT 12: Select Supplement or Transfer Option

		<img alt="additional allocation options" src="/documents/10308/948874/XRAS+transfer+allocation+1/bdabc89d-070e-4f48-9ddd-ad0cf9bef92a?t=1431101942073" style="width: 600px; height: 251px; border-width: 1px; border-style: solid;" />

		When requesting a Transfer please select a "From" and a "To" resources, entering a negative balance for the "From" resource and the corresponding positive number for the "To" resource:

		SCREENSHOT 13: Select Transfer Between Two Resources

		<img alt="select two resources" src="/documents/10308/948874/XRAS+transfer+allocation+2/7f85d527-69df-4c67-bb4e-e8c43395d68b?t=1431100207000" style="width: 600px; height: 341px; border-width: 1px; border-style: solid;" />

		You'll receive an email confirmation upon submitting a Supplement or Transfer request. These requests must be reviewed by XSEDE staff and may take a few weeks. Please email [help@xsede.org](mailto:help@xsede.org) with any questions.

#references
	:markdown
		# [References](#references)

		* [XSEDE Allocations page](/allocations/announcements) - allocation policies, process, and eligibility information
		* [XSEDE Resources page](/allocations/resource-info) - specifications for all resources plus customizable comparisons and searches
		* [Quarterly XRAC Calendar](/allocations/research#xracquarterly) - scheduled dates for upcoming submission windows
