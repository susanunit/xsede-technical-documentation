#!/bin/sh

# 01/21/18 this script needs to be updated - many of these variables are not used or can be replaced

input=$1
output=$2

echo "Changing links to presentations"

# URLS
helpdesk="http:\/\/www.xsede.org\/help"
xup="https:\/\/portal.xsede.org"

# New user training presentation
newuserlink="<iframe\ src=\"https:\/\/www.youtube.com\/embed\/Y-VQgDCO7wU\"\ frameborder=\"1\"\ allow=\"accelerometer;\ autoplay;\ encrypted-media;\ gyroscope;\ picture-in-picture\"\ allowfullscreen><\/iframe>"
newuserdate="September 20, 2020"
newuserrt="2 hours 1 minute"

# Writing and submitting a successful XSEDE proposal presentation
# xproplink="<iframe\ src=\"https:\/\/www.youtube.com\/embed\/9NuhvgwnCD8\"\ frameborder=\"1\"\ allow=\"accelerometer;\ autoplay;\ encrypted-media;\ gyroscope;\ picture-in-picture\"\ allowfullscreen><\/iframe>"
xproplink="<iframe\ src=\"https:\/\/www.youtube.com\/embed\/W-tdqZiIM_A\"\ frameborder=\"1\"\ allow=\"accelerometer;\ autoplay;\ encrypted-media;\ gyroscope;\ picture-in-picture\"\ allowfullscreen><\/iframe>"


xpropdate="April 5, 2022"
xproprt="1 hour 29 minutes"

sed	-e "s/NEWUSERLINK/$newuserlink/g" \
	-e "s/NEWUSERDATE/$newuserdate/g" \
	-e "s/NEWUSERRT/$newuserrt/g" \
	-e "s/XPROPLINK/$xproplink/g" \
	-e "s/XPROPDATE/$xpropdate/g" \
	-e "s/XPROPRT/$xproprt/g" \
	-e "s/BASE/$base/g" \
	-e "s/XSEDEUSERPORTAL/$xup/g" \
	-e "s/DATEMODIFIED/$datemodified/g" < $input > $output

