%section#intro
	:markdown
		## [XRAS - Submit Allocation Requests](#home)
		The [XSEDE Resource Allocation System](http://portal.xsede.org/submit-request#) (XRAS) allows XSEDE users to submit project allocation requests for the use of XSEDE compute, visualization and storage resources.  These requests are evaluated and granted by the XSEDE Resource Allocation Committee (XRAC).  XSEDE PIs may submit Startup, Education, Campus Champions and Testbed allocation requests year-round.  Research allocation requests are evaluated by the XRAC at [quarterly meetings](#table1). 

		Users may also submit [Supplemental](#transfers) requests to add additional SUs/storage to an existing allocation, and [Transfer](#transfer) requests to move allocation units from one resource to another.

%section#submitedit
	:markdown
		## [Submit and Edit Allocation Requests](#submitedit)

		The [XRAS home page](https://portal.xsede.org/allocations/xras-testing) presents two columns to submitters, Available Opportunities and Requests.  The Available Opportunities column lists currently open submission opportunities for allocation requests of all [types](#table2).  Research, Educational, Startup and Campus Champions opportunities are available year-round, while Research opportunities are available quarterly according to the schedule in [Table 1](#table1).  The Requests column displays the user's allocation requests, all those submitted previously and those currently in progress.  

		<u>SCREENSHOT: XRAS Submit Home Page</u>

		<img alt="submit home page" src="/image/image_gallery?uuid=610320e9-62e4-43aa-8f51-5217b4bd0949&amp;groupId=10746&amp;t=1407791801343" style="width: 600px; height: 437px; border-width: 1px; border-style: solid;" />


	:markdown

		To begin a new allocation request, select the "Start a New Submission" button under the desired allocation type.  For each allocation type you'll be presented with a list of required items, and have the choice of starting a Guided Submission or Advanced Submission.  The Guided Submission interface takes the user through seven steps, one page per step as outlined [below](#guidedsubmission), while the Advanced Submission interface displays a single form on one page.  New XSEDE users should make use of the Guided Submission.  Experienced users and those familiar with the allocation requirements will find the Advanced Submission quicker.

		<u>SCREENSHOT: Choose Between Guided or Advanced Submission Interface</u>

		<img alt="choose interface" src="/image/image_gallery?uuid=a551fad0-86de-4ff0-a5ca-fba132334334&amp;groupId=10746&amp;t=1407791801343" style="width: 500px; height: 188px; border-width: 1px; border-style: solid;" />

%section#guidedsubmission
	:markdown
		## [XRAS Guided Submission](#guidedsubmission)

		The Guided Submission wizard takes the user through seven pages of required and optional information depending on type of allocation request.  At any time users may click on the navigation bar at the top of each page to switch between pages.  And also, a user may switch to the "Advanced" view to see all information on one page.

		**Edits are saved automatically.  You may exit the XRAS application at any time, returning later to finish any unsubmitted requests.**

		<u>SCREENSHOT: Submit Request Navigation</u>

		<img alt="submit request navigation" src="/image/image_gallery?uuid=f77458a5-4f88-4a1e-ab42-6a2ca713cccf&amp;groupId=10746&amp;t=1407791801342" style="width: 650px; height: 101px; border-width: 1px; border-style: solid;" />

		
%section#personnel
	:markdown
		#### [Step 1. Personnel](#personnel)

		Enter personnel associated with an allocation request here.  All requests must have a PI.  Co-PIs and Allocation Managers are optional.  You must know your users' XUP usernames.  For security reasons XSEDE does not allow username lookups.

		<u>SCREENSHOT: Enter Personnel Associated with this Allocation Request</u>

		<img alt="personnel" src="/image/image_gallery?uuid=f742f537-6e3e-46a0-8700-39ecbaffbb54&amp;groupId=10746&amp;t=1407791801342" style="width: 600px; height: 160px; border-width: 1px; border-style: solid;" />
		
%section#title
	:markdown
		#### [Step 2. Title / Abstract](#title)

		Required information on this page includes the title of your request, an abstract, keywords and field of science.

		<u>SCREENSHOT: XRAS Title and Abstract Information</u>

		<img alt="submit title abstract" src="/image/image_gallery?uuid=9193df62-5ae8-4b0e-a764-62d1d0327db1&amp;groupId=10746&amp;t=1407865333874" style="width: 600px; height: 346px; border-width: 1px; border-style: solid;" />


%section#resources
	:markdown
		#### [Step 3. Resources](#resources)

		Select one or more resources needed for your allocation request.  Please visit the [Resources Overview](http://www.xsede.org/resources) page and click on the machine name to view each resource's specifications and recommended use.

		Resources are grouped according to type: Compute, Vis and Storage.  Note that some resources require additional companion resource requests, usually in the case of compute resources being accompanied by storage resources.  Companion resources include but are not limited to:

		* PSC's [Blacklight](http://www.xsede.org/psc-blacklight) (compute) and [Data Supercell](http://www.xsede.org/psc-datasupercell) (storage)
		* SDSC's [Gordon Compute Cluster]() (compute) and [Data Oasis]() (storage)
		* SDSC's [Trestles]() (compute) and [Data Oasis]() (storage)	

		<u>SCREENSHOT: Select XSEDE Resources</u>

		<img alt="submit select XSEDE resources" src="/image/image_gallery?uuid=2205998a-4837-4583-a322-faf808c533eb&amp;groupId=10746&amp;t=1407944800032" style="width: 600px; height: 251px; border-width: 1px; border-style: solid;" />

		Compute resource allocations are requested in [Service Units](http://www.xsede.org/allocations#overview-metrics) (SUs) while storage allocations are requested in Gigabytes.
	
		<u>SCREENSHOT: Compute and Storage Resources</u>

		<img alt="resources2" src="/image/image_gallery?uuid=56661119-7148-43a0-b34b-515adb5c4621&amp;groupId=10746&amp;t=1407944800032" style="width: 600px; height: 305px; border-width: 1px; border-style: solid;" />

%section#documents
	:markdown
		#### [Step 4. Documents](#documents)
	
		Upload all documents pertaining to this allocation request in this section.  Documentation requirements differ per type of request.  Please see [Table 3](#table3) below for document types and descriptions and the [Required and Optional Documents](https://www.xsede.org/allocation-policies#submitresearch-docs-required) section in the [Allocations Policies](https://www.xsede.org/allocation-policies) for detailed information.
 
		<u>SCREENSHOT: Upload Supporting Documents</u>

		<img alt="submit supporting documents" src="/image/image_gallery?uuid=befade08-c066-4f67-8063-36cfea59b588&amp;groupId=10746&amp;t=1407865333874" style="width: 600px; height: 180px; border-width: 1px; border-style: solid;" />
	
%section#grants
	:markdown
		#### [Step 5. Grants](#grants)

		Supporting grant information informs the reviewers that the science has been reviewed and that sufficient funding (for staff and student time, etc.) is available to complete the work proposed. For startup and education allocations, supporting grant information is not required, but helps XSEDE in its reports to NSF. <i>All current research funding affected by or benefitting from the use of the allocation requested in your research request should be listed</i>.  The Grants section is required for Research requests and optional for all other types of requests.  If your project has one or more supporting grants, complete all required fields for each grant for final submission.  


		<u>SCREENSHOT: Required Grant Information</u>

		<img alt="required supporting grant information" src="/image/image_gallery?uuid=8fb6bce5-d44c-4a47-9d74-d30d7b1cb352&amp;groupId=10746&amp;t=1407954459820" style="width: 600px; height: 329px; border-width: 1px; border-style: solid;" />

		Most of the information required for each grant is self-explanatory; additional information for some fields is provided below. 

		* **PI**:  Enter the name of PI on the grant.  This is not necessarily the same PI on this allocation request.
		* **Grant Number and Awarded Amount**:  Please enter the grant number in the agency-defined format. For grants spanning several years, enter the Award Amount for a 12-month period. For grants covering less than 12 months, enter the full award amount. The amount should be in US dollars, no punctuation or extra characters.
		* **Percentage Support**:  Please enter the percentage of the grant amount entered above that will support the work described in this allocation request. Reasonable rounding is acceptable.
		

%section#publications
	:markdown
		#### [Step 6. Publications](#publications)

		Users may associate publications to each allocation request. The publications data is pulled from the user's XSEDE User Portal profile. To add publications for your allocation request proceed to [My XSEDE -> Profile](https://portal.xsede.org/group/xup/profile), click on "Add a Publication". Users can add a publication and associate it with multiple existing projects or with a generic Allocation Request.

		<u>SCREENSHOT: Choose Publications to Attach to Request</u>

		<img alt="submit add publications" src="/image/image_gallery?uuid=d5c8fc67-3826-4e42-9c85-332c8f276593&amp;groupId=10746&amp;t=1407954459816" style="border-width: 1px; border-style: solid; width: 600px; height: 283px;" />

%section#submit
	:markdown
		#### [Step 7. Submit](#submit)

		Submit your allocation request from this page.  At this point all entered information is validated, any errors or omissions are flagged and users may use the wizard to navigate between sections or use the Advanced interface to access all fields on one page.

		<u>SCREENSHOT: Correct Any Errors</u>

		<img alt="submit fix errors" src="/image/image_gallery?uuid=24f3e0b9-2176-4999-9e1e-4baf981119aa&amp;groupId=10746&amp;t=1407791801342" style="border-width: 1px; border-style: solid; width: 650px; height: 225px;" />

		Once you've successfully submitted your allocation request, it will appear on the far right of the [XRAS home page](https://portal.xsede.org/allocations/xras-testing) under the "Submitted" heading.  Users may edit both incomplete and submitted requests.  Approved requests are not editable.

%section#transfers
	:markdown
		## [Supplemental and Transfer Requests](#transfers)

		Allocation managers may also request supplemental computing time (SUs) or storage (GBs) to an existing allocation, or request a transfer of computing time or storage from one resource to another.  

		From the XRAS home page, click on "Actions" under the desired allocation, and select either "Transfer" or "Supplement".

		<u>SCREENSHOT: Select "Supplement" or "Transfer" Option</u>

		<img alt="additional allocation options" src="/documents/10308/948874/XRAS+transfer+allocation+1/bdabc89d-070e-4f48-9ddd-ad0cf9bef92a?t=1431101942073" style="width: 600px; height: 251px; border-width: 1px; border-style: solid;" />

		When requesting a transfer please select a "From" and a "To" resources, entering a negative balance for the "From" resource and the corresponding positive number for the "To" resource:

		<u>SCREENSHOT: Select Transfer Between Two Resources</u>

		<img alt="select two resources" src="/documents/10308/948874/XRAS+transfer+allocation+2/7f85d527-69df-4c67-bb4e-e8c43395d68b?t=1431100207000" style="width: 600px; height: 341px; border-width: 1px; border-style: solid;" />

		You'll receive an email confirmation upon submitting a supplement or transfer request. These requests must be reviewed by XSEDE staff and may take a few weeks.  Please email <allocations@xsede.org> with any questions.

		
		
%section#refs
	:markdown
		## [References](#refs)  

		* [Allocations Overview](https://www.xsede.org/web/guest/allocations) - allocation policies, process, and eligibility information 
		* [XSEDE Resources Overview](https://www.xsede.org/web/guest/resources) - specifications for all resources plus customizable comparisons and searches 

%section#appendix
	:markdown
		## [Appendix](#appendix)

		* [Table 1. Research Allocation Submission Schedule](#table1)
		* [Table 2. Allocation Types](#table2)
		* [Table 3. Document Types and Descriptions](#table3)

%section#table1
	:markdown
		[Table 1. Research Allocation Submission Schedule](#table1)

	%table(border="1" cellpadding="3" cellspacing="3")
		%tr
			%th Submit Requests during 
			%th for the Allocation Period of
		%tr
			%td December 15 thru January 15
			%td April 1 thru March 31
		%tr
			%td March 15 thru April 15
			%td July 1 thru June 30
		%tr
			%td June 15 thru July 15
			%td October 1 thru September 30
		%tr
			%td September 15 thru October 15
			%td January 1 thru December 31

%section#table2
	:markdown
		[Table 2. Allocation Types](#table2)

	%table(border="1" cellpadding="3" cellspacing="3")
		%tr
			%th Allocation Type
			%th Purpose
		%tr
			%td Research 
			%td year-long allocation for specific grant supported projects
		%tr
			%td Education 
			%td for classroom instruction and training courses
		%tr
			%td Startup
			%td for investigators new to XSEDE
		%tr
			%td Campus Champions 
			%td Campus Champions are entitled to login access to computational facilities and the XSEDE User Portal just as any user of XSEDE. In order to gain such access, each champion must apply for a Campus Champion allocation
		%tr
			%td Testbeds 
			%td for researchers needing a testbed to support experimental computer science research, software evaluation and testing, education and training, as well as the development of computational science applications

%section#table3
	:markdown
		[Table 3. Document Types and Descriptions](#table3)

	%table(border="1" cellpadding="3" cellspacing="3")
		%tr
			%th Document Type
			%th Description
		%tr
			%td PI CV
			%td The Principal Investigator's CV
		%tr 
			%td Syllabus
			%td Required for Education allocations
		%tr
			%td Main Document
			%td Required for Research allocations, contains scientific background and research justification
		%tr
			%td Code Performance and Scaling
			%td Required documentation for Research allocations


%section#refs
	:markdown
		## [References](#refs)  

		* [Allocations Overview](https://www.xsede.org/web/guest/allocations) - allocation policies, process, and eligibility information 
		* [XSEDE Resources Overview](https://www.xsede.org/web/guest/resources) - specifications for all resources plus customizable comparisons and searches 


		*Last update: January 25, 2017*
