<h3> Allocated Resource Listing</h3>
<table border="0" cellpadding="0" cellspacing="0" class="display" id="rdr_data" width="800"> <thead> <tr> <th> Resource</th> <th> Org</th> <th> Type</th> <th> Startup Allocation Limit</th> <th> User Guide</th> </tr> </thead> <tbody> </tbody> </table>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script><script type="text/javascript">

jQuery(document).ready(getRDR());

function getRDR(){
		jQuery.ajax({
		   url: 'https://api.xsede.org/systems/v1/',
           cache: 'false',
           error: function(err, msg){
            if(console) console.log("sdl"+msg);
           },
           dataType: 'json',
		   success: function(jsonresp) {
				rdrData = jsonresp;
				setData(jsonresp);
		   }
		});	
}

function setData(jsonresp){
			jQuery('#rdr_data').DataTable( {
				"data":  jsonresp.result,
				"paging": false,
				"searching" : false,
			    columns: [
			        { data: "descriptive_name", "defaultContent": "" },
			        { data: "organization_abbrev", "defaultContent": ""  },
			        { data: ".resource_type", "defaultContent": ""  },
			        { data: "startup_allocation_limit", "defaultContent": "N/A"  },
                    { data: "user_guide_url", render: function ( data, type, row ) { return '<a href="'+data+'">'+'User Guide</a>'; 			}} ]});}

</script>
<p>
	<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
.dataTables_paginate{width:auto;}	</style>
</p>

