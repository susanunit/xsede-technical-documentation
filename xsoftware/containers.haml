<span style="font-size:235%; font-weight:bold;">Containers at XSEDE</span><br>
<span style="font-size:95%;"><i>Last update: October 13, 2021</span></i>

#top
	:markdown
		# [What are Containers?](#top)

		Containers are a standard unit of software that package up code and all its dependencies so that an application can run quickly and reliably on different computing platforms. There are a number of types of containers but the 2 widely used versions are Docker and Singularity. The differences between them are subtle but distinctive.  

		Singularity is a runtime container, like Docker, but doesn't require the specialized privileges that Docker requires. As such, many sites find Singularity a useful tool not only for administrators, but users as well.  

	#containersvms
		:markdown
			## [Containers vs. Virtual Machines](#containersvms)

		Containers are similar to virtual machines (VMs) since they include everything needed to run in a single package. However, unlike VMs, containers do not include a guest OS. Instead containers run on top of a "container platform," like Docker or Singularity, which is installed on an operating system. See figure 1 below.  

		Containers are "lightweight," meaning they require far less disk space than VMs. Additionally, multiple containers can run side-by-side on the same container platform.

		<p><img alt="Containers vs VMs" src="/documents/10308/900501/Containers+%26+VMs/99c93cdf-e909-410d-b809-f750bc29e6b1?t=1607454746141"/></p>

#resources
	:markdown
		# [XSEDE Container Resources](#resources)

		Each XSEDE site offers different container related resources. Cloud resources like Jetstream focus specifically on providing users with container resources. Other sites and resources may have different focus areas but still provide access to containers.  

		As mentioned above, Docker and Singularity are the two most common types of containers. Some XSEDE sites discourage usage of Docker as it requires users to have escalated root privileges. Another popular container tool, Kubernetes is available at most XSEDE sites, but is more widely used on Jetstream which provides users assistance using the tool. 

		Several container tutorials were developed and presented at various conferences. The <a href="https://github.com/XSEDE/Container_Tutorial" target="_blank">XSEDE Container Tutorial</a> is available on GitHub.

		Each site&#39;s area of expertise and offering for Containers is listed below: 

	#resources-jetstream
		:markdown
			## [IU/TACC's Jetstream ](#resources-jetstream)

			Basic Docker container support is provided through pre-installed Docker and Docker Compose packages, available in many of the staff-maintained and featured CentOS 7, CentOS 8, Ubuntu 16, Ubuntu 18, and Ubuntu 20 images. Users of Jetstream’s Atmosphere management interface have additional easy-to-use built-in command line options to quickly install Docker and Singularity on any image. Since Jetstream is a cloud provider, most container technologies are available -- users get root access so they can use Docker, Singularity, CharlieCloud, Shifter, or whatever else they may need.

			Other Helpful Links: 

			* [Jetstream Atmosphere EZ tools](http://wiki.jetstream-cloud.org/Jetstream+Atmosphere+EZ+tools)
			* [Container Orchestration on Jetstream](http://wiki.jetstream-cloud.org/Container+Orchestration+on+Jetstream)
			* [Container Orchestration Using the OpenStack Horizon GUI Interface](http://wiki.jetstream-cloud.org/Container+Orchestration+Using+the+OpenStack+Horizon+GUI+Interface)
			* [Deploy scalable Jupyterhub with Kubernetes on Jetstream](https://zonca.dev/2017/12/scalable-jupyterhub-kubernetes-jetstream.html)
			* [Deploy Kubernetes and JupyterHub on Jetstream with Magnum](https://zonca.dev/2020/05/kubernetes-jupyterhub-jetstream-magnum.html)
			* [Kubeadm Bootstrapper](https://github.com/data-8/kubeadm-bootstrap/)
			* [Jetstream Public Wiki](http://wiki.jetstream-cloud.org/)

			The XSEDE link to Jetstream is <a href="https://portal.xsede.org/jetstream">Jetstream User Guide</a>. 

	#resources-bridges-2
		:markdown
			## [PSC's Bridges-2](#resources-bridges-2)

			PSC Supports Singularity containers only. PSC offers a variety of pre-built containers for users, or users are welcome to build and deploy their own. This can be done through a reservation, the &#39;interact&#39; mode, or by requesting a VM and running containers on that VM.

			* <a href="https://portal.xsede.org/psc-bridges#containers">Containers on Bridges-2</a>
			* <a href="http://psc.edu/resources/software/singularity">Singularity at PSC </a>
			* <a href="https://www.psc.edu/resources/bridges-2/user-guide/">Bridges-2 User Guide</a></ul>

	#resources-expanse
		:markdown
			## [SDSC's Expanse](#resources-expanse)

			SDSC continues to support Singularity-based containerization on *Expanse*. Users can search for available software on XSEDE resources with the XSEDE software search tool.

			* <a href="http://portal.xsede.org/sdsc-expanse#software">Expanse User Guide</a>
			* <a href="https://www.sdsc.edu/education_and_training/tutorials1/about_comet_singularity_containers.html">Running Singularity Containers on SDSC Resources</a></ul>

	#resources-stampede2
		:markdown
			## [TACC's Stampede2 ](#resources-stampede2)

			 TACC offers training for Singularity containers - including <a href="https://containers-at-tacc.readthedocs.io/en/latest/containers/00.overview.html" target="_blank">Docker containers converted to Singularity</a> on their systems (including Stampede2), <a href="https://github.com/TACC/tacc-containers" target="_blank">curated container images</a> for developers to use as a base container image to build their application to run at scale on various TACC systems, and documentation on <a href="https://containers-at-tacc.readthedocs.io/en/latest/singularity/03.mpi_and_gpus.html" target="_blank">how and where to use and interact with</a> these base images.

			TACC-supported images are specifically *not portable*, contrary to typical containerization philosophy, due to the added hardware-specific drivers needed for TACC systems.

#quickstart
	:markdown
		# [Quick Start Guide](#quickstart)

		A Quick Start guide can help users become familiar with Singularity and containers.  One offered by Singularity is available here.  <a href="https://sylabs.io/guides/3.6/user-guide/quick_start.html">Singularity Quick Start Guide</a>

#containerlist
	:markdown
		# [XSEDE Container List ](#containerlist)

		See the following web site for more information about public container registries: <a href="https://software.xsede.org/tiny-registries">https://software.xsede.org/tiny-registries</a>

#support
	:markdown
		# [Consulting and Extended Support ](#support)

		Users needing assistance finding and using container software, services, training, or needing lightweight consulting may contact the XSEDE help desk at help@xsede.org. The Novel and Innovative Projects (NIP) team can provide this type of lightweight assistance. XSEDE allocated projects needing more in depth consulting on data preparation, data flows, data analytics, data locality, data visualization and other data techniques can <a href="https://www.xsede.org/for-users/ecss">request Extended Collaborative Support Services (ECSS) assistance</a>. The Novel and Innovative Projects (NIP) area of ECSS provides lightweight consulting to assist in this process. This can be requested via help@xsede.org.

#refs
	:markdown
//		# [References](#refs)

//		* [Jetstream User Guide](https://iujetstream.atlassian.net/wiki/spaces/JWT/pages/76286652/Table+of+Contents)

