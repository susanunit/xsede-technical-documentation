#top
	:markdown
		# [Globus at XSEDE](#top)

		XSEDE provides the [Globus](https://www.globus.org/) service for file transfer and sharing. Designed specifically for researchers, Globus provides fast, reliable, and secure file transfer among XSEDE resources or between an XSEDE resource and another system (such as a campus cluster, lab server, or personal computer). Beyond file transfer, Globus allows researchers to securely share data with collaborators, and to publish data for broader access as required by many data management plans.

		Use Globus to:

		* Move data between any two systems: in XSEDE, at your campus or laboratory, or at your home.  Easily move small numbers of very large files (even terabyte-sized) as well as large numbers of small files (thousands or more at a time). 
		* Share data with your colleagues and collaborators, whether the data is stored at your laboratory on campus, your office, your home, or on selected XSEDE resources.

		With Globus:

		* You don't have to move your data to a public cloud service to share it. 
		* You have full control over what data is shared and with whom.
		* You can share data with colleagues who aren't registered with XSEDE.

#figure1
	:markdown
		[Figure 1. The Globus website](#figure1)
		<p><img alt="" src="/documents/10308/1469033/GlobusUG-image01.jpg/bb22ecf3-f2c3-487b-9d38-07f6241d3267?t=1487010672865" style="height: 361px; border-width: 1px; border-style: solid; width: 600px;" /></p>

		Globus services are available to everyone with an [XSEDE User Portal](http://portal.xsede.org) account.

		* Use the Globus interfaces described below, including the Web interface (used by most people), the command-line interface (used by advanced users and developers), and the REST APIs (used by application developers). 
		* move files to and from any XSEDE resource on which you have a current allocation (permission to use).
		* download and install the Globus Connect Personal software on your personal computer to transfer files between it and XSEDE or other systems.
		* Enable sharing on your Globus Connect Personal endpoints. (See below.) 
		* Download and install the Globus Connect Server software on any of your personal or campus systems and use it to provide multi-user file transfer access on those systems. File sharing on multi-user systems is limited to XSEDE service providers and is otherwise available with a Globus subscription.

#access
	:markdown
		# [Use XSEDE to access Globus](#access)

		Make sure that Globus knows you are registered with XSEDE. This will allow you to use any of the features listed above. First, open a web browser and navigate to the Globus website, <https://www.globus.org>. 

		1. Click the "Login" button at the top-right of the page.  

			* If you've recently used Globus, you'll be automatically logged in. See [Add XSEDE to your Globus Profile](#addxsede). 
			* If you haven't used Globus recently (or ever), you will be prompted to pick an organization where you already have an account. Type or select "XSEDE" to specify that you want to use your XSEDE identity.

			<div id="figure2"><p><a href="figure2">Figure 2. Use your XSEDE identity to log in to Globus</a><br>
			<img alt="" src="/documents/10308/1469033/GlobusUG-image02.jpg/843aff1b-a706-4eca-8ac0-a1ff13e7af4a?t=1487010855547" style="width: 600px; height: 382px; border-width: 1px; border-style: solid;" /></br> </div>

		1. When you click "Continue," you will be asked to enter your XSEDE username and password. The web page should have the familiar XSEDE interface and should have an address beginning with "weblogin.xsede.org/".

			<div id="figure3"><p><a href="figure3">Figure 3. Enter your XSEDE username and password at the XSEDE login page</a><br>
			<p><img alt="" src="/documents/10308/1469033/GlobusUG-image03.jpg/e5d3656a-407c-42d7-9503-4d20240643c2?t=1487010898931" style="width: 600px; height: 382px; border-width: 1px; border-style: solid;" /></br></div>

		1. Once you enter your XSEDE username and password, you will be logged into Globus and you should see the main file transfer page shown in [Figure 4](#figure4).

			<div id="figure4"><p><a href="figure4">Figure 4. Successfully logged in to Globus</a><br>
			<p><img alt="" src="/documents/10308/1469033/GlobusUG-image04.jpg/777f85d8-2e9a-47a2-91d6-001207c36650?t=1487088884085" style="width: 600px; height: 505px; border-width: 1px; border-style: solid;" /></br></div>

#addxsede
	:markdown
		# [Add XSEDE to your Globus Profile](#addxsede)

		If you've used Globus before, then you already have a Globus profile. Add your XSEDE identity to it to enable the XSEDE features described here. 

		After you are logged into Globus, click the "Account" button in the upper-right of the web page. You will see a list of the identities that Globus has associated with your profile. If XSEDE isn't already listed, click "Add Linked Identity" in the upper-right of the web page. You will be asked to select an organization. Type or select "XSEDE" to specify that you want to use your XSEDE identity.

	#figure5
		:markdown
			[Figure 5. Link your XSEDE identity to an existing Globus profile](#figure5)
			<p><img alt="" src="/documents/10308/1469033/GlobusUG-image05.jpg/c9bd116e-bbeb-4dec-b072-5bfd897e9d5f?t=1487010985774" style="width: 600px; height: 349px; border-width: 1px; border-style: solid;" /></p>

			You should see the XSEDE login page shown in [Figure 3](#figure3), where you can enter your XSEDE username and password and then give Globus permission to access your XSEDE identity information. Once you successfully authenticate to XSEDE, you will have access to all of XSEDE's Globus features as described below.

#movefiles
	:markdown
		# [Move files](#movefiles)

		Once you are logged in to Globus, the Transfer Files web interface (see [Figure 4](#figure4)) is self-explanatory and you will probably understand how to use it without further help. If you need some hints or further details, the Globus website provides an excellent [Getting Started](https://docs.globus.org/how-to/get-started/) guide. The next few sections explain how to find XSEDE's systems within Globus so you can move data to and from them, and how to access your local systems (personal or campus computers).

	#findxsede
		:markdown
			## [Find XSEDE's Resources in Globus](#findxsede)

			As you can see in [Figure 4](#figure4), Globus lets you move files to and from endpoints. All XSEDE resources have Globus endpoints that allow you to move files to and from them. All of XSEDE's endpoints have the word "XSEDE" in their names, making them easy to look up. Just click the "start here" box in Globus, and a panel will appear where you can type the endpoint's name. Type "XSEDE," and all of the XSEDE endpoints will appear, as shown in [Figure 6](#figure6). XSEDE keeps its endpoints up-to-date in Globus on a routine basis. If the resource you are looking for is especially new or is no longer online and you can't find it in the list, use the XSEDE Help Desk to inquire about its availability. Select the endpoint you want to move files to or from, and you will be connected to that system and shown the files and directories there.

		#figure6
			:markdown
				[Figure 6. Type "XSEDE" to get a list of all of XSEDE's Globus endpoints](#figure6)
				<p><img alt="" src="/documents/10308/1469033/GlobusUG-image06.jpg/9c0e0f59-2d59-473c-ac3b-29cccaeca0e9?t=1487011053868" style="width: 600px; height: 500px; border-width: 1px; border-style: solid;" /></p>

	#findlocal
		:markdown
			## [Find your local computers in Globus](#findlocal)

			You can easily create a Globus endpoint on your own computer so that you can upload or download files. To do this, download and install the [Globus Connect Personal](https://www.globus.org/globus-connect-personal) software. (Globus Connect Personal is available for [Windows](https://docs.globus.org/how-to/globus-connect-personal-windows/), [Mac](https://docs.globus.org/how-to/globus-connect-personal-mac/), and [Linux](https://docs.globus.org/how-to/globus-connect-personal-linux/) systems.) You can set Globus Connect Personal up as a background task (so that it is always running in the background) or you can start and stop it like any other application. While Globus Connect Personal is running, your system will be available in Globus's transfer interface (but only to you!) and you will be able to start transfers between it and any other Globus endpoint, including XSEDE resources.

			Note that you can start and stop Globus Connect Personal on your system whenever you need to, even shutting your system down, hibernating, and moving from one network to another. Globus will automatically find your system and continue any active transfers whenever you are connected to a network and have the tool running. Globus Connect Personal is designed to work with most firewalls and NAT devices. It does not require administrative privileges to run on your system.

	#findothers
		:markdown
			## [Find other multi-user research systems in Globus](#findothers)

			Many colleges, universities, and national research institutions and laboratories offer Globus endpoints for their systems. To find an endpoint associated with an organization, try typing the name of the organization in the "start here" endpoint box in Globus. You may be surprised how many organizations are already listed and available. You will typically need to have an account with the organization to access its endpoints.

			Any multi-user HPC or shared storage system can be configured as a Globus endpoint using Globus Connect Server. On most servers, installing Globus Connect Server requires just a few commands. Once the software is installed, any user with a local account on that system can move and share files between it and an XSEDE resource. Information on Globus Connect Server is available on the [Globus documentation web site](https://docs.globus.org/globus-connect-server-installation-guide/).

#share
	:markdown
		# [Share data](#share)

		You can share files from a Globus endpoint with your colleagues and collaborators. Your collaborators will receive an email message from you inviting them to login to Globus and access the files you've shared with them. You can give them read-only or both read and write access to your files. They can log into Globus using their campus credentials (with InCommon-participating campuses), their XSEDE account (if they have one), or a new Globus account. 

	#share-personal
		:markdown
			## [Share your personal servers](#share-personal)

			To enable sharing on your own system running Globus Connect Personal, join XSEDE's "XSEDE Globus Plus Users" group. When logged in to Globus, click the "Groups" button in the top bar and select "Search for Groups" as shown in [Figure 7](#figure7).

		#figure7
			:markdown
				[Figure 7. Search for the XSEDE Globus Plus Users group](#figure7)
				<p><img alt="" src="/documents/10308/1469033/GlobusUG-image07.jpg/716dfa56-0927-4ffa-8e04-471f9b30f49c?t=1487088911377" style="width: 600px; height: 425px; border-width: 1px; border-style: solid;" /></p>

				You will be taken to a search page. Type "`XSEDE Globus Plus`" in the search box and press Enter. Click on the "XSEDE Globus Plus Users" group in the search results box, and you will be shown brief information about the group and given a button labeled "Join Group" on the right side of the page, as shown in [Figure 8](#figure8).

		#figure8
			:markdown
				[Figure 8. Join the XSEDE Globus Plus Users group](#figure8)
				<p><img alt="" src="/documents/10308/1469033/GlobusUG-image08.jpg/0ca1ca84-9480-4988-a475-1ce75bebd800?t=1487011186382" style="width: 600px; height: 446px; border-width: 1px; border-style: solid;" /></p>

				When you click the "Join Group" button, you will be asked for some very brief information about yourself (name, email organization, etc.). The first question on the form is, "Select which username to join as." Click the pop-up list and select your XSEDE identity if it is available in the list. (If your XSEDE identity isn't in the list, you may use another identity to join.) Click the button "Submit Application to Join." You will receive an email message shortly (during normally staffed hours) letting you know that your request has been approved.

				Once Globus Plus has been enabled for your account, [follow Globus's instructions](https://docs.globus.org/how-to/share-files/) for enabling sharing on your system and creating a Shared Endpoint.

	#sharexsede
		:markdown
			## [Share files across XSEDE ecosystem](#sharexsede)

			As of January 2017, only the XSEDE resources managed by the San Diego Supercomputing Center and the Wrangler system at TACC and Indiana University allow sharing on their endpoints. If you would like to use sharing on any other XSEDE endpoint, please contact the XSEDE Help Desk.

	#shareother
		:markdown
			## [Share files on other systems](#shareother)

			To enable sharing on any other multi-user system running Globus Connect Server (such as a shared campus system), you or your campus will need a [Globus subscription](https://www.globus.org/subscriptions). Many campuses have subscriptions already; enquire with your local staff to find out if your systems include Globus and sharing.

#developer
	:markdown
		# [Globus Interfaces](#developer)

		Globus provides a user command-line interface as well as the REST API programmatic interface.

	#developer-cli
		:markdown
			## [Command Line Interface](#developer-cli)

			Globus provides a command line interface (CLI) that may be accessed using any standard SSH terminal client. Prior to running shell commands, you must upload your public SSH key to your Globus account. See [Getting started with the Command Line Interface](https://docs.globus.org/cli/) for more details.

			Once your key is uploaded, launch a secure Globus shell:

			<pre class="cmd-line">$ <b>ssh globususername@cli.globusonline.org</b></pre>

			You will see the Globus command prompt:

			<pre class="cmd-line">Welcome to globusonline.org, .  Type "help" for help.
			$ _</pre>

			Commonly used commands include:

			* Find endpoints matching a keyword with endpoint-search "keyword"
			* Start a transfer with scp
			* Display the transfer tasks status with status

			Information on using Globus commands is available in the [Using the Command Line Interface](https://docs.globus.org/cli/using-the-cli/) guide. A complete list of Globus commands, along with with detailed descriptions is available in the [CLI command reference](https://docs.globus.org/cli/reference/).

			When using the CLI, you may find it helpful to turn off email notifications. Issue the following Globus CLI command:

			<pre class="cmd-line">$ <b>profile -n off</b></pre>

			This command will also disable alert emails, such as a notification that your credentials have expired, requiring you to keep a slightly closer watch on long-running transfer requests.

	#developer-rest
		:markdown
			## [REST API](#developer-rest)

			Globus also provides programmatic access to the file transfer mechanism via a full-featured [REST API](https://docs.globus.org/api/). The API is a great way to integrate Globus services into your custom portal or science gateway. A [Python SDK](http://globus.github.io/globus-sdk-python/) is available, along with many [examples of open source code](https://github.com/globus). A good starting point for working with the API is the [Globus Jupyter](https://github.com/globus/globus-jupyter-notebooks) notebook.

			You are also encouraged to use the [developer mailing list](https://groups.google.com/a/globus.org/forum/#!forum/developer-discuss/home) to engage directly with the Globus engineering team and other developers.


		
#refs
	:markdown
		# [References](#refs)

		The Globus service is quite powerful and includes many features not mentioned above (e.g., group management, monitoring, endpoint management, etc.) You can learn more using the following resources:

		* [Getting started with Globus file transfer](https://docs.globus.org/how-to/get-started/)
		* [Frequently Asked Questions](https://docs.globus.org/faq/)
		* [Sharing data using Globus](https://docs.globus.org/how-to/share-files/)
		* [Getting started with the Globus command line interface (CLI)](https://docs.globus.org/cli/)
		* [Globus REST API](https://docs.globus.org/api/)

		*Last update: February 14, 2017*

