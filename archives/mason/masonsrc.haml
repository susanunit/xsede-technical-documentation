%section#overview
	:markdown
		## [System Overview](#overview)
		Mason (**`mason.iu.xsede.org`**) at Indiana University is a large memory computer cluster configured to support data-intensive, high-performance computing tasks. Mason consists of 16 Hewlett Packard DL580 servers, each containing four Intel Xeon L7555 8-core processors and 512 GB of RAM. The total RAM in the system is 8 TB. Each server chassis has a 10-gigabit Ethernet connection that connects to the XSEDE network (XSEDENet).

		The Mason nodes run Red Hat Enterprise Linux 6.0. Job management is provided by the TORQUE resource manager and the Moab job scheduler. The Modules system is used to simplify application and environment configuration. XSEDE researchers may log into the cluster using GSI-SSH via the XSEDE User Portal, authenticating with their XSEDE-wide logins.

		Mason is intended for use by researchers using genome assembly software (particularly software suitable for assembly of data from next-generation sequencers), large-scale phylogenetic software, or other genome analysis applications requiring large amounts of computer memory. In addition, educators providing instruction on genome analysis software, and developers of such software, are also welcome to use Mason.


%section#overview-config
	%h3 
		%a(href="#overview-config")System Configuration

	%section#table1
		%p
			%a(href="#table1")Table 1. Mason System Configuration
		%table(border="1" cellpadding="3")
			%tr
				%th SYSTEM CONFIGURATION
				%th AGGREGATE INFORMATION
				%th PER-NODE INFORMATION (WHERE APPLICABLE)
			%tr
				%td Machine type
				%td large memory computer cluster configured to support data-intensive, high-performance computing tasks	 
				%td &nbsp;
			%tr
				%td Operating system
				%td Red Hat Enterprise Linux 6	 
				%td &nbsp;
			%tr
				%td Memory model
				%td Distributed	 
				%td &nbsp;
			%tr
				%td Processor cores
				%td 512	
				%td 32
			%tr
				%td CPUs
				%td 64 Intel Xeon L7555 8-core processors
				%td 4 Intel Xeon L7555 8-core processors
			%tr
				%td Nodes
				%td 16 Hewlett Packard DL580 servers	 
				%td &nbsp;
			%tr
				%td RAM
				%td 8 TB
				%td 512 GB
			%tr
				%td Network
				%td 10-gigabit Ethernet per node	 
				%td &nbsp;
			%tr
				%td Local storage
				%td 8 TB
				%td 500 GB

%section#overview-filesystems
	%h3 
		%a(href="#overview-filesystems")File Systems

	%section#table2
		%p The &quot;<code>$HOME</code>&quot; environment variable is automatically set to point to the user's Mason home directory, <code>/N/u/<i>username</i>/Mason</code>.
		%p
			%a(href="#table2")Table 2: Mason File Systems
		%table(border="1" cellpadding="3")
			%tr
				%th FILE SYSTEM
				%th DESCRIPTION
				%th TOTAL DISK SPACE
				%th QUOTAS
				%th BACKUPS
			%tr
				%td <code>/N/u/<i>username</i>/Mason</code>
				%td User home directories reside on a network-attached storage (NAS) device. 
				%td 8 TB local
				%td 10 GB home directory
				%td The home directory file system on Mason makes daily "hourly" and "nightly" snapshots, and saves them to hidden <code>.snapshot</code> directories. To recover a file, go to the directory where the file existed, and then enter its <code>.snapshot</code> directory. Additionally, the IU High Performance Systems (HPS) group makes weekly backups.
			%tr
				%td <code>/scratch</code>
				%td Local scratch space
				%td 450GB
				%td N/A
				%td Local scratch space is not intended for permanent storage of data, and is not backed up. Files are automatically deleted once they are 14 days old.
			%tr
				%td <code>/N/dc/scratch/<i>username</i></code>
				%td IU Data Capacitor shared scratch space
				%td 427TB
				%td N/A
				%td IU Data Capacitor (DC) shared scratch space is not intended for permanent storage of data, and is not backed up. Files in DC scratch space may be purged if they have not been accessed for more than 60 days.

%section#access
	:markdown
		## [System Access](#access)
		Log into Mason using GSI-SSHTerm from the XSEDE User Portal. This is the most convenient method to use GSI-SSHTerm. Once you log in to the portal, you don't have to supply your login information again to connect to a machine. Alternatively, you can download a Java-based terminal that uses MyProxy and GSI-enabled SSH to create a single sign-on session for logging into Mason, in which you can open windows to other resources without re-entering a password. You must have Java SDK 1.5 or higher installed to run the application. Please consult [Accessing Resources](https://www.xsede.org/web/guest/accessing-resources) for further information.

		As an alternative to GSI-SSHTerm via the XSEDE User Portal, you can also access Mason via `gsissh` from the command line, using GSI authentication:
		<pre>
		localhost$ <b>gsissh mason.iu.xsede.org</b></pre>

		You will need to first use your XSEDE password to authenticate with a myproxy certificate:
		<pre>
		localhost$ <b>myproxy-login -s myproxy.xsede.org</b>
		localhost$ <b>gsissh <i>userid</i>@gsissh.mason.iu.xsede.org</b></pre>

		Note: This is automatically done when using the XSEDE user portal described above.

		File transfer is supported via `scp`,  [`sftp`](https://portal.xsede.org/knowledge-base/-/kb/document/akqg), `gridftp`, the XSEDE File Manager, and Globus Online (see [Data Management & Transfers](https://www.xsede.org/web/guest/data-transfers)).  Use of public key authentication is permitted on Mason (see [In SSH and SSH2 for Unix, how do I set up public key authentication?](https://www.xsede.org/web/xup/knowledge-base/-/kb/document/aews)).  Access policy: XSEDE users can request an allocation on Mason via the [XSEDE User Portal](http://www.xsede.org/web/xup) (XUP).

		User responsibilities: For information about your responsibilities as a user of this resource, see [What are my responsibilities as an XSEDE user?](https://www.xsede.org/web/xup/knowledge-base/-/kb/document/bazt).



%section#compenv
	%h2
		%a(href="#compenv")Computing Environment

	%section#compenv-unix
		%h3
			%a(href="#compenv-unix")Unix shell
		%p The shell is the primary method of interacting with the Mason cluster. The command line interface provided by the shell lets users run built-in commands, utilities installed on the system, and even short ad hoc programs.
		%p Mason supports the Bourne-again (<code>bash</code>) and TC (<code>tcsh</code>) shells. New user accounts are assigned the <code>bash</code> shell by default. For more on bash, see the Bash Reference Manual and the Bash (Unix shell) Wikipedia page.
		%p To change your shell on Mason, use the <code>changeshell</code> command.
		%p Note: Running <code>chsh</code> (instead of <code>changeshell</code>) changes your shell only on the node on which you run it, and leaves the other nodes of the cluster unchanged; <code>changeshell</code> prompts you with the shells available on the system, and changes your login shell system-wide within 15 minutes.


	%section#compenv-envvars
		%h3
			%a(href="#compenv-envvars")Environment Variables
		%p The shell uses environment variables primarily to modify shell behavior and the operation of certain commands. A good example is the <code>$PATH</code> environment variable.
		%p When the shell parses a command you have entered (i.e., after you hit Enter or Return), it interprets certain words you've typed as program files that should be executed. The shell then searches various directories on the system to locate these files. The <code>$PATH</code> variable determines which directories are searched, and the order in which they are searched. In the bash shell, the <code>$PATH</code> variable is a string of directories separated by colons (e.g., <code>/bin:/usr/bin:/usr/local/bin</code>). The shell searches for an executable file in the <code>/bin</code> directory, then the <code>/usr/bin</code> directory, and finally the <code>/usr/local/bin</code> directory. If files of the same name (e.g., foo) exist in all three directories, <code>/bin/foo</code> will be run, because the shell will find it first.
		%p To display and change the values of environment variables:

%section#table3
	%p
		%a(href="#table3")Table 3. Modifying Environment Variables
	%table(border="1" cellpadding="3")
		%tr
			%td 
			%th	<code>bash</code>
			%th	<code>tcsh</code>
		%tr
			%th Display A Value
			%td	echo $VARNAME
			%td	echo $VARNAME
		%tr
			%th Change A Value
			%td	export VARNAME=VALUE
			%td	setenv VARNAME VALUE

	%section#compenv-startupscripts
		%h3
			%a(href="#compenv-startupscripts")Startup scripts
		%p Shells offer much flexibility in terms of startup configuration. On login, bash by default reads and executes commands from the following files in this order:
		%ul 
			%li <code>/etc/profile</code>
			%li <code>~/.bash_profile</code>
			%li <code>~/.bashrc</code>
		%p Note: The &quot;<code>~</code>&quot; (tilde) represents your home directory (e.g., &quot;<code>~/.bash_profile</code>&quot; is the <code>.bash_profile</code> file in your home directory).
		%p On logout, the shell reads and executes the <code>~/.bash_logout</code>. For more on bash startup files, see the "Bash Startup Files" section of the <a href="http://www.gnu.org/software/bash/manual/bashref.html">Bash Reference Manual</a>. 
		%p On login, the tcsh shell reads and executes commands from the following files in this order:  
		%ul
			%li <code>/etc/csh.cshrc</code>
			%li <code>/etc/csh.login</code>
			%li <code>~/.tcshrc</code> (if it exists, otherwise ~/.cshrc)
			%li <code>~/.history</code>
			%li <code>~/.login</code>
			%li <code>~/.cshdirs</code>
		%p In practice, on Mason, only the first two files exist. You may create the others, and add commands and variables to them as you see fit.

	%section#compenv-modules
		%h3
			%a(href="#compenv-modules")Modules
		%p Modules provide a convenient method for dynamically modifying your computing environment. A few simple commands provide easy access to various applications on the cluster:

		%section#table4
			%p
				%a(href="#table4")Table 4: Common Modules commands
			%table(border="1" cellpadding="3")
				%tr
					%th COMMAND
					%th	ACTION
				%tr
					%td $ <b><code>module avail</code></b>
					%td	List available modules on the system
				%tr
					%td $ <b><code>module load</code></b>
					%td	Load (modifies your environment)
				%tr
					%td $ <b><code>module list</code></b>
					%td	Display your currently loaded modules
				%tr
					%td $ <b><code>module rm</code></b>
					%td	Unload (modifies your environment)

		%p For more about the Environment Modules package, see the <code>module</code> man page and the <code>modulefile</code> man page or see <a href="https://www.xsede.org/software-environments">Computing Environments</a>. 

%section#transferring
	:markdown
		## [Transferring your files to Mason](#transferring)
		Mason supports only `scp`, [`sftp`](https://portal.xsede.org/knowledge-base/-/kb/document/akqg), `gridftp`, the XSEDE File Manager, and Globus Online for transferring files. For a high level overview on the recommended XSEDE data transfer methods, see [Data Transfers & Management](https://www.xsede.org/web/guest/data-transfers).

%section#appdev
	%h2 
		%a(href="#appdev")Application development
	
%section#appdev-models
	%h3 
		%a(href="#appdev")Programming models
	%p Mason is designed to support codes that have extremely large memory requirements. As these codes typically do not implement a distributed memory model, Mason is geared toward a serial or shared memory parallel programming paradigm. However, Mason can support distributed memory parallelism.

	%section#appdev-compiling
		%h3 
			%a(href="#appdev")Compiling
		%p The following compilers are available on mason:
		%ul
			%li	Intel, Portland Group, GNU (default)
			%li	Fortran, C, C++
			%li	OpenMPI
		%p Compiler options:
		%ul
			%li	Recommended options are &quot;<code>-O3</code>&quot; and &quot;<code>-xHost</code>&quot;, where the <code>-xHost</code> option will optimize based on the processor of the current host. 
			%li GCC compiler options such as <code>mtune=native</code> and <code>march=native</code> are recommend to generate instructions for the machine and cpu-type. 

		%section#tablex
			%p
			%table(border="1" cellpadding="3")
				%tr
					%td &nbsp;
					%td &nbsp;
					%th Fortran 77/90
					%th C
				%tr
					%td(rowspan="2")Serial<br>Programs
					%td GNU
					%td <pre>f77 -o simple simple.f</pre>
					%td <pre>gcc -O2 -o -mtune=native -march=native simple simple.c</pre>
				%tr
					%td Intel
					%td <pre>ifort -O2 -o simple -lm simple.f</pre>
					%td <pre>icc -o simple simple.c</pre>
				%tr
					%td(rowspan="2")Parallel<br>Programs
					%td MPI
					%td <pre>mpif90 -o simple -O2 simple.f</pre>
					%td <pre>mpic++ -o simple simple.cc</pre>
				%tr
					%td OPENMP
					%td <pre>ifort -openmp -o simple -lm simple.f</pre>
					%td	<pre>mpicc -O2 -openmp -o simple simple.c</pre>

	%section#appdev-libraries
		%h3	
			%a(href="#appdev-libraries")Libraries
		%p Scientific/numerical subroutine libraries available on Mason are MKL, ACML.

%section#appdev-debugging
	%h3
		%a(href="#appdev-debugging")Debugging 
	%p The Intel Debugger (IDB) and GNU Project Debugger (GDB) are available on Mason.
	%p For information about using the IDB, see <a href="http://software.intel.com/en-us/articles/idb-linux">Intel's IDB page</a>.
	%p For information about using the GDB, see the <a href="http://www.gnu.org/software/gdb/">GNU GDB page</a>.
	%p For more about debugging code on Mason, see <a href="https://www.xsede.org/web/xup/knowledge-base/-/kb/document/aqsy">Step-by-step example for using GDB within Emacs to debug a C or C++ program</a>.

%section#running
	%h2 
		%a(href="#running")Running your applications

%section#running-cpulimits
	:markdown
		### [CPU/Memory limits and batch jobs](#running-cpulimits)
		User processes on the login nodes are limited to 20 minutes of CPU time. Processes exceeding this limit are automatically terminated without warning. If you require more than 20 minutes of CPU time, use the TORQUE `qsub` command to submit a [batch job](https://portal.xsede.org/web/xup/knowledge-base/-/kb/document/afrx) (see [How to Submit a Job](#running-jobcontrol-submit) below).

		Implications of these limits on the login nodes are as follows:

		* The Java Virtual Machine must be invoked with a maximum heap size. Because of the way Java allocates memory, under `ulimit` conditions an error will occur if Java is called without the "`-Xmx##m`" flag.
		* Memory-intensive jobs started on the login nodes will be killed almost immediately. Debugging and testing on Mason should be done by submitting a reqeust for an interactive job via the batch system, for example:
		<pre>
		username@Mason: ~&gt; <b>qsub -I -q shared -l nodes=1:ppn=4,vmem=10gb,walltime=4:00:00</b></pre>
		The interactive session will start as soon as the requested resources are available.

	%h3 
		%a(href="")Short definitions
	%ul
		%li A job is an instance of an application you wish to run.
		%li A queue is a pool of compute resources that accepts job to run, and executes them according to a fairshare policy.
		%li Job schedulers are the applications responsible for scheduling jobs.

%section#running-queues
	%h3 
		%a(href="#running-queues")Queues
	%p <b>BATCH</b>: this queue is the default queue on Mason cluster and it is for general purpose. The walltime limit of this queue is two weeks. However, if you have a special case and need more than two weeks of walltime for your jobs, you can send a request to <a href="mailto:help@xsede.org">help@xsede.org</a>.

%section#running-queues-policies
	%h4 
		%a(href="#running-queues-policies")Queue policies
	%p Fairshare scheduling allows historical resource usage to affect job priority decisions. Administrators can set target usage goals for each user, group, class, or service group. Moab, a job scheduler, tracks class usage and gives priority to other job classes when one job class exceeds its usage goals. The fairshare policy on Mason records daily usage and then uses data from the previous seven days to adjust job priorities. A .80 decay factor is applied to reduce the impact of previous data by 20% each day.  Each usage class (usually a username) is assigned a goal of 5% usage. When a user exceeds the usage goal, that user's jobs are then given lower scheduling priority. For more about job scheduling, see <a href="https://www.xsede.org/web/xup/knowledge-base/-/kb/document/avmu">What is Moab?</a>

	%p The default walltime is 1 hour and the walltime limit is two weeks.

	%section#running-jobcontrol
		%h3 
			%a(href="#running-jobcontrol")Job Control
		%p To delete queued or running jobs, use the <code>qdel</code> command. Occasionally, a node will become unresponsive and unable to respond to the TORQUE server's requests to kill a job. In such cases, try using &quot;<code>qdel -W <i>delay</i></code>&quot; to override the delay between SIGTERM and SIGKILL signals, where the <code>delay</code> argument specifies an unsigned integer number of seconds. For more about the <code>qdel</code> command in TORQUE, see the <a href="http://docs.adaptivecomputing.com/torque/4-0-2/help.htm#topics/commands/qdel.htm ">Adaptive Computing <code>qdel</code></a> page and the <code>qdel</code> manual page.

%section#running-jobcontrol-submit
	%h4 
		%a(href="#running-jobcontrol-submit")How to Submit a Job
	%p Use <code>qsub</code> to submit jobs to run on Mason. If the command exits successfully, it will return a job ID, for example:
	<pre> 
	username@Mason: ~&gt; <b>qsub job.script</b>
	123456.m1.mason</pre>
	%p If you need attribute values different from the defaults, but less than the maximum allowed, specify these either in the job script using TORQUE directives, or on the command line with the <code>-l</code> switch. For example, to submit a job that needs more than the default 60 minutes of walltime, use: 
	<pre>
	username@Mason: ~&gt; <b>qsub -l walltime=10:00:00 job.script</b></pre>
	%p Note: Jobs on Mason will default to a per-job virtual memory resource of 8 MB. So, for example, to submit a job that needs 100 GB of virtual memory, use:
	<pre>
	username@Mason: ~&gt; <b>qsub -l nodes=1:ppn=4,vmem=100gb job.script</b></pre>
	%p Note: Command-line arguments override directives in the job script, and you may specify many attributes on the command line, either as comma-separated options following the <code>-l</code> switch, or each with its own <code>-l</code> switch. The following two commands are equivalent:
	<pre>
	username@Mason: ~&gt; <b>qsub -l nodes=1:ppn=16,vmem=1024mb job.script</b>
	username@Mason: ~&gt; <b>qsub -l nodes=1:ppn=16 -l vmem=1024mb job.script</b></pre>
	%p Useful <code>qsub</code> switches include:

%section#table5
	%p
		%a(href="#table5")Table 5: Common <code>qsub</code> Command options
	%table(border="1" cellpadding="3")
		%tr
			%td <code>-q &lt;queue name&gt;</code>
			%td	Specify user-selectable queues 
		%tr
			%td <code>-r y|n</code>
			%td	Declare whether the job is rerunnable. If the argument is yes, user can use <code>qrerun</code> command to return the job to the queued state in the designated queue. 
		%tr
			%td <code>-a &lt;date_time&gt;</code>
			%td	Execute the job only after specified date and time
		%tr
			%td <code>-V</code>
			%td	Export environment variables in your current environment to the job
		%tr
			%td <code>-I</code>
			%td	Run interactively
		%tr
			%td <code>-m e</code>
			%td	Mail job summary report when job terminates

	%p For more, see the <code>qsub</code> man page.

%section#running-jobcontrol-monitor
	%h4 
		%a(href="#running-jobcontrol-monitor")How to monitor a job
	%p Use <code>qstat</code> for monitoring the status of a queued or running job. Switches include:

	%section#table6
		%p
			%a(href="#table6")Table 6. Common <code>qstat</code> command options
		%table(border="1" cellpadding="3")
			%tr
				%td <code>-u <i>user_list</i></code>
				%td	Display jobs for users in user list
			%tr
				%td <code>-a</code>
				%td	Display all jobs
			%tr
				%td <code>-r</code>
				%td	Display running jobs
			%tr
				%td <code>-f</code>
				%td	Display full listing of jobs (excessive detail)
			%tr
				%td <code>-n</code>
				%td	Display nodes allocated to jobs


%section#running-jobcontrol-delete
	%h4 
		%a(href="#running-jobcontrol-delete")How to delete a job
	%p Use <code>qdel</code> to delete queued or running jobs. Occasionally, a node will become unresponsive and unable to respond to the TORQUE server's requests to kill a job. In such cases, try &quot;<code>qdel -W</code>&quot;.

%section#tools
	%h2
		%a(href="#tools")Tools
	%p The performance tools currently available on Mason are VampirTrace and VAMPIR. VampirTrace is a profiling tool for Message Passing Interface (MPI) applications. VampirTrace produces tracefiles that can be analyzed with the VAMPIR performance analysis tool. For more, see the <a href="http://www.vampir.eu/">VAMPIR</a> web site.  The corresponding modules for VampirTrace and VAMPIR are: 
	%ul
		%li	vampirtrace/5.14-gnu
		%li	vampir/server-8.0.0
		%li	vampir/gui-8.0.0


%section#ref
	%h2 
		%a(href="#ref")Reference
	%ul
		%li 
			%a(href=" http://ark.intel.com/products/46494/Intel-Xeon-Processor-L7555-24M-Cache-1_86-GHz-5_86-GTs-Intel-QPI")Intel Xeon Processor L7555 specifications
		%li 
			%a(href="http://bizsupport1.austin.hp.com/bc/docs/support/SupportManual/c02126499/c02126499.pdf")DDR3 memory technology (an HP white paper)

