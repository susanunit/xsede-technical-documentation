// Notes: this UG is slightly out of sync with the XSEDE version online.  XStream is decommissioned from XSEDE on October 1, 2018.  Moving this guide to the archives.
<span style="font-size:225%; font-weight:bold;">XStream User Guide</span><br>
<em>Last update: March 22, 2017</em>

#overview
	:markdown
		# [System Overview](#overview)

		XStream is a GPU cluster hosted at the <a href="https://srcc.stanford.edu/">Stanford Research Computing Center</a> and funded by the National Science Foundation's (NSF) Major Research Instrumentation (MRI) Program. Twenty percent of its computational resources are reserved to XSEDE awards.

		XStream is a compute cluster specifically designed by Cray for GPU computing, or more precisely, heterogeneous parallel computing with CPUs and GPUs. It differs from traditional CPU only based HPC systems as it has almost a Petaflop (PF) of GPU compute power. Each of the 65 nodes has 8 NVIDIA K80 cards or 16 NVIDIA Kepler GPUs, interconnected through PCI-Express PLX-based switches. Each GPU has 12GB of GDDR5 memory. Compute nodes also feature 2 Intel Ivy-Bridge 10-core CPUs, 256 GB of DRAM and 450 GB of local SSD storage. The system features 1.4 petabytes of 22 GB/s Lustre storage (Cray Sonexion 1600).

		The system also includes two login node each of them has a 10 GigE connection to Stanford's campus backbone which has 100 Gbps connectivity to various national research and education networks.

		XStream was ranked #87 on the June 2015 Top500 list of fastest supercomputers (using LINPACK benchmark). Even with the extreme GPU computing, near Petaflop density, this system was #6 in the June 2015 Green 500 list and moved to #5 in the November 2015 list.

	#overview-configuration
		:markdown
			## [System Configuration](#overview-configuration)

			All XStream nodes run RHEL 6.7 and are managed with batch services through SLURM. Global `$WORK` storage area is supported by Lustre parallel distributed file system with 6 IO servers. Inter-node communication (MPI/Lustre) is through a FDR Mellanox InfiniBand network.

			2 Login Nodes, each with:

			* Two 2.6GHz 6-Core Ivy Bridge-EP E5-2630 v2 Xeon 64-bit Processors
			* Two NVIDIA Tesla K80 GPU cards (4 Kepler GPUs)
			* 64GB DDR3 1600MT/s DRAM
			* 56Gbps Infiniband FDR network interface
			* 10 Gigabit Ethernet network interface
			* 1 Gigabit Ethernet network interface
			* Red Hat Enterprise Linux Server 6.7

			65 Compute Nodes, each with

			* Cray CS-Storm 2626X8N Compute Node
			* Two 2.8GHz 10-Core Ivy Bridge-EP E5-2680 v2 Xeon 64-bit Processors
			* Eight NVIDIA Tesla K80 24GB GPU cards (16 x GK210 12GB GPU)
			* 256GB DDR3 1600MT/s DRAM
			* Three Intel SSD (MLC) in RAID-0 (striped volume) totalling 480GB
			* 56Gbps Infiniband FDR network interface
			* 1 Gigabit Ethernet network interface
			* Red Hat Enterprise Linux Server 6.7

			The hardware architecture of the compute nodes is that each CPU socket (PCI root) is connected to PLX switches that connect four K80 cards (eight GPUs) together.

			2626X8N Compute Node Diagram

			<img alt="Compute Node Diagram" src="/documents/10308/0/XStream+compute+node+diagram/7c8de868-064f-4fa7-aebd-e68b195efab5?t=1469825818000" style="width: 624.00px; height: 516.00px;">


			You basically have 2 domains, one for each CPU. You can use the "`lstopo`" command on a compute node (eg. xs-0001) for full details of the PCI bus.

			If you plan on doing GPU peer-to-peer communication, the "`nvidia-smi`" command on a compute node will show you the GPUDirect topology matrix for the system:

			<pre>xs-0001$ <b>nvidia-smi topo -m</b></pre>

			**PIX** gives you the lower latency, **SOC** the highest.

			*Please note that you need to have a job actually running on a compute node with all 16 GPUs allocated in order to SSH to it and see the full GPUDirect topology matrix for the system.*

	#overview-gpusettings
		:markdown
			## [GPU and Nvidia driver settings](#overview-gpusettings)

			GPUs on the compute nodes have the following static settings:

			* Auto Boost ON
			* Persistent mode
			* Compute Mode: Exclusive Process

			Please note that GPUs on the login nodes are configured in *Default Compute Mode*, meaning that multiple contexts are allowed per device.

			As of June 2016, the version of the Nvidia driver is 352.93. Stanford continually updates the driver as it is made available by Nvidia.

	#overview-storage
		:markdown
			## [Storage](#overview-storage)

			Login and compute nodes are connected to a private Lustre storage system, a Cray Sonexion appliance, with fast I/O performance. This system is capable of providing more than 22 GB/s of sustained Lustre bandwidth over the Infiniband fabric and about 1.4 PB of usable space.

			* 492 x 4 TB SAS hard drive
			* 48 x Lustre Object Storage Target (OST) - each 32 TB
			* 6 x Embedded Lustre Object Storage Server (OSS) Infiniband FDR 56Gbps

			A low-performance 3.3TB NFS-mounted /home disk storage is also available.

	#overview-filesystems
		:markdown
			## [Filesystems](#overview-filesystems)

		#overview-filesystems-home
			:markdown
				### [Home file system](#overview-filesystems-home)

				Each user on XStream has a home directory referenced by `$HOME` with a quota limit of 5GB (not purged). It is a small and low performance NFS storage space used to keep scripts, binaries, source files, small log files, etc.

				The `$HOME` filesystem is accessible from any node in the system.

				The `$HOME` directory is not intended to be used for computation. The Lustre parallel file system `$WORK` is much larger and faster, thus much more suited for computation.

				Each project has a shared home directory referenced by `$GROUP_HOME`. Like `$HOME`, it is a NFS storage space used to store small files shared by all members of your primary POSIX group (usually your primary project).

				*Note: in `$GROUP_HOME`, only the owner of the files can delete them.*

				**Important note on home backups:** The system doesn't come with any backup system, however user and group home directories are backed up every night by Stanford Research Computing. Contact the XSEDE helpdesk in order to recover any lost files. We also recommend that you periodically back up your files outside of XStream.

		#overview-filesystems-work
			:markdown
				### [Work file system](#overview-filesystems-work)

				Work is a Lustre file system mounted on `/cstor` on any node in the system. This parallel file system has multiple purposes:

				* perform fast large I/Os
				* store large computational data files
				* allow multi-node jobs to write coherent files

				Each user has a work directory referenced by `$WORK` with a quota limit of 1TB which is not purged. Each project has a shared work directory referenced by `$GROUP_WORK` on the same file system with a group quota limit of 50TB. This space is shared by all members of the project.

				*Note: in `$GROUP_WORK`, only the owner of the files can delete them.*

				User and group quota values are not cumulative, ie. the first limit reached takes precedence. 

				**Important note on work backup:** The parallel file system work is not replicated nor backed up.

		#overview-filesystems-scratch
			:markdown
				### [Local scratch](#overview-filesystems-scratch)

				A local SSD-based scratch space is available on each compute node (NOT on login nodes). It is made of 3 x Intel SSD (MLC) aggregated using Linux dm-raid for a total of 480 GB per node (447 GB usable) and intended for high IOPS local workload.

				To access this local scratch space, please use the `$LSTOR` or `$TMPDIR` environment variables. This space will be purged when the compute node reboots or when this space becomes full.

		

#access
	:markdown
		# [System Access](#access)

		XStream is accessible to XSEDE users with allocations. To obtain an account or submit a proposal through the [XSEDE Allocation Request System](https://portal.xsede.org/submit-request) (XRAS), please read the [XSEDE Allocations Overview](https://portal.xsede.org/allocations-overview).

	#access-methods
		:markdown
			## [Methods of Access](#access-methods)

		#access-methods-sso
			:markdown
				### [XSEDE Single Sign-On Hub](#access-methods-sso)

				The recommended way to access XStream is through <a href="https://portal.xsede.org/single-sign-on-hub">XSEDE Single Sign-On Hub</a>:

				<pre>ssohub$ <b>gsissh xstream</b></pre>

		#access-methods-gsiopenssh
			:markdown
				### [GSI-OpenSSH (gsissh)](#access-methods-gsiopenssh)

				Assuming you have the proper <a href="https://software.xsede.org/packages/production/CA/CA-install.html">XSEDE CA certificates</a> installed on your local machine, the following commands authenticate using the XSEDE myproxy server, then connect to the gsissh port 2222 on XStream:

				<pre>
				localhost$ <b>myproxy-logon -l userid -s myproxy.xsede.org</b>
				localhost$ <b>gsissh -p 2222 xstream.stanford.xsede.org</b>
				</pre>

				When you log in to `xstream.stanford.xsede.org`, you will be assigned one of the two login nodes: `xstream-ln[01-02].stanford.edu`. These nodes are identical in both architecture and software environment. Users should normally log in through `xstream.stanford.xsede.org`, but may specify one node directly if they see poor performance.

				Please, do NOT use the login nodes for computationally intensive processes. These nodes are meant for compilation, file editing, simple data analysis, and other tasks that use minimal compute resources. All computationally demanding jobs should be submitted and run through the batch queuing system. You may however use the few GPUs available on the login nodes to perform simple and short tests. Please note that GPUs on the login nodes are configured in Default Compute Mode, meaning that multiple contexts are allowed per device. They are not suitable for performance evaluation.

	#access-userresponsibilities
		:markdown
			## [User Responsibilities](#access-userresponsibilities)

			Please note that XStream is *not* HIPAA compliant and should not be used to process PHI. See <https://privacy.stanford.edu/faqs/hipaa-faqs> for more information.

#compenv
	:markdown
		# [Computing Environment](#compenv)

		XStream's default and supported shell is `bash`. Users may still request a shell change (eg. `ksh`, `zsh` or `tcsh`) by contacting the <a href="http://portal.xsede.org/help-desk">XSEDE Help Desk</a>.

	#compenv-modules
		:markdown
			## [Modules](#compenv-modules)

			Modules provide a convenient way to dynamically change the users' environment through modulefiles. This includes easily adding or removing directories to the "`$PATH`" environment variable.

			Lmod is used as a replacement to the original module command. For more information, please take a look at the <a href="https://www.tacc.utexas.edu/research-development/tacc-projects/lmod">Lmod user guide</a>.

			On XStream, modules follow a **hierarchical module naming scheme**, so only packages that can be directly loaded are displayed by "`module avail`".

			You can list all available modules using the "`spider`" sub-command:

			<pre>login1$ <b>module spider</b></pre>

			Using the full name will give you details on how to load the module by listing any required dependencies:

			<pre>login1$ <b>module spider FFTW/3.3.4</b></pre>

			Also, you can use "`module list`" to see currently loaded packages.

		

#transferring
	:markdown
		# [Transferring your files to XStream](#transferring)

		XStream supports Globus services such as Globus Connect and Globus "`globus-url-copy`" utility to transfer files to XStream or between XSEDE sites. Please note that common command line utilities such as `scp`, `sftp`, and `rsync` are also available to transfer files from XStream to a remote host.

	#transferring-globus
		:markdown
			## [Globus Connect](#transferring-globus)

			<a href="https://www.globus.org/globus-connect-personal">Globus Connect</a> (formerly Globus Online) is recommended for transferring data between XSEDE sites. Globus Connect provides fast, secure transport via an easy-to-use web interface using pre-defined and user-created "endpoints". XSEDE users automatically have access to Globus Connect via their XUP username/password. Other users may sign up for a free <a href="https://www.globus.org/globus-connect-personal">Globus Connect Personal account</a>.

	#transferring-commandline
		:markdown
			## [Linux Command-line Data Transfer Utilities](#transferring-commandline)

		#overview-commandline-globusurlcopy
			:markdown
				### [`globus-url-copy`](#overview-commandline-globusurlcopy)

				XSEDE users may also use Globus's `globus-url-copy` command-line utility to transfer data between XSEDE sites. `globus-url-copy`, like Globus Connect described above, is an implementation of the GridFTP protocol, providing high speed transport between GridFTP servers at XSEDE sites. The GridFTP servers mount the specific file systems of the target machine, thereby providing access to your files or directories.

				This command requires the use of an XSEDE certificate to create a proxy for passwordless transfers. To obtain a proxy, use the 'myproxy-logon' command with your XSEDE User Portal (XUP) username and password to obtain a proxy certificate. The proxy is valid for 12 hours for all logins on the local machine. On XStream, the `myproxy-logon` command is available on the login nodes.

				<pre>xstream-ln01$ <b>myproxy-logon -T -l XUP_username</b></pre>

				Each `globus-url-copy` invocation must include the name of the server and a full path to the file. The general syntax looks like:

				<pre>globus-url-copy [options] source_url destination_url</pre>

				where each XSEDE URL will generally be formatted:

				<pre>gsiftp://gridftp_server/path/to/file</pre>

				Users may look up XSEDE <a href="https://portal.xsede.org/data-management#gridftpendpoints">GridFTP servers</a> on the <a href="https://portal.xsede.org/data-management">Data Transfer & Management</a> page.

				The following command copies `"directory1"` from Stanford's XStream to TACC's Stampede system, renaming it to "`directory2`". Note that when transferring directories, the directory path must end with a slash ( '/'):

				<pre>
				login1$ <b>globus-url-copy -r -vb \
					gsiftp://xstream.stanford.xsede.org:2811/`pwd`/directory1/ \
					gsiftp://gridftp.stampede.tacc.xsede.org:2811/home/0000/johndoe/directory2/</b>
				</pre>

		#transferring-commandline-gsiopenssh
			:markdown
				### [GSI-OpenSSH](#transferring-commandline-gsiopenssh)

				Command-line transfer utilities supporting standard SSH and grid authentication are offered by the Globus GSI-OpenSSH implementation of OpenSSH. The `gsissh`, `gsiscp` and `gsiftp` commands are analogous to the OpenSSH `ssh`, `scp` and `sftp` commands. Grid authentication is provided to XSEDE users by first executing the "`myproxy-logon`" command (see above).

				You must explicitly connect to port 2222 on XStream. The following command copies "`file1`" on your local machine to Stampede renaming it to "`file2`".

				<pre>
				localhost$ <b>gsiscp -oTcpRcvBufPoll=yes -oNoneEnabled=yes \
					-oNoneSwitch=yes -P2222 file1 xstream.stanford.xsede.org:file2</b>
				</pre>

			Please consult Globus' <a href="http://toolkit.globus.org/toolkit/docs/4.0/security/openssh/user-index.html">GSI-OpenSSH User's Guide</a> for further info.

#software
	:markdown
		# [Software on XStream](#software)

		This section contains software or libraries available on XStream as of June 2016. 

		#### cuDDN 3.0, 4.0 and 5.0 

		The NVIDIA CUDA Deep Neural Network library (<a href="https://developer.nvidia.com/cudnn">cuDDN</a>) is a GPU-accelerated library of primitives for deep neural networks.

		<pre>login1$ <b>module load CUDA/7.5.18 cuDNN/4.0</b></pre>

		#### GROMACS 5.1

		<a href="http://www.gromacs.org">GROMACS</a> is a versatile package to perform molecular dynamics, i.e. simulate the Newtonian equations of motion for systems with hundreds to millions of particles.

		<pre>login1$ <b>module load intel/2015.5.223 CUDA/7.5.18 GROMACS/5.1-hybrid</b></pre>

		#### OpenMM 6.3.1 

		<a href="http://openmm.org">OpenMM</a> is a high performance toolkit for molecular simulation. On XStream, it is compiled against the foss toolchain (GCC 4.9.2) and CUDA 7.0.28.

		<pre>login1$ <b>module load foss/2015.05 OpenMM/6.3.1</b></pre>

		#### PostgreSQL 9.5.2 with PG-Strom 

		PostgreSQL is an open source relational database management system (DBMS) developed by a worldwide team of volunteers.

		<a href="https://wiki.postgresql.org/wiki/PGStrom">PG-Strom</a> is an extension of PostgreSQL designed to off-load several CPU intensive workloads to GPU devices, to utilize its massive parallel execution capability.

		<pre>
		login1$ <b>module load foss/2015.05 CUDA/7.5.18 PostgreSQL/9.5.2-Python-2.7.9</b>
		login1$ <b>pg_config --version</b>
		PostgreSQL 9.5.2
		login1$ <b>initdb -D $WORK/postgres/data</b>
		</pre>

		Edit "`$WORK/postgres/data/postgresql.conf`" and add the following line to load the PG-Strom extension:

		<pre>shared_preload_libraries = '$libdir/pg_strom'</pre>

		Start the PostgreSQL server:

		<pre>login1$ <b>pg_ctl -D $WORK/postgres/data -l logfile start</b></pre>

		Connect using your login name and the password postgres and create the pg_strom extension:

		<pre>login1$ <b>psql -U $USER postgres</b>
		psql (9.5.2)
		Type 'help' for help.

		postgres=# CREATE EXTENSION pg_strom;
		CREATE EXTENSION
		</pre>

		Stop the PostgreSQL server:

		<pre>login1$ <b>pg_ctl -D $WORK/postgres/data stop</b></pre>

		**Please always use PostgreSQL within a job, not on the login nodes.**

		#### R 3.2.4

		<a href="https://www.r-project.org/">R</a> is a free software environment for statistical computing and graphics.

		<pre>login1$ <b>ml foss/2015.05 R/3.2.4-libX11-1.6.3</b></pre>

		#### RStudio 0.99.893

		<a href="https://www.rstudio.com/">RStudio</a> IDE is a powerful and productive user interface for R.

		<pre>login1$ <b>module load foss/2015.05 git/2.4.1 RStudio/0.99.893</b></pre>

		#### TensorFlow 0.9

		<a href="https://www.tensorflow.org/">TensorFlow</a> is an Open Source Software Library for Machine Intelligence originally developed by Google with CUDA support.

		<pre>login1$ <b>ml tensorflow/0.9</b></pre>

		Note: Tensorflow is a special module that will load the foss toolchain automatically.

		#### Theano 0.8.2

		[Theano](http://deeplearning.net/software/theano) is a Python library that allows you to define, optimize, and evaluate mathematical expressions involving multi-dimensional arrays efficiently. It has support for cuDNN 3.0 and is compiled against the Intel toolchain.

		Usage example without MPI support:

		<pre>login1$ <b>module load foss/2015.05 Theano/0.8.2-Python-2.7.9-noMPI</b></pre>

		Usage example with MPI support:

		<pre>login1$ <b>module load foss/2015.05 Theano/0.8.2-Python-2.7.9</b></pre>

		#### Torch

		<a href="http://torch.ch/">Torch</a> is a scientific computing framework with wide support for machine learning algorithms that puts GPUs first. It has support for cuDNN 4.0 and 5.0 and is compiled against the foss toolchain (GCC based).

		<pre>login1$ <b>module load torch/20160414-cbb5161</b></pre>

		Note: Torch is a special module that will load the foss toolchain automatically.

		#### VMD 1.9.2

		<a href="http://www.ks.uiuc.edu/Research/vmd/">VMD</a> is a molecular visualization program for displaying, animating, and analyzing large biomolecular systems using 3-D graphics and built-in scripting.

		VMD on XStream is built against CUDA 7.0.28 and Nvidia OptiX 3.8.0, enabling the TachyonL-OptiX GPU-accelerated ray tracing renderer available in VMD 1.9.2. At least the following features should also be available: ACTC library support, collective variables, Python support, Pthreads, NetCDF, ImageMagick, ffmpeg and NetPBM.

		Prerequisite: use ssh X11 forwarding by adding "`-X`" to your `ssh` command when connecting to XStream.

		For non-computationally expensive tasks with VMD, you may launch VMD from a login node:

		<pre>
		login1$ <b>module load foss/2015.05 VMD/1.9.2-Python-2.7.9</b>
		login1$ <b>vmd</b>
		</pre>

		To perform computationally expensive tasks with VMD, please launch VMD using srun with the X11 option as shown here (example with 1 task, 4 CPUs and 4 GPUs):

		<pre>
		login1$ <b>module load foss/2015.05 VMD/1.9.2-Python-2.7.9</b>
		login1$ <b>srun --x11=first -n1 -c4 --gres gpu:4 vmd</b>
		</pre>

		Please refer to the [Modules](#compenv-modules) section to learn how to search for software on XStream.

#appdev
	:markdown
		# [Application Development](#appdev)

	#appdev-compilers
		:markdown
			## [Compiler toolchains](#appdev-compilers)

			Compiler toolchains are basically a set of compilers together with libraries that provide additional support that is commonly required to build software. In the HPC world, this usually consists of a library for MPI (inter-process communication over a network), BLAS/LAPACK (linear algebra routines) and FFT (Fast Fourier Transforms).

			Compiler toolchains on XStream:

			* **foss/2015.05**: the FOSS toolchain is a GNU Compiler Collection (GCC) based compiler toolchain, including OpenMPI for MPI support, OpenBLAS (BLAS and LAPACK support), FFTW3 and ScaLAPACK. This version is based on GCC 4.9.2.
			* **intel/2015.5.223**: Intel Cluster Toolkit Compiler Edition provides Intel C/C++ and Fortran compilers, Intel MPI ' Intel MKL. Based on Intel Parallel Studio XE 2015 update 5.

			To load the FOSS toolchain:

			<pre>login1$ <b>module load foss/2015.05</b></pre>

			To load the Intel compiler toolchain:

			<pre>login1$ <b>module load intel/2015.5.223</b></pre>

			Note: loading a toolchain will offer you additional software to load through module. Check module avail once your preferred compiler toolchain is loaded.

	#appdev-cuda
		:markdown
			## [Nvidia CUDA](#appdev-cuda)

			CUDA is an essential software for XStream. CUDA is not part of the above compiler toolchains and should be loaded aside. The following versions of CUDA are available on XStream: 6.5.14, 7.0.28 and 7.5.18 (default).

			For example, to load CUDA 6.5.14, please use:

			<pre>login1$ <b>module load CUDA/6.5.14</b></pre>

			Always use "`-arch=sm_37`" to select the correct architecture specification for Nvidia Tesla K80 GPU.
		

#running
	:markdown
		# [Running Jobs on XStream](#running)

	#running-accounting
		:markdown
			## [Job Accounting](#running-accounting)

			Computing services for XSEDE users are allocated and charged in Service Units (SUs). On XStream, the rule is simple: 1 SU = 1 GPU hour (GK210 architecture). 

			**XStream SUs charged (GPU hours) = # GPUs * wallclock time**

			The following job submission rules apply:

			* A job should at least request one GPU as **CPU-only jobs are not allowed on XStream**.
			* **1 CPU per GPU maximum allowed** (or 12,800MB node memory/GPU) with the following exceptions:

				* **Half node exception**: If the number of requested GPUs per node is 8 and if the `"--gres-flags=enforce-binding`" option is specified, up to 10 CPUs are allowed (or 128,000MB of memory).
				* **Exclusive node exception**: If the number of requested GPUs is 16 per node and if `"--gres-flags=enforce-binding`" is specified, up to 20 CPUs are allowed (or 256,000MB of memory).

			<p class="portlet-msg-alert">XStream is a GPU cluster, before using it, please ensure that your codes are making heavy use of GPUs and not CPUs.</p>

	#running-queues
		:markdown
			## [Queues](#running-queues)

			A single default partition, 'normal', is configured and represents all compute nodes. XStream uses SLURM QoS (Quality of Service) to enforce resource usage limits. The table below shows the current job limits per SLURM QoS:

		%table(border="1" cellpadding="3")
			%tr
				%th SLURM QoS
				%th Max CPUs
				%th Max GPUs
				%th Max Jobs
				%th Max Nodes
				%th Job time limits
			%tr
				%td <code>normal</code> (default)
				%td 320/user 400/group
				%td 256/user 320/group
				%td 512/user
				%td 16/user 20/group
				%td Default: 2 hours<br> Max: 2 days
			%tr
				%td <code> long</code>
				%td 20/user 80/group<br>200 max total
				%td 16/user 16/group<br>160 max total
				%td 4/user
				%td 4/user<br>64 max total
				%td Default: 2 hours<br> Max: 7 days 

	#running-schedulers
		:markdown
			## [Job schedulers](#running-schedulers)

			XStream runs Simple Linux Utility for Resource Management (SLURM) batch environment and doesn't provide any wrapper commands for now. Please refer to the official <a href="http://slurm.schedmd.com/documentation.html">SLURM Documentation</a> for more details.

		#running-jobcontrol-submission
			:markdown
				### [Job submission](#running-jobcontrol-submission)

				SLURM supports a variety of job submission techniques. By accurately requesting the resources you need, you'll be able to get your work done.

				A job consists in two parts: resource requests and job steps. Resource requests consist of a number of CPUs, GPUs, computing expected duration, amount of memory, etc. Job steps describe tasks that must be done, software which must be run.

				The typical way of creating a job is to write a submission script. A submission script is a shell script, e.g. a Bash script, whose first comments, if they are prefixed with "`SBATCH`", are understood by SLURM as parameters describing resource requests and other submissions options. You can get the complete list of parameters from the `sbatch` manpage ("`man sbatch`").

				SLURM will ignore all lines after the first blank line, even the ones containing SBATCH. Always put your SBATCH parameters at the top of your batch script.

				The script itself is a job step. Other job steps are created with the "`srun`" command.  For instance, the following script, hypothetically named "submit.sh":

				<pre>
				#!/bin/bash
				#
				#SBATCH --job-name=test
				#SBATCH --output=res.txt
				#
				#SBATCH --time=10:00
				#SBATCH --ntasks=1
				#SBATCH --cpus-per-task=1
				#SBATCH --mem-per-cpu=500
				#SBATCH --gres gpu:1
				#SBATCH --gres-flags=enforce-binding

				srun hostname
				srun sleep 60
				</pre>

				This script requests one task with one CPU and one GPU for 10 minutes, along with 500 MB of RAM, in the default partition. The "`--gres-flags=enforce-binding`" option will ensure the allocated GPU is locally bound to the allocated CPU, to avoid any slow QPI traffic. When started, the job would run a first job step srun hostname, which will launch the command hostname on the node on which the requested CPU was allocated. Then, a second job step will start the "`sleep`" command.

				Once the submission script is written properly, you need to submit it to SLURM through the "`sbatch`" command, which, upon success, responds with the jobid attributed to the job.

				<pre>
				login1$ <b>sbatch submit.sh</b>
				Submitted batch job 4011
				</pre>

				The job then enters the queue in the `PENDING` state. Once resources become available and the job has highest priority, an allocation is created for it and it goes to the `RUNNING` state. If the job completes correctly, it goes to the `COMPLETED` state, otherwise, it is set to the `FAILED` state.

				Upon completion, the output file contains the result of the commands run in the script file. In the above example, you can see it with "`cat res.txt`".

				Note that you can create an interactive job with the "`salloc`" command or by issuing an "`srun`" command directly.

		#running-jobcontrol-gpuids
			:markdown
				### [SLURM and GPU IDs](#running-jobcontrol-gpuids)

				When requesting GPUs with the option `--gres gpu:N` of `srun` or `sbatch` (not `salloc`), SLURM will set the `$CUDA_VISIBLE_DEVICES` environment variable to store the GPU ids that have been allocated to the job. So for instance, with `--gres gpu:2`, depending on the current state of the node GPUs, `$CUDA_VISIBLE_DEVICE` could be set to '0,1', meaning that you will be able to use GPU 0 and GPU 1. Most applications automatically detect the existence of `$CUDA_VISIBLE_DEVICES` and run on the allocated GPUs, but some don't and allow to explicitly set GPU ids, which would need to be done manually.

		#running-jobcontrol-cancel
			:markdown
				### [Cancel a job](#running-jobcontrol-cancel)

				Use the `scancel *jobid*` command with the jobid of the job you want canceled. In the case you want to cancel all your jobs, type `scancel -u $USER`. You can also cancel all your pending jobs for instance with `scancel -t PD`.

		#running-jobcontrol-jobinfo
			:markdown
				### [Job information](#running-jobcontrol-jobinfo)

				The "<a href="http://slurm.schedmd.com/squeue.html">`squeue`</a>" command shows the list of jobs which are currently running (they are in the `RUNNING` state, noted as "`R`") or waiting for resources (noted as "`PD`").

				<pre>
				login1$ <b>squeue</b>
				JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
				 2252    normal     job2     mike PD       0:00      1 (Dependency)
				 2251    normal     job1     mike  R 1-16:18:47      1 xs-0022
				</pre>

				The above output shows that one job is running, whose name is job1 and whose jobid is 2251. The jobid is a unique identifier that is used by many SLURM commands when actions must be taken about one particular job. For instance, to cancel job job1, you would use `scancel 2251`. Time is the time the job has been running until now. Node is the number of nodes which are allocated to the job, while the Nodelist column lists the nodes which have been allocated for running jobs. For pending jobs, that column gives the reason why the job is pending.

				As with the "`sinfo`" command (below), you can choose what you want `squeue` to output with the "`--format`" argument.

				<pre>login1$ <b>scontrol show job</b></pre>

				To get full details of a pending or running job: 

				<pre>login1$ <b>scontrol show job <i>jobid</i></b></pre>

				You can get near-realtime information about your program (memory consumption, etc.) with the <a href="http://slurm.schedmd.com/sstat.html">sstat</a> command.

				You can get the state of your finished jobs with the "`sacct`" command:

				<pre>
				login1$ <b>sacct</b>
				JobID    JobName  Partition    Account  AllocCPUS      State ExitCode
				|----------- ---------- ---------- ---------- ---------- ---------- --------
				4011               test     normal       srcc          1  COMPLETED      0:0
				4011.batch        batch                  srcc          1  COMPLETED      0:0
				4011.0         hostname                  srcc          1  COMPLETED      0:0
				4011.1            sleep                  srcc          1  COMPLETED      0:0
				</pre>

				Use the "<a href="http://slurm.schedmd.com/sacct.html">`sacct`</a>" command with its many options to interface to the SLURM accounting database. Here is an example of getting memory information of your recent past jobs:

				<pre>
				login1$ <b>sacct --format JobID,jobname,NTasks,nodelist,MaxRSS,MaxVMSize,AveRSS,AveVMSize</b>
				JobID    JobName   NTasks  NodeList     MaxRSS  MaxVMSize     AveRSS  AveVMSize
				|----------- ---------- -------- --------- ---------- ---------- ---------- ----------
				4011               test            xs-0024        16?        16?
				4011.batch        batch        1   xs-0024      1496K    150360K      1496K    106072K
				4011.0         hostname        1   xs-0024          0    292768K          0    292768K
				4011.1            sleep        1   xs-0024       624K    292764K       624K    100912K
				</pre>

		#running-jobcontrol-resources
			:markdown
				### [Resource information with "`sinfo`"](#running-jobcontrol-resources)

				SLURM offers a few commands with many options you can use to interact with the system. For instance, the <a href="http://slurm.schedmd.com/sinfo.html">sinfo</a> command gives an overview of the resources offered by the cluster, while the "`squeue`" command shows to which jobs those resources are currently allocated.

				By default, `sinfo` lists the partitions that are available. A partition is a set of compute nodes grouped logically. Typical examples include partitions dedicated to batch processing, debugging, post processing, or visualization.

				<pre>login1$ <b>sinfo</b>

				PARTITION AVAIL  TIMELIMIT  NODES  STATE NODELIST
				normal*      up 2-00:00:00      2  drain xs-[0054,0057]
				normal*      up 2-00:00:00      4    mix xs-[0007,0051-53]
				normal*      up 2-00:00:00     48  alloc xs-[0001-0006,0008-0009,0011-0050]
				normal*      up 2-00:00:00     11   idle xs-[0010,0055-0056,0058-0065]
				</pre>

				In the above example, we see one partition normal. This is the default partition as it is marked with an asterisk. In this example, 48 nodes of the normal partition are being used, 4 are in the mix state (partially allocated), 11 are idle (available) and 2 are drained which means some maintenance operation is taking place.

				The "`sinfo`" command can also output the information in a node-oriented fashion, with the "`-N`" argument. Along with the "`-l`" option, it will display more information about the nodes: number of CPUs, memory, temporary disk (also called local scratch space), features of the nodes (such as processor type for instance) and the reason, if any, for which a node is down.

				**Node characteristics and Generic Resources (GRES)**: SLURM associates to each nodes a set of Features and a set of Generic resources. Features are immutable characteristics of the node (e.g. CPU model, CPU frequency) while generic resources are consumable resources, meaning that as users reserve them, they become unavailable for the others (e.g. GPUs).

				To list all node characteristics including GRES, you can use the following command:

				<pre>login1$ <b>scontrol show nodes</b></pre>

				However, the output of this command is quite verbose. So you can also use `sinfo` to list GRES of each node using specific output parameters, for example:

				<pre>login1$ <b>sinfo -o "%10P %8c %8m %11G %5D %N"</b>

				PARTITION  CPUS     MEMORY   GRES        NODES NODELIST

				test       20       258374   gpu:k80:16  3     xs-[0007,0051,0058]
				normal*    20       258374   gpu:k80:16  62    xs-[0001-0006,0008-0050,0052-0057,0059-0065]
				</pre>

				On XStream, all compute nodes are identical, so no Features are set, only GRES are interesting for jobs allocation as GPUs are handled there. GRES appear under the form resource:type:count. On XStream, resource is always gpu and type is k80, and count is the number of logical K80 GPUs per node (16).

#tools
	:markdown
		# [Tools](#tools)

		Connecting to the compute nodes using `ssh` is allowed when at least one job of yours is running. You have access to some system tools there to debug your program, like "`top`", "`htop`" or "`strace`".  Debuggers from the compiler toolchains are also available. Don't forget to load the proper compiler toolchain first.

#refs
	:markdown
		# [References](#refs)

		* [SLURM documentation](http://slurm.schedmd.com/documentation.html)

#policies
	:markdown
		# [Policies](#policies)

		XStream is for authorized users only and all users are expected to comply with all Stanford computing, network and research policies.  For more info, see <http://acomp.stanford.edu/about/policy> and  <http://doresearch.stanford.edu/policies/research-policy-handbook>.

		Also please note that XStream is *not* HIPAA compliant and should not be used to process PHI. See <https://privacy.stanford.edu/faqs/hipaa-faqs> for more information.

