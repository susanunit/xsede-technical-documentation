#vmlaunch:actions
	:markdown
		# [Instance Management Actions](#vmlaunch:actions)

		After launching an instance, several options are available under the **Actions** menu located on the right hand side of your screen.

	#vmlaunch:actions:screen
		:markdown
			**Shutting down, suspending, stopping and shelving instances**

			<img alt="" src="/documents/10308/1181156/image-actions.png/6e9af687-7a3e-4848-b964-d462659d8b3c?t=1538594287179" style="width: 600px; height: 575px;" />

			**Figure 6:** Management Actions available on a launched instance

			## [Definitions](#vmlaunch:actions:defs)

			* **Suspend**: Instance in-memory state and disk are preserved, such that the instance can quickly be resumed. <span style="color: red">Network IP addresses will not be maintained</span>. Applications and other processes running at the time of *suspend* **will resume** when the instance boots. Note: Some processes **may not resume naturally**, such as processes involving network connections. This state is comparable to "hibernate" or "sleep" modes in a laptop.

			*SUSPENDing* an instance continues to consume system compute and storage resources. (75% normal usage rate)

			* **Shutdown**: Instance operating system is shut down, in-memory states are not stored, and only disk is preserved for a quick start. This state is comparable to "shutdown" modes in a laptop.

			While the system storage footprint is smaller than that of *SUSPEND*, *SHUTDOWN* continues to consume system storage resources (50% normal usage rate).

			* **Shelve**: Much like *SHUTDOWN*, the instance operating system is shut down and instance in-memory states are not stored. Unlike *SHUTDOWN*, a disk snapshot is stored to long-term storage and thus will be relatively slower to re-start. HOWEVER, *shelving* BENEFITS the cloud by no longer consuming computation resources and using far less disk resources. This state does not currently consume service units (0% normal usage rate).

			## [Actions](#vmlaunch:actions:acts)

			**Report** an instance if these situations occur:

			* you can't connect via [SSH](#vmlogin:ssh) or [VNC](#vmlogin:vnc)
			* the status never changes from pending to running
			* you receive errors when running or installing software
			* the metrics for the instance do not display
			* the instance exhibits any other unexpected behavior

			**Image	** request - see [Customizing and Saving a VM](#vmcust)

			**Suspend** an instance to:

			* stop active computing while still consuming system resources
			* safely preserve the state of your instance (though cannot image)
			* preserve your allocation (75% normal usage rate) <sup>a</sup>
			* **NOTE:** The IP address is _**likely to change**_ when the instance is resumed.
			* It may take 1-5 minutes to resume your instance
			* status will change from "Active" to "Suspended"

			**Shelve** an instance to:

			* free up compute resources for other users
			* preserve your allocation (0% normal usage rate) <sup>a</sup>
			* allow for imaging of the instance
			* **NOTE:** The IP address is _**likely to change**_ when the instance is resumed.
			* <span style="color: red">We strongly recommend detaching volumes before shelving.</span>
			* It may take 5-15 minutes to resume (Unshelve) your instance
			* status will change from "Active" to "Shelved_offloaded"

			**Stop** an instance to:

			* stop active computing, consuming fewer resources than a *suspend*
			* preserve your allocation (50% normal usage rate) <sup>a</sup>
			* allow for imaging of the instance
			* **NOTE:** The IP address is _**likely to change**_ when the instance is resumed.
			* It may take 1-5 minutes to resume your instance
			* status will change from "Active" to "Shutoff"

			**Reboot** an instance to send:

			* an 'ACPI Restart' request to the VM that will start the reboot process for your VM
			* a 'Hard Reboot', which will forcibly restart your VM, if it doesn't respond to a 'Reboot'

			**Redeploy** an instance:

			* to fix instances that show up as '*active - deploy_error*'
			* Contact support if your VM returns to the *deploy_error* state after redeployment.

			**Delete** an instance:

			* Unmount volumes within your instance before deleting the instance or risk corrupting your data and the volume.
			* Your instance will be shut down and **all data** will be permanently lost!
			* Your resource usage charts will not reflect changes until the instance is completely deleted and has disappeared from your list of instances.

			<sup>a</sup> Currently, actively running instances consume SUs at the rate of 1 SU per cores*hours used. Suspended instances consume 75% and Stopped instances consume 50%. Policy subject to change.

	#vmlaunch:states-table
		:markdown
			## [Comparison Table](#vmlaunch:states-table)
			### Instance state characteristics

	%table(border="1" cellpadding="3")
		%tr
			%th Action
			%th Active
			%th Suspend
			%th Shutdown
			%th Shelve
		%tr
			%td Consumes allocation
			%td Yes
			%td Yes, 75%
			%td Yes, 50%
			%td No
		%tr
			%td Consume system resources
			%td Yes
			%td Yes
			%td Yes
			%td No <sup>b</sup>
		%tr
			%td Saves running programs
			%td Yes
			%td Yes <sup>c</sup>
			%td No
			%td No
		%tr
			%td Preserves IP address
			%td Yes
			%td No
			%td No
			%td No
		%tr
			%td Time to resume
			%td n/a
			%td 1-5 min
			%td 1-10 min
			%td 1-15 min

	<p><sup>b</sup> Some disk resources consumed<br /><sup>c</sup> Some processes, particularly those tied to networking, may not resume fully.</p>

	#vmlaunch:actions:shutdown
		:markdown
			## [Linux VM shutdown](#vmlaunch:actions:shutdown)
			### Shutting down a VM from Linux

	<div class="portlet-msg-alert"><p>If you are going to stop your instance, shut down the VM gracefully and securely. In a GUI environment on Linux, the methods may vary.</p>
	<p>Launching a terminal and running the <span style="font-style: italic">shutdown</span> command as root (or with <span style="font-style: italic">sudo</span> privileges) should work consistently across Linux versions:</p>
	<pre>/sbin/shutdown -h now</pre>
	<p>OR</p>
	<pre>sudo /sbin/shutdown -h now</pre>
	<p>This will stop all operations, log out other active users on your VM as the system powers down and show your VM as "Shutoff" in Atmosphere once the VM has shut down.</p>
	<p>Note that sometimes the Atmosphere interface doesn't update in a timely manner to reflect the true status of the VM. If you have done a <span style="font-style: italic">shutdown</span> from the console, then do a hard refresh of your browser about a minute after shutdown. This is usually Command-R on a Mac and Ctrl-F5 on a PC. Check your browser's documentation to verify.</p>
	<p>In general, refresh the browser to verify the VM state if in doubt.</p>
	</div>

	#vmlaunch:actions:links
		:markdown
			### [Links](#vmlaunch:actions:links)

			[Open Web Shell](#vmlogin:web) to instance:

			* Web/Browser-based command line interface

			[Open Web Desktop](#vmlogin:vnc) to instance:

			* Web/Browser-based VNC Graphical User Interface Desktop

