#policies
	:markdown
		# [Policies and Best Practices](#policies)
	#policies:citizenship
		:markdown
			## [Good citizenship](#policies:citizenship)

			* Each _instance_ burns SUs for the time it is in operation.
				* It is beneficial to you and other Jetstream users to shelve instances when they are not in active use.
				* This frees up resources for other users and also preserves more of your SUs for future use.
				* Additional information on VM sizes and SU burn rates may be found at [Virtual Machine Sizes and Configurations](#vmsizes) and [XSEDE Service Units and Jetstream](#overview:su).

	#policies:recovery
		:markdown
			## [Resource recovery policies](#policies:recovery)

			* Instances that have been suspended or stopped can be shelved after two (2) weeks in said state.
			* Shelved images/instances will be deleted after six (6) months of inactivity.
			* All entities owned by a project (e.g. running instances, images, objects, volumes, networks, IPs allocated etc.) will be deleted six (6) months after the allocation ends.
			* <span style="color: red; font-weight: bold">NOTE:</span> If you are using a community-contributed image (i.e. non-Featured image) that belongs to another Atmosphere user, if that user no longer has an allocation or is removed for other reasons, their image will go away as well! If your work is dependent on someone else's image, you might consider making your own version using the instructions [Customizing and saving a VM](#vmcust).

	#policies:accept-use
		:markdown
			## [Acceptable Usage](#policies:accept-use)

			* [Acceptable Use Policies and Research and Export Control Guidance](#policies:acceptable-use-guidance)
				* This includes EAR sanctioned countries and countries under heightened restrictions, as well as definitions for fundamental research.
				* As stated in the [XSEDE Usage Policy](https://www.xsede.org/ecosystem/operations/usagepolicy), users of the Jetstream system are expected to abide by policies established by the service providers.

					* Indiana University 
						* <a href="http://policies.iu.edu/policies/categories/information-it/it/IT-02.shtml" target="_blank">IT-02 Misuse and Abuse of Information Technology Resources</a>
						* <a href="http://policies.iu.edu/policies/categories/information-it/it/IT-12.shtml" target="_blank">IT-12 Security of Information Technology Resources</a>
				* TACC
						* <a href="https://portal.tacc.utexas.edu/tacc-usage-policy" target="_blank">Acceptable Use Policy</a>
			* [Acceptable Usage Policies for Jetstream-hosted Gateways](#policies:acceptable-use-gw) - this also includes a link to a sample AUP you should consider implementing for your gateway.

	#policies:security
		:markdown
			## [Security](#policies:security)

			* <span style="color: red">Periodically apply operating system updates to your running VMs</span>
				* The Ubuntu 20.04, 18.04, 16.04 and Centos 7 featured images are utilizing unattended security updates. Instances will not reboot, but they will apply any update marked as a security update. It's still a good idea to update your VM, just in case.
				* **CentOS**: <code>sudo yum update</code>
				* **Ubuntu**: <code>sudo apt-get update</code> then <code>sudo apt-get upgrade</code>
				* If the kernel or glibc/libc packages are being updated, rebooting is necessary to implement those updates.
				* **_Always run updates before requesting a new custom image in Atmosphere._**
				* ** An actively updating instance may be slow to deploy and may require a redeploy or even a reboot after updating in order to fully successfully deploy.

			* Remember to log out from the menu at the top right (where it shows your username).
				* This ensures that you do not inadvertently allow others to access your Jetstream account.
			* Update and configure software that utilizes the network to preclude unauthorized access.
				* Commonly neglected examples include **_MongoDB_**.

	#policies:image-mgmt
		:markdown
			## [Image management policies](#policies:image-mgmt)

			* As operating systems near the end of their supported life cycles, they will be removed from Featured Images. In addition, custom images based on those operating systems will no longer be supported on Atmosphere. You may be notified that you must migrate your workflows to a newer version of the operating system you're using. Security policies noted above require that any operating system that is no longer receiving timely security patching may not remain in service on IU or TACC networks.
				* [Ubuntu Support Life Cycle](https://www.ubuntu.com/info/release-end-of-life)
				* [CentOS Support Life Cycle](https://wiki.centos.org/FAQ/General#head-fe8a0be91ee3e7dea812e8694491e1dde5b75e6d)

	#policies:software
		:markdown
			## [Software installation/development](#policies:software)

			* SSH/SSHD
				* The Secure Shell client (SSH) and daemon (SSHD) come preinstalled on all instances.
				* Configurations settings appropriate and/or required for normal Jetstream operation have been put in place.
				* Under normal circumstances, <span style="color: orange; font-weight: bold">SSH/SSHD should not need to be upgraded or reinstalled</span>.
				* If you do reinstall or modify SSH/SSHD or the related configuration settings, you may disrupt essential Jetstream features and <span style="color: red; font-weight: bold">MAY LOCK YOURSELF AND ADMINISTRATORS OUT OF THE INSTANCE</span>.
					* Fixing this may be difficult or impossible, thus forcing you to delete your instance.

			* Python
				* Python2 comes preinstalled on all featured images. This will change in the future as Python2 is deprecated and all operations for Jetstream will utilize or are utilizing Python3.
				* Configurations settings appropriate and/or required for normal Jetstream operation have been put in place.
				* If you wish to reconfigure, reinstall, or upgrade Python, <span style="color: green; font-weight: bold">we recommend implementing a user-level virtual-environment technology</span>, such as VIRTUALENV, ANACONDA, or MINICONDA to avoid conflicts with the system-level installation.
				* <span style="color: orange; font-weight: bold">The default system-level Python must remain at Python2</span>, though you may add a non-default Python3.
				* If you do reinstall or modify Python or the related configuration settings or remove it from the system PATH, you may disrupt essential Jetstream features and <span style="color: red; font-weight: bold">MAY PREVENT SUCCESSFUL BOOTING AND CLOUD DEPLOYMENT</span>.
					* Fixing this may be difficult or impossible, thus forcing you to delete your instance.
				* Be aware that some PIP upgrades, even within Python2 or within Python3, particularly from pip9 to a higher version, require several additional steps to ensure continued instance operation or backward compatibility with installed software.

	#policies:security-updates
		:markdown
			## [Updating only security packages](#policies:security-updates)

			While updating all packages is recommended, if you would just like to install security updates, you can do the following:

			### Ubuntu 16, 18 and 20

				grep security /etc/apt/sources.list | sudo tee /etc/apt/security.sources.list
				sudo apt-get upgrade -o Dir::Etc::SourceList=/etc/apt/security.sources.list

			### CentOS 7 and up

				sudo yum update --security

	#policies:acceptable-use-guidance
		:markdown
			## [Acceptable Use Policies and Research and Export Control Guidance](#policies:acceptable-use-guidance)

			In general, fundamental, publishable research is permitted on Jetstream from any non-EAR sanctioned countries. Fundamental research is defined as:

		<p class="portlet-msg-info">Fundamental research means basic and applied research in science and engineering, the results of which ordinarily are published and shared broadly within the scientific community, as distinguished from proprietary research and from Industrial development, design, production, and product utilization, the results of which ordinarily are restricted for proprietary or national security reason. <br />[National Security Decision Directive (NSDD) 189, National Policy on the Transfer of Scientific, Technical, and Engineering Information]</p>

		:markdown
			For anything other than fundamental, publishable research, you may wish to consult with your institutional export control office for any export/sharing restrictions.

			## Acceptable Use of Jetstream

			Jetstream requires compliance with all XSEDE, Indiana University, and University of Texas Austin/TACC usage policies, including but not limited to:

			* [XSEDE Usage Policy](https://www.xsede.org/ecosystem/operations/usagepolicy)
			* Indiana University 
			** [IT-02 Misuse and Abuse of Information Technology Resources](http://policies.iu.edu/policies/categories/information-it/it/IT-02.shtml)
			** [IT-12 Security of Information Technology Resources](http://policies.iu.edu/policies/categories/information-it/it/IT-12.shtml)
			* TACC
			** [Acceptable Use Policy](https://portal.tacc.utexas.edu/tacc-usage-policy)

			Jetstream researchers agree to:

			* Ensure that data that must be protected by Federal security or privacy laws (e.g., HIPAA, FERPA, ITAR, classified information, export control, etc.) are not stored on this system unless such storage and usage is specifically authorized by the responsible University administrator and complies with any processes for management of access to such information. For export controlled information, including ITAR information, approval of the University Export Compliance Office is required prior to use of the Jetstream systems for storage/processing of export controlled data. The Jetstream system is not intended, by default, to meet the security requirements of these laws or regulations and specific usage related controls or restrictions may be required prior to authorization of the use of the Jetstream system for such purposes.

			* Ensure that the project does not violate any export control end use restrictions contained in [Part 744 of the EAR](https://www.bis.doc.gov/index.php/documents/regulations-docs/2343-part-744-control-policy-end-user-and-end-use-based-2/file).

			### Countries with heightened restrictions

			Some countries have some heightened control over certain types of research and technology. Please consult with your institutional export control office to ensure that you are in full compliance with US regulations and institutional policies when working with countries under heightened controls and/or when working with research or information that may be limited by export control law.

			The US Department of Commerce, Bureau of Industrial Security (BIS) publishes a list of EAR Country Group designations. Country Group D (countries of national security concern to the United States) face these heightened control restrictions and are included in this [document of country groups](https://www.bis.doc.gov/index.php/documents/regulation-docs/2255-supplement-no-1-to-part-740-country-groups-1/file). (Please note that Country Group D begins on page 6 of this document.)

			### Sanctioned countries (not eligible to use Jetstream)

			In accordance to federal guidelines, Jetstream will not be accessible to IP ranges from the following countries:

			* Cuba
			* Iran
			* North Korea
			* Syria
			* Crimea Region of the Ukraine

			The Bureau of Industry and Security (BIS) implements U.S. Government certain sanctions against these countries pursuant to the Export Administration Regulations (EAR), either unilaterally or to implement United Nations Security Council Resolutions.

			The US Department of Commerce, Bureau of Industrial Security (BIS) publishes a list of EAR Country Group designations. Country Groups E1 and E2 identify embargoed countries subject to comprehensive restrictions and are included in this [document of country groups](https://www.bis.doc.gov/index.php/documents/regulation-docs/2255-supplement-no-1-to-part-740-country-groups-1/file). (Please note that Country Group E begins on page 8 of this document.)

	#policies:acceptable-use-gw
		:markdown
			## [Acceptable Usage Policies for Jetstream-hosted Gateways](#policies:acceptable-use-gw)

			Gateways hosted on Jetstream agree to abide by the policies defined in the [Acceptable Use Policies and Research and Export Control Guidance](#policies:acceptable-use-guidance) document. We also recommend having an Acceptable Usage Policy (AUP) on your gateway.

			We recommend adding a statement to any acceptable use agreement that targets export control issues, such as the language below:

			* Ensure that data that must be protected by Federal security or privacy laws (e.g., HIPAA, FERPA, ITAR, classified information, export control, etc.) are not stored on this system unless such storage and usage is specifically authorized by the responsible University administrator and complies with any processes for management of access to such information. For export controlled information, including ITAR information, approval of the University Export Compliance Office is required prior to use of the [name of system] systems for storage/processing of export controlled data. The [name of system] system is not intended, by default, to meet the security requirements of these laws or regulations and specific usage related controls or restrictions may be required prior to authorization of the use of the [name of system] system for such purposes.

			* Ensure that the project does not violate any export control end use restrictions contained in Part 744 of the EAR.

			The [TrustedCI project](https://www.trustedci.org) maintains a [sample AUP](https://docs.google.com/document/d/1kZ2p5VjVGdi83_hlGWheUNspI7XPsoOYU3CNUNNs7uY/edit) that may serve as a gateway policy model.
