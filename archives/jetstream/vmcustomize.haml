#vmcust
	:markdown
		# [Customizing and saving a VM](#vmcust)

		An image is a type of template for a virtual machine (VM).

		You can launch an instance, custom install the software and files you want to use, then [request an image of the instance](#vmcust:request). This will save all of the changes and updates within Atmosphere, but also make your custom changes available to yourself and collaborators. Saving instances as images helps leverage both your time and existing resources. The saved image can be used to launch a new child instance at any time. This allows you to discard non-active instances, thus saving resources.

		<div class="portlet-msg-alert"><strong>CAUTION!</strong><br />
			<ul>
				<li><strong>THE <em>INSTANCE</em> MUST BE <span style="color: green">ACTIVE</span> OR <span style="color: red">STOPPED</span></strong> in order to request an image. Currently, Jetstream cannot image <em>Suspended</em> or <em>Shelved</em> instances.</li>
				<li>ALL <em>VOLUMES</em> MUST BE <span style="color: red">DETACHED</span> FROM THE <em>INSTANCE</em>. Failure to detach Volumes will result in a image whose child instances <span style="color: red">fail to boot</span>.</li>
				<li><em>INSTANCES</em> MUST NOT BE DELETED UNTIL <span style="color: green; font-weight: bolder; font-style: italic">AFTER</span> THE IMAGING PROCESS HAS COMPLETED.</li>
				<li><a href="#vmcust:guidelines">Imaging Guidelines</a>: There are several other <a href="#vmcust:guidelines">restrictions and notes</a> of which you must be aware in order to achieve a successful image. For example, several directories and files are <span style="color: red">removed</span> during image creation.</li>
				<li>At this time, only the creator of an image may update an image.</li>
				<li style="color:red; font-style: italic; font-weight: bolder">Please note that s1.* based customized instances will NOT be able to be used to create images in Atmosphere.<br />Additionally, new s1.* instances are no longer supported on Jetstream. Please use m1.* instances with attached volumes instead.</li>
			</ul>
		</div>

	:markdown
		After submitting the form, the Jetstream Atmosphere support staff will review and process the request. Future versions of Atmosphere will allow users to initiate the VM imaging process automatically.

		Be sure to test launch any image created to validate that it behaves as expected _**BEFORE**_ suspending or removing the current active instance, since only active instances can be imaged.

		**<span style="color: blue">BEST PRACTICE:</span> It is recommended to upgrade the operating system before imaging. For CentOS based systems, it's <span style="color: green">sudo yum update</span>. For Ubuntu based systems, do <span style="color: green">sudo apt-get update</span> and then <span style="color: green">sudo apt-get upgrade</span>.**
	<p class="portlet-msg-alert"><strong>VERY IMPORTANT NOTE</strong><br /><em>Creating an image on the SMALLEST possible size VM on which it will run will allow the image to be launched on VMs of the same size and larger.<br />For example, an image created on a Tiny size VM can be launched on a VM of any size; an image created on a Medium VM can only be launched on a Medium or larger size VM.</em></p>
	<p class="portlet-msg-alert"><strong>WARNING:  Atmosphere vs. API</strong><br /><em>We cannot recommend strongly enough not using Atmosphere images on the API side of Jetstream or vice-versa.<br />The Atmosphere user interface is designed to be extremely easy to use, particularly for beginning users but the trade-off is that Atmosphere enforces certain practices behind the scenes.<br />The API user interface is close to raw openstack, makes almost no assumptions, requires advanced knowledge of Openstack and Linux, but is therefore much more flexible.<br />API and Atmosphere base images provided by Jetstream staff require SUBSTANTIAL work to make them CLEANLY work in the other environment.</em></p>

	#vmcust:guidelines
		:markdown
			## [Imaging Guidelines](#vmcust:guidelines)

			Here are some tips to help ensure a viable importable image:

			* **Operating system**: Base the image on 
				* CentOS6, CentOS 7 and later
				* Ubuntu 14.04 and later, Long Term Support (LTS) versions of Ubuntu recommended
			* **File system**: Ext3, Ext4, or XFS.
			* **Image format**: RAW or QCOW2.
			* **Software**: Image must contain no licensed software that would prohibit use within a cloud or virtualized environment. It is recommended that software be installed in `/opt` or `/usr/local/`.
			* **New Image Name**:<br>**<span style="color: red">DO NOT</span>** use the name of an _existing featured image_. Please be mindful of re-using the name of one of your own existing images.<br>**<span style="color: red">DO NOT</span>** use a period(".") as any of the last 5 characters of the name.
			* **Description of the Image**: <span style="color: green; font-weight: bold">DO</span> add an _informative description_ of your image and make note of how it varies from any base images.
			* **Instance status**: The instance **MUST** be <span style="color: green">ACTIVE</span> or <span style="color: red">STOPPED</span> in order to image. <span style="color: darkorange">_Suspended_ or _Shelved_ images cannot be imaged at this time.</span>
			* **<span style="color: red">DO NOT</span>** delete _Instances_ until <span style="color: green; font-weight: bold">AFTER</span> the IMAGING process has completed.
			* **Volumes**: All Volumes **MUST** be <span style="color: red">DETACHED</span> from the instance. Failure to detach Volumes will result in a image whose child instances <span style="color: red">fail to boot</span>.
			* **Owner**: At this time, only the creator of an image may update an image. Developer teams wishing to update a shared image should contact [Jetstream support](mailto:help@jetstream-cloud.org).
			* **<span style="color: red">BEST PRACTICE:</span> Users are recommended to upgrade the operating system before imaging.<br />For CentOS based systems:** _sudo yum update_<br />**For Ubuntu based systems: do** _sudo apt-get update_ **and then** _sudo apt-get upgrade_

			Before submitting a request for an image of your instance, **<span style="color: red">remove</span>** the following software from the instance:

			* **Licensed software**: including software not purchased by Jetstream or otherwise not in the public domain.
			* **Not cloud suitable**: software in which the licensing, features, or activity of the software otherwise precludes its use or inhibits the activity of other software within a cloud or virtualized environment.

			Volumes and iRODS FUSE mounts are <span style="color: darkorange">not copied</span> as part of the image.

	<p class="portlet-msg-alert"><strong>VERY IMPORTANT NOTE:</strong><br />CHOOSE SMALLEST SIZE<br />Users are encouraged to test launch any image created to validate that it behaves as expected before suspending or removing their current active instance.<br /><em>Creating an image on the SMALLEST possible size VM on which it will run will allow the image to be launched on VMs of the same size and larger.<br />For example, an image created on a Tiny size VM can be launched on a VM of any size; an image created on a Medium VM can only be launched on a Medium or larger size VM.</em></p>

	<div class="portlet-msg-alert">
	<p><strong>About your <code>/home</code> directory</strong></p>

	:markdown
		* All files, directories, and icons located under `/home/<username>/Desktop` will be deleted. To preserve them, email <help@jetstream-cloud.org>.
		* Any files installed in `/home` must be saved to your volume, to iRODS, or to another storage device or system external to your VM.
	</div>

	<p class="portlet-msg-info"><strong>Can an image be exported?</strong><br />Yes...and no. Currently an automated method for exporting images to run on other systems is not available but if you find that you do need to export an image to something other than IU Scholarworks contact the <a href="mailto:help@xsede.org">XSEDE Help Desk</a>.</p>

	<div class="portlet-msg-alert">
	<p><strong>CAUTION!</strong></p>
	<p>The following directories are deleted as part of the imaging process:</p>

	:markdown
		* `/home/`
		* `/mnt/`
		* `/tmp/`
		* `/root/`
	</div>

	:markdown
		The following system files are typically overwritten by the Jetstream imaging process for security and operational reasons:

		* `/etc/fstab`
		* `/etc/group`
		* `/etc/host.allow`
		* `/etc/host.conf`
		* `/etc/host.deny`
		* `/etc/hosts`
		* `/etc/ldap.conf`
		* `/etc/passwd`
		* `/etc/resolve.conf`
		* `/etc/shadow`
		* `/etc/sshd/`
		* `/etc/sysconfig/iptables`
		* `/root/`
		* `/var/log`

	#vmcust:request
		:markdown
			## [Request an image](#vmcust:request)

			<p class="portlet-msg-alert"><strong>Before you begin</strong><br />Read and follow the directions in <a href="#vmcust:guidelines">Imaging Guidelines</a>.</p>
 
			You can request an image (a type of template for a virtual machine) of a running instance. This saves a complete copy of all changes and updates made to the instance since it was launched it so it can be reused at any time. It also saves resources by launching the instance only when you need it.

			You also can see the list of all image requests you have made.

			You can add a script before requesting the image that executes after an instance using the image is launched and active.

		<span style="color: darkorange; font-style: italic">PLEASE NOTE: Images will get created on the IU cloud by default, no matter which cloud they originated from. They will automatically be synced nightly to the TACC cloud.</span>
		<div class="portlet-msg-alert">
		<p><strong>CAUTION!</strong></p>
		<p>The following directories are deleted as part of the imaging process:</p>

		:markdown
			* `/home/`
			* `/mnt/`
			* `/tmp/`
			* `/root/`
		</div>

		:markdown
			**To get started:**

			1. Log in to Jetstream web interface <https://use.jetstream-cloud.org/>.
			1. <span style="color: green">Detach</span> all attached volumes from the instance. _[Detailed instructions](#volumes:detach)._
			1. Click **Projects** on the menu bar and open the project with the instance to use for the new image.
			1. Click the **instance name**. The instance can have a status of <span style="color: green">Active</span> or <span style="color: red">Stopped</span> but it cannot be **Suspended** or **Shelved**.
			1. In the Actions list on the right, click **Image**.

	#vmcust:request-image
		:markdown
			### [Image Info](#vmcust:request-image)

			The information you provide on here will help others to discover this image.

			1. **Create or Update? (optional):** The default is to create a new image, but if you are updating an image to a new revision, you can uncheck this box and it will be created as a new version of the same image. You can see examples of this with the [Ubuntu 18.04 Featured image](https://use.jetstream-cloud.org/application/images/717) on Atmosphere. We recommend you update the image when possible.
			1. **New Image Name (required):** Enter the name, up to 30 characters, to assign to the new image. Please <span style="color: red; font-weight: bold">DO NOT</span> use the name of an _existing featured image._ Please be mindful of reusing the name of one of your own existing images.
			1. **Description of the Image (required):** The description should include keywords that concisely describe the tools installed, the purpose of the tools (e.g., *This image performs X analysis*), and the initial intent of the machine image (e.g. *designed for XYZ workshop*). Include keywords that will help users search for this image.
			1. **Image Tags (optional):** Click in the field and select tags that will enhance search results for this image. You may include the operating system, installed software, or configuration information (e.g. Ubuntu, NGS Viewers, MAKER, QIIME, etc.). Tags can be added and removed later, if needed.
			1. Click <span style="color: rgb(153,51,0);">Next</span>.

			JSIMAGEINFO

			**Figure 1:** Image Request - Image Info screen

	#vmcust:request-version
		:markdown
			### [Version Info](#vmcust:request-version)

			Versioning is an important part of the imaging process. Use this information to track how your image changes over time. This information will also be helpful to others that wish to use your image.

			1. **New Version Name (required)**: Enter the new (unique) name or number of the image. Versioning helps users understand how your changes relate to the overall progress of the Application. Versions are alphanumeric (e.g. 2.0-stable, 2.1-beta, 2.2-testing). Limit the name to 30 characters and keep versioning consistent.
			1. **Change Log (required)**: Concisely describe what you've changed in this specific version. This description will help users understand how your application as changed over time.
			1. Click <span style="color: rgb(153,51,0);">Next</span>.

			JSVERSIONINFO

			**Figure 2:** Image Request - Version Info screen

	#vmcust:request-provider
		:markdown
			### [Provider](#vmcust:request-provider)

			1.	Select the cloud provider to use for the image. If you would like the image to be available on multiple clouds, email <help@jetstream-cloud.org>.
			1.	Indicate minimum CPU and memory requirements (optional).
			1.	Click <span style="color: rgb(153,51,0);">Next</span>.

			JSPROVIDER

			**Figure 3:** Image Request - Provider screen

	#vmcust:request-privacy
		:markdown
			### [Privacy](#vmcust:request-privacy)

			1.	Select the visibility for the image:

				* **Public**: The image will be visible to all users and anyone will be able to launch it.
				* **Private**: The image will be visible only to you and only you will be able to launch it.
				* **Specific Users**: The image will be visible to only you and the users you specify. Only you and those specific users will be able to launch it. If you chose Specific Users, select the users who will be able to launch the image.

			2.	Click <span style="color: rgb(153,51,0);">Advanced Options</span> or <span style="color: rgb(153,51,0);">Submit</span>.

				<span style="color: rgb(153,51,0);">Advanced Options</span> will allow you to:

				* Exclude files from the image 
				* Add a deployment script
				* Require the user to verify understanding of any license restrictions

			JSPRIVACY

			**Figure 4:** Image Request - Privacy screen

	#vmcust:request-excludefiles
		:markdown
			### [Advanced Option](#vmcust:request-excludefiles)
			#### [Exclude Files](#vmcust:request-excludefiles)

			Note the list of directories that will automatically be excluded form the image:

			* /home/
			* /mnt/
			* /tmp/
			* /root/

			In the box provided, list any additional files or directories to be excluded from the image. Write one path per line.

			JSEXCLUDEFILES

			**Figure 5:** Image Request - Exclude Files screen

	#vmcust:request-bootscripts
		:markdown
			### [Advanced Option](#vmcust:request-bootscripts)
			#### [Boot Scripts & Licenses](#vmcust:request-bootscripts)

			Deployment scripts are executed when a user launches the image and each time an instance is 'Started', 'Resumed', or 'Restarted'. These scripts should be able to handle being run multiple times without adverse effects.

			Click <span style="color: rgb(153,51,0);">Next</span> to continue to the next screen without adding a new script or a software license.

			1. To add a deployment script, click in the search field and search for the title of the script.
			1. To create a new deployment script:
				1. Click <span style="color: rgb(153,51,0);">Create New Script</span>, enter a title for the script, then either click <span style="color: rgb(153,51,0);">URL</span> and enter the script URL or click <span style="color: rgb(153,51,0);">Full Text</span> and enter the deployment script.
				1. When done, click <span style="color: rgb(153,51,0);">Create and Add</span>, then click <span style="color: rgb(153,51,0);">Next</span>.

	:markdown
		1. To list any licensed software used in the image and require users to agree to the license agreement before launching, click in the search field and search for the license title.
		1. To create a new license:
			* Click <span style="color: rgb(153,51,0);">Create New License</span>, enter a title for a license, then either click <span style="color: rgb(153,51,0);">URL</span> and enter the license URL or click <span style="color: rgb(153,51,0);">Full Text</span> and enter the full license text.
			* When done, click <span style="color: rgb(153,51,0);">Create and Add</span>, then click <span style="color: rgb(153,51,0);">Next</span>.

			JSBOOT

			**Figure 6:** Image Request - Boot Scripts & Licenses screen

	#vmcust:request-review
		:markdown
			### [Review Image Request](#vmcust:request-review)

			On the Review screen, verify the information entered on the previous screens. Click <span style="color: rgb(153,51,0);">Back</span> to return to the previous screens and make corrections.  When all is OK, click the <span style="color: rgb(153,51,0);">checkbox</span> certifying that the license does not contain any license-restricted software that is prohibited from being distributed within a virtual or cloud environment..

			1.  Click <span style="color: rgb(153,51,0);">Request Image</span>.

			You will receive an email from Support when the image is completed. Please email questions to <help@jetstream-cloud.org>.

			JSREVIEW

			**Figure 7:** Image Request - Review screen

	#vmcust:listimages
		:markdown
			## [Viewing your images](#vmcust:listimages)

			You can view your lists of images and image requests.

			1.	Click <span style="color: rgb(153,51,0);">Images</span> on the top menu bar.
			1.	Click:
				* <span style="color: rgb(153,51,0);">MY IMAGES</span> to view the list of your images.
				* <span style="color: rgb(153,51,0);">MY IMAGE REQUESTS</span> to view the list of your image requests.

			JSIMAGELIST

			**Figure 8:** Viewing a list of images

	#vmcust:delete
		:markdown
			## [Request image deletion](#vmcust:delete)

			Currently, the only way to delete (archive) an image you requested is to email <help@jetstream-cloud.org>. You will receive an email confirmation when your image has been archived.

			However, if you are the owner of an image, you may change the <strong>"Date to hide image from public view"</strong> as well as the <strong>"Visibility"</strong> for your image.

			To do so, select _Images_, then select _My Images_.

			Select the image you wish to modify and click _Edit Details_.

	#vmcust:submit
		:markdown
			## [Image publication](#vmcust:submit)
			### [Submit your Jetstream image for storage and digital object identifier (DOI)](#vmcust:submit)

			Images that you have customized and saved may be made available for publications or to the public, within certain limits (for example, they may not contain secure or protected information or data).

			To request an image be stored, given a digital object identifier (DOI), please follow <a target="_blank" rel="noopener" href="http://jetstream-cloud.org/request-doi.php">these instructions</a>.

			Please be aware that at the current time, such a process requires the manual intervention of staff and is therefore subject to scheduling limits.

