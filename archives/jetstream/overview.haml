<font color="red" size="+1"><b>NOTICE: Jetstream has been replaced by <a href="/jetstream2">Jetstream2</a> and is no longer available for allocations.</b></font></p>

<span style="font-size:225%; font-weight:bold;">Jetstream User Guide</span><br>
<em>Last update: August 08, 2022</em>

#overview
	:markdown
		# [System Overview](#overview)

		<strong>Jetstream</strong> is an NSF-funded (NSF-1445604), user-friendly <em>cloud environment</em> designed to give researchers access to interactive computing and data analysis resources on demand, whenever and wherever they want to analyze their data. It is __not__ a traditional High Performance Computing (HPC) or High Tech Computer (HTC) environment.

		It provides a library of <strong>virtual machines</strong> designed to do discipline-specific scientific analysis. Software creators and researchers will also be able to create their own customized virtual machines (VM) or their own private computing system within Jetstream.

		Jetstream features a <strong>web-based user interface</strong> based on the popular Atmosphere cloud computing environment developed by [CyVerse](http://www.cyverse.org/) (formerly known as the iPlant Collaborative) and extended to support science and engineering research generally. The operational software environment is based on [OpenStack](http://www.openstack.org/).

	<div class="portlet-msg-info"><h4>Accessing Jetstream</h4>
	<p>Jetstream is accessible through a <a href="https://use.jetstream-cloud.org">web interface</a> using <a href="http://www.xsede.org">XSEDE credentials</a> via <a href="https://www.globus.org/tags/globus-auth">Globus Auth</a>.<br />Jetstream is <strong>NOT</strong> accessible via the <a href="https://portal.xsede.org/single-sign-on-hub">XSEDE Single Sign-On Login Hub</a>.<br />Newly created XSEDE accounts must be added to a specific allocation by the PI or Resource manager in order to access Jetstream.</p>
	</div>

	:markdown
		Jetstream is meant primarily for <strong>interactive</strong> research, small scale processing <strong>on demand</strong>, or as the backend to science <strong>gateways</strong> to send research jobs to other HPC or HTC resources.

		Jetstream is different in that you can work with <strong>GUIs</strong> that you couldn't otherwise use on most HPC systems.

		Jetstream may be used for <strong>prototyping</strong>, for creating tailored <strong>workflows</strong> to either use at smaller scale with a handful of CPUs or to port to larger environments after doing your <strong>proof of concept</strong> work at a smaller level.

		Jetstream is <strong>not</strong> a typical High Performance Computing (HPC) or High Throughput Computing (HTC) environment and <strong>won't be used for large scale</strong> parallel processing or high-throughput computing.

		JSTOPO

		**Figure 1:** System overview of Jetstream environment

	#overview:features
		:markdown
			## [Key Features](#overview:features)

			Jetstream utilizes [Atmosphere](http://www.cyverse.org/atmosphere), an easy to use web application, on-demand environment that is designed to accommodate computationally and data-intense research tasks, including [Infrastructure as a Service (IaaS)](#glossary) with advanced APIs; [Platform as a Service (PaaS)](#glossary) for developing and deploying software to the science community; and [Software as a Service (SaaS)](#glossary).

			Some of the key features include:

			* Access virtual machine images preconfigured with an operating system and software to help you do scientific computations in domain-specific tasks
			* Find and use tools with the intuitive self-service portal 
			* Easily manage virtual machines
			* Publish your own software suites, create your own work environments, and run the software for community use 
			* Integrate with existing infrastructure components using API services
			* Easily generate and manage statistical reporting of user resources for total CPU hours and memory usages, total instances and applications launched by user, cloud monitoring, and on-demand intelligence resource allocation

			Within Atmosphere, you launch an *instance* (a launched image of a virtual machine), selecting from the list of available *images* (a template of a virtual machine containing an installed operating system, software, and configuration). It is recommended that you familiarize yourself with the [Linux command line](http://linuxcommand.org/lc3_learning_the_shell.php) as some actions require some degree of knowledge of the Command Line Interface (CLI).

	#overview:config
		:markdown
			## [System Configuration](#overview:config)

			The computing environment consists of two homogenous clusters at [Indiana University](https://www.indiana.edu) and [Texas Advanced Computing Center (TACC)](https://www.tacc.utexas.edu) with a test environment at the [University of Arizona](http://www.arizona.edu). The system provides over 1/2 a PetaFLOPS of computational capacity and 2 petabytes of block and object storage. The individual nodes contain two Intel "Haswell" processors, 128 GB of RAM, 2 terabytes of local storage, and 10 gigabit Ethernet networking. The system leverages 40 gigabit Ethernet for network aggregation and each of the production clusters connect to Internet2 at 100 Gbps. The physically distributed system allows Jetstream to be highly available and resilient. [GlobusAuth](https://www.globus.org/tags/globus-auth) is used for large-scale file transfer and authentication.

			JSCONFIG

			**Figure 2:** System configuration of Jetstream environment

	#overview:specs
		:markdown
			## [System Specs](#overview:specs)

	#overview:specs-table1
		:markdown
			[Table 1. System specs](#overview:specs-table1) 

	%table(border="1" cellpadding="3")
		%tbody
			%tr
				%th System Configuration
				%th Aggregate information
				%th Per Node (Compute Node)
			%tr
				%td Machine type
				%td Dell (Various)
				%td Dell M630 Nodes
			%tr
				%td Operating system
				%td RHEL/CentOS/Ubuntu
				%td CentOS
			%tr
				%td Memory model
				%td N/A
				%td N/A
			%tr
				%td Processor cores
				%td 7,680 per site
				%td 24
			%tr
				%td CPUs
				%td 640 per site
				%td 2
			%tr
				%td Nodes
				%td 320 per site
				%td -
			%tr
				%td RAM
				%td 40TB
				%td 128GB
			%tr
				%td Network
				%td 100gbps to Internet2<br>40gbps x 4 to local infrastructure<br>10gbps to XSEDE
				%td 10gbps
			%tr
				%td Local Storage
				%td 640TB
				%td 2TB
			%tr
				%th Storage information
				%th Aggregate information
				%th Per Node
			%tr 
				%td File systems
				%td Block storage for VMs, other storage options forthcoming
				%td Varies
			%tr 
				%td Total disk space
				%td 640TB Local<br>960TB Attached Storage
				%td 2TB local
			%tr 
				%td Total scratch space
				%td N/A
				%td N/A





	#overview:network
		:markdown
			## [Network configuration and policies](#overview:network)
	#overview:network:hardware
		:markdown
			### [Hardware Configuration](#overview:network:hardware)

			The underlying cloud providers have hardware with

			* 10 Gbps network connectivity from the compute hosts to the cloud's internal network infrastructure
			* 4-fold 40 Gbps uplinks from the cloud infrastructure to the site's enterprise infrastructure
			* 100 Gbps connectivity from the site infrastructure to the Internet2 backbone 
			* 10 Gbps redundant connectivity to the XSEDE research network
			* Individual instances have full access to this infrastructure with no added speed limits.

	#overview:network:ips
		:markdown
			### [Persistent IPs](#overview:network:ips)
			#### Atmosphere

			Presently, Atmosphere does not allow for persistent IPs. If your VM is down for more than 5 minutes, regardless if it is a planned system outage, emergency outage, or if you have taken your VM offline, you can and very well may end up with a different IP number. If you require a persistent IP for a service, we recommend you obtain API access and bring your service up there. Please see [Using the Jetstream API](#api) for more information.

			#### API

			The Jetstream API provides persistent IPs that will remain through VM actions or outages. We do ask that you release any unused IPs back to the public pool. The Jetstream administration team reserves the right to release any IP addresses not associated with a VM as needed.

	#overview:network:security
		:markdown
			### [Security Policies](#overview:network:security)
			#### Atmosphere

			**Public IP:** For instances managed by the Atmosphere user interface, all instances are currently provided access to an optional public IP address.

			**Open ports:** Current Atmosphere security policy only opens ports 22, 80, and 443 or ports &gt; 1024 by default.

			All other ports are closed by default.

			Default open ports may be closed through local instance based security controls, though care should be taken to not disrupt essential Jetstream services.

			#### API

			**Public IP:** All API instances have only private network IP addresses by default. Public IP addresses can be requested.

			**Open ports:** All API instances have no open security by default.

			Control of networking and security for API instances is described in the <a href="#api:commands">API (OpenStack command line)</a> documentation.

	#overview:su
		:markdown
			## [XSEDE Service Units ](#overview:su)

			Jetstream allocations are measured in XSEDE Service Units (SUs). On Jetstream, SUs are consumed at a rate of 1 SU per vCPU_core-hour (use of one virtual core of a CPU per hour).

			SU cost per hour for each Jetstream VM size is outlined in <a href="#vmsizes">Virtual Machine (VM) Sizes and Configurations</a>.

			Currently, <span style="color:red;font-weight:bold">only instances that have an <span style="color:green">Active green</span> status light decrement SUs at the full rate.</span>

			Starting October 1, 2018: 

			<div style="font-weight:bold;margin:0 0 1rem 3rem">
			SUSPENDED instances will be decremented only 0.75 SUs per vCPU_core-hour (75%).<br/>
			STOPPED instances will be decremented only 0.50 SUs per vCPU_core-hour (50%).<br/>
			SHELVED instances will not be decremented SUs. (0%)<br/>
			ACTIVE-DEPLOYING instances will not be decremented SUs. (0%)
			</div>

			JSACTIVE

			**Figure 3:** Jetstream instance with Active status

			<p class="portlet-msg-info">Note: To use the system you must have a valid Jetstream allocation. Allocations are processed through the <a href="https://www.xsede.org" target="_blank">eXtreme Science and Engineering Discovery Environment (XSEDE)</a>. For information on how to apply see <a href="#allocations">Jetstream Allocations</a>.</p>

	#overview:storage
		:markdown
			## [XSEDE Storage Allocation](#overview:storage)

			Jetstream has 2 petabytes of storage total across the entire system. A smaller portion of that total is usable by researchers, educators, and students. 

			Jetstream Storage allocations are measured in Gigabytes (GB) or Terabytes (TB). 

			<span style="color: red">An XSEDE storage allocation is the __SUM__ of all your storage across all of Jetstream's __clouds, storage modes, access methods__, or subsidiary __users__.</span>

			Jetstream has multiple provider clouds, one each at:

			* IU
			* TACC

			Jetstream has two modes of storage:

			* Block storage (implemented as virtual volumes)
			* Object Storage ( experimental &dash; data stored as objects in an object database and accessible via protocols like S3 or Swift)

			Jetstream has multiple control access methods:

			* default web-user-interface: ATMOSPHERE
			* available-upon-request advanced interface(s): API

			<span style="color: red; font-weight: bold">ALL USERS ARE CREATED WITH A DEFAULT 100 GB STORAGE QUOTA, REGARDLESS OF STORAGE ALLOCATION AWARDED.</span>

			* <span style="color: blue; font-weight: bold">Changes to this default are by request and only up to maximums available to the user's aggregate allocation profile.</span>
			* When requesting upgrades from the default 100 GB, PI's of allocations should inform support staff how individual quotas will be parsed over:
				* IU vs. TACC cloud provider
				* BLOCK vs. OBJECT storage at each cloud.
				* ATMOSPHERE vs. API interface at each cloud .
				* per USER
				* <span style="color: blue; font-style: italic; font-weight: bold">EXAMPLE:</span> Dr. Jane Smith has been awarded 10 TB of storage. She requests:
					* 5 TB each on IU and TACC providers => 10 TB aggregate across Jetstream
						* 5 TB of block storage on TACC, but 3 TB of block storage and 2 TB of Object storage on IU
							* ALL 5 TB of TACC block storage under ATMOSPHERE, but only 2 TB of ATMOSPHERE block storage at IU. The remainder at IU under API.
								* 1 TB each to herself, one postdoc, and 3 students at TACC ATMOSPHERE. => 5 TB aggregate at TACC
								* 1 TB each to herself and one postdoc under IU ATMOSPHERE; 500 GB each to herself and one postdoc under API; 2 TB of Object storage =>  5 TB aggregate at IU.
									* NOTE: the 3 students maintain their default 100 GB at IU.

			<div class="portlet-msg-info"><h4>Note</h4>
			<p>To use the system you must have a valid Jetstream allocation. Allocations are processed through the  <a target="_blank" href="https://portal.xsede.org/allocations/announcements">eXtreme Science and Engineering Discovery Environment</a> (XSEDE). For information on how to apply, see <a href="#allocations">Jetstream Allocations</a>.
			</div>

	#overview:fi
		:markdown
			## [Jetstream featured images](#overview:fi)

			[Table 1.1. Featured images](#overview:fi)

	%table(border="1" cellpadding="3")
		%tbody
			%tr
				%th Image name
				%th Description
			%tr
				%td
					:markdown
						[Matlab Minimal](https://use.jetstream-cloud.org/application/images/791)
				%td 
					:markdown
						* **Tags: docker, docker-compose, Featured, gui, m1_small, Mathematics, MATLAB, Ubuntu**
						* Created: 10/25/2018
						* DOES NOT CONTAIN FULL MATLAB Toolsets<br />Products: MATLAB Bioinformatics Toolbox 4.10 Curve Fitting Toolbox 3.5.7 Database Toolbox 8.1 Econometrics Toolbox 5.0 Financial Toolbox 5.11 Global Optimization Toolbox 3.4.4 Image Processing Toolbox 10.2 Neural Network Toolbox 11.1 Optimization Toolbox 8.1 Parallel Computing Toolbox 6.12 Partial Differential Equation Toolbox 3.0 Statistics and Machine Learning Toolbox 11.3 Symbolic Math Toolbox 8.1
						* Based on Ubuntu 18.04 LTS Development + GUI support + Docker
						* **Requires a Small VM size or larger**
			%tr
				%td
					:markdown
						[Ubuntu 18.04 Devel and Docker](https://use.jetstream-cloud.org/application/images/717)
				%td 
					:markdown
						* **Tags: base, desktop, development, docker, docker-compose, Featured, Ubuntu, vnc**
						* Ubuntu 18.04 LTS Development + GUI support + Docker
						* Based on Ubuntu cloud image for 18.04 LTS with basic dev tools, GUI/Xfce added
						* /etc/systemd/network/10-network.link added
						* Installation size ~ 4.4 GB
			%tr
				%td
					:markdown
						[CentOS 7 (7.7) Development GUI](https://use.jetstream-cloud.org/application/images/241)
				%td 
					:markdown
						* **Tags: Featured, CentOS, desktop, development, gui, x2go**
						* [x2go installed](http://wiki.x2go.org/doku.php) -- [DEPRECATED] [Instructions for x2go](#images:x2go)
						* Based on Centos 7 Cloud 1601 Image
						* Development tools added
						* X Window System and Xfce Groups added
						* Added yum-plugin-changelog, irods-icommands-4.1.9. firefox . thunderbird, xfce4-terminal, nm-connection-editor, network-manager-applet, net-tools, xterm, Lmod
			%tr
				%td
					:markdown
						[Ubuntu 16.04 Devel and Docker](https://use.jetstream-cloud.org/application/images/107)
				%td 
					:markdown
						* **Tags: base, desktop, development, docker, docker-compose, Featured, Ubuntu, vnc**
						* Ubuntu 16.04 LTS Development + GUI support + Docker
						* Based on Ubuntu cloud image for 16.04 LTS with basic dev tools, GUI/Xfce added
						* Installation size ~ 4.3 GB
			%tr
				%td
					:markdown
						[Matlab (R2017b - Ubuntu 16.04)](https://use.jetstream-cloud.org/application/images/714)
				%td 
					:markdown
						* **Tags: development, Featured,gui,Mathematics,MATLAB,Simulink,Ubuntu
						* Created on 4/22/2018
						* Based on Ubuntu 16.04 LTS Development + GUI support + Docker
						* **REQUIRES a m1.medium or larger VM to launch**
			%tr
				%td
					:markdown
						[Genomics Toolkit](https://use.jetstream-cloud.org/application/images/701)
				%td 
					:markdown
						* **Tags: Featured, bioinformatics, genomics**
						* Created on 3/8/2018
						* Based on CentOS 7 (7.4) Development GUI
						* [Complete list of tools](https://wiki.jetstream-cloud.org/Genomics+Toolkit+image)
						* **REQUIRES a m1.medium or larger VM to launch**
			%tr
				%td
					:markdown
						[R and ShinyServer with GCC (CentOS 7)](https://use.jetstream-cloud.org/application/images/357)
				%td 
					:markdown
						* **Tags: CentOS, development, Featured, gui, iRODS, ShinyServer**
						* R, R Studio, and Shiny Server with GCC
						* Based on Centos 7 Cloud 1601 Image, Development tools + GUI + iRODs access added
						* Installation size ~ 6.6GB --> While this can run on an m1.tiny, an m1.small is suggested
			%tr
				%td
					:markdown
						[R with Intel compilers (CentOS 7)](https://use.jetstream-cloud.org/application/images/161)
				%td 
					:markdown
						* **Tags: Featured, CentOS, desktop, development, gui, Intel, vnc**
						* Created 9/06/2016
						* R-3.3.1 with Intel compilers built on CentOS 7 (7.2)
			%tr
				%td
					:markdown
						[Intel Development (CentOS 7)](https://use.jetstream-cloud.org/application/images/148)
				%td 
					:markdown
						* **Tags: Featured, CentOS, desktop, development, gui, Intel, vnc**
						* Created: 09/01/2016
						* **Intel compilers and development environment**
						* Based on Centos 7 Cloud 1601 Image, Development tools + GUI + iRODs access added
						* Patched up to date as of 8/23/2016
						* **REQUIRES a m1.small or larger VM to launch**
			%tr
				%td
					:markdown
						[Galaxy 16.01 Standalone](https://use.jetstream-cloud.org/application/images/58)
				%td 
					:markdown
						* **Tags: Featured, community-contributed, Ubuntu**
						* Created: 3/30/2016Galaxy 16.01 Standalone
						* Based on Ubuntu 14.04.4 LTS
						* This is a standalone Galaxy server that comes preconfigured with hundreds of tools and commonly used reference datasets: just launch and use. 
						* **Requires a Large VM size or larger**
			%tr
				%td
					:markdown
						[CentOS 6 (6.10) Development GUI](https://use.jetstream-cloud.org/application/images/61)
				%td 
					:markdown
						* **Tags: Featured, CentOS, desktop, development, gui, VNC**
						* Created: 7/15/2016
						* Based on CentOS 6 (6.7) Development
						* Patched up to date as of 5/31/16 - updated from 6.7 to 6.8 to 6.9 to 6.10
						* Turned off the following from default startup in GNOME:[MAKER 2.31.8 with CCTools 5.4](https://use.jetstream-cloud.org/application/images/119)
						* Bluetooth Power Manager Volume Control PulseAudio Package Manager
			%tr
				%th(colspan="2") Removed from Jetstream Library
			%tr
				%td
					:markdown
						[BioLinux 8](https://use.jetstream-cloud.org/application/images/55)
				%td 
					:markdown
						* **Tags: Featured, bioinformatics, desktop, gui, m1_small, Ubuntu, x2g0**
						* Created 8/10/2016
						* Based on Ubuntu 14.04.3 -Trusty Tahr - server - cloudimg
						* Installed BioLinux 8 (<a href="http://environmentalomics.org/bio-linux/" class="external-link" rel="nofollow">http://environmentalomics.org/bio-linux/</a>)
						* Patched up to date as of 8/10/16
						* Instructions for x2go are here: <a href="#images:x2go">[DEPRECATED] Using x2go with the BioLinux 8 image</a>
						* x2go installed (<a href="http://wiki.x2go.org/doku.php" class="external-link" rel="nofollow">http://wiki.x2go.org/doku.php</a>)
						* **REQUIRES m1.small instance size or larger**
						* <span style="color: red; font-weight: bold; font-style: italic">This image is deprecated and will be retired on 12/31/18</span>
			%tr
				%td
					:markdown
						[MATLAB (Based on CentOS 6)](https://use.jetstream-cloud.org/application/images/123)
				%td 
					:markdown
						* **Tags: Featured, CentOS, desktop, development, gui, m1_medium, VNC**
						* Created: 8/2/2016
						* **Requires a Medium VM size or larger**
						* <span style="color: red; font-weight: bold; font-style: italic">This image is deprecated and will be retired on 12/31/18</span>
			%tr
				%td
					:markdown
						[Ubuntu 14.04.3 Development GUI](https://use.jetstream-cloud.org/application/images/54)
				%td 
					:markdown
						* **Tags: Featured, desktop, development, gui, Ubuntu, VNC**
						* Created: 7/22/2016
						* Based on Ubuntu 14.04.3 Development
						* Patched up to date as of 6/24/16
						* Base Ubuntu 14.04.3 + Xfce + Xfce-goodies, firefox, icon sets and themes, added Emacs and cmake
						* <span style="color: red; font-weight: bold; font-style: italic">This image is deprecated and will be retired on 12/31/18</span>

	#overview:sl
		:markdown
			## [Software licenses](#overview:sl)

			Software licenses are generally <span style="color:red;font-weight:bold">the responsibility of the user</span> to obtain, coordinate, implement, and maintain.

			Some licenses may require users to coordinate with Jetstream staff to allow for proper configuration. Such coordination is handled on a case-by-base basis.

			Licensed software maintained by staff but available to users are listed below. 

			### Compilers and Parallel Libraries

			* The Intel compiler is covered at IU and TACC by a site license and has been configured to point to the proper license server upon boot.
			* All components of the Intel Parallel Studio are covered by the IU and TACC site licenses, including the MKL libraries.</li>

			### Specialty software

			* MATLAB licenses are also available at both providers.
				* Two pre-configured MATLAB-specific images are available within Jetstream:
					* <a target="_blank" href="https://use.jetstream-cloud.org/application/images/714">MATLAB</a> (with all licensed toolsets; minimum flavor size: 6-core m1.medium)
					* <a target="_blank" href="https://use.jetstream-cloud.org/application/images/791">MATLAB Minimal</a> (small subset of toolsets, but minimum flavor size 2-core m1.small)

			If you need licenses for any other software then you will have to provide your own license.

	<p class="portlet-msg-alert" style="font-weight: normal">Please be aware that if you take an image or software from Jetstream and run it somewhere else then <span style="color: red">the license will <strong>not</strong> work</span>. Jetstream license servers restrict access to only valid Jetstream IP addresses.</p>

