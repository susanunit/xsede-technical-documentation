#volumes
	:markdown
		# [Volumes](#volumes)
		
		**_Volumes_:** Small virtual filesystems that may be attached to the user's running/active _Instances_.

		Files/data saved to a _Volume_ are maintained across successive attachment/detachment actions to the user's _Instances_.

		_Volume_ actions with the Atmosphere interface:

		* [Creation](#volumes:create)
		* [Attachment to an active _Instance_](#volumes:attach)
		* [Detachment](#volumes:detach)
		* [Backup](#volumes:backup)

		_Volumes_ can also be controlled directly from the [OpenStack API](https://wiki.jetstream-cloud.org/Creating+and+using+volumes+from+the+command+line).

		<div class="portlet-msg-info">Detaching Volumes<br />REMINDER: Volumes can only be detached if:<br />

		<ol><li>They are not in active use by a process on the instance<br />Try:</li>
		<code>fuser -m /&lt;volume&gt;</code> <em>to LIST all processes using a volume</em><br />
		<code>fuser -km /&lt;volume&gt;</code> <em>to KILL all processes using a volume</em>
		<li>The instance to which they are attached is active</li></ol>
		</div>

	:markdown
		**Cost and Size:**

		While _Volumes_ are available to facilitate research at no additional [Service Unit charge](https://wiki.jetstream-cloud.org/Virtual+Machine+Sizes+and+Configurations), and may be requested during initial or supplemental Jetstream allocation requests, **large capacity storage is beyond the scope of Jetstream**.

		Users who have been approved for _Volumes_ are limited to **10 volumes** with an aggregate capacity of **100 GB** by default.

		**Project and Providers:**

		As with _Instances_, _Volumes_ are associated/organized with User _Projects_ and with particular _Providers_ (e.g. IU or TACC). You **cannot** attach a _Volume_ from one _Provider_ to an instance on a **different** _Provider_.

		**Sharing:**

		_Volumes_ may only be attached to <span style="color: red; font-weight: 600">one</span> active _Instance_ at a time.

		_Volumes_ may be shared using standard methods (e.g. NFS) to other active _Instances_ within Jetstream.

		**Backup &amp; Exporting:**

		Users should regularly [backup](#volumes:backup) (via ssh, rsync, tar, or the like) any critical data contained on _Volumes_ as no automated backup functions are currently provided by Jetstream.

	#volumes:create
		:markdown
			## [Create a Volume](#volumes:create)
		
			A _Volume_ must be created in the Jetstream Atmosphere interface before it can be attached to an active _Instance_.
 
			From the Jetstream Atmosphere Dashboard:

			* Click on "Projects"
			* Select your Project
			* Click "New"
			* Select "Volume"

			JSVOLCREATEDIALOG

			**Figure 1:** Volume creation dialog

			* A window will pop up (Figure 1) in which you can:
				* Name the _Volume_
				* Enter the desired volume size (subject to your allocation limit)
				* Select the _Provider_ on which the _Volume_ will reside
			* Click "Create volume"
			* After creation, the volume will appear in your list of available volumes (Figure 2)

			JSVOLCREATELIST

			**Figure 2:** Available volumes in a project

	#volumes:attach
		:markdown
			## [Attach a Volume](#volumes:attach)

			In order to access a _Volume_, it must be _Attached_ to an active _Instance_.
 
			From the Jetstream Atmosphere Dashboard:

			* Click on "Projects"
			* Select your Project 
			* Select the desired _Volume_ and click "Attach"
				* NOTE: You will only be able to select **Active** _Instances_ running on the same _Provider_ as the _Volume_
			* The _Volume_ will now be automatically mounted in the selected instance, e.g. on device `/dev/sdb` as `/vol1`
			NOTE: The volume name (`vol_b`) will match the device name (`sdb`).

			<pre>[USER@js-169-6 ~]$ df -kh
			Filesystem      Size  Used Avail Use% Mounted on
			/dev/sda1        59G  3.0G   54G   6% /
			tmpfs           7.8G  148K  7.8G   1% /dev/shm
			/dev/sdb        9.9G  151M  9.2G   2% /vol1
			</pre>

			* NOTE: A _Volume_ may only be attached to one active _Instance_ at a time. However, _Volumes_ may be shared using standard methods (e.g. NFS) to other active _Instances_ within Jetstream.

			After attaching, the _Volume_ status in the Atmosphere interface will change to reflect the _Instance_ to which it is attached.

			JSVOLATTACHLIST

			**Figure 3:** Volume status after attaching to an instance

	#volumes:detach
		:markdown
			## [Detach a Volume](#volumes:detach)

			In order to remove a _Volume_ from an active _Instance_, it must be detached. <span style="color: red; font-weight: bold">Volumes can only be detached from <span style="color: green">ACTIVE</span> instances.</span>

			Files and data saved to a _Volume_ are maintained across successive attach and detach actions.

			From the Jetstream Atmosphere Dashboard:

			* Click on "Projects"
			* Select your Project 
			* Select the desired Volume and click "Detach"
				* WARNING: If data are being written to the volume when it is detached, the data may become corrupted. Therefore, we recommend you make sure there are no data being written to the volume before detaching it.
			* Click "Yes, detach the volume"
				* NOTE: A _Volume_ will <span style="color: red">fail to detach</span> if:
					* it is in active use on the active _Instance_ to which it is attached.
					* the Instance to which it is attached is in an inactive state (shelve/shutdown/suspend) (i.e. the _Instance_ must be active in order to detach)
			* After the _Volume_ has successfully detached, it may be subsequently re-attached to any of the user's active _Instances_ on the same provider cloud.

		<div class="portlet-msg-info">Detaching Volumes<br />REMINDER: Volumes can only be detached if:<br />
		<ol><li>They are not in active use by a process on the instance<br /><em>(Try:</em></li>
		<code>fuser -m /&lt;volume&gt;</code> <em>to LIST all  processes using a volume</em><p>You might also try <code>sudo fuser -m /volume</code> if nothing comes up; this will check for root processes holding the volume open</p>
		<code>fuser -km /&lt;volume&gt;</code> <em>to KILL all processes using a volume)</em><p>If you get results with sudo above, you'll need to do <code>sudo fuser -km /<volume></code> to kill the processes</p>
		<li>The instance to which they are attached is active</li></ol>
		<p><code>sudo lsof /vol_dir</code> will also show you processes using the volume.</p>
		</div>

	#volumes:backup
		:markdown
			## [Backup and Export a Volume](#volumes:backup)

			Users should regularly backup any critical data in their _Instance_, especially data contained on _Volumes_, as no automated backup functions are currently provided by Jetstream.

			Listed below are examples of methods to transfer files from _Volumes_, though similar syntax would work from any directory of the _Instance_.

			In the following examples, the user is assumed to be using the command line **from within an _Instance_**, thus <span style="color: blue; font-weight: bold">local-user</span> means a user id on the Instance and <span style="color: green; font-weight: bold">remote-user</span> means a user id on a computer outside of Jetstream.

			### SCP, SSH, and TAR

			**_scp_**: One simple way to transfer the data on your _Volume_ is using scp:
				$ scp /vol1/file remote-user@destination_server_ip:/path/

			**_tar with ssh_**: Use tar with ssh to transfer an entire directory or a file:
				$ tar czf - /vol1/directory-or-file | ssh remote-user@destination_server_ip 'tar xzf -'

			**_tar with ssh_**: As above, but leave the files in a remote archive:
				$ tar czf - /vol1/directory | ssh remote-user@destination_server_ip 'cat - > archive.tar.gz'

			### DD with SSH

			**_dd with ssh_**: Use dd with ssh to copy your entire _Volume_ to a remote image file:

		<pre>$ df -kh
		/dev/sdb        9.9G  151M  9.2G   2% /vol1
		$ dd if=/dev/sdb | ssh remote-user@destination_server_ip 'dd of=vol1.img'
		</pre>

	:markdown
		### RSYNC

		**_rsync_**: Use rsync to transfer an entire directory without encryption:

			$ rsync -avtr /vol1 remote-user@destination_server_ip:/path/

		**_rsync with ssh_**: Use rsync with ssh to transfer an entire directory **with encryption** and between different local and remote users:
			$ rsync -avtr -e 'ssh -l local-user' /vol1 remote-user@destination_server_ip:/path/

	:markdown
		### GLOBUS File Transfer Tools

		Instructions are available for <a target="_blank" rel="noopener" href="https://www.globus.org/data-transfer">installing and using Globus file transfer tools</a>.
