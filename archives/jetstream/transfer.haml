#transfer
	:markdown
		# [Transferring Files](#transfer)

		There are a number of clients and methods for transferring files to and from your VM Instances.

		All involve establishing a secure connection between your instance and another computer, whether inside or outside Jetstream.

		**SSH/SCP/SFTP**

		* <strong>SCP/SFTP:</strong> Instructions and recommended clients for <a href="#transfer:scp">transferring files with SCP or SFTP</a>
		* Examples of <a href="#volumes:backup">how to backup/export files</a> using ssh/scp/sftp from within your instance

		See [how to use SSH/SCP/SFTP](#transfer:scp).

		**Web shell/Web Desktop**

		* The web shell and web desktop provide a mechanism for transferring files. <a href="#transfer:webshell">See instructions below</a>.
		* <em>Please note that this only works on the Guacamole based web shell and web desktop. The versions marked "Old" do not have this functionality.</em>

		See [how to use Web shell/Web Desktop](#transfer:webshell).

		**RSYNC**

		* Examples of <a href="#volumes:backup">how to backup/export files</a> using rsync from within your instance

		**GLOBUS File Transfer tools**

		* <a href="#transfer:globus">Transferring Files with Globus</a>
		* <a href="https://www.globus.org" target="_blank">Globus Online</a> is a fast, reliable, and secure file transfer service for easily moving data to, from, and between digital resources on XSEDE.
		* NCGAS HOWTO: <a href="https://ncgas.org/Blog_Posts/Getting%20Started%20with%20Globus.php" target="_blank">File transfer using Globus on Your Home Computer, Clusters, and Jetstream</a>

		See [how to use GLOBUS File Transfer tools](#transfer:globus).

	#transfer:scp
		:markdown
			## [Transfer with SCP or SFTP](#transfer:scp)

			There are a number of clients and methods for transferring files to and from your VMs from a computer outside Jetstream.

			* [CyberDuck](https://cyberduck.io/) is a well-known client for both Windows and Mac OSX to move files to and from individual VMs.
			Here are the [instructions for scp/sftp](https://trac.cyberduck.io/wiki/help/en/howto/sftp).
				* You'll need to either have an SSH key saved on Jetstream or have the password set on your VM.
					* [Here are instructions on adding keys](#)
					* [Here are instructions on using SSH -- with info on setting the password on your VM at the top](#)

			* [WinSCP](https://winscp.net/eng/download.php) is another well-known Windows specific option.
			* [FileZilla](https://filezilla-project.org) is supporting several operating systems, including Windows.
			These clients, while suggested, are not endorsed by Jetstream in any way. We have limited means to help with support issues related to these clients.

			Here are some [examples of how to backup/export files using ssh/scp/sftp from within your instance](#volumes:backup).

	#transfer:webshell
		:markdown
			## [Transfer with the web shell and web desktop](#transfer:webshell)

			The web shell and web desktop allow file transfers to be easily done with a built in facility. 

			Files can be transferred to the remote computer by dragging and dropping the files into your browser window, or through using the file browser located in the Guacamole menu. The Guacamole menu is a sidebar which is hidden until explicitly shown. On a desktop or other device which has a hardware keyboard, you can show this menu by pressing Ctrl+Alt+Shift. If you are using a mobile or touchscreen device that lacks a keyboard, you can also show the menu by swiping right from the left edge of the screen. To hide the menu, you press Ctrl+Alt+Shift again or swipe left across the screen.

			<figure style="margin-left: 12%">
			<img alt="" src="/documents/10308/1181156/transfer-files-1.jpg/f5c98aaa-46e3-4801-af87-8a1de8df197a?t=1517983463845" style="width: 444px; height: 250px;" />
			<figcaption style="font-weight: 400; width: 444px; text-align: center"><span style="font-weight: 600">Figure 1: </span>Accessing the Guacamole menu</figcaption>
			</figure>

			You can select where to transfer files by clicking the "Devices" option in the Guacamole menu. That will give you a file manager type menu where you can select the directory to transfer files into.

			<p class="portlet-msg-alert"><strong>Please note that you will only be able to read or write files in directories that your user account would have access to. This means unless you've created directories where you can write, by default you'll only be able to write into your home directory, /tmp, and mounted volumes (/vol_X), etc.</strong></p>

			<figure style="margin-left: 30%">
			<img alt="" src="/documents/10308/1181156/transfer-files-2.jpg/30e496dd-43a3-4130-ba72-9b23222369c3?t=1517984084173" style="letter-spacing: 0.4px;" />
			<figcaption style="font-weight: 400; italic; width: 208px; text-align: center"><span style="font-weight: 600">Figure 2: </span>File manager type menu</figcaption>
			</figure>

			Double-clicking on any directory will change the current location of the file browser to that directory, updating the list of files shown as well as the "breadcrumbs" at the top of the file browser. Clicking on any of the directory names listed in the breadcrumbs will bring you back to that directory, and clicking on the drive icon on the far left will bring you all the way back to the root level.

			Downloads are initiated by double-clicking on any file shown, while uploads are initiated by clicking the "Upload Files" button. Clicking "Upload Files" will open a file browsing dialog where you can choose one or more files from your local computer, ultimately uploading the selected files to the directory currently displayed within the file browser. You can also drag and drop files into the dialog box from your computer's file windows.

			The state of all file uploads can be observed within the notification dialog that appears once an upload begins, and can be cleared once completed by clicking the "Clear" button. Downloads are tracked through your browser's own download notification system.

			<figure style="margin-left: 20%">
			<img alt="" src="/documents/10308/1181156/transfer-files-3.jpg/00f36eab-316c-49b2-b2e8-189f580e0eb7?t=1517984098853" style="width: 380px; height: 99px;" />
			<figcaption style="font-weight: 400; width: 380px; text-align: center"><span style="font-weight: 600">Figure 3: </span>In-progress and completed file transfers</figcaption>
			</figure>

			When you are done browsing the filesystem and transferring files, click "Back" to return to the Guacamole menu. You can close the menu by pressing Ctrl+Alt+Shift.

	#transfer:globus
		:markdown
			## [Transferring files with Globus](#transfer:globus)

			**Prerequisites**

			* A [Globus](http://app.globus.org) account
			* [Globus Connect Personal](https://www.globus.org/globus-connect-personal) installed on your computer

			**On your Virtual Machine**

			1. Log into VM via either ssh, Web Shell or terminal Web Desktop

			1. Download and install Globus connect on the VM

				<pre><code>wget https://s3.amazonaws.com/connect.globusonline.org/linux/stable/globusconnectpersonal-latest.tgz
				tar xzvf globusconnectpersonal-latest.tgz</code></pre>

			1. Go to the Globus Connect directory on your VM

				`[jetstream-vm ~]$ cd globusconnectpersonal-3.1.4/`

				1. `cd ~/globusconnectpersonal-{your-version}`

				1. Run `.globusconnect`

					<pre><code>[jetstream-vm globusconnectpersonal-3.1.4]$ ./globusconnect
					Detected that setup has not run yet, and '-setup' was not used
					Will now attempt to run</code></pre>
					1. Go to the URL
					1. Copy the auth code and paste it in the VM
					1. Give the new endpoint a unique name

			1. Activate the endpoint on your VM

				1. `./globusconnect -start &`

			**To Transfer Files**

			1. To transfer files between endpoints, use the <a target="_blank" href="https://app.globus.org/file-manager">Globus File Manager</a>.

				1. Click in the Endpoint dialogue box to select endpoints (look under tab "Administered by You").

			1. Here you can select from the endpoints you have set up.

				<img alt="" src="/documents/10308/1181156/globus_file_transfer2.jpg/3bb16426-e001-4569-8611-d8ca05f6b5a2?t=1621890400362" style="width: 600px; height: 466px;" />

			**To Stop Globus**

			1. To stop globusconnect on your VM:

				`./globusconnect -stop`

				<img alt="" src="/documents/10308/1181156/Globus_connect_stop.png/8d740c48-85e1-4051-a6e7-8fe2de86f6b9?t=1542415635000" style="width: 600px; height: 118px;">
