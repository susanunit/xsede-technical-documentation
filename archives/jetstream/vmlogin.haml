#vmlogin
	:markdown
		# [Logging in to your VM](#vmlogin)
		## [Accessing Jetstream](#vmlogin)

	<p class="portlet-msg-info">Jetstream is accessible through a <a href="https://use.jetstream-cloud.org/application">web interface</a> using <a href="http://www.xsede.org">XSEDE</a> credentials via <a href="https://www.globus.org/tags/globus-auth">Globus Auth</a>.<br /><br />Jetstream is <span style="color:red">not</span> directly accessible via the <a href="https://portal.xsede.org/single-sign-on-hub">XSEDE Single Sign-On Login Hub</a>.<br /><br />It is possible to <a href="#vmlogin:sso">use the XSEDE SSO Hub</a> as a jump-host to individual VMs but <em>this does not follow normal use.</em><br /><br />Users are required to have <a href="http://www.xsede.org">XSEDE</a> accounts. Newly created XSEDE accounts must be added to a specific allocation by the PI or Resource manager in order to access Jetstream.<br /><br /><span style="color:red; font-sytle: italic">Please note that your XSEDE Portal password is not used for logging into VMs. If you wish to use password authentication to ssh into a VM (not recommended!) you will need to set up a password (refer to <a href="#vmlogin:ssh">Logging in with SSH</a> for information).</span></p>

	:markdown
		Jetstream allows multiple methods for accessing and using your VM.

		* Command line interface
			* **Open Web Shell**: The [Open Web Shell](#vmlogin:webshell) and Open Web Desktop available on [Jetstream Cloud](https://use.jetstream-cloud.org) are the preferred methods.
			* **SSH**: You may also log in to your instance directly via an [SSH](#vmlogin:ssh) command line.
		* Graphical interface
			* **Web Desktop** - Images with VNC/GUI/Desktop enabled will display a <span style="font-weight: bold; color: rgb(153,51,0)">Web Desktop</span> icon under the <strong>LINKS</strong> items in the <em>instance</em> detailed view (found by clicking on the instance title). <span style="color:red">You may have to use your browser's refresh button to make it appear.</span><br />Simply click on <span style="font-weight: bold; color: rgb(153,51,0)">Web Desktop</span> to start a browser-based VNC implementation.
			* **VNC client**: graphical encrypted [VNC](#vmlogin:vnc) desktop from your host computer.

		Some images support additional mechanisms for accessing your VM. Some of the additional access methods are noted in [image-specific documentation](#images).

	#vmlogin:web
		:markdown
			## [Logging in with Web Shell](#vmlogin:web)

			<ol>
				<li>Log in to Jetstream via the <a href="https://use.jetstream-cloud.org/application">web interface</a> and launch the instance.</li>
				<li>When the status shows as Active, click the <span style="font-weight: bold; color: rgb(153,51,0);">Open Web Shell </span> link found on the lower right hand side of the screen. If this link is unavailable, try refreshing your window. If the link is still not enabled, log in to your instance via <a href="#vmlogin:ssh">SSH</a>.
					<figure>
					<img width="600px" src="/documents/10308/1181156/webshell-login-1.png/12782579-eed5-4e54-82c0-3b6117065b06?t=1517609831000" />
					<figcaption style="font-weight: 400; width: 600px; text-align: center"><span style="font-weight: 600">Figure 1: </span>Active instance with Web Shell link in lower right corner</figcaption>
					</figure>
					<p class="portlet-msg-info">NOTE:<br />Currently, Web Shell allows <strong>ONLY ONE</strong> Web Shell window per browser login to use.jetstream-cloud.org. Each new Web Shell will disconnect previous windows/tabs. If you need more than one Web Shell, you will need to use a tab or window in <strong>INCOGNITO</strong> mode or open a second browser. You may also wish to use direct SSH connections.</p>
				</li>
				<li>Under normal circumstances <span style="font-weight: bold; color: rgb(153,51,0);">Web Shell (Beta)</span> will automatically use your <em><strong>USERID</strong></em> credentials or any <a href="#sshkeys">SSH public keys</a> you have uploaded to Jetstream and you will not have to enter your XSEDE username and password.
					<figure>
					<img width="600px" src="/documents/10308/1181156/webshell-login-2.png/1a09da54-8e12-4f89-80d8-15ebbacd2114?t=1517609832000" />
					<figcaption style="font-weight: 400; width: 600px; text-align: center"><span style="font-weight: 600">Figure 2: </span>Successful login resulting in a common shell interface</figcaption>
					</figure>
				</li>
				<li>Control + D logs you out of the Web Shell and the window can be closed.
					<figure>
					<img src="/documents/10308/1181156/webshell-login-3.png/be84ee2e-6f5f-4c5e-bc5c-37c7bff701a7?t=1517609833000" />
					<figcaption style="font-weight: 400; width: 600px; text-align: center"><span style="font-weight: 600">Figure 3: </span>After logout via Ctrl-D</figcaption>
					</figure>
				</li>
			</ol>

	#vmlogin:web:copy-paste
		:markdown
			### [Copy and Paste in Web Shell](#vmlogin:web:copy-paste)

			**To Copy Text to Web Shell**

			<ol>
				<li>Shift+Control + Alt (Shift+Control+Command on MacOS) brings up the clipboard pane</li>
					<figure>
					<img width="600px" src="/documents/10308/1181156/webshell-copypaste-1.jpg/75d1ab2c-721e-4a04-858a-245efdf8ccf9?t=1518205151035" />
					<figcaption style="font-weight: 400; width: 600px; text-align: center"><span style="font-weight: 600">Figure 4: </span>Web Shell with Clipboard Open</figcaption>
					</figure>
				</li>
				<li>Paste text in the clipboard
					<figure>
					<img width="600px" src="/documents/10308/1181156/webshell-copypaste-2.jpg/251c62f3-410c-446e-b753-92a371b401dd?t=1518205187471" />
					<figcaption style="font-weight: 400; width: 600px; text-align: center"><span style="font-weight: 600">Figure 5: </span>Clipboard with Text Copied In</figcaption>
					</figure>
				</li>
				<li>Shift + Control + Alt to exit clipboard</li>
				<li>Paste text in Web Shell with Shift + Control + V
					<figure>
					<img width="600px" src="/documents/10308/1181156/webshell-copypaste-3.jpg/d1520b92-eafd-49e3-b2f3-c790b05cae86?t=1518205215031" />
					<figcaption style="font-weight: 400; width: 600px; text-align: center"><span style="font-weight: 600">Figure 6: </span>Text Copied Outside of Web Shell Clipboard</figcaption>
					</figure>
				</li>
			</ol>

			**To Copy Text from Web Shell**

			<ol>
				<li>Select text in Web Shell. Selected text is automatically copied to clipboard</li>
				<li>Shift + Control + Alt to bring up the clipboard pane</li>
				<li>Select text in the clipboard with Control + C</li>
				<li>Shift + Control + Alt to exit clipboard</li>
				<li>Paste text outside Web Shell with Control + V</li>
			</ol>

	#vmlogin:ssh
		:markdown
			## [Logging in with SSH](#vmlogin:ssh)

			Regardless of whether you're logging from a Linux, Mac or Windows machine:

			1. [add SSH keys](#sshkeys) to your account and
			1. copy the instance IP address, either from the confirmation email or from the IP address displayed in the My Instances list.
			1. Or if you want to use password authentication (not recommended but sometimes necessary) use the web shell and log in and type <span style="color:red; font-weight: bold">sudo passwd <em>your_username</em></span>.

	#vmlogin:ssh-mac
		:markdown
			### [MacOS & Linux/Unix](#vmlogin:ssh-mac)

			<ol>
				<li>Open a terminal window for Mac OS X (from Finder, go to Applications, click Utilities, and then double-click Terminal).<br />For Linux, there are many terminal options, including <strong>xterm</strong>, <strong>konsole</strong>, or <strong>gnome-terminal</strong>.</li>
				<li>In the terminal window, enter the following command, using your XSEDE username and password, and the instance IP address:<br />

			<pre>$ ssh <em>xsede_username</em>@instance_ip_address</pre>
				</li>

				<li>Press Enter.</li>
			</ol>

			A successful login will look similar to the following:

			JSSSHLOGIN

			<span style="text-align: center">**Figure 7:** Screenshot of a successful MacOS X login shell</span>

	#vmlogin:ssh-windows
		:markdown
			### [Windows using PuTTY](#vmlogin:ssh-windows)

			PuTTY is an SSH client for Windows. It operates a bit differently than Terminal to make the initial SSH connection. For a useful guide to using PuTTY, see [PuTTY - Remote Terminal and SSH Connectivity](https://www.lifewire.com/learn-how-linux-4102755).

			1. [Download the PuTTY application.](http://www.putty.org/)
			1. Launch PuTTY.
			1. The first time PuTTY is used for login, add your private key.
				1. Single click the 'Default Settings' session to save your private key for all future sessions.
				1. Click on the + symbol next to the 'SSH' category on the left side.
				1. Click on the 'Auth' category to bring up the PuTTY Configuration screen (see screenshot below).
				1. The key is set down at the bottom under 'Private key file for authentication'. Click on the Browse button next to the 'Private key file for authentication' field and locate your private key file on the file system. Select the file and press 'Ok'. (It is probably in your My Documents folder.)
				1. Click the 'Session' category from the left hand side.
				1. Make sure "Default Settings" is still selected.
				1. Click Save.
			1. Enter the IP address, either copied from your My Instances list or from the confirmation email, and click Connect.
			1. Enter your XSEDE username when prompted and click Enter.

	<div class="portlet-msg-info" style="width: 80%; margin-right: 20%"><span style="font-size: larger; font-weight: bold">From the VM command line, find the public IP assigned to your instance</span><br />If your VM has a public IP address and you need to find that IP (and don't have ready access to the Jetstream interface), you can use wget or curl from the command line to get your public IP:<br /><code>wget http://169.254.169.254/latest/meta-data/public-ipv4 -qO -<br />wget http://ipinfo.io/ip -qO -<br />curl http://169.254.169.254/latest/meta-data/public-ipv4<br />curl http://ipinfo.io/ip</code><br />*Note: http://169.254.169.254/latest/meta-data/public-ipv4 works even in conditions in which external DNS servers are not accessible.</div>
	<p class="portlet-msg-info" style="width: 80%; margin-right: 20%"><span style="font-size: larger; font-weight: bold">To become root</span><br />Enter <code>sudo su -</code> at the command prompt or type <code>sudo command</code> and replace <em>command</em> with the command for which you want to use <em>sudo</em>.</p>

	#vmlogin:vnc
		:markdown
			## [Logging in with VNC desktop](#vmlogin:vnc)

			_VNC is only available on certain images. Please look for the **GUI** or **Desktop** tags on the Featured images._

			### Web Desktop

			Images with VNC/GUI/Desktop enabled will display  <span style="font-weight: bold; color: rgb(153,51,0)">Web Desktop</span> under the **LINKS** items in the _instance_ detailed view (found by clicking on the instance title). You may have to refresh your browser to make it appear.

			Simply click on  <span style="font-weight: bold; color: rgb(153,51,0)">Web Desktop</span> to start a browser-based VNC implementation.

			### External VNC Desktop

			The default and recommended way to graphically connect to the desktop is with our [Web Desktop](#vmlogin:web-desktop) feature.

			If you would like to use a different VNC client (which <span style="color:red">must support encrypted connections</span>), for example [realvnc viewer](https://www.realvnc.com/download/viewer/) (free), you need to take extra steps to secure the connection.

			Please follow these instructions:

	<div class="portlet-msg-info">
	:markdown
		#### For VNC launches

		1. Set up SSH_keys for your local laptop/desktop: [Adding SSH keys to the Jetstream Atmosphere environment](#ssh-keys).
		1. <strong>Start/Reboot/Redeploy</strong> your instance to activate the SSH_keys.
		1. Once your instance comes up, login via [SSH](#vmlogin:ssh) with your favorite terminal app (e.g. Putty, Mac Terminal) from the local machine that matches the SSH_keys. Instructions for finding your instance's IP_number are in the SSH instructions. Type: <span style="color: red">ssh &lt;username&gt;@&lt;instance_IP&gt;
		1. Set a password for your username while logged in to the instance. Type: <span style="color: red">sudo passwd &lt;username&gt;
		1. Start an SSH-tunnel from your local machine to the VNC desktop. Either exit from your ssh or otherwise start a new terminal session and then Type: <span style="color: red">ssh -L &lt;localhost port, try 5901&gt;:&lt;instance_IP_number&gt;:&lt;instance port, try 5901&gt; -N &lt;username&gt;@&lt;instance_IP_number&gt;</span> e.g. *ssh -L 5901:129.114.17.256:5901 -N jsmith@129.114.17.256*
		1. Use realvnc viewer (free) to connect to x.x.x.x:1 (your_ip:1) with your username and the password you just set.
		1. When finished with the VNC, please disconnect your VNC session via the Real VNC viewer or by closing the VNC window on your host computer.
		1. Return to the terminal app running the SSH-tunnel and close it (ctrl-c or close the window).

		DO NOT LOG OUT INSIDE THE DESKTOP OF THE VM: this will make your VNC unusable until you [manually restart the VNC](#vmlogin:manage).
	</div>
	:markdown
		_**Note:**_ The default VNC session will only work for the user that launched the instance. Other user accounts on that instance that wish to have an individual desktop will have to [start their own VNC manually](#vmlogin:manage). Additionally, the port number will generally increase by 1 for every active desktop: 1st desktop = 5901, 2nd desktop = 5902. Please use the appropriate port number in the commands above.

		_**Note:**_ If you are on a Mac OS X based machine and you get a warning about AppleScreenSharing, then __cancel__ the connect, close the tunnel, and restart it with ONLY the localhost-port increased to 5902 or higher:

		e.g. _Type_: `ssh -L 5902:129.114.17.256:5901 -N jsmith@129.114.17.256`, then then try your VNC_Viewer Client again but specify __localhost:5902__ (or whatever port you chose).

		Seeing the AppleScreenSharing message means if you proceed, your computer may to try to connect recursively to your local screen over and over which will result in a loop which may hang your screen.

	<p class="portlet-msg-info"><span style="font-size: larger; font-weight: bold">Note on resizing</span><br />From the command line inside your instance use <code>xrandr -s 1920x1080</code> to get a larger screen; adjust the numbers to get the screen size that you need.<br /> <code>xrandr</code> works reliably in many Linux operation systems.<br />Valid screen resolutions are: 800x600, 1024x700, 1024x768, 1200x740, 1280x800, 1280x960, 1280x1024, 1600x1000, 1600x1200, 1680x1050, 1920x1080, 1920x1200, 3360x1050, 3200x1000, 3200x1200</p>

	<p class="portlet-msg-info"><span style="font-size: larger; font-weight: bold">To become root</span><br />Enter <code>sudo su -</code> at the command prompt or type <code>sudo command</code> and replace <em>command</em> with the command for which you want to use <em>sudo</em>.</p>

	#vmlogin:manage
		:markdown
			### [Managing a VNC server](#vmlogin:manage)
			#### Starting a VNC server manually:

			By default for images with VNC capability, a VNC is only launched for the User that launched the Instance.

			If you have added additional users accounts to your _Instance_, and those users have set their password, they must manually start their own VNC desktop using the `'vncserver'` command:

				[USER@js-19-210 ~]$ vncserver :1

			where `:1` is a display number (1-9).

			**Note:** If a VNC server is already running on that particular display number, you will receive an error:

				Error: A VNC or X Server is already running as :1 [DisplayInUse]

			If this arises, choose a different display number.

			**Note:** If a _User_ has logged out within the VNC desktop, this manuallly stops the VNC server.

			By default, the primary owner of the instance has a guacamole-based vncserver running on display :5, as well as servers on displays :1 and :2. To restart the default guacamole-based server, use the command:

				[USER@js-19-210 ~]$ vncserver -config ~/.vnc/config.guac :5

	#vmlogin:stop
		:markdown
			#### [Stopping a VNC server](#vmlogin:stop)

			A VNC server may be stopped using the command:

				[USER@js-19-210 ~]$ vncserver -kill :1

			where `:1` is the display number of a VNC server owned by that _User_.

			Logging out of the desktop in the VNC also stops the VNC server.

			**Note:** Killing an active VNC server before data/files have been save or icon settings stored _may result in a loss of data/settings._

	#vmlogin:restart
		:markdown
			#### [Restarting a VNC server](#vmlogin:restart)

			If a User has stopped their VNC server either by logging out of the desktop within the VNC or with the `-kill` option to the `vncserver` command, they may simply restart the server after SSH'ing in, [as above](#vmlogin). 

	#vmlogin:vnc
		:markdown
			## [XSEDE SSO As Jump Host](#vmlogin:jumphost)

			**PLEASE NOTE: This is <span style="color: red">NOT the normal mechanism</span> to use the XSEDE SSO Hub as described in the [XSEDE SSO documentation](https://portal.xsede.org/single-sign-on-hub):**

			To make use of XSEDE's Single Sign-On Login Hub to simplify logging in to Jetstream instances:

			**FIRST TIME ONLY**

				ssh -l <XSEDE_username> login.xsede.org

			Enter XSEDE password and perform multi-factor authentication

				[<XSEDE_username>@ssohub ~]$ ssh-keygen

			Accept defaults and hit return for your password
 
				[<XSEDE_username>@ssohub ~]$ cat .ssh/id_rsa.pub

			Copy the entire entry all the way from "ssh-rsa" through "xsede.org"
 
				[<XSEDE_username>@ssohub ~]$ exit

			Browse to <https://use.jetstream-cloud.org> using your XSEDE credentials.

			Follow the ssh-key instructions and add the xsede.org key from above into your settings.

			**NORMAL USE**

			From now on, you can use the following to connect to your Jetstream instances:

				ssh -l <XSEDE_username> -J login.xsede.org <individual Jetstream instance IP#>

	#vmlogin:web-desktop
		:markdown
			## [Logging in with Web Desktop - also copying and pasting](#vmlogin:web-desktop)
	#vmlogin:web-desktop-login
		:markdown
			### [Logging in with Web Desktop](#vmlogin:web-desktop-login)
			<ol>
				<li>Log into Jetstream via the web interface, <a target="_blank" href="http://use.jetstream-cloud.org">use.jetstream-cloud.org</a>, and launch the instance.</li>
				<li>When the status shows as Active, click the 'Open Web Desktop' link found on the lower right hand side of the screen. If this link is unavailable try refreshing your window. If the link is still not enabled, verify that the status is green Active and not any of the yellow variants (e.g. Active Networking)
					<p class="portlet-msg-info">NOTE:<br />Currently, Web Desktop allows ONLY ONE Web Desktop window per browser login to <a target="_blank" href="http://use.jetstream-cloud.org">use.jetstream-cloud.org</a>. Each new Web Shell will disconnect previous windows/tabs. If you need more than one Web Shell, you will need to use a tab or window in INCOGNITO mode or open a second browser.</p>
					<figure>
					<img width="600px" src="/documents/10308/1181156/web-desktop-1.jpg/e6a30623-4717-46f6-9b1e-a3521ea90091?t=1591997039000" />
					<figcaption style="font-weight: 400; width: 600px; text-align: center"><span style="font-weight: 600">Figure 9: </span>Active status showing Open Web Desktop link</figcaption>
					</figure>
				</li>
				<li><p>Under normal circumstances Web Desktop will automatically use your credentials and you will not have to enter your XSEDE username and password.</p>
					<figure>
					<img width="600px" src="/documents/10308/1181156/web-desktop-2.jpg/c43f2e19-6048-475d-8576-e1a10acbb7c2?t=1591997049000" />
					<figcaption style="font-weight: 400; width: 600px; text-align: center"><span style="font-weight: 600">Figure 10: </span>Automatic login with XSEDE credentials</figcaption>
					</figure>
				</li>
				<li><p>Closing the tab is one way to close the session, but the cleaner, best practice method is press Shift + Control + Alt or Shift + Control + Opt (MacOS) to bring up the side panel. Select the right side tab with the small user icon and long ID string. Then select "Disconnect" to close the session. Once you are back to the Jetstream logo, the window can be closed.</p>
					<figure>
					<img width="600px" src="/documents/10308/1181156/web-desktop-3.jpg/68d89c3c-3838-43af-9872-55568cd0f0d2?t=1591997075000" />
					<figcaption style="font-weight: 400; width: 600px; text-align: center"><span style="font-weight: 600">Figure 11: </span>Best practice for closing a session</figcaption>
					</figure>
				</li>
			</ol>

	#vmlogin:copy-paste
		:markdown
			### [Copy and Paste in Web Shell](#vmlogin:copy-paste)

			<p><strong>To Paste text into Web Shell</strong></p>
			<ol>
				<li>Select and copy text from other source using normal method for your system. (Control + C or Command + C)</li>
				<li>Paste text in Web Shell with Shift + Control + V or right-click</li>
			</ol>
			<p>** Older versions of Firefox may not directly support clipboard access. Some versions can enable the beta feature by visiting the URL <code>about:config</code> and searching for <code>dom.events.testing.asyncClipboard</code>. Setting the value of this feature to true may enable clipboard use. Alternatively, use the Clipboard pane.</p>
			<figure>
			<img width="600px" src="/documents/10308/1181156/web-desktop-4.jpg/2abaea37-c7d3-49b9-a216-8ac1a0ee09db?t=1591997097000" />
			<figcaption style="font-weight: 400; width: 600px; text-align: center"><span style="font-weight: 600">Figure 12: </span>Pasting text in Insert mode</figcaption>
			</figure>
			<p><strong>To Paste text into Web Shell using Clipboard Pane</strong></p>
			<ol>
				<li>Select and copy text from other source</li>
				<li>Shift + Control + Alt or Shift + Control + Opt (MacOS) to bring up the clipboard pane</li>
				<li>Paste text in the clipboard</li>
				<li>Shift + Control + Alt to exit clipboard</li>
				<li>Paste text in Web Shell with Shift + Control + V or right-click and select "Paste"</li>
			</ol>
			<figure>
			<img width="600px" src="/documents/10308/1181156/web-desktop-5.jpg/d1fb40b1-0631-4824-8357-80ccbfceaab0?t=1591997102000" />
			<figcaption style="font-weight: 400; width: 600px; text-align: center; margin-bottom: 1rem"><span style="font-weight: 600">Figure 13: </span>The clipboard pane in Web Shell</figcaption>
			</figure>
			<figure>
			<img width="600px" src="/documents/10308/1181156/web-desktop-6.jpg/47fe2ae2-e944-44f9-b89c-a712e4577fc0?t=1591997108000" />
			<figcaption style="font-weight: 400; width: 600px; text-align: center; margin-bottom: 1rem"><span style="font-weight: 600">Figure 14: </span>Right-click menu in Web Shell</figcaption>
			</figure>
			<figure>
			<img width="600px" src="/documents/10308/1181156/web-desktop-7.jpg/696a5dd7-bf44-4e68-8624-ef83fe0c85b8?t=1591997113000" />
			<figcaption style="font-weight: 400; width: 600px; text-align: center"><span style="font-weight: 600">Figure 15: </span>Result of Paste in Web Shell</figcaption>
			</figure>



			<p><strong>To Copy text from Web Desktop</strong></p>
			<ol>
				<li>Select text in Web Desktop. Selected text is automatically copied to the copy buffer</li>
				<li>Paste text into other location using normal method for your system. ( Control + V or Command + V)</li>
			</ol>
			<p>** BE AWARE: Copy activity happening AFTER you first select text in the Web Shell will replace the selected text. To copy again, you will have to re-select.</p>
			<p>** Older versions of Firefox may not directly support clipboard access. Some versions can enable the beta feature by visiting the URL <code>about:config</code> and searching for <code>dom.events.testing.asyncClipboard</code>. Setting the value of this feature to true may enable clipboard use. Alternatively, use the Clipboard pane.</p>


			<p><strong>To Copy text from Web Desktop using Clipboard pane</strong></p>
			<ol>
				<li>Select text in Web Desktop. Selected text is automatically copied to clipboard</li>
				<li>Shift + Control + Alt(Opt) to bring up the clipboard pane</li>
				<li>Select text in the clipboard using normal method for your system (e.g. Control + C or Command + C)</li>
				<li>Shift + Control + Alt(Opt) to exit clipboard</li>
				<li>Paste text into other location using normal method for your system. ( Control + V or Command + V)</li>
			</ol>
			<h3>File Upload/Download using Web Desktop</h3>
			<p><strong>UPLOAD</strong></p>
			<p>To upload a file, you can simply drag-and-drop it into the browser window. It will automatically be uploaded to your home directory.</p>
			<p>Alternatively, pull up the side-menu/clipboard-pane [ Shift + Control + Alt(Opt) ] and double-click on the drive under "Devices". You can then navigate the filesystem by double-clicking through directories and clicking the "Upload Files" button.</p>
			<p style="font-style: italic">Note: File upload can be pretty slow so it is recommended that you use a different method for files larger than 100 MB.</p>
			<figure>
			<img width="600px" src="/documents/10308/1181156/web-desktop-8.jpg/8d9c66f8-0747-4e58-98e1-1791e4cb7610?t=1591997119000" />
			<figcaption style="font-weight: 400; width: 600px; text-align: center"><span style="font-weight: 600">Figure 16: </span>The Upload Files interface in Web Desktop</figcaption>
			</figure>
			<p><strong>DOWNLOAD</strong></p>
			<p>To download files, pull up the side-menu/clipboard-pane [Shift + Control + Alt (Opt)] and double-click on the drive under "Devices". Navigate the filesystem by double-clicking through directories and then double-click on a file that you want to download.</p>
			<p style="font-style: italic">Note: File download can be pretty slow so it is recommended that you use a different method for files larger than 100 MB.</p>
