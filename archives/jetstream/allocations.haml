#allocations
	:markdown
		# [Allocations](#allocations)

		Jetstream allocations are awarded exclusively through the [eXtreme Science and Engineering Discovery Environment (XSEDE)](https://www.xsede.org). XSEDE provides [XSEDE User Portal (XUP) accounts](https://portal.xsede.org) free of charge. XSEDE allocations require that the PI be a US-based researcher. 

		You can read the [XSEDE Allocations Guide](https://portal.xsede.org/allocations/announcements) for more detail on Jetstream allocations. [Examples of successful requests](https://portal.xsede.org/allocations/research#examples) are available there as well.

	#allocations:xsede
		:markdown
			## [Create a portal account](#allocations:xsede)

		<div class="norm">
		:markdown
			1. Browse to the [XSEDE User Portal](https://portal.xsede.org)
			1. Click **Create Account** on the left side of your screen. 
			1. Fill out the form and click Submit. 
			1. Upon receipt of the email notification click the link in the email to verify your account and set your username and password. If the link doesn't work, go to the [XSEDE User Portal](https://portal.xsede.org), click **Sign In** and then select **Verify Account** under the **Other Sign In Options**.
			1. Following account verification, if not already logged in, go to the [XSEDE User Portal](https://portal.xsede.org), click **Sign In** and sign in with the username and password set in the verification step.
			1. You will be asked to read and accept the User Responsibilities form. This outlines acceptable use to protect shared resources and intellectual property.
		</div>

	#allocations:apply
		:markdown
			## [Apply for an allocation](#allocations:apply)

		<style>
		li > ol {
		list-style-type: lower-latin;
		}
		li li > ol {
		list-style-type: lower-roman;
		}
		</style>
		<div class="norm">
		:markdown
			1. Read the [XSEDE Allocations Overview](https://portal.xsede.org/allocations-overview). There are sample allocation requests in the overview that you may find helpful.
			1. Go to XRAS, the [XSEDE Resource Allocation System](https://portal.xsede.org/submit-request). On the Available Opportunities page, click **Start a New Submission** under **Startup**. If you are not familiar with the process, select **Begin Guided Submission** for step-by-step instructions.
			1. Before submitting an allocation request have the following information available:
				1. PI, Co-PIs, and Allocation Managers (username)
				1. Add additional users that will be able to use your allocation time and resources (optional)
				1. Title
				1. Abstract (typically a paragraph or two for a Startup request will suffice)
				1. Keywords
				1. Field of science (secondary areas of science may be also be added)
				1. Resources 
					1. Select **Jetstream IU/TACC** from the list.
					1. A startup allocation is typically 50,000 SUs for Jetstream.
					1. Fill in number of Virtual Machines needed
					1. Fill in number of public IP addresses needed
					1. Select **Jetstream Storage**. For Startup allocations, the block storage provided will be limited. Read [Virtual Machine Sizes and Configurations](#vmsizes) for a list of the various VM sizes and the RAM and associated storage. If additional storage for your VM is not needed, enter 1 in the Amount Requested box. _Note: The maximum allowed is 500GB. A justification will be needed in the comments for any significant storage request in the Comments box._
				1. Supporting documents - PDF format required
					1. PI CV (2 page limit)
					1. CoPI CV required for every CoPI added to request (2 page limit)
				1. Supporting Grants (Optional)
				1. Publications of previous/supporting work (optional)
			1. Submit allocation request. At this point, all entered information is validated, errors or omissions are flagged.

			**Allow 1-2 business days for your application to go through the approval process.**

			_You can view detailed information, with screenshots, about the [allocation request process](https://portal.xsede.org/allocation-request-steps)._
		</div>

		<p class="portlet-msg-info">XSEDE also offers <a href="https://portal.xsede.org/group/xup/jetstream-trial-access">Trial Access Allocations</a> to Jetstream. Trial Access Allocations are designed to give potential users faster but limited access to Jetstream in order to try the features of cloud-computing.</p>

	#allocations:add-res
		:markdown
			## [Request additional resources](#allocations:add-res)

			**I have a current XSEDE allocation but need additional Service Units (SUs):**

			* The PI, co-PI, or delegate may submit a request via the [XSEDE User Portal](https://portal.xsede.org/submit-request).
			* For more information on Supplemental requests, please see [Requesting additional SUs or storage for Jetstream -- Supplemental Allocations](#allocations:supplemental)

			**My limits on the number of cores, number of instances, or memory are too low:**

			* Additional resources, such as CPUs and memory, may be requested via Atmosphere.
			* After logging in, click on **Change Your Settings**:

		<img alt="Screenshot: Change Settings" src="/documents/10308/1181156/change-settings.png/394b2011-1ed0-4f3f-9484-69a9cfe39aaa?t=1479154730206" style="width: 600px; height: 214px;">

		:markdown

			**Figure 1:** The Jetstream Dashboard, with access to the Change Your Settings button

			* Click on **request more resources** in the Allocation section:

		<img alt="Screenshot: Request More" src="/documents/10308/1181156/request-more.png/aa579105-9afb-48d8-aa77-fcf05176d4b7?t=1479154716788" style="width: 600px; height: 479px;">

		:markdown

			**Figure 2:** The Jetstream Change Your Settings UI, showing the link to use when requesting more resources

			On the popup form:

			1. Determine if you need more **Service Units** or **Quota**
			1. Click on **Next**.

			NOTE: if you select **Service Units**, you will be redirected to the [XSEDE User Portal](https://portal.xsede.org/submit-request) to complete the request.

			On the popup form:

			1. List the resources needed, e.g., 4 CPUs and 8GB memory, running 4 cores for 1 week
			1. Enter a justification for the resource request, describing how the additional resources will be used.

			Click on **Complete Request** to submit the form.

	#allocations:faq
		<style>
		dt {
		font-weight: bold;
		font-size: larger;
		}
		dd {
		color: #a52a2a;
		}
		</style>
		:markdown
			## [Allocations FAQ](#allocations:faq)
		<dl>
		<dt>1. Is there an overview of the types of allocations available as well as any restrictions those allocations have?</dt>
		<dd>
		:markdown
			The [Getting Started Guide](https://www.xsede.org/web/xup/documentation-overview) describes the process of getting onto XSEDE, applying for allocations and using XSEDE resources.
			To review the types of allocations XSEDE and the process to get an allocation, here are some links you might find useful:

			* [Allocations overview](https://portal.xsede.org/allocations/announcements)
			* [Types of allocations, eligibility, details](https://www.xsede.org/web/xup/documentation-overview#allocations-types)
			* [Allocation policies](https://portal.xsede.org/allocation-policies)
			* [Submit and manage allocation requests](https://www.xsede.org/group/xup/submit-request)
			* [Proposal deadlines](https://portal.xsede.org/allocations/research#schedule)
		</dd>

		<dt>2. Is there a example or demonstration of how to get an allocation that I could follow?</dt>
		<dd>There is a Cornell Virtual Workshop (CVW) on <a href="https://cvw.cac.cornell.edu/JetstreamReq">Getting a Research Allocation for Jetstream</a>.</dd>

		<dt>3. How do I let other XSEDE accounts use my allocation?</dt>
		<dd>To add users to, or remove them from, an active Extreme Science and Engineering Discovery Environment (XSEDE) allocation, the principal investigator, co-principal investigator, or allocation manager can follow [these instructions](https://portal.xsede.org/allocations/managing).

		<dt>4. How often can I get a startup allocation?</dt>
		<dd>Applications for startup allocations will only be accepted once. After the startup runs out it is best to apply for a research allocation.</dd>

		<dt>5. How do I request additional SUs?</dt>
		<dd>If you already have an XSEDE allocation and need to request additional service units (SUs) the PI, co-PI, or a delegate may submit a request via the XSEDE User Portal. For instructions on how to submit the request, see <a href="#allocations:supplemental">How do I request supplemental service units for an XSEDE allocation?</a></dd>

		<dt>6. How do I request additional resources such as CPUs and memory?</dt>
		<dd>Additional CPUs and memory may be requested via Atmosphere.  See <a href="#allocations:add-res">How to request additional resources on Jetstream</a>.</dd>
		</dl>

	#allocations:supplemental
		:markdown
			## [Supplemental Allocation](#allocations:supplemental)
			**Requesting additional SUs or storage**

		<div style="display:grid;grid-template-columns:auto 70%;padding:1rem">
		<div style="padding:1rem;text-align:left">
		:markdown
			Login to the [XUP](https://portal.xsede.org)
		</div>
		<div style="padding:1rem;text-align:center"><img alt="Screenshot: XUP login screen" src="/documents/10308/1181156/portal-login-screen.jpg/e33ca0e0-94ee-4144-857f-028218327cff?t=1546631285045" style="width: 400px; height: 339px;" />
		</div>
		<div style="padding:1rem;text-align:left">
		:markdown
			Go to the [XSEDE Resource Allocation System (XRAS)](https://portal.xsede.org/group/xup/submit-request)

			(You can also mouse over the **Allocations** tab and then select **Submit/Review Request**)
		</div>
		<div style="padding:1rem;text-align:center"><img alt="Screenshot: XUP Go to request" src="/documents/10308/1181156/portal-go-to-request-screen.jpg/f9807dac-cae9-489f-a5c3-6e1f14da88a4?t=1546641707250" style="width: 400px; height: 317px;" />
		</div>
		<div style="padding:1rem;text-align:left">
		:markdown
			Find your allocation. Under the Action menu for your allocation, select **Supplement**.<br />
			<span style="color:red">Note: If your allocation is going to expire in 30 days or less, you will not see the Supplement option.</span> You should see the Extension option, though. Choose a 3 month extension and once it's approved, the supplement button should appear.
		</div>
		<div style="padding:1rem;text-align:center"><img alt="Screenshot: XUP Supplement action" src="/documents/10308/1181156/portal-supplement.jpg/90992274-b9d7-4955-a028-972b6dde1aac?t=1546641764400" style="width: 400px; height: 220px;" />
		</div>
		<div style="padding:1rem;text-align:left">
		:markdown
			Choose **Start Supplement**
		</div>
		<div style="padding:1rem;text-align:left"><img alt="Screenshot: XUP Start Supplement button" src="/documents/10308/1181156/portal-start-supplement.jpg/7aa067c5-069a-43e4-a972-dcd529d35e45?t=1546641736637" style="width: 400px; height: 195px;" />
		</div>
		<div style="padding:1rem;text-align:left">
		:markdown
			Choose **IU/TACC Jetstream** and/or **IU/TACC Jetstream Storage** depending on whether you need SUs, additional storage, or both.
		</div>
		<div style="padding:1rem;text-align:center"><img alt="Screenshot: XUP Resources selection" src="/documents/10308/1181156/portal-resource-select.jpg/ac1aaed0-8e41-4f0c-9ac2-ef48cb95eac0?t=1546641726961" style="width: 400px; height: 287px;" />
		</div>
		<div style="padding:1rem;text-align:left">
		:markdown
			Once you have selected the resources you need, you'll need to fill in the values of your request. The SU request has the same options as when you got your allocation. You'll fill in the SUs needed, the number VMs and IPs you expect to use (for forecasting purposes), and any comments.
		</div>
		<div style="padding:1rem;text-align:center"><img alt="Screenshot: XUP SU select" src="/documents/10308/1181156/portal-su-select.jpg/69541223-3c37-428b-912c-215da924ead8?t=1546641769680" style="width: 400px; height: 323px;" />
		</div>
		<div style="padding:1rem;text-align:left">
		:markdown
			For storage requests, you'll fill in the value in gigabytes - e.g. 1000gb = 1TB.
		</div>
		<div style="padding:1rem;text-align:center"><img alt="Screenshot: XUP storage select" src="/documents/10308/1181156/portal-storage-select.jpg/e4d26576-c7de-4dda-9be1-e17b05c25942?t=1546641758407" style="width: 400px; height: 234px;" />
		</div>
		<div style="padding:1rem;text-align:left">
		:markdown
			You will have to include a PDF "Progress Report" basically a paragraph or two on the need for the storage or SU request.

			After you have added that, you may submit the request.
		</div>
		<div style="padding:1rem;text-align:center"><img alt="Screenshot: XUP support docs submit" src="/documents/10308/1181156/supplement-add-doc-submit.jpg/64dd4df0-778b-48d2-8bfa-e7d4a616e5d4?t=1546641798164" style="width: 400px; height: 222px;" />
		</div>
		</div>
		<p>Requests are typically reviewed within 1 to 2 business days. You'll receive notification from XSEDE Allocations on the status of your request once it has been reviewed by all service providers on the request.</p>

	#allocations:education
		:markdown
			## [Getting an Education Allocation](#allocations:education)

			Jetstream allocations are awarded exclusively through the eXtreme Science and Engineering Discovery Environment (XSEDE). XSEDE provides XSEDE User Portal (XUP) accounts free of charge. XSEDE allocations require that the Principal Investigator (PI) be a US-based researcher. 

			The XSEDE website has a guide with [more detail on Jetstream allocations](https://www.xsede.org/resources/overview).

			You'll need a copy of your CV, the course syllabus, and a resource justification. The latter is basically figuring the size of the VM your students will need, the number of weeks they'll need it, and the number of students. An example of the resource selection is below.

			You'll first need to create an XSEDE User Portal (XUP) account if you do not already have one.

	#allocations:education-xupacct
		:markdown
			### [Create an XSEDE Portal Account](#allocations:education-xupacct)

			1. Go to the [XSEDE User Portal (XUP)](https://portal.xsede.org)
			1. Click **Create Account** on the left side of your screen. 
			1. Fill out the form and click **Submit**.
			1. Upon receipt of the email notification click the link in the email to verify your account and set your username and password. If the link doesn't work, go to the [XUP](https://portal.xsede.org), click **Sign In** and then select **Verify Account** under the **Other Sign In Options**.
			1. Following account verification, if not already logged in, go to the [XUP](https://portal.xsede.org), click **Sign In** and sign in with the username and password set in the verification step.
			1. You will be asked to read and accept the User Responsibilities form. This form outlines acceptable use to protect shared resources and intellectual property.

	#allocations:education-apply
		:markdown
			### [Apply for an XSEDE Education Allocation](#allocations:education-apply)

			1. Read the [Allocations Overview](https://portal.xsede.org/allocations/announcements). There are sample allocation requests in the overview that you may find helpful.
			1. Go to the [XSEDE Resource Allocation Request page](https://portal.xsede.org/submit-request). On **Available Opportunities**, click **Start a New Submission** under **Educational**. If you are not familiar with the process, select **Begin Guided Submission** for step-by-step instructions.
			1. Before submitting an allocation request, have the following information available:

				* PI, Co-PIs, and Allocation Managers (username)
				* Add additional users that will be able to use your allocation time and resources (_optional_)
				* Title
				* Abstract (typically a paragraph or two for an Educational request will suffice)
				* Keywords
				* Field of science (secondary areas of science may be also be added)
				* Resources
					* Select **Jetstream IU/TACC** from the list.
					* Enter the number of SUs you'll need. The [Virtual Machine Sizes and Configurations page](https://portal.xsede.org/jetstream#vmsizes) can help you figure VM sizes needed. See the box below for a walkthrough of creating your resource justification.
					* Fill in number of Virtual Machines needed _(this is an estimate for planning purposes - there's no wrong answer - try to best guess at how many instances you'll run at one time)_
					* Fill in number of public IP addresses needed (_same as above_)
					* Select **Jetstream Storage**. For Startup allocations, the block storage provided will be limited. By default, all Atmosphere users get 100 GB of volume space in addition to any VM root disks, so if that's enough, you need not request Jetstream Storage and do not need to select it.
				* Supporting documents - *__PDF format required__*
					* PI CV (2 page limit)
					* CoPI CV required for every CoPI added to request (2 page limit)
					* Syllabus
					* Resource Justification (see below)
				* Supporting Grants (Optional)
				* Publications of previous/supporting work (optional)

			1. Submit allocation request. At this point, all entered information is validated, errors or omissions are flagged.

			**Allow 3-5 business days for your application to go through the approval process.**

			*Detailed information about the allocation request process, with screenshots, can be found in the [XRAS Submit Allocation Request Step-by-Step Guide](https://portal.xsede.org/allocation-request-steps).*

	#allocations:education-justify
		:markdown
			### [Writing your resource justification](#allocations:education-justify)

			A resource justification is required for an Educational allocation. You'll need to explain how the resources will be used and for how long. A sample calculation for your SU request might be:

			* 16 week course
			* 20 students
			* m1.medium instance size (6 vCPU)

			Planning for 24 hour/day usage:

				6 vCPU (SUs) x 24 hours/day x 7 days x 16 weeks = 16,128 SUs/student x 20 students = 322,560 SUs

			It would be standard practice to round that to 350,000 SUs for development time for the instructors plus instructor VMs during the duration of the course.

			You'll also need to briefly explain why you chose the VM sizes that you did - what you expect each student to be running on those VMs. The [Virtual Machine Sizes and Configurations page](https://portal.xsede.org/jetstream#vmsizes) can help you figure VM sizes needed. A paragraph or two should suffice to cover this.

			If you have questions about the resource justification, please contact Jetstream Help via the [XSEDE Help Desk](mailto:help@xsede.org) and we can assist you.

