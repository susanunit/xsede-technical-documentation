#!/bin/sh

# this script is now unnecessary since we're not doing a TACC version of this guide.  Still, it has all the images URLS in here.  Can copy this back into src files eventually.

if [ "$1" = "t" ]  
then
	echo "building Jetstream user guide for TACC"
	outputfile="tjetstream.html"

	ugtitle="<h1\>Jetstream\ User\ Guide<\/h1\>"
	hostname="jetstream.tacc.utexas.edu"

	# Jetstream images on TACC
	jstopo="<img alt=\"\" src=\"\/documents\/10157\/1310756\/Jetstream-topology-1.jpg\/08f0ff9b-2907-495b-8e9a-ee51505ceaee\" style=\"width: 600px; height: 450px;\">"
	jsconfig="<img alt=\"\" src=\"\/documents\/10157\/1310756\/Jetstream-2.jpg\/5e86bf6d-269e-4f7e-8f67-dfe70fcf631e\" style=\"width: 600px; height: 191px; border-width: 1px; border-style: solid;\">"
	jsaccess1="<img alt=\"\" src=\"\/documents\/10157\/1310756\/Jetstream-3.jpg\/060d37b4-7b30-4cda-9dc2-90ce815f2fb4\" style=\"width: 600px; height: 421px; border-width: 1px; border-style: solid;\">"
	jsaccess2="<img alt=\"\" src=\"\/documents\/10157\/1310756\/Jetstream-4.jpg\/54df26bb-9065-4c46-9c8d-5553fddfe362\" style=\"width: 600px; height: 334px; border-width: 1px; border-style: solid;\">"
	jsaccess3="<img alt=\"\" src=\"\/documents\/10157\/1310756\/Jetstream-5.jpg\/33063ef1-326f-4f3d-8e36-f5c704c7ec9a\" style=\"width: 600px; height: 349px; border-width: 1px; border-style: solid;\">"
	jsaccess4="<img alt=\"\" src=\"\/documents\/10157\/1310756\/Jetstream-6.jpg\/03945bc8-e7d7-49dd-ac7c-e82782aeee8c\" style=\"width: 600px; height: 317px; border-width: 1px; border-style: solid;\">"
	jsaccess5="<img alt=\"\" src=\"\/documents\/10157\/1310756\/Jetstream-7.jpg\/33c6f301-54ce-493f-96ca-923cbd5d0704\" style=\"width: 600px; height: 422px; border-width: 1px; border-style: solid;\">"
	jsaccess6="<img alt=\"\" src=\"\/documents\/10157\/1310756\/Jetstream-8.jpg\/975c8f05-347e-4810-a0f8-e60e37e0d3ee\" style=\"width: 600px; height: 421px; border-width: 1px; border-style: solid;\">"
	jsssh1="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-sshkeys-1.jpg\/5ad3fa6f-2608-4821-8f22-9f0d50def3f7\" style=\"width: 600px; height: 402px; border-width: 1px; border-style: solid;\">"
	jsssh2="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-sshkeys-2.jpg\/adce8a02-be6f-4a23-ba01-c9cf0a158809\" style=\"width: 600px; height: 400px; border-width: 1px; border-style: solid;\">"
	jsssh3="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-sshkeys-3.jpg\/13de99f3-a471-4a2c-ad5a-721a70a29e6e\" style=\"width: 600px; height: 401px; border-width: 1px; border-style: solid;\">"
	jslaunchvm1="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-launchvm-1.jpg\/20fbc3e5-8dfa-465f-b7ec-6517d198d86a\" style=\"width: 600px; height: 419px; border-width: 1px; border-style: solid;\">"
	jslaunchvm2="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-launchvm-2.jpg\/39009115-56a8-43af-9715-915d6dc623ab\" style=\"width: 600px; height: 421px; border-width: 1px; border-style: solid;\">"
	jslaunchvm3="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-launchvm-3.jpg\/81e9b76a-d445-45b5-a264-8e844e86370a\" style=\"width: 600px; height: 422px; border-width: 1px; border-style: solid;\">"
	jslaunchvm4="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-launchvm-4.jpg\/d816e144-bd79-4585-b5a6-a3e050a1b1f4\" style=\"width: 600px; height: 421px; border-width: 1px; border-style: solid;\">"
	jslaunchvm5="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-launchvm-5.jpg\/0ae0276a-d1af-46ba-af25-893dfe97926e\" style=\"width: 600px; height: 423px; border-width: 1px; border-style: solid;\">"
	jslaunchvm6="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-launchvm-6.jpg\/b18f1d5f-e31d-41da-8a72-db8f501e6fc2\" style=\"width: 600px; height: 422px; border-width: 1px; border-style: solid;\">"
	jslaunchvm7="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-launchvm-7.jpg\/8428ad37-9a61-4cd9-8bee-a97c5d571137\" style=\"width: 600px; height: 421px; border-width: 1px; border-style: solid;\">"
	jslaunchvm8="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-launchvm-8.jpg\/9d26ef05-5db4-4457-9c57-840e5a7e84d4\" style=\"width: 600px; height: 398px; border-width: 1px; border-style: solid;\">"
	jslaunchvm9="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-launchvm-9.jpg\/ae59d429-0517-4207-b80f-1045c2e337a6\" style=\"width: 600px; height: 152px; border-width: 1px; border-style: solid;\">"
	jslaunchvm10="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-launchvm-10.jpg\/9458463d-92cd-4e7f-8186-c154915d92c9\" style=\"width: 600px; height: 145px; border-width: 1px; border-style: solid;\">"
	jslaunchvm11="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-launchvm-11.jpg\/a0bb8a6f-fc46-446d-b381-5fe5d1b07fc7\" style=\"width: 602px; height: 147px; border-width: 1px; border-style: solid;\">"
	jslaunchvm12="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-launchvm-12.jpg\/e61c3a3b-002f-45d4-9113-271e1f027492\" style=\"width: 700px; height: 163px; border-width: 1px; border-style: solid;\">"
	jsweblogin1="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-weblogin-1.jpg\/60f2288b-b714-4222-b03b-0487c0cad26b\" style=\"width: 600px; height: 297px; border-width: 1px; border-style: solid;\">"
	jsweblogin2="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-weblogin-2.jpg\/6d05cfef-abc7-4b39-a95b-a1bfa8cd0fbc\" style=\"width: 600px; height: 419px; border-width: 1px; border-style: solid;\">"
	jssshlogin="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-sshlogin-1.jpg\/c9714d14-8b8d-422e-88d1-b20bc15a269b\" style=\"width: 600px; height: 232px; border-width: 1px; border-style: solid;\">"
	jsvnc="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-vnc-disconnect.jpg\/0e6a594b-f21f-4149-9424-abaa1c3f7e2c\" style=\"width: 600px; height: 476px; border-width: 1px; border-style: solid;\">"
	jsimageinfo="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-imagereq-imageinfo.jpeg\/a0097b83-47dc-451e-b55a-8cfd0ca817c4\" style=\"width: 600px; height: 1072px; border-width: 1px; border-style: solid;\">"
	jsversioninfo="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-imagereq-versioninfo.jpeg\/3bbe78d9-5fec-4cb7-8bb5-8605836e8a94\" style=\"width: 600px; height: 759px; border-width: 1px; border-style: solid;\">"
	jsprovider="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-imagereq-provider.jpeg\/c98f21d6-2445-4e40-bc20-96030c48a0f1\" style=\"width: 600px; height: 766px; border-width: 1px; border-style: solid;\">"
	jsprivacy="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-imagereq-privacy.jpeg\/6751ed7d-8988-49e8-99a3-7dcfb0f0337c\" style=\"width: 600px; height: 759px; border-width: 1px; border-style: solid;\">"
	jsexcludefiles="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-imagereq-excludefiles.jpeg\/8371e701-b19e-46d3-8c09-9d0d4e7218b3\" style=\"width: 600px; height: 763px; border-width: 1px; border-style: solid;\">"
	jsboot="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-imagereq-boot.jpeg\/bea1e6ab-409d-48e2-9157-08a30238d215\" style=\"width: 600px; height: 761px; border-width: 1px; border-style: solid;\">"
	jsreview="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-imagereq-review.jpeg\/62a95093-9e6c-42e2-b7e3-dec61cb8d6b2\" style=\"width: 600px; height: 816px; border-width: 1px; border-style: solid;\">"
	jsimagelist="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-imagelist.jpg\/6ff2b0f5-381c-497e-ab21-b6143b666d1d\" style=\"width: 600px; height: 234px; border-width: 1px; border-style: solid;\">"
	jsshutdown1="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-shutdown-1.jpg\/382e45c3-b203-48cb-9599-85c5445ea8db\" style=\"width: 600px; height: 398px; border-width: 1px; border-style: solid;\">"
	jsshutdown2="<img alt=\"\" src=\"\/documents\/10157\/1310756\/jetstream-shutdown-2.jpg\/38571cc2-d334-4d62-8ffe-9df828b99eaf\" style=\"width: 600px; height: 378px; border-width: 1px; border-style: solid;\">"


elif [ "$1" = "x" ] 
then
	echo "building Jetstream user guide for XSEDE"
	outputfile="jetstream.html"

	ugtitle=""
	hostname="jetstream.iu.xsede.org"

	# Jetstream images on XSEDE
	jstopo="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/Jetstream-topology.jpg\/cc8b9ae6-4b06-4eb1-8d75-71c5edcadd9a\"\ style=\"width:\ 600px;\ height:\ 450px;\">"
	jsconfig="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-sysconfig.jpg\/3e4db897-40b2-4c23-b092-82cb52b76d10\"\ style=\"width:\ 600px;\ height:\ 191px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsactive="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/active.jpg\/5f4d5170-f498-4754-81d0-2ac4ed28ff43?t=1537921538029\" style=\"width: 608px; height: 143px; border-width: 1px; border-style: solid;\">"
	jsaccess1="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-systemaccess-1.jpg\/37a2cfb1-84d1-469d-a0f2-f57c5e13adfb\"\ style=\"width:\ 600px;\ height:\ 421px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	mdtest2a="<img\ alt=\"The\ Globus\ Auth\ screen\ for\ Jetstream\ Web\ App\ with\ XSEDE\ credentials\"\ src=\"\/documents\/10308\/1181156\/js-system-access-2a.jpg\/1b679c19-ad21-4fea-a902-8f805d1e50f7\"\ style=\"width: 600px;\ height:\ 637px;\">"
	mdtest3a="<img\ alt=\"The\ XSEDE\ credentials\ screen\"\ src=\"\/documents\/10308\/1181156\/js-system-access-3a.jpg\/a3ee653d-67b4-4cbe-8bdb-786cfe0f921b\"\ style=\"width:\ 600px;\ height:\ 378px;\">"
	mdtest3b="<img\ alt=\"The\ DUO\ Auth\ Dialog\"\ src=\"\/documents\/10308\/1181156\/js-system-access-3b.jpg\/74977ccd-28bf-410b-bd53-034ff06aecaa\"\ style=\"width:\ 600px;\ height:\ 638px;\">"
	jsaccess4="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-systemaccess-4.jpg\/d26d9446-778d-440e-9dc8-8bda317a1fba\"\ style=\"width:\ 600px;\ height:\ 317px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsaccess5="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-systemaccess-5.jpg\/dc838fdb-4c9a-4a54-87d6-5061f78844c5\"\ style=\"width:\ 600px;\ height:\ 422px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsaccess6="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-systemaccess-6.jpg\/a0430c6e-d2b0-4572-bce6-6b09f580f884\"\ style=\"width:\ 600px;\ height:\ 421px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsssh1="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-sshkeys-1.jpg\/ab889d51-eb44-437c-befe-a5af3441c16c\"\ style=\"width:\ 600px;\ height:\ 402px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsssh2="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-sshkeys-2.jpg\/286900c3-ef50-475c-8f66-248006734231\"\ style=\"width:\ 600px;\ height:\ 400px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsssh3="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-sshkeys-3.jpg\/9f819230-0105-4c99-aca4-962d43f13885\"\ style=\"width:\ 600px;\ height:\ 401px;\ border-width:\ 1px;\ border-style:\ solid;\">"

	jslaunchvm1b="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/list-of-images.jpg\/8b9987de-19c2-4bb8-b66b-51d3f12c3d72?t=1538510643843\"\ style=\"width:\ 600px;\ height:\ 424px;\ border-width:\ 1px;\ border-style:\ solid;\"\ \/>"
	jslaunchvm1u="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/Image-List-for-Launch.png\/984f662c-715a-4033-9818-14257d9db213?t=1632332229628\"\ style=\"width:\ 600px;\ height:\ 457px;\ border-width:\ 1px;\ border-style:\ solid;\"\ \/>"


	jslaunchvm2b="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/image-details.jpg\/73b68d92-4cb2-4a8c-b852-98df0c2e7e3c?t=1538510652747\"\ style=\"width:\ 600px;\ height:\ 451px;\ border-width:\ 1px;\ border-style:\ solid;\ margin-top:\ 1rem\"\ \/>"
	jslaunchvm2u="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/Ubuntu-Image-For-Launch.png\/d346b149-21dc-40e6-98ab-b1f256179996?t=1632332250252\"\ style=\"width:\ 600px;\ height:\ 460px;\ border-width:\ 1px; border-style:\ solid;\ margin-top:\ 1rem;\"\ \/>"

	jslaunchvm3b="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/launch-instance.jpg\/997de928-d4ba-4223-9487-975ae5dbb430?t=1538510659486\" style=\"width:\ 600px;\ height:\ 503px;\ border-width:\ 1px;\ border-style:\ solid;\ margin-top:\ 1rem\"\ \/>"
	jslaunchvm3u="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/Launch-Screen.png\/f16127ea-5a7c-4c9e-8d63-dc2c147edc1e?t=1632332267124\"\ style=\"width:\ 600px;\ height:\ 466px;\ border-width:\ 1px;\ border-style:\ solid;\ margin-top:\ 1rem\"\ \/>"

	jslaunchvm9="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-launchvm-9.jpg\/98967ef2-76e8-4ae7-b704-ed519890d812\"\ style=\"width:\ 600px;\ height:\ 152px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jslaunchvm10="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-launchvm-10.jpg\/4ad26099-7b29-4b58-9fdd-dd6ac37ef6dd\"\ style=\"width:\ 600px;\ height:\ 145px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jslaunchvm11="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-launchvm-11.jpg\/0970a305-69ba-4fc1-a19a-b469df316b7c\"\ style=\"width:\ 602px;\ height:\ 147px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jslaunchvm12="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-launchvm-12.jpg\/f31f35e7-79c2-4a8d-9bdd-cbb88a0bbb1f\"\ style=\"width:\ 600px;\ height:\ 163px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsweblogin1="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-weblogin-1.jpg\/6ea39c86-daa4-4920-95d7-38b50c89b770\"\ style=\"width:\ 600px;\ height:\ 297px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsweblogin2="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-weblogin-2.jpg\/c7098c1b-720a-430a-99de-edbcda78c9c0\"\ style=\"width:\ 600px;\ height:\ 419px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jssshlogin="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-sshlogin-1.jpg\/82bef3b3-ff95-4423-ae61-b3f227f0e1a5\"\ style=\"width:\ 600px;\ height:\ 232px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsvnc="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-vnc-disconnect.jpg\/66cac2d6-76c9-48bf-bf30-07c1b8788ff9\"\ style=\"width:\ 600px;\ height:\ 476px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsimageinfo="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-imagereq-imageinfo.jpeg\/4e3ab742-0c36-48ee-abef-434c4a651bbd\"\ style=\"width:\ 600px;\ height:\ 1072px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsversioninfo="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-imagereq-versioninfo.jpeg\/adf6f862-b3ab-4e91-baea-644d237f7bbb\"\ style=\"width:\ 600px;\ height:\ 759px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsprovider="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-imagereq-provider.jpeg\/616259a0-ea03-44fd-ad0c-0907560d37d5\"\ style=\"width:\ 600px;\ height:\ 766px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsprivacy="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-imagereq-privacy.jpeg\/6e4a7198-236d-4619-a642-ca418ea37b80\"\ style=\"width:\ 600px;\ height:\ 759px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsexcludefiles="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-imagereq-excludefiles.jpeg\/76e94ed3-19f4-4346-b32f-0d2372a47d03\"\ style=\"width:\ 600px;\ height:\ 763px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsboot="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-imagereq-boot.jpeg\/2a97ca95-520e-45b0-8c4f-9c439c0d9bef\"\ style=\"width:\ 600px;\ height:\ 761px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsreview="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-imagereq-review.jpeg\/8b1b167d-b3b1-44e0-a158-592ef39ce083\"\ style=\"width:\ 600px;\ height:\ 816px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsimagelist="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-imagelist.jpg\/c06743a3-9f71-4584-9458-429ec1f8de8f\"\ style=\"width:\ 600px;\ height:\ 234px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsshutdown1="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-shutdown-1.jpg\/d6ec97cd-a3ba-497e-b0e3-f597191381e2\"\ style=\"width:\ 600px;\ height:\ 398px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsshutdown2="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/jetstream-shutdown-2.jpg\/4634dd47-0305-4f2c-b548-cabf8f9e39fe\"\ style=\"width:\ 600px;\ height:\ 378px;\ border-width:\ 1px;\ border-style:\ solid;\">"
	jsoslogin="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/openstack-login.jpg\/417de014-f4da-4d39-a137-7c478d3eb090?t=1479536402156\"\ style=\"width: 250px; height: 342px; float: right;\">"

	jsapilogin="<img\ alt=\"\"\ src=\"\/documents\/10308\/1181156\/h_api_login.jpg\/3344b531-2145-4749-8bbc-60116bfbb329?t=1479583851929\"\ style=\"width: 250px; height: 342px; float: right;\">"
	jsapitopo1="<img\ alt=\"create network screenshot\"\ src=\"\/documents\/10308\/1181156\/h_api_net_topo1.jpg\/d460e60b-3182-4fca-b467-12b2599a886d?t=1479583879496\"\ style=\"width: 400px; height: 114px; float: right;\">"
	jsapicnet1="<img\ alt=\"create network screenshot\"\ src=\"\/documents\/10308\/1181156\/h_api_c_net1.jpg\/ca8e1a91-25d2-4281-a5f6-2171fda18bd9?t=1479583651487\"\ style=\"width: 400px; height: 207px; float: right;\">"
	jsapicnet2="<img\ alt=\"create network screenshot 2\"\ src=\"\/documents\/10308\/1181156\/h_api_c_net2.jpg\/96c3cadb-ae15-4f61-9813-22ef2a6d356a?t=1479583807395\"\ style=\"width: 400px; height: 284px; float: right;\">"
	jsapicnet3="<img\ alt=\"create network screenshot 3\"\ src=\"\/documents\/10308\/1181156\/h_api_c_net3.jpg\/4650ab88-1a91-49f5-96a3-d5f5db252778?t=1479583815386\"\ style=\"width: 400px; height: 344px; float: right;\">"
	jsapitopo2="<img\ alt=\"network topo 2 screenshot\"\ src=\"\/documents\/10308\/1181156\/h_api_net_topo2.jpg\/f19c29e4-6a1d-49dd-9fcb-a7daa7449498?t=1479583894369\"\ style=\"width: 400px; height: 114px; float: right;\">"
	jsapicroute="<img\ alt=\"create router screenshot\"\ src=\"\/documents\/10308\/1181156\/h_api_c_route.jpg\/e7f213f7-a40e-4774-af57-00ddfa8588c7?t=1479583840375\"\ style=\"width: 400px; height: 197px; float: right;\">"
	jsapicpriv="<img\ alt=\"connect private screenshot\"\ src=\"\/documents\/10308\/1181156\/h_api_c_priv.jpg\/075e3f3b-c7c6-4604-9137-a5ac256cbc37?t=1479583824766\"\ style=\"width: 400px; height: 150px; float: right;\">"
	jsapiaddif="<img\ alt=\"add interface screenshot\"\ src=\"\/documents\/10308\/1181156\/h_api_add_if.jpg\/fc329f4a-fece-46f0-8d05-3d2281ee0488?t=1479583641755\"\ style=\"width: 400px; height: 234px; float: right;\">"
	jsapidiag="<img\ alt=\"network diagram screenshot\"\ src=\"\/documents\/10308\/1181156\/h_api_net_diag.jpg\/5f006b93-e07f-49d3-9afb-9279a35a90ed?t=1479583867395\"\ style=\"width: 250px; height: 233px; float: right;\">"

	jswrangdata="<img\ alt=\"atmosphere login screenshot\"\ src=\"\/documents\/10308\/1181156\/wrangdata.jpg\/99a8e42f-77b6-4f85-ad96-e7cdcb0932ee?t=1479780517802\"\ style=\"width: 600px; height: 334px;\"\ >"

	jsvolcreatedialog="<img\ src=\"\/documents\/10308\/1181156\/vol-create-dialog.png\/a9f6caca-5aeb-41d2-9c9b-f98b617ef2c5?t=1479846800416\"\ style=\"width: 600px; height: 652px; margin-left: 2rem\">"
	jsvolcreatelist="<img\ src=\"\/documents\/10308\/1181156\/vol-create-list.png\/e584dce5-78e5-41d8-a4c8-349ebc314278?t=1479847221146\"\ style=\"width: 600px; height: 113px; margin-left: 2rem\">"

	jsvolattachlist="<img\ src=\"\/documents\/10308\/1181156\/vol-attach-list.png\/c421ea4e-67ea-416e-abf3-89ae62c49a4a?t=1479850017213\" style=\"width: 600px; height: 102px; margin-left: 2rem\">"

else
	echo "invalid command line argument"
	exit 1
fi


sed	-e "s/UGTITLE/$ugtitle/" \
	-e "s/HOSTNAME/$hostname/" \
	-e "s/JSPAUSE/$jspause/" \
	-e "s/JSTOPO/$jstopo/" \
 	-e "s/JSCONFIG/$jsconfig/" \
 	-e "s/JSACTIVE/$jsactive/" \
 	-e "s/JSACCESS1/$jsaccess1/" \
 	-e "s/JSACCESS2/$jsaccess2/" \
 	-e "s/JSACCESS3/$jsaccess3/" \
 	-e "s/JSACC2A/$mdtest2a/" \
 	-e "s/JSACC3A/$mdtest3a/" \
 	-e "s/JSACC3B/$mdtest3b/" \
 	-e "s/JSACCESS4/$jsaccess4/" \
 	-e "s/JSACCESS5/$jsaccess5/" \
 	-e "s/JSACCESS6/$jsaccess6/" \
 	-e "s/JSSSH1/$jsssh1/" \
 	-e "s/JSSSH2/$jsssh2/" \
 	-e "s/JSSSH3/$jsssh3/" \
 	-e "s/JSLAUNCHVM01U/$jslaunchvm1u/" \
 	-e "s/JSLAUNCHVM02U/$jslaunchvm2u/" \
 	-e "s/JSLAUNCHVM03U/$jslaunchvm3u/" \
 	-e "s/JSLAUNCHVM09/$jslaunchvm9/" \
 	-e "s/JSLAUNCHVM10/$jslaunchvm10/" \
 	-e "s/JSLAUNCHVM11/$jslaunchvm11/" \
 	-e "s/JSLAUNCHVM12/$jslaunchvm12/" \
 	-e "s/JSWEBLOGIN1/$jsweblogin1/" \
 	-e "s/JSWEBLOGIN2/$jsweblogin2/" \
 	-e "s/JSSSHLOGIN/$jssshlogin/" \
 	-e "s/JSVNC/$jsvnc/" \
 	-e "s/JSIMAGEINFO/$jsimageinfo/" \
 	-e "s/JSVERSIONINFO/$jsversioninfo/" \
 	-e "s/JSPROVIDER/$jsprovider/" \
 	-e "s/JSPRIVACY/$jsprivacy/" \
 	-e "s/JSEXCLUDEFILES/$jsexcludefiles/" \
 	-e "s/JSBOOT/$jsboot/" \
 	-e "s/JSOSLOGIN/$jsoslogin/" \
 	-e "s/JSAPILOGIN/$jsapilogin/" \
 	-e "s/JSAPITOPO1/$jsapitopo1/" \
 	-e "s/JSAPICNET1/$jsapicnet1/" \
 	-e "s/JSAPICNET2/$jsapicnet2/" \
 	-e "s/JSAPICNET3/$jsapicnet3/" \
 	-e "s/JSAPITOPO2/$jsapitopo2/" \
 	-e "s/JSAPICROUTE/$jsapicroute/" \
 	-e "s/JSAPICPRIV/$jsapicpriv/" \
 	-e "s/JSAPIADDIF/$jsapiaddif/" \
 	-e "s/JSAPIDIAG/$jsapidiag/" \
 	-e "s/JSWRANGDATA/$jswrangdata/" \
 	-e "s/JSVOLCREATEDIALOG/$jsvolcreatedialog/" \
 	-e "s/JSVOLCREATELIST/$jsvolcreatelist/" \
 	-e "s/JSVOLATTACHLIST/$jsvolattachlist/" \
 	-e "s/JSREVIEW/$jsreview/" \
 	-e "s/JSIMAGELIST/$jsimagelist/" \
 	-e "s/JSSHUTDOWN1/$jsshutdown1/" \
 	-e "s/JSSHUTDOWN2/$jsshutdown2/" < js.html > $outputfile


