%section#overview
	:markdown
		## [System Overview](#overview)

		Quarry, an XSEDE resource located at Indiana University, is a machine servicing researchers with projects that have a web service component. Web service components facilitate access to grid resources that have been allocated to researchers allowing them to access data and results. They provide a portal to the data that might not otherwise be available to those without direct access to XSEDE resources. Quarry utilizes virtual machines to give researchers web based access that they can configure to meet their individual needs.

		The system consists of multiple Intel-based Hewlett-Packard ([HP](http://www8.hp.com/us/en/home.html)) servers geographically distributed for failover at the IU Bloomington and IUPUI Data Centers:

		* Eight HP ProLiant DL160 and two HP ProLiant DL360 front-end servers at each location host virtual machines (VMs) based on the Kernel-based Virtual Machine ([KVM](http://www.linux-kvm.org/page/Main_Page)) virtualization infrastructure. Each server runs Ubuntu 14.04 LTS, and is configured with dual quad-core Intel Xeon E5603 processors and a 10 gigabit Ethernet (GbE) adapter. Each DL160 has 96 GB of RAM; each DL360 has 128 GB of RAM. 

		* Four HP ProLiant DL180 servers with HP storage arrays and two HP ProLiant DL380 servers at each location provide VM block storage. Each server is configured with a quad-core Intel Xeon E5606 processor, a 10 GbE adapter, and a RAID controller attached to an HP storage array. Each DL180 has 12 GB of RAM; each DL380 has 32 GB of RAM.

		A standard VM consists of one virtual CPU, 4 GB of memory, and 10 GB of persistent local storage. Service owners get root access to their VMs. Supported VM operating systems are Red Hat Enterprise Linux (RHEL), CentOS, Debian Stable, and Ubuntu Linux.  

		This resource is restricted to members of approved XRAC grants that have a web service component. Only users who have been granted such an allocation, or users added to an existing allocation, are allowed to access this system. For more information on XSEDE [allocations](http://www.xsede.org/allocations) please consult the [XSEDE website](http://www.xsede.org/).


		<img src="https://www.xsede.org/image/image_gallery?uuid=28978f50-7552-40d6-b3a7-24b1228080cb&groupId=10157&t=1404857211894">

		<u>Figure 1. Quarry rack housing</u>

		<img src="https://www.xsede.org/image/image_gallery?uuid=5755253f-8873-4008-9600-43263afb0a0d&groupId=10157&t=1404857211892">

		<u>Figure 2. Quarry interconnect</u>

%section#overview-config
	:markdown
		### [System Configuration](#overview-config)

	%table(border="1" cellspacing="3" cellpadding="3")
		%tr
			%th Quarry Gateway Hosting System Configuration
			%th Aggregate Information
		%tr
			%td Machine type
			%td Gateway Web Hosting Service
		%tr
			%td Operating system
			%td Ubuntu 14.04 LTS (Host OS)<br>Supported VM operating systems are Red Hat Enterprise Linux (RHEL), CentOS, Debian Stable, and Ubuntu Linux
		%tr
			%td Memory model
			%td Kernel Virtual Machines
		%tr
			%td Processor cores
			%td 4-8 per node / 144 total
		%tr
			%td CPUs
			%td Intel Xeon E5603, E5606, E5-2660 
		%tr
			%td Nodes
			%td HP DL160, DL360, DL180, DL380 
		%tr
			%td RAM
			%td 6-64GB per CPU / 1TB total
		%tr
			%td Network
			%td 10 GB Ethernet
		%tr
			%td Storage
			%td 264 TB Ceph Virtual Block Storage (local shared disk), 340 - 360 TB Lustre global scratch (Data Capacitor), 340 - 360 TB Lustre global scratch (Data Capacitor - WAN)
		%tr
			%th Storage Information
		%tr
			%td File systems
			%td Ceph Virtual Block Storage (local), <a href="http://kb.iu.edu/data/ayfh.html">Lustre</a> on Data Capacitor and Data Capacitor WAN 
		%tr
			%td Total disk space
			%td 944 - 984 TB
		%tr
			%td Total scratch space
			%td Varies based on system usage 
		%tr
			%td Availability scope 
			%td Members of approved XRAC grants that have a web service component 
		%tr
			%td Quotas 
			%td No default quotas on virtual machines, adjustable by node 
		%tr
			%td Backup and purge policies 
			%td Virtual hosts are backed up daily by IBM Tivoli Storage Manager. Files in scratch space may be purged if they have not been accessed for more than 60 days.


%section#overview-datacapacitor
	:markdown
		### [Data Capacitor](#overview-datacapacitor)

		The Data Capacitor projects directory is dedicated to long-term projects with storage and ongoing access requirements that cannot be met with other existing systems. Requests for project space will be submitted to Team Data Capacitor and evaluated by the [Data Capacitor Allocation Committee](http://rt.uits.iu.edu/systems/hpfs/allocation-request-form.php). Default requests are 10TB, but requests for larger requests may be granted after evaluation.  

		Indiana University researchers will already have their credentials for access. XSEDE researchers will need to apply for the space they need from the [DC Allocation Committee](http://rt.uits.iu.edu/systems/hpfs/allocation-request-form.php). Since XSEDE researchers do not have Indiana University UIDs, this process is necessary in order to create UID mappings from the virtual machine on the Quarry Gateway to the Data Capacitor.  

%section#access
	:markdown
		## [System Access](#access)

%section#access-methods
	:markdown
		### [Methods of Access](#access-methods)

		* ssh (via unique IP/FQDN provided after allocation)
		* sftp/scp (via unique IP/FQDN provided after allocation)
		* other methods provided by the administrators of each virtual machine

%section#access-login
	:markdown
		###	[Logging into Quarry](#access-login)

		Quarry provides a virtual machine environment for XSEDE projects. There is no central login for Quarry resources. Access is granted once allocation is approved and is unique to each VM created. Once an allocation for a virtual machine has been approved and the VM created, information on accessing the VM will be provided to the user.  

%section#compenv
	:markdown
		## [Computing Environment](#computing)

		Quarry does not have a default environment consistent to each user or project. The environment, startup scripts, and modules will all vary from gateway to gateway depending on how each is set up. Unless specified, when a Quarry virtual machine is requested, Red Hat Enterprise Linux will be installed in its base configuration. Other distributions can be installed upon request. The base environment is installed.  It is then up to the machine's user to create the environment they will need for their research.  

%section#backups
	:markdown
		## [Backup Schedule](#backups)

		Virtual machines hosted on the Quarry Gateway Web Services Hosting resource at Indiana University are backed up daily using the IBM Tivoli Storage Manager (TSM).  

%section#policies
	:markdown
		## [Policies](#policies)

		The Quarry Gateway Web Services Hosting resource is used solely for hosting Extreme Science and Engineering Discovery Environment (XSEDE) Science Gateway and Web Service allocations, and is restricted to members of approved projects that have a web service component.


		*Last updated August 14, 2014*
