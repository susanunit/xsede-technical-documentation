:markdown
	This document provides a brief introduction to the Grid Resource Allocation and Management (GRAM) component of the [Globus Toolkit](http://www.globus.org/toolkit/).   For detailed GRAM documentation please consult the <a href="http://www.globus.org/toolkit/docs/5.0/5.0.0/execution/gram5/" target="_blank">GRAM5 User Guide</a>.  

	GRAM is a set of services and clients for communicating with a range of different batch/cluster job schedulers using a common protocol. GRAM is meant to address a range of jobs where reliable operation, stateful monitoring, credential management, and file staging are important.  GRAM client commands are used to locate, submit, monitor, and cancel jobs on XSEDE resources.  GRAM is ideal for experienced users with multiple allocations on multiple machines as one can manage jobs on different resources from a single command-line.

%section#prereqs
	:markdown
		### [Prerequisites for Running Jobs with GRAM](#prereqs)

		* <b>Proxy Certificate</b>:  A proxy certificate is required to run commands. When a gatekeeper receives a job request, it must authenticate the requester before it can start a jobmanager on the target resource. If you use XSEDE's "Single Sign-on", a proxy certificate is automatically created when you log in using GSI-OpenSSH. If you log in using a different method, then you will need to follow the <a href="https://www.xsede.org/web/guest/accessing-resources#portal" target="_blank">Single Sign-on instructions</a> for getting a proxy with `myproxy-logon`.
		* <b>Binary Code Compatibility</b>:  Any code to be run on a remote resource must be compiled for that resource. The target execution environment may differ from the one where the GRAM request is initiated.

%section#using
	:markdown
		### [Using GRAM on XSEDE resources](#using)
		The following GRAM examples demonstrate GRAM commands that employ the [RSL language](http://www.globus.org/toolkit/docs/5.0/5.0.0/execution/gram5/pi/#gram5-rsl) to provide the job variables, instead of multiple command-line options to describe various job aspects.  A `globus-job-submit` command returns a URL that is your job ID.  Use the URL to look up job status, cancel the job, etc.  For more detail please consult Globus' [GRAM User Guide](http://www.globus.org/toolkit/docs/5.0/5.0.0/execution/gram5/user/).  

%section#simple
	:markdown
		### [A Simple Session](#simple)

		You may execute GRAM commands from your desktop after downloading and installing the Globus Toolkit binaries.  An easier alternative is logon to any XSEDE resource where you have an account and issue commands from that resource's command line.  

		In the following example, user `jsmith` logs into TACC's [Stampede](https://www.xsede.org/web/guest/tacc-stampede) from her desktop machine, loads the `globus` module to properly set up the programming environment, generates a proxy, and then executes a simple GRAM command to display the hostname on SDSC's [Trestles](https://www.xsede.org/web/guest/sdsc-trestles).  Notice that you are submitting a job from Resource A (Stampede) to be run on Resource B (Trestles).  

		In GRAM, a **Gatekeeper Endpoint** contains the host, port, service name, and service identity required to contact a particular GRAM service.  [Table 1.](#xsedeendpoints) below lists XSEDE resource's endpoints.

		**Important**: When authenticating with the `myproxy-logon` command, use your **XSEDE Portal** username and your **XSEDE portal** password
		<pre>localhost$ <b>ssh -l jsmith stampede.tacc.utexas.edu</b>
		jsmith@stampede.tacc.utexas.edu's password: 
		...
		stampede$ <b>module load globus</b>
		stampede$ <b>myproxy-logon -l jsmith</b>
		Enter MyProxy pass phrase:
		A credential has been received for user jsmith in /tmp/x509up_u804387.
		stampede$ <b>globus-job-run trestles-login.sdsc.xsede.org:2119/jobmanager-fork /bin/hostname</b>
		trestles-login2.sdsc.edu
		stampede$</pre>


%section#mpisession
	:markdown
		#### [An MPI Session](#mpisession)
		In the following session, the user is logged onto TACC's Lonestar and launches an MPI job on TACC's Stampede.  The user submits the job, monitors job status, gathers results and cleans up.  The executable, `hello.out`, is located on the remote machine (Lonestar) and has been compiled for that machine.

	<ol>
	<li>	Submit an MPI job, hello.out, with 4 processes
	<pre>
	lonestar$ <b>globus-job-submit login5.stampede.tacc.utexas.edu:2119/jobmanager-slurm -np 4 -x  '&(jobtype=mpi)(project=TG-STA110012S)' hello.out</b>
	https://login5.stampede.tacc.utexas.edu:50384/16289936284489898541/13331958173416058768/</pre>
	<li>	Monitor the job status
	<pre>
	lonestar$ <b>globus-job-status https://login5.stampede.tacc.utexas.edu:50384/16289936284489898541/13331958173416058768/</b>
	ACTIVE
	lonestar$ <b>globus-job-status https://login5.stampede.tacc.utexas.edu:50384/16289936284489898541/13331958173416058768/</b>
	DONE</pre>
	<li>	Gather output
	<pre>
	lonestar$ <b>globus-job-get-output \
	&nbsp;&nbsp;&nbsp;&nbsp;https://login5.stampede.tacc.utexas.edu:50384/16289936284489898541/13331958173416058768/</b>
	TACC: Starting up job 778538
	TACC: Setting up parallel environment for MVAPICH2+mpispawn.
	TACC: Starting parallel tasks...
	Hello world from process 0 of 4
	Hello world from process 2 of 4
	Hello world from process 1 of 4
	Hello world from process 3 of 4
	TACC: Shutdown complete. Exiting.</pre>
	<li>	Clean up
	<pre>
	lonestar$ <b>globus-job-clean https://login5.stampede.tacc.utexas.edu:50384/16289936284489898541/13331958173416058768/</b>
	&nbsp;&nbsp;&nbsp;&nbsp;WARNING: Cleaning a job means:
	&nbsp;&nbsp;&nbsp;&nbsp;	- Kill the job if it still running, and
	&nbsp;&nbsp;&nbsp;&nbsp;	- Remove the cached output on the remote resource
	&nbsp;&nbsp;&nbsp;&nbsp;Are you sure you want to cleanup the job now (Y/N) ?
	&nbsp;&nbsp;&nbsp;&nbsp;<b>Y</b>
	Cleanup successful.</pre>
	</ol>


%section#examples-example1
	:markdown
		#### [Example 1](#examples-example1)
		Starting a Job with GRAM: Output is delivered to a gsiftp Server.
		<pre>
		$ <b>globusrun -b -r gridftp1.ls4.tacc.utexas.edu:2120/jobmanager-sge \
		'&amp;(executable=/bin/env) (stdout=output) (stderr=error) \
		(file_stage_out = \
		(output gsiftp://gridftp-qb.loni-lsu.teragrid.org:2811/home/kenneth/info/qa/output.lonestar) \
		(error gsiftp://gridftp-qb.loni-lsu.teragrid.org:2811/home/kenneth/info/qa/error.lonestar)) \
		(maxTime=3)
		(project=TG-STA060014N)'</b></pre>

%section#examples-example2
	:markdown
		#### [Example 2](#examples-example2)
		 MPI job that returns files to gsiftp server - does not wait for the job to start, giving a contact URL instead. At job completion, stdout and stderr are sent to the gsiftp server</a>
		<pre>
		$ <b>globusrun -b -r gridftp1.ls4.tacc.utexas.edu:2120/jobmanager-sge \
		'&amp; (executable=/home1/00202/uxkennet/info/qa/cpi) \
		(count=4) (jobtype=mpi) (hostcount=2) (stdout=output) (stderr=error) \
		(file_stage_out = \
		(output gsiftp://gridftp-qb.loni-lsu.teragrid.org:2811/home/kenneth/info/qa/output.gsiftp.lonestar) \
		(error gsiftp://gridftp-qb.loni-lsu.teragrid.org:2811/home/kenneth/info/qa/error.gsiftp.lonestar)) \
		(maxTime=3) \
		(project=TG-STA060014N)'</b>
		</pre>

%section#examples-example3
	:markdown
		#### [Example 3](#examples-example3)
		MPI job that returns contact URL, waits for the job to start, and returns stdout to the user through the `globusrun` command]
		<pre>
		$ <b>globusrun -o -r gridftp1.ls4.tacc.utexas.edu:2120/jobmanager-sge \
		'&amp; (executable=/home1/00202/uxkennet/info/qa/cpi) \
		(count=4) (hostcount=2) (maxTime=3) (project=TG-STA060014N) (jobtype=mpi)' </b></pre>

%section#examples-example4
	:markdown
		#### [Example 4](#examples-example4)
		Checking the Status of a GRAM Job.  There are two commands, `globusrun` and `globus-job-status`, that monitor the job status.  Both require the job ID,  in order to view the status of a job. 
		<pre> $ <b>globusrun -status https://grid.example.org:38824/16001608125017717261/5295612977486019989/</b>
		PENDING
		</pre>
		<pre>
		$ <b>globus-job-status https://grid.example.org:38824/16001608125017717261/5295612977486019989/</b>
		</pre>
		For a complete list of job status states please view the <a href="http://www.globus.org/toolkit/docs/5.0/5.0.0/execution/gram5/user/#gram5-cmd-globus-job-status" target="_blank">GRAM 5 job status descriptions</a>.

%section#examples-example5
	:markdown
		#### [Example 5](#examples-example5)
		
		Cancelling a GRAM Job: To cancel a gram job you can run one of the following commands, this will remove the job from the local scheduler: 
		<pre>$ <b>globusrun -k https://grid.example.org:38824/16001608125017717261/5295612977486019989/</b></pre>
		<pre>$ <b>globus-job-cancel -force https://grid.example.org:38824/16001608125017717261/5295612977486019989/</b></pre>

%section#examples-example6
	:markdown
		#### [Example 6](#examples-example6)
		
		Cleaning up after a GRAM Job: cleans out temporary globus state files only; does not delete output/error files
		<pre>$ <b>globus-job-clean -force https://grid.example.org:38824/16001608125017717261/5295612977486019989/</b></pre>

%section#xsedeendpoints
	:markdown
		### [XSEDE Resource GRAM Endpoints](#xsedeendpoints)
		The following table lists currently supported Gatekeeper Host contact strings for XSEDE resources supporting GRAM. 
