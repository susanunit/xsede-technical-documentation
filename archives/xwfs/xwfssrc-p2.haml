%section#access-allocations
	:markdown
		### [Allocations](#access-allocations)
		The XSEDE-Wide File System is available only via an [XSEDE allocation](https://www.xsede.org/web/guest/allocations).  XSEDE allocation requests are made via the [XSEDE User Portal](https://portal.xsede.org) (XUP).  Allocations will be considered based on the appropriateness of the intended usage for this specialized resource. Users are encouraged to provide information on the ways in which use of the XWFS will facilitate research activities involving multiple sites within XSEDE.

		Allocations are granted for a certain number of Gigabytes or Terabytes, and are valid for 1 year from the starting date of the allocation.

		Allocations are accounted for, and limits are enforced, via group-based quotas where the group consists of all the members of your project in the XSEDE accounting database. To add or remove users from your XWFS allocation, use the project management functions in the [XSEDE User Portal](https://portal.xsede.org).


%section#access-methods
	:markdown
		### [Methods of Access](#access-methods)
		The XSEDE-Wide File System is meant to be accessed via the resources on which it is mounted, and will support all data transfer mechanisms supported by each resource. In most cases this will include SSH/SCP and GridFTP/Globus. Please consult the resource-specific documentation under &quot;System Access&quot; for more details.

<div class="portlet-msg-info">XWFS mounts are available only on the front end (login) nodes of XSEDE resources.</div>

%section#access-mountpoints
	:markdown
		### [Mount points from the associated resources](#access-mountpoints)
		In all cases the XSEDE-Wide File System will be mounted as `/xwfs`, and your project path under `/xwfs` will be accessible in the same location on all resources.

%section#access-compenv
	:markdown
		### [Access from computing environments](#access-compenv)
		The XWFS may be accessed using normal POSIX/UNIX commands and library calls. Since the mount point is the same on all XSEDE resources, you may hard-code the path to your project directory in scripts and applications and use them on any site or resource, provided other dependencies are satisfied. Keep in mind that compute nodes will generally not mount the file system, and you should migrate your data to a local `$WORK` or `$SCRATCH` file system before attempting to run jobs that make use of data on the XWFS.


%section#managing
	:markdown
		## [Managing Data &amp; Permissions](#managing)
		The XSEDE-Wide File System is a POSIX file system and can be used with all normal UNIX file management utilities, e.g., the `ls`, `mv`, `cp` commands.  The XWFS is not directly accessible from the compute nodes within most XSEDE resources. For this reason, many users will wish to use the XWFS in concert with local cluster-level file systems to provide a complete storage solution for workflows including computational tasks. This can be done by migrating data from the XWFS to a local scratch file system before job submission, and migrating data from a local scratch area to the XWFS after job completion. A simple example might look like this:

		<pre>
		login1$ <b>cp /xwfs/tacc/my_project/my_jobfile /scratch/my_project/my_job_directory</b>

		...Run commands to execute job...

		login1$ <b>cp /scratch/my_project/my_output_files /xwfs/tacc/my_project/my_output_directory</b>
		login1$ <b>rm /scratch/my_project/my_output_files</b></pre>

		Note that scratch file system locations, and specific job submission commands, will vary from system to system, but the basic pattern shown above should work on all XSEDE resources with appropriate modifications.

		In addition to the standard UNIX &quot;`chmod`&quot; command, users may also wish to employ Access Control Lists (ACLs) for fine-grained management of file permissions. ACLs allow for a list of permissions to be attached to a file.  Please consult TACC's "[Manage Permissions with Access Control Lists](https://www.tacc.utexas.edu/user-services/user-guides/acls)" doc for more information on this topic.
 
%section#caveats
	:markdown
		### [Caveats to Users](#caveats)
		The XSEDE-Wide File System is a wide-area file system, meaning that data may be stored on a server that is geographically remote from any given client system. Some operations, such as listing directories with large numbers of files, or copying very large files to and from the file system, will perform more slowly than a local `$SCRATCH`. file system. For this reason, the XWFS is not mounted on compute nodes, and users will need to manage data migration to and from local file systems before and after job execution.

%section#transferring
	:markdown
		## [Transferring Data](#transferring)
		The XSEDE [Data Transfers and Management](http://www.xsede.org/web/guest/data-transfers) page provides general documentation on the use of XSEDE data transfer tools to manage data across XSEDE systems. These tools may be used on any resource mounting the XWFS.

%section#storagepolicies
	:markdown
		## [Data Storage Policies](#storagepolicies)

%table(border="1" cellpadding="3")
	%tr
		%th User Quotas
		%td(nowrap) There are no user-specific quotas on the XSEDE-Wide File System.
	%tr
		%th(valign="middle" nowrap) Data retention
		%td Once an allocation has expired, the data owner has 6 months to request an allocation renewal or to copy their data to a different resource. After 6 months from the date of allocation expiration, data will be removed from the file system.
	%tr
		%th(nowrap) File purging
		%td No regular purging is performed on the XSEDE-Wide File System.


%section#help
	:markdown
		## [Help](#help)
		For help or questions about using this resource, please submit a help ticket via the XSEDE User Portal, or [email](mailto:help@xsede.org) the XSEDE help desk.
	
%section#reference
	:markdown
		## [Reference](#reference)
		
		* [Manage Permissions with Access Control Lists](http://www.tacc.utexas.edu/user-services/user-guides/acls)
		* [IBM's General Parallel File System](http://www-03.ibm.com/systems/software/gpfs/)
	
		*Last update: August 24, 2015*

