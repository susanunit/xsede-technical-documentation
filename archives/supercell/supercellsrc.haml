#overview
	:markdown
		# [PSC Data Supercell User Guide](#overview)

	#overview:overview
		:markdown
			## [System Overview](#overview:overview)

			The Data Supercell (DSC) is a PSC-designed and built system for managing and archiving petabyte-scale data for researchers and industrial users. The DSC provides low-latency, high-capacity, high-reliability, high-bandwidth and low-cost data storage and retrieval.  The Data Supercell has an initial capacity of 4 Petabytes, but its architecture will enable it to scale well beyond this initial deployment. The Data Supercell storage system is managed by the SLASH2 file system software, developed at the PSC. This document explains how to store and access your data on the Data Supercell.

			Because the Data Supercell is a completely disk-based system it can support data resource applications well beyond those of traditional tape-based data storage resources. Traditional archivers were designed to be used for data retrieval to support batch processing. The fact that the Data Supercell is disk-based means it offers very high transfer rates for your data. Because it is disk-based it is also a very flexible system. It can accommodate datasets of different sizes, including extremely large datasets. Its flexibility also means that interface and access methods for the Data Supercell can be customized to meet your needs. The Data Supercell, because it is disk-based, has been constructed to be highly reliable and highly secure.

			The Data Supercell is strictly a file storage system. It is not used for computing nor will you, in a running program, directly open files that reside on it. Moreover, you will not login to the Data Supercell to perform file transfers. You will access your archived files indirectly using remote file transfer software and services on other systems.

	#overview:maintenance
		:markdown
			## [Scheduled Maintenance](#overview:maintenance)

			When necessary, downtime for system maintenance will be taken Wednesdays between 8:30 am and 5:00 pm Eastern Time.

			Users will be notified the day before a scheduled outage. If you find that a scheduled downtime will be disruptive to your work, please contact <remarks@psc.edu> with as much lead time as possible and we will try to accommodate your needs.


#policies
	:markdown
		# [File Storage Policies](#policies)

	#policies:quotas
		:markdown
			## [Quotas](#policies:quotas)

			Your storage allocation, or quota. is tied to your associated grant. Each file you create while using your grant is assigned to that grant. Every one of your files must be assigned to one of your active grants.

			Your quota must cover not only new files you create while using your grant, but also existing files on your grant, if you are applying for a Renewal grant, or existing files you intend to move from another grant. Thus, the time when you apply for a new storage allocation is a good time to remove unneeded Data Supercell files.

			You cannot exceed your quota. This quota is a grant quota. Even though files are stored in directories by individual, your Data Supercell quota applies across all the individuals on your grant. There are no individual Data Supercell quotas. 

			However, there are no maximum file sizes or a maximum number of files you can store.

			If you exceed your quota, you will not be able to edit files or create any more files on the Data Supercell.  In that case, your PI can end email to remarks@psc.edu to brief access to the Data Supercell for you to transfer your files.

	#policies:expiration
		:markdown
			## [Grant expiration](#policies:expiration)

			Three months after your grant expires all files on the Data Supercell that have been assigned to that grant will be deleted, even if you have other active grants. The deletion process is based on grant, not on individual user. Thus, when you are creating files you must ensure that they have been assigned to the proper grant to guarantee that files you want to keep are not deleted.  The [Managing your Allocation](#managing) section discusses how to ensure that your files are assigned to the correct grant.

			When your grant expires, you will not have access to your files on the Data Supercell. After three months, the files will be deleted.


#managing
	:markdown
		# [Managing your Allocation](#managing)

	#managing:overview
		:markdown
			## [Overview](#managing:overview)

			All Data Supercell users must manage their storage allocations, both in terms of staying under your Data Supercell quota and handling your files when your Data Supercell allocation ends. The procedures that non-XSEDE users must follow are described in the next paragraph. The procedures that XSEDE users must follow are described in the paragraphs following that paragraph.

			If you are a non-XSEDE user of the Data Supercell you can monitor your Data Supercell usage by sending email to <remarks@psc.edu> and asking what your current usage is. If you are over your quota send email to corp-relations@psc.edu or remarks@psc.edu to discuss increasing your Data Supercell quota. When your allocation is nearing its end send email to corp-relations@psc.edu or remarks@psc.edu to discuss either extending your allocation or how to handle the transition of your Data Supercell files.

			If you are an XSEDE user of the Data Supercell, you must keep your Data Supercell storage usage under your quota for each grant. Otherwise you are in jeopardy of having your write access to the Data Supercell turned off. If you want to increase your storage quota you can apply to increase your quota by asking for a Data Supercell Supplement through the [XSEDE Portal](https://www.xsede.org/user-portal). To request a Data Supercell Supplement go to the Submission Home page and follow the link for a Supplemental action. You can contact the PSC at remarks@psc.edu if you want to make other arrangements to get extra space. You can, of course, also delete unneeded files or move them to another location to stay under your quota.

			There are several tools you can use to monitor your Data Supercell storage usage. The xbanner command, which is available on PSC production systems, shows your Data Supercell quota and also the total Data Supercell usage across all users on your grant. The [Grant Management System](https://grants.psc.edu/cgi-bin/arms/authenticate/GMS_grant.pl) also shows your Data Supercell quota and total Data Supercell usage across all users on your grant. However, even though quotas are grant quotas and not individual quotas, if you are the PI, co-PI or a user who has been designated by the grant's PI to see account information, you can use the Grant Management System to see Data Supercell usage for each individual user on a grant, as well as the total usage by all users on a grant. Other users can only see their individual usage and the total usage across the grant. Both of these tools will show quotas and usage for all your active grants.

			The [Portal login dashboard](https://portal.xsede.org) displays your accounting data for all of your XSEDE resources, including the Data Supercell. It shows individual usage as well as total usage for a grant.

			Our recommended method for copying files into the Data Supercell is to use [Globus Online](https://www.psc.edu/resources/storage#go). If you only have one active grant, files you copy in with Globus Online will be assigned to that grant. If you have more than one active grant your files will be assigned to your default group. The first group listed by the groups command on any PSC production platform is your default group on the Data Supercell. Other file tranfers methods will work similarly with respect to group assignment.

			The PSC uses the Unix group as a charge-id. Moreover, there is a one-to-one correspondence between charge-id and grant number. To see which grant number corresponds to a charge-id you can use xbanner or the Grant Management System. To determine to which charge-id a file has been assigned you can cd to your Data Supercell home directory on a PSC production machine and use the ls -l command. Your Data Supercell home directory is /arc/users/joeuser, where you substitute your PSC userid for 'joeuser'. The output from these commands can be used to determine to which grant a Data Supercell file has been assigned.

			If your files have been assigned to an incorrect grant you should take steps to assign them to the correct grant. If you do not do this you could inadvertently exceed a grant quota or, more ominously, expose your files to being deleted if they are assigned to a grant that expires. To change the charge-id for a file use the chgrp command. There is a man page for chgrp. As was mentioned, at PSC group is synonymous with charge-id. The chgrp command can be used to change the charge-id for a single file, for a group of files using wildcards, or for all the files in a directory.

			For example, suppose you want to change the charge-id for all your files in the directory mynewdata, which is a directory you created off your Data Supercell home directory. First, you would cd to your Data Supercell home directory. Then you would issue the command


		<pre>chgrp -R mc3ts7p mynewdata</pre>

		:markdown

			This command will assign all the files in mynewdata to charge-id mc3ts7p.

			If you exceed your Data Supercell quota your normal methods of access to the Data Supercell will be blocked. You can either use the Portal to increase your quota and then send email to <remarks@psc.edu> to have your normal methods of access restored or your PI will need to request access for you to the Data Suprcell so you can get under your quota.  You can use the xbanner command or the Grant Management System to see your Data Supercell usage. Once you are under your quota send email to <remarks@psc.edu> and your normal access to the Data Supercell will be restored.

			The second issue you confront when managing your Data Supercell storage allocation is what to do to keep wanted files from being deleted when your grant expires. As was mentioned, when a grant expires, the files stored on that grant are deleted three months after the grant expires. 

			You should move any Data Supercell files you want to maintain to another location. If you have been actively monitoring your usage during the course of your grant this process will be less daunting. Any files you do not want to save you should delete.


#transfer
	:markdown
		# [Transferring Files](#transfer)

	#transfer:overview
		:markdown
			## [Overview](#transfer:overview)

			A variety of transfer methods are available to copy files to and from the Data Supercell. These methods are discussed below

			Our recommended method of file transfer is Globus Online, if you can obtain credentials to use Globus Online, either through XSEDE or InCommon. If you cannot use Globus Online, but do have access to Globus software, our next recommended method is globus-url-copy. Otherwise, you should use either the tar method discussed below or scp. You should use the former method if you are transferring a lot of small files.

			PSC maintains a Web page that lists average data transfer rates between all XSEDE resources, including the Data Supercell. For example, you can find the average data transfer rate between blacklight and the Data Supercell in this table. If your transfer rates are significantly below these average rates or you believe that your file transfer performance is subpar, send email to remarks@psc.edu and we will examine methods of improving your file transfer performance.

			Your home directory on the Data Supercell is /arc/users/joeuser, where 'joeuser' is your PSC userid. You will usually need to know this path when transferring files to and from the Data Supercell.

			If you are going to store a file that is 2 Tbytes or larger or you intend to store more than 500 Gbytes in a single day, send email to remarks so that special arrangements can be made to handle your large file transfers.


	#transfer:globus
		:markdown
			## [Globus Online](#transfer:globus)

			[Globus Online](https://www.globus.org) is our recommended method of transferring data to and from the Data Supercell. To use Globus Online you must first create a Globus Online userid and password at the Globus Online Web site. Once you have logged in to Globus Online you can initiate your file transfer. For each transfer you must select two Globus Online endpoints, to which you must then authenticate. The endpoint to use for the Data Supercell is xsede#psc.data. If you are an XSEDE user you can use your XSEDE User Portal userid and password to authenticate to the Globus Online endpoint xsede#pscdata. When connecting to the xsede#pscdata endpoint on Globus Online you may be redirected to the XSEDE OAuth page to enter your XSEDE User Portal username and password for authentication. After authentication, you will automatically be returned to the Globus Online site to initiate your transfers.

			If you are affiliated with an [InCommon](http://www.incommon.org) institution you can use your userid and password for that institution to authenticate to endpoint psc#dsc-cilogon if you have previously registered with PSC as an InCommon user. To register with PSC as an InCommon user you must follow the steps below. You only need to follow these steps once.

			1. Point your browser to https://cilogon.org/, select your institution from the 'Select an Identity Provider' list, and click on the 'Log On' button. This should take you automatically to a login page for your institution.
			1. Enter your username and password for your institution on the login page, and click on the 'Login' button. You should then get redirected back to the CILogon Service webpage.
			1. In a box near the top of the CILogon Service webpage there should appear a field called "Certificate Subject" with a string like "/DC=org/DC=cilogon/C=US/O=My Institution/CN=My Name A1234". Copy this string to a clipboard or text file. You'll need to paste it into a field on another webpage shortly.
			1. Log Off from the CILogin Service webpage.
			1. Point your browser to https://dirs.psc.edu/cgi-bin/teragrid/userpage/list.pl and login with your PSC username and password. This site lists the certificate subjects (DNs) that we have in our PSC database for your PSC account. You will be adding the CILogon certficate subject (DN) to this list.
			1. Click on the 'Add DN' link at the top left. This should get you to the "Adding DN" page, where you paste into the DN: field the certificate subject that you copied in step 3 above. Make sure there are no extra spaces before or after the pasted string. Click on 'Create' to add your new CILogon DN (certificate subject) to the PSC database.
			1. Click on the 'List DNs' link at the top left to confirm that your new DN was added.

			Within an hour you should be able to use Globus Online by authenticating to the psc#dsc-cilogon endpoint using your userid and password at your institution. You should only need to perform this authentication once per Globus Online session or for as long as the retrieved credentials are valid. Send email to remarks@psc.edu if you having any questions about using InCommon.

			If you are unable to use either the XSEDE or InCommon methods of authentication to Globus Online send email to remarks@psc.edu to see if you can use other methods of authentication to Globus Online.

			For the endpoints xsede#pscdata or psc#dsc-cilogon you must explicitly enter your Data Supercell home directory if you want your destination to be your Data Supercell home directory. You can enter a different path if you want a different destination on the Data Supercell.

	#transfer:sftp
		:markdown
			## [Sftp and scp](#transfer:sftp)

			You can transfer files between your local systems and the Data Supercell using the SSH file transfer clients `sftp` and `scp`. When using sftp and scp to transfer files to and from the Data Supercell you do not connect directly to the Data Supercell. You transfer your files using a PSC high-speed data conduit named data.psc.xsede.org. You transfer files to and from the Data Supercell via data.psc.xsede.org. If you are not connecting to the data conduit from an XSEDE host you must use the name data.psc.edu for the data conduit. If you have a graphical `sftp` or `scp` client application on your local system, you can use it to connect and authenticate to `data.psc.xsede.org` and transfer files accordingly. Use your PSC userid and password for authentication.

			If you need to (re)set your PSC password, you can do so via the `kpasswd` command on any PSC production system, or using the http://apr.psc.edu/ Web form.

			You can use the command-line `sftp` client to transfer files to and from the Data Supercell interactively. When using `sftp` from the command line, you first connect and authenticate to data.psc.xsede.org, and then issue commands at the `sftp>` prompt to transfer and manage files:
				$ sftp joeuser@data.psc.xsede.org

			where `joeuser` is your PSC userid. The first time you connect to data.psc.xsede.org using `sftp` or `scp`, you may be prompted to accept the server's host key. Enter yes to accept the host key:

				The authenticity of host 'data.psc.xsede.org (128.182.70.103)' can't be established.
				RSA key fingerprint is d5:77:f2:d9:07:f6:32:b6:c3:eb:0d:d1:29:ed:9b:80.
				Are you sure you want to continue connecting (yes/no)? yes
				Warning: Permanently added 'data.psc.xsede.org' (RSA) to the list of known hosts.

			You will then be prompted to enter your PSC password:
				joeuser@data.psc.xsede.org's password:
				Connected to data.psc.xsede.org.
				sftp>

			At the `sftp>` prompt, you can then enter `sftp` commands (e.g., `pwd`, `ls`, `put`, `get`, etc.) to manage and transfer your files to/from the Data Supercell. Enter a question mark for a list of available `sftp` commands. You must explicitly `cd` to your Data Supercell home directory in order to work with your Data Supercell home directory files.

	#transfer:sftp-examples
		:markdown
			### [Examples](#transfer:sftp-examples)
			(where `joeuser` is the user's PSC userid):

			**Where am I?**
				sftp> cd /arc/users/joeuser
				sftp> pwd
				Remote working directory: /arc/users/joeuser</pre>

			**Where am I on my local system?**
				sftp> lpwd
				Local working directory: /Users/JoeUser/Documents

			**Change directories on my local system to `/usr/local/projects`**:
				sftp> lcd /usr/local/projects

			**Make a new directory called `newdata` under my current directory on the Data Supercell**:
				sftp> mkdir newdata

			**Copy a file (`file1.dat`) from my current local directory to my `newdata` subdirectory on the Data Supercell**:
				sftp> put file1.dat newdata/file1.dat
				Uploading file1.dat to /arc/users/joeuser/newdata/file1.dat
				file1.dat          100% 1016KB   1.0MB/s   00:00

			**Copy a file on the Data Supercell (`/arc/users/joeuser/file1`) to `/usr/local/projects/newfile1` on my local system**:
				sftp> get /arc/users/joeuser/file1 /usr/local/projects/newfile1
				Fetching /arc/users/joeuser/file1 to /usr/local/projects/newfile1
				/arc/users/joeuser/file1          100%   31     0.0KB/s   00:00

			**Exit from this `sftp` session**:
				sftp> exit

			or
				sftp> bye

			For scripted transfers, or transfers that you want to execute directly from your command-line shell, you can use the SSH scp client. You must explicitly give your Data Supercell home directory in your path if you want to work with your Data Supercell home directory.

	#transfer:sftp-sshexamples
		:markdown
			### [SSH Examples](#transfer:sftp-sshexamples)
			(where `joeuser` is the user's PSC userid, and the user enters their PSC password when prompted):

			**Copy my local file (`/usr/local/projects/file1.dat`) to my home directory on the Data Supercell**:
				$ scp /usr/local/projects/file1.dat joeuser@data.psc.xsede.org:/arc/users/joeuser
				joeuser@data.psc.xsede.org's password: 
				file1.dat          100% 1016KB   1.0MB/s   00:00

			**Copy the contents of my newdata directory on the Data Supercell to directory `/usr/local/projects` on my local system (creating `/usr/local/projects/newdata` and copying all the files from newdata)**:
				$ scp -r joeuser@data.psc.xsede.org:/arc/users/joeuser/newdata /usr/local/projects
				joeuser@data.psc.xsede.org's password: 
				file2.dat          100% 1016KB   1.0MB/s   00:00
				file3.dat          100% 1016KB   1.0MB/s   00:01
				file1.dat          100% 1016KB   1.0MB/s   00:00


	#transfer:rsync
		:markdown
			## [Rsync](#transfer:rsync)

			You can use the rysnc command to keep the contents of a local directory synchronized with a directory on the Data Supercell. A sample rsync command is

				rsync -rltpDvP -e 'ssh -l joeuser' source_directory data.psc.xsede.org:target_directory

			where 'joeuser' is your PSC userid and 'source_directory' is the name of your local directory and 'target_directory' is the name of the directory on the Data Supercell. If you are not issuing the command on an XSEDE host you must use data.psc.edu as the address in your rsync command. We recommend the rsync command options -rltpDvP. See the rsync man page for information on these options and on other rsync options you might want to use.

			We have several other recommendations for the use of rsync. First, we recommend that you install the HPN-SSH patches to improve the performance of rsync. These patches are available online.

			If you install the HPN-SSH options you can use the ssh options

				-oNoneSwitch=yes
				-oNoneEnabled=yes

			in the -e option of your rsync command for faster data transfers. With the use of this option your authentication is encrypted but your data transfer is not. If you want encrypted data transfers you should not use this option.

			Finally, whether or not you install the HPN-SSH patches we recommend the option

			<pre>-oMACS=umac-64@openssh.com</pre>

		:markdown
			If you use this option your transfer will use a faster data validation algorithm.

			A convenient way to use these options is to define a variable whose value is the options you want to use. An example command is to do this is

			<pre>setenv SSH_OPTS '-oMACS=umac-64@openssh.com -oNoneSwitch=yes -oNoneEnabled=yes'</pre>

		:markdown
			You can then issue your rsync command as

			<pre>rsync -rltpDvP -e 'ssh -l joeuser $SSH_OPTS' source_directory data.psc.xsede.org:target_directory</pre>

		:markdown
			The above recommendations, excluding the recommendation for the rsync options, are also appropriate for sftp and scp. You should apply the HPN-SSH patches and the use the above ssh options if suitable. If you define an SSH_OPTS variable which has the value of your ssh options you can issue your sftp command as

			<pre>sftp $SSH_OPTS joeuser@data.psc.xsede.org</pre>

		:markdown
			and your scp command as

			<pre>scp $SSH_OPTS local_file joeuser@data.psc.xsede.org:remote_file</pre>

		:markdown
			[Globus Online](https://www.psc.edu/resources/storage#go) can also be used to keep a local directory synchronized with a directory on the Data Supercell.

	#transfer:url
		:markdown
			## [Globus-url-copy](#transfer:url)

			XSEDE users may use GridFTP clients to transfer files to and from the Data Supercell.

			To use the command-line client globus-url-copy on an XSEDE-system login host  first ensure that you have a current user proxy certificate for authentication with enough time on it to complete your transfer, e.g.:

				joeuser@tg-login1:~> grid-proxy-info
				subject  : /C=US/O=National Center for Supercomputing Applications/CN=Joe User
				issuer   : /C=US/O=National Center for Supercomputing Applications/OU=Certificate Authorities/CN=MyProxy
				identity : /C=US/O=National Center for Supercomputing Applications/CN=Joe User
				type     : end entity credential
				strength : 2048 bits
				path     : /tmp/x509up_u99999
				timeleft : 11:58:33

			If the `timeleft` is not sufficient, or you get an `ERROR: Couldn't find a valid proxy` message, then use `myproxy-logon` (or if you have your own long term user certificate, `grid-proxy-init`) to obtain a new user proxy certificate, e.g.:

				joeuser@tg-login1:~> myproxy-logon -l joexsedeuser -t 24
				Enter MyProxy pass phrase:
				A credential has been received for user joexsedeuser in /tmp/x509up_u99999.
			where `joexsedeuser` is your XSEDE User Portal login name, `-t 24` requests a 24-hour certificate, and the `MyProxy pass phrase` entered is your XSEDE User Portal password.

			You can then use globus-url-copy to transfer files to/from the Data Supercell using the GridFTP server address gsiftp://gridftp.psc.xsede.org. This transfer will go through the PSC high-speed data conduit data.psc.xsede.org.

			The `gsiftp://` URLs are absolute paths to files. This means that when referring to a file or directory in your Data Supercell home directory, you must use path `gsiftp://gridftp.psc.xsede.org/arc/users/joeuser/` to refer to your home directory (where `joeuser` is your PSC userid).

	#transfer:url-examples
		:markdown
			### [Examples](#transfer:url-examples)

			**List the files in my home directory on the Data Supercell**:
				joeuser@tg-login1:~> globus-url-copy -list gsiftp://gridftp.psc.xsede.org/arc/users/joeuser
				gsiftp://gridftp.psc.xsede.org/~/
				file1.dat
				file2.dat
				file3.dat
				newdata/
				olddata/

			**Transfer a file (testfile) from my scratch space on TACC Lonestar to my newdatadirectory on the Data Supercell**:
				joeuser@tg-login1:~> globus-url-copy -stripe -tcp-bs 32M \ 
				gsiftp://gridftp.lonestar.tacc.xsede.org/scratch/99999/tg987654/testfile gsiftp://gridftp.psc.xsede.org/arc/users/joeuser/newdata/

			where `-stripe` and `-tcp-bs 32M` are used to improve transfer performance, and `/scratch/99999/tg987654` is the scratch directory on Lonestar at TACC.

	#transfer:gsi
		:markdown
			## [Gsisftp and gsiscp](#transfer:gsi)

			If you have a current user proxy certificate you can also use gsisftp or gsiscp to transfer files to and from the Data Supercell. The method of obtaining a valid user proxy certificate is described above in the discussion of the globus-url-copy command. To work with files in your Data Supercell home directory you must explicitly include your home directory path in your gsiftp and gsiscp commands.
